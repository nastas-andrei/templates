<div id="sns_content" class="wrap">
   <div class="container">
        <div class="row-fluid">
                <!-- global messages -->
            <div id="sns_mainmidle" class="span12 clearfix">
                <!-- primary content -->
                <div class="page-title">
                    <h1>{$smarty.const.LOGIN_BOX_PASSWORD_FORGOTTEN}</h1>
                </div>
                {$password_forgotten_form}
                    <div class="fieldset">
                        <h2 class="legend">{$smarty.const.NAVBAR_TITLE_2}</h2>
                        <p>{$smarty.const.TEXT_MAIN}</p>
                        <ul class="form-list">
                            <li>
                                <label for="email_address" class="required"><em>*</em>{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
                                <div class="input-box">
                                     {$email_address_input}
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="buttons-set">
                        <p class="back-link"><a href="{$LOGIN_URL}"><small>&larr;&nbsp;</small> {$smarty.const.IMAGE_BUTTON_BACK}</a></p>
                        <button type="submit" title="Submit" class="button"><span><span>Submit</span></span></button>
                    </div>
                </form>
                   <!-- // primary content -->
            </div>
        </div>
    </div>
</div>