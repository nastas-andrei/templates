{if $arr_last_viewd}
<div class="box box-last-viewd">
  <div class="box-heading">
    <h3 class="box-title">{$arr_last_viewd.title}</h3>  
  </div>
  <div class="products-media-gallery">
  {foreach from=$arr_last_viewd.products key=i item=product name=lastviewdproducts}
    
  <div class="media">
    <a class="pull-left" href="{$product.link}">
                  {if $product.image} 
                    {php}
                       {$product = $this->get_template_vars('product');}
                       {$pattern_src = '/src="([^"]*)"/';}          
                       {preg_match($pattern_src, $product['image'], $matches);}
                       {$src = $matches[1];}
                       {$this->assign('src', $src);}          
                    {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                  {/if}
          <div class="media-object image-canvas" style="width:64px;height:64px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
    </a>
    <div class="media-body">
        <a href="{$product.link}" class="product-title-link media-heading">{$product.name}</a>
        {if $product.preisalt > 0}
          <span class="product-old-price">{$product.preisalt}</span>
          <span class="product-new-price">{$product.preisneu}</span>
        {/if}
    </div>
   </div>             
  {/foreach}
  </div>
</div>
{/if}