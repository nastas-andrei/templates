<table border="0" width="100%" cellspacing="0" cellpadding="2">
	<tr>
 		<td>
 			<table border="0" width="100%" cellspacing="0" cellpadding="2">
      			<tr>
        			<td class="main"></td>
        			<td class="inputRequirement" align="right">{$smarty.const.FORM_REQUIRED_INFORMATION}</td>
      			</tr>
    		</table>
    	</td>
  	</tr>
  	<tr>
    	<td>
    		<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
      			<tr>
        			<td>
        				<table border="0" cellspacing="2" cellpadding="2">
        					{if $smarty.const.ACCOUNT_GENDER == 'true'}
 	          				<tr>
            					<td class="main">{$smarty.const.ENTRY_GENDER}</td>
            					<td class="main">
									{$ab_gender_m_radio}&nbsp;&nbsp;{$smarty.const.MALE}&nbsp;&nbsp;{$ab_gender_f_radio}&nbsp;&nbsp;{$smarty.const.FEMALE}&nbsp;{if $ab_entry_gender_text}<span class="inputRequirement">{$smarty.const.ENTRY_GENDER_TEXT}</span>{/if}
            					</td>
          					</tr>
							{/if}
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_FIRST_NAME}</td>
            					<td class="main">
            						{$ab_firstname_input}&nbsp;{if $ab_entry_firstname_text}<span class="inputRequirement">{$smarty.const.ENTRY_FIRST_NAME_TEXT}</span>{/if}            					
            						{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}<small>&nbsp;({$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH})</small>{/if}
            					</td>
          					</tr>
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_LAST_NAME}</td>
            					<td class="main">
            						{$ab_lastname_input}&nbsp;{if $ab_entry_lastname_text}<span class="inputRequirement">{$smarty.const.ENTRY_LAST_NAME_TEXT}</span>{/if}
            						{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}<small>&nbsp;({$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})</small>{/if}
            					</td>
          					</tr>
          					<tr>
            					<td colspan="2">{$ab_pixel_trans_100_10}</td>
          					</tr>
          					{if $smarty.const.ACCOUNT_COMPANY == 'true'}
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_COMPANY}</td>
            					<td class="main">
            						{$ab_company_input}&nbsp;{if $ab_entry_company_text}<span class="inputRequirement">{$smarty.const.ENTRY_COMPANY_TEXT}</span>{/if}
            						{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}<small>&nbsp;({$smarty.const.ENTRY_COMPANY_MAX_LENGTH})</small>{/if}
            					</td>
          					</tr>
          					<tr>
            					<td colspan="2">{$ab_pixel_trans_100_10}</td>
          					</tr>          					
          					{/if}          		
							<tr>
            					<td class="main">{$smarty.const.ENTRY_STREET_ADDRESS}</td>
            					<td class="main">
            						{$ab_street_address_input}&nbsp;{if $ab_entry_street_address_text}<span class="inputRequirement">{$smarty.const.ENTRY_STREET_ADDRESS_TEXT}</span>{/if}            						
            						{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}<small>&nbsp;({$smarty.const.ENTRY_STREET_ADDRESS_MAX_LENGTH})</small>{/if}
            					</td>
          					</tr>
          					{if $smarty.const.ACCOUNT_SUBURB == 'true'}
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_SUBURB}</td>
            					<td class="main">{$ab_suburb_input}&nbsp;{if $ab_entry_suburb_text}<span class="inputRequirement">{$smarty.const.ENTRY_SUBURB_TEXT}</span>{/if}</td>
          					</tr>
							{/if}
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_POST_CODE}</td>
            					<td class="main">{$ab_postcode_input}&nbsp;{if $ab_entry_postcode_text}<span class="inputRequirement">{$smarty.const.ENTRY_POST_CODE_TEXT}</span>{/if}</td>
          					</tr>
          					<tr>
            					<td class="main">{$smarty.const.ENTRY_CITY}</td>
            					<td class="main">{$ab_city_input}&nbsp;{if $ab_entry_city_text}<span class="inputRequirement">{$smarty.const.ENTRY_CITY_TEXT}</span>{/if}</td>
          					</tr>
          					{if $smarty.const.ACCOUNT_STATE == 'true'}
    						<tr>
        						<td class="main">{$smarty.const.ENTRY_STATE}</td>
            					<td class="main">
            						<div id="DivRegions">            					
            							{$ab_state_field}&nbsp;{if $ab_entry_state_text}<span class="inputRequirement">{$smarty.const.ENTRY_STATE_TEXT}</span>{/if}
            						</div>
            					</td>
          					</tr>
          					{/if}
          					<tr>
          						<td class="main">{$smarty.const.ENTRY_COUNTRY}</td>            					
            					<td class="main">{$ab_country_field}&nbsp;{if $ab_entry_country_text}<span class="inputRequirement">{$smarty.const.ENTRY_COUNTRY_TEXT}</span>{/if}</td>
          					</tr>
          					{if $ab_is_pimary_checkbox}
          					<tr>
            					<td colspan="2">{$ab_pixel_trans_100_10}</td>
          					</tr>
          					<tr>
            					<td colspan="2" class="main">{$ab_primary_checkbox} {$smarty.const.SET_AS_PRIMARY}</td>
          					</tr>
          					{/if}
        				</table>
        			</td>
      			</tr>
    		</table>
    	</td>
  	</tr>
</table>
          					