var revapi;
jQuery(document).ready(function () {
    revapi = jQuery('.tp-banner').revolution(
        {
            delay: 9000,
            startwidth: 1170,
            startheight: 500,
            hideThumbs: 10,
            fullWidth: "on",
            forceFullWidth: "on"
        });
    var rsi = $('#slider-in-laptop').royalSlider({
        autoHeight: false,
        arrowsNav: false,
        fadeinLoadedSlide: false,
        controlNavigationSpacing: 0,
        controlNavigation: 'bullets',
        imageScaleMode: 'fill',
        imageAlignCenter: true,
        loop: false,
        loopRewind: false,
        numImagesToPreload: 6,
        keyboardNavEnabled: true,
        autoScaleSlider: true,
        autoScaleSliderWidth: 486,
        autoScaleSliderHeight: 315,

        imgWidth: 792,
        imgHeight: 479

    }).data('royalSlider');
    $('#slider-next').click(function () {
        rsi.next();
    });
    $('#slider-prev').click(function () {
        rsi.prev();
    });
});
(function ($) {
    jQuery('a[data-gal]').each(function () {
        jQuery(this).attr('rel', jQuery(this).data('gal'));
    });
    jQuery("a[data-gal^='prettyPhoto']").prettyPhoto({
        animationSpeed: 'slow',
        slideshow: false,
        overlay_gallery: false,
        theme: 'light_square',
        social_tools: false,
        deeplinking: false
    });
})(jQuery);
(function ($) {
    $(window).load(function () {
        $('.flexslider').flexslider({
            animation: "fade",
            controlNav: false,
            start: function (slider) {
                $('body').removeClass('loading');
            }
        });
    });
})(jQuery);
(function ($) {
    if ($(document).height() - $(window).height() > 400) {
        $("#header-style-1").affix({
            offset: {
                top: 100, bottom: function () {
                    return (this.bottom = $('#copyrights').outerHeight(true))
                }
            }
        })
    }
})(jQuery);
function toggleSearch() {
    jQuery(function ($) {
        $('#form-search-container').fadeToggle("fast", "linear");
    });
}
