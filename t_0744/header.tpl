<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/bootstrap-select.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic" rel="stylesheet">
    <link href="{$templatepath}lib/css/jetmenu.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/prettyPhoto.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/hoverex-all.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/effects.css" rel="stylesheet" media="screen">
    <link href="http://fonts.googleapis.com/css?family=Qwigley" rel="stylesheet">
    <link href="{$templatepath}custom.css" rel="stylesheet">
    <link type="text/css" href="{$templatepath}lib/css/settings.css" rel="stylesheet" media="screen">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/bootstrap-select.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
    <script src="{$templatepath}lib/js/categories.js"></script>
    <script src="includes/general.js"></script>
</head>
<body>
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="rect1"></div>
            <div class="rect2"></div>
            <div class="rect3"></div>
            <div class="rect4"></div>
            <div class="rect5"></div>
        </div>
    </div>
</div>
<div class="wrapper">
    <section class="header-wrapper">
        <div class="container">
            <div class="topbar">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div id="site-info">
                            <span><i class="fa fa-phone"></i>{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</span>
                            <span>
                                <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">
                                    <i class="fa fa-envelope"></i>  {$smarty.const.HEAD_REPLY_TAG_ALL}
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="select-options pull-right">
                            <div class="btn-toolbar" role="toolbar">
                                {include file="boxes/box_languages.tpl"}
                                {include file="boxes/box_shopping_cart.tpl"}
                                {include file="boxes/box_loginbox.tpl"}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="header" class="clearfix">
                {*<div class="col-lg-4 col-md-4 col-sm-4">*}
                    {*<div class="social">*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-facebook" title="Facebook"></a>*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-google-plus" title="Google+"></a>*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-twitter" title="Twitter"></a>*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-pinterest" title="Pinterest"></a>*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-linkedin" title="Linkedin"></a>*}
                        {*<a href="#" data-toggle="tooltip" data-placement="bottom" class="fa fa-rss" title="Feedburner"></a>*}
                    {*</div>*}
                {*</div>*}
                <div class="col-sm-4 col-xs-12">
                    <div id="logo-wrap">
                        <div class="logo">
                            {$cataloglogo}
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 col-xs-12">
                    {include file="boxes/box_search.tpl"}
                </div>
            </div>
        </div>
        <div id="categories-box"></div>
        <div class="shadow"></div>
    </section>
    {if $breadcrumb_array|@count> 1}
    <section class="singleheader-wrap">
        <div class="singleheader">
            <div class="container">
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="single-title">
                        {assign var=last value=$breadcrumb_array|@end}
                        <h3>{$last.title}</h3>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6">
                    <div class="breadcrumb-container">
                        <ul class="breadcrumb">
                            {foreach from=$breadcrumb_array key=key item=crumb}
                                {assign var=last value=$breadcrumb_array|@end}
                                {if $last.title == $crumb.title}
                                    <li id="crumb">{$crumb.title}</li>
                                {else}
                                    <li><a href="{$crumb.link}">{$crumb.title}</a></li>
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {/if}
