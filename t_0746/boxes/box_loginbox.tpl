{if $arr_loginbox}
    <nav class="navbar navbar-default" role="navigation" id="login-box">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-menu">
                <span class="fa fa-anchor"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="top-menu">
            <ul class="list-panel">
                {if $arr_loginbox.boxtyp == "myaccount"}
                    <li><a href="{$arr_topmenu.4.link}" title="{$arr_topmenu.4.name}" ><i class="fa fa-user"></i> {$arr_topmenu.4.name}</a></li>
                    <li><a href="{$arr_loginbox.items.4.target}"><i class="fa fa-sign-out"></i> {$arr_loginbox.items.4.text}</a></li>
                {else}
                    <li><a href="{$domain}/login.php"><i class="fa fa-lock"></i> {$arr_smartyconstants.LOGIN_BOX_REGISTER}</a></li>
                {/if}
                <li><a href="{$arr_topmenu.2.link}"><i class="fa fa-comments"></i> {$arr_topmenu.2.name}</a></li>
            </ul>
        </div>
    </nav>
{/if}
