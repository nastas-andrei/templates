<div class="account-container">





<!-- button back -->               
<div class="row">
    <div class="pull-left shopping"> 
        <a class="btn button_add" href="{$account_href}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>                                    
    </div>
</div>
<!-- button back end -->  



{if !$is_delete}
	
	{$addressbook_edit_form}

{/if}
	 
{if $is_addressbook_message}
	{$addressbook_message}
{/if}

{if $is_delete}
	{$smarty.const.DELETE_ADDRESS_TITLE}
	{$smarty.const.DELETE_ADDRESS_DESCRIPTION}
    {$smarty.const.SELECTED_ADDRESS}
    {$delete_address_label}
    <a href="{$addressbook_href}" >{$smarty.const.IMAGE_BUTTON_BACK}</a>
	<a href="{$addressbook_delete_href}" class="btn">{$smarty.const.IMAGE_BUTTON_DELETE}</a>	
{else}
	<div class="offset1">
		{$ADDRESS_BOOK_DETAILS_HTML}
	</div>

	{if $is_edit}
		<div class="text-center">			
			{$edit_hidden_fields}		
			<!-- <input type="submit" class="btn button_add" value="{$smarty.const.IMAGE_BUTTON_UPDATE}"> -->
			<button type="submit" class="btn button_add" ><i class="fa fa-check-circle"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_UPDATE}</button>	
		</div>
		
	{else}		
		<div class="text-center">			
			{$action_hidden_field}
			<!-- <input type="submit" class="btn button_add" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}"> -->
			<button type="submit" class="btn button_add" >{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
		</div>
	{/if}
{/if}

{if !$is_delete}
	</form>
{/if}
</div>
