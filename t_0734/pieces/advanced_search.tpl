{$advanced_search_form}
	{include file="pieces/page_title_section.tpl"}
	{if $is_search_message}
  	{$search_message}
  {/if}
	<div id="advanced_searachHLD">
  	{*$info_box_content*} 
    <input type="text" class="advanced_searchFIELD" name="keywords" />

    
<button type="submit" target="payment" value="" title="suchen" alt="suchen" class="btn btn-primary checkoutBTNall searchBTN fleft"><i class="fa fa-check-circle"></i></button>




  </div>
	<div class="advanced_searchOPTIONS">
    <div class="advanced_searchOPTION">
      <div class="fleft"><input type="checkbox" value="1" name="search_in_description" checked="checked" id="1" /> Auch in den Beschreibungen suchen</div>
      <div class="fleft block1">{$inc_subcat_field}{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}</div>  
    </div>
    <div class="advanced_searchOPTION">
      <div class="fleft block2">{$smarty.const.ENTRY_CATEGORIES}</div>
      <div class="fleft">{$categories_id_field}</div>
    </div>
    <div class="clearer"></div>
    <div class="advanced_searchOPTION">
      <div class="fleft block2">{$smarty.const.ENTRY_MANUFACTURERS}</div>
      <div class="fleft">{$manufacturers_id_field}</div>
    </div>
    <div class="clearer"></div>
    <div class="advanced_searchOPTION">
      <div class="fleft block2">{$smarty.const.ENTRY_PRICE_FROM}</div>
      <div class="fleft">{$pfrom_field}</div>
    </div>
    <div class="clearer"></div>
    <div class="advanced_searchOPTION">
      <div class="fleft block2">{$smarty.const.ENTRY_PRICE_TO}</div>
      <div class="fleft">{$pto_field}</div>
    </div>
	</div>         
</form>