<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{$templatepath}lib/css/flexslider.css">
    <link rel="stylesheet" href="{$templatepath}custom.css">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/bootstrap-select.min.js"></script>
    <script src="{$templatepath}lib/js/categories.js"></script>
    <script src="includes/general.js"></script>
</head>
<body class="wide_layout relative w_xs_auto">
    <header role="banner" class="header">
        <div class="panel">
            <div class="container">
                <!--===========top menu===============-->
                <div class="col-xs-4 col-sm-6 top-menu">
                    {include file="boxes/box_loginbox.tpl"}
                </div>
                <!--===========end top menu===============-->
                <!--===========block_language===============-->
                <div class="col-xs-8 col-sm-6 language">
                    <ul class="block_language">
                        <li class="dropdown">
                            {include file="boxes/box_languages.tpl"}
                        </li>
                        <li class="dropdown">
                            {include file="boxes/box_currencies.tpl"}
                        </li>
                    </ul>
                </div>
                <!--===========end block_language===============-->
            </div>
        </div>
        <div class="header_top">
            <div class="container">
                <div class="col-xs-12 col-md-4 col-lg-3">
                    <h1 class="brand">{$cataloglogo}</h1>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-7 margin-search">
                    {include file="boxes/box_search.tpl"}
                </div>
                <div class="col-xs-12 col-md-2 col-lg-2">
                    {include file="boxes/box_shopping_cart.tpl"}
                </div>
            </div>
            <!--========main-menu========-->
            <div class="main-menu" id="categories-box"></div>
            <!--========end main-menu========-->
        </div>
    </header>
    {if $breadcrumb_array|@count> 1}
    <section class="breadcrumb-section">
        <div class="container padding-0">
            <div class="col-lg-6 col-sm-6 col-md-6 padding-left-0">
                <div class="single-title">
                    {assign var=last value=$breadcrumb_array|@end}
                    <h2>{$last.title}</h2>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-md-6 padding-right-0">
                <ul class="breadcrumb pull-right">
                    {foreach from=$breadcrumb_array key=key item=crumb}
                        {assign var=last value=$breadcrumb_array|@end}
                        {if $last.title == $crumb.title}
                            <li id="crumb">{$crumb.title}</li>
                        {else}
                            <li><a href="{$crumb.link}">{$crumb.title}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
    </section>
    {/if}