{$account_edit_form}

{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}
{include file="pieces/page_title_section.tpl"}
{$smarty.const.BETWEEN_HL_CONTENT}
<table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="pageContentTable">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                {else}
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            {include file="pieces/page_title_section.tpl"}
                        </td>
                    </tr>
                    {$smarty.const.BETWEEN_HL_CONTENT}
                    {/if}

                    <tr>
                        <td class="cont_abst">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
                                {if $account_message_exists}
                                    <tr>
                                        <td>{$account_message}</td>
                                    </tr>
                                    <tr>
                                        <td>{$pixel_trans_100_10}</td>
                                    </tr>
                                {/if}
                                <tr>
                                    <td>
                                        <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                        <tr>
                                                            <td class="main"><b>{$smarty.const.MY_ACCOUNT_TITLE}</b>
                                                            </td>
                                                            <td class="inputRequirement" align="right">{$smarty.const.FORM_REQUIRED_INFORMATION}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
                                                        <tr>
                                                            <td>
                                                                <table border="0" cellspacing="2" cellpadding="2">
                                                                    {if $smarty.const.ACCOUNT_GENDER == 'true'}
                                                                        <tr>
                                                                            <td class="main">{$smarty.const.ENTRY_GENDER}</td>
                                                                            <td class="main">
                                                                                {$gender_radio_m}&nbsp;&nbsp;{$smarty.const.MALE}&nbsp;&nbsp;{$gender_radio_f}&nbsp;&nbsp;{$smarty.const.FEMALE}&nbsp;{if $entry_gender_text}
                                                                                <span class="inputRequirement">{$smarty.const.ENTRY_GENDER_TEXT}</span>{/if}
                                                                            </td>
                                                                        </tr>
                                                                    {/if}
                                                                    <tr>
                                                                        <td class="main">{$smarty.const.ENTRY_FIRST_NAME}</td>
                                                                        <td class="main">
                                                                            {$firstname_input}&nbsp;{if $entry_firstname_text}
                                                                            <span class="inputRequirement">{$smarty.const.ENTRY_FIRST_NAME_TEXT}</span>{/if}{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                                                                                <small>&nbsp;({$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH})</small>{/if}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="main">{$smarty.const.ENTRY_LAST_NAME}</td>
                                                                        <td class="main">
                                                                            {$lastname_input}&nbsp;{if $entry_lastname_text}
                                                                            <span class="inputRequirement">{$smarty.const.ENTRY_LAST_NAME_TEXT}</span>{/if}{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                                                                                <small>&nbsp;({$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})</small>{/if}
                                                                        </td>
                                                                    </tr>
                                                                    {if $smarty.const.ACCOUNT_DOB == 'true'}
                                                                        <tr>
                                                                            <td class="main">{$smarty.const.ENTRY_DATE_OF_BIRTH}</td>
                                                                            <td class="main">{$dob_input}&nbsp;{if $entry_dob_text}
                                                                                <span class="inputRequirement">{$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}</span>{/if}
                                                                            </td>
                                                                        </tr>
                                                                    {/if}
                                                                    <tr>
                                                                        <td class="main">{$smarty.const.ENTRY_EMAIL_ADDRESS}</td>
                                                                        <td class="main">{$email_address_input}&nbsp;{if $entry_email_address_text}
                                                                            <span class="inputRequirement">{$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}</span>{/if}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="main">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</td>
                                                                        <td class="main">{$telephone_input}&nbsp;{if $entry_telephone_text}
                                                                            <span class="inputRequirement">{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</span>{/if}
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="main">{$smarty.const.ENTRY_FAX_NUMBER}</td>
                                                                        <td class="main">{$fax_input}&nbsp;{if $entry_fax_text}
                                                                            <span class="inputRequirement">{$smarty.const.ENTRY_FAX_NUMBER_TEXT}</span>{/if}
                                                                        </td>
                                                                    </tr>
                                                                    {if $is_active_b2b}
                                                                        <tr>
                                                                            <td class="main" colspan="2">
                                                                                <b>{$smarty.const.MY_ACCOUNT_TAX_ID}</b>
                                                                            </td>
                                                                        </tr>
                                                                        {if $smarty.const.ACCOUNT_CF == 'true'}
                                                                            <tr>
                                                                                <td class="main">{$smarty.const.ENTRY_CF}</td>
                                                                                <td class="main">{$entry_cf}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td class="main">{$smarty.const.ENTRY_PIVA}</td>
                                                                                <td class="main">{$entry_piva}</td>
                                                                            </tr>
                                                                        {/if}
                                                                    {/if}
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{$pixel_trans_100_10}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                        <tr>
                                                            <td width="10">{$pixel_trans_10_1}</td>
                                                            <td><a href="{$account_href}">{$button_back_img}</a></td>
                                                            <td align="right">{$button_continue_submit_img}</td>
                                                            <td width="10">{$pixel_trans_10_1}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
</form>
                {if $account_delete_enabled}
                    <div class="row">
                        <div class="col-md-12">
                            <form class="form-inline" action="{$account_delete_href}" method="post">
                                <div class="form-group">
                                    <label class="control-label" for="password">Delete account: </label>
                                    <input class="form-control" name="password" id="password" type="password" title="Password" placeholder="Password" required="required"/>
                                    <button type="submit" class="btn btn-danger">Confirm</button>

                                </div>
                                <div class="form-group">
                                </div>
                            </form>
                        </div>
                    </div>
                {/if}
                {if !$smarty.const.PH_INSIDE}
                </td>
                </tr>
            </table>
            {/if}
            {$smarty.const.AFTER_CONTENT}
