{if $is_default}
    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
    {*<div class="slider-wrapper">*}
    {*<div class="tp-banner-container">*}
    {*<div class="tp-banner">*}
    {*{$user_html}*}
    {*</div>*}
    {*</div>*}
    {*</div>*}
    {*{/if}*}
    {if $slider_images|@count > 0}
        <div class="slider-wrapper">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        {foreach from=$slider_images item=slider}
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500">
                                <img src="templates/t_0740/images/top-slider/sliderbg_05.jpg" data-bgfit="cover" data-bgposition="center" data-bgrepeat="no-repeat">

                                <div class="tp-caption high_title2 customin customout start"
                                     data-x="left" data-hoffset="60"
                                     data-y="130"
                                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="500"
                                     data-easing="Back.easeInOut"
                                     data-endspeed="300">{$slider.title}
                                </div>
                                <div class="tp-caption light_title customin customout start"
                                     data-x="left" data-hoffset="60"
                                     data-y="265"
                                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1000"
                                     data-start="700"
                                     data-easing="Back.easeInOut"
                                     data-endspeed="300">{$slider.description}
                                </div>
                                {if $slider.button_title}
                                <div class="tp-caption small_title  customin customout start"
                                     data-x="left" data-hoffset="60"
                                     data-y="340"
                                     data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="1600"
                                     data-start="900"
                                     data-easing="Back.easeInOut"
                                     data-endspeed="300"><a href="{$slider.link}" class="btn btn-primary btn-lg">{$slider.button_title}</a>
                                </div>
                                {/if}
                                <div class="tp-caption customin customout"
                                     data-x="right" data-hoffset="-60"
                                     data-y="bottom" data-voffset="-20"
                                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="800"
                                     data-start="700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 3"><img src="{$slider.image|replace:'.jpg':'.png'}?t[]=maxSize:width=800" alt="{$slider.title}">
                                </div>
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    {/if}
    <section class="blog-wrapper">
        <div class="container">
            <div class="clearfix"></div>
            <div id="content" class="col-xs-12">
                {$FILENAME_FEATURED_HTML}
                {include file="boxes/box_whats_new.tpl"}
                {$UPCOMING_PRODUCTS_HTML}
            </div>
            {*<div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">*}
            {*{include file="boxes/box_best_sellers.tpl"}*}
            {*</div>*}
        </div>
    </section>
{/if}
<div class="container">
    {if $products_not_found_message}
        {$products_not_found_message}
    {/if}
    {if $is_nested}
        {if !empty($categories_html)}
            {$categories_html}
        {/if}
        {if isset($categorieslist)}
        {else}
            {$FILENAME_FEATURED_HTML}
        {/if}
        {if !empty($categories_html_footer)}
            {$categories_html_footer}
        {/if}
    {/if}
    {if $is_products == 1}
        {$smarty.const.HEADING_TITLE}
        {if $filterlistisnotempty}
            {$filter_form}
            {$smarty.const.TEXT_SHOW}
        {/if}
        {if !empty($categories_html)}
            {$categories_html}
        {/if}
        {if !empty($manufacturers_html_header)}
            {$manufacturers_html_header}
        {/if}
        {if !empty($manufacturers_html_header) || isset($categorieslist)}
            {if isset($categorieslist)}
                {if $categorieslist|@count > 0}
                {/if}
            {else}
                {if !empty($manufacturers_html_header)}
                {/if}
            {/if}
        {/if}
        {if !empty($PRODUCT_LISTING_HTML)}
            <div class="shop_wrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="shop-top">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <p class="amount">{$arr_modul_productlisting.count_products_info|strip_tags:false}</p>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="pull-right">
                                {if $manufacturer_select}
                                    {$manufacturer_select}&nbsp;
                                {/if}
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="pull-right">
                                <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink" />
                                <input type="hidden" value="{$max}" id="max" name="max" />
                                <input type="hidden" value="{$standard_template}" id="standard_template"
                                       name="standard_template" />
                                <select name="sortierung" id="sortierung" class="custom-select form-control"
                                        onchange="goHin(this)">
                                    {foreach from=$arr_sort_new key=key item=sort}
                                        {if $sort.class == "p"}
                                            {if $sort_select eq $sort.id}
                                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                            {else}
                                                <option value="{$sort.id}">{$sort.text}</option>
                                            {/if}
                                        {else}
                                            {if $sort_select == $sort.id}
                                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                            {else}
                                                <option value="{$sort.id}">{$sort.text}</option>
                                            {/if}
                                        {/if}
                                    {/foreach}
                                </select>
                            </div>
                            <div class="pull-right">
                                <div class="view-mode">
                                    {if $standard_template == "liste"}
                                        <a href="{$templ_link}" title="Grid" class="box glyphicon grid"></a>
                                        <strong title="List" class="box glyphicon list"></strong>
                                    {/if}
                                    {if $standard_template == "galerie"}
                                        <strong title="Grid" class="box glyphicon grid"></strong>
                                        <a href="{$templ_link}" title="List" class="box glyphicon list"></a>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div id="product-list-container" class="">
                        <div class="col-md-12">
                            {$PRODUCT_LISTING_HTML}
                        </div>
                    </div>
                    {if $arr_modul_productlisting.pagination|@count > 1}
                        <div class="pagination_wrapper">
                            <div class="pager text-center ">
                                <ol>
                                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                                    {if $page.active}
                                    <li class="active current">
                                        <a>{$page.text}</a>
                                        {elseif $page.previous}
                                    <li>
                                        <a class="previous" href="{$page.link}" title="{$page.title}">
                                            &laquo;
                                        </a>
                                        {elseif $page.next}
                                    <li>
                                        <a class="next" href="{$page.link}" title="{$page.title}">
                                            &raquo;
                                        </a>
                                        {else}
                                    <li>
                                        <a href="{$page.link}" title="{$page.title}">
                                            {$page.text}
                                        </a>
                                        {/if}
                                        {/foreach}
                                    </li>
                                </ol>
                            </div>
                        </div>
                    {/if}
                </div>
            </div>
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                {include file="boxes/box_best_sellers.tpl"}
            </div>
        {/if}
        {if !empty($manufacturers_id)}
            {$manufacturers_html_footer}
        {/if}
        {if !empty($categories_html_footer)}
            {$categories_html_footer}
        {/if}
    {else}
        {if $is_default}
            &nbsp;
        {else}
            <div class="row">
                <p class="block">{$tep_image_category}</p>

                <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
            </div>
        {/if}
    {/if}
</div>
