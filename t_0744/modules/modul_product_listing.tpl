{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort='+wert+'&max='+max;
            location.href = link;
        }
    </script>
{/literal}
    <div id="content" class="single col-lg-12 col-md-12 col-sm-12">
        <div class="shop-top">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <p>{$arr_modul_productlisting.count_products_info|strip_tags:false}</p>
            </div>
            <div class="pull-right col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink"/>
                <input type="hidden" value="{$max}" id="max" name="max"/>
                <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template"/>
                <select name="sortierung" id="sortierung" class="custom-select form-control" onchange="goHin(this)">
                    {foreach from=$arr_sort_new key=key item=sort}
                        {if $sort.class == "p"}
                            {if $sort_select eq $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {else}
                            {if $sort_select == $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div class="pull-right col-lg-3 col-md-3 col-sm-3 col-xs-12">
                {if $manufacturer_select}
                    {$manufacturer_select}&nbsp;
                {/if}
            </div>
        </div>
        <div class="clearfix"></div>
        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/nopicture.gif"}
            {/if}
            <div class="shopcol col-lg-3 col-md-3 col-sm-6 col-xs-12" data-effect="slide-bottom">
                <div class="product1 image-hover he-wrap tpl6">
                    <div class="image_view_product" style="background-image:url('{$src}')"></div>
                    <div class="he-view">
                        <div class="bg a0" data-animate="fadeIn">
                            <h3 class="a1" data-animate="fadeInDown">{$product.products_name|truncate:15:"...":true}</h3>
                            <a href="{$product.link}" class="btn a2" data-animate="rotateInLeft"><i class="fa fa-search"></i> {$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                            <a href="{$product.buy_now_link}" class="btn a2" data-animate="rotateInRight"><i class="fa fa-shopping-cart"></i> {$smarty.const.IMAGE_BUTTON_IN_CART}</a>
                        </div>
                    </div>
                </div>
                <div class="product-details">
                    <h4><a href="{$product.link}">{$product.products_name|truncate:15:"...":true}</a></h4>
                    <span class="price">{$product.newprice|regex_replace:"/[()]/":""}</span>
                </div>
            </div>
        {/foreach}
        {if $arr_modul_productlisting.pagination|@count > 1}
            <div class="clearfix text-center">
                <ul class="pagination">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                    {if $page.active}
                    <li class="active current">
                        <a>{$page.text}</a>
                        {elseif $page.previous}
                    <li>
                        <a class="previous" href="{$page.link}" title="{$page.title}">
                            &laquo;
                        </a>
                        {elseif $page.next}
                    <li>
                        <a class="next" href="{$page.link}" title="{$page.title}">
                            &raquo;
                        </a>
                        {else}
                    <li>
                        <a href="{$page.link}" title="{$page.title}">
                            {$page.text}
                        </a>
                        {/if}
                        {/foreach}
                    </li>
                </ul>
            </div>
        {/if}
    </div>
{/if}
