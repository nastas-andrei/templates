<div id="navbar-collapse-1" class="navbar-collapse collapse navbar-right">
    <ul class="nav navbar-nav">
        {foreach from=$categories key=key item=category name=catlist}
            <li class="yamm-fw static-menu dropdown">
                <a href="{$category.link}">
                    {$category.name}
                    {if $category.children|@count > 0}
                        <div class="arrow-up"></div>
                    {/if}
                </a>
                {if $category.children|@count > 0}
                    <ul class="dropdown-menu fullwidth">
                        <li>
                            <div class="yamm-content">
                                <div class="row">
                                    {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                                        <ul class="col-sm-3">
                                            <li class="level2"><a href="{$categorySub.link}" class="single">
                                                    <span class="mega-menu-sub-title">{$categorySub.name|truncate:20:"...":true}</span>
                                                </a>
                                                {if $categorySub.children|@count > 0}
                                                    <ul class="sub-menu scroll_list">
                                                        {foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}
                                                            <li><a href="{$categorySub2.link}">{$categorySub2.name|truncate:15:"...":true}</a></li>
                                                        {/foreach}
                                                    </ul>
                                                {/if}
                                            </li>
                                        </ul>
                                    {/foreach}
                                </div>
                            </div>
                        </li>
                    </ul>
                {/if}
            </li>
        {/foreach}
    </ul>
</div>
