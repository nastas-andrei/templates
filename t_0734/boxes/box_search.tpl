{if $arr_searchbox}
    {$arr_searchbox.form}
        <div class="input-group">
            <input type="text"  class="form-control" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}" value="{$arr_searchbox.inputtextfield}"/>
            <span class="input-group-btn">
                <button class="btn btn-default no-border-left" type="submit"><i class="fa fa-search search-header"></i></button>
            </span>
        </div>
        {$arr_searchbox.hidden_sessionid}
        </form>
{/if}