<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-8">
                {if $is_account_password_message}
                  <p>{$account_password_message}</p>
                {/if}
                <button type="button" class="btn btn-primary btn-icon" style="margin:20px 0
                " onclick="window.location='{$account_href}';">
                    <span>
                      <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
                    </span>
                </button> 
                {$account_password_form}
                <div class="inputHLDbox">
                    <div class="inputHLDhead">{$smarty.const.MY_PASSWORD_TITLE}</div>
                    <div class="inputHLDinnerChangePass">
                        {*******************PASSWORD CURRENT****************}
                        <div class="inputHLD">
                            <div class="inputTXT" style="white-space: nowrap;">{$smarty.const.ENTRY_PASSWORD_CURRENT}
                                <small style="color:red">
                                {if $entry_password_current_text}
                                    {$smarty.const.ENTRY_PASSWORD_CURRENT_TEXT}
                                {/if}
                                </small>
                            </div>
                            {$password_current_field}
                        </div>
                        {*******************PASSWORD NEW****************}
                        <div class="inputHLD">
                            <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD_NEW}
                                <small style="color:red">
                                    {if $entry_password_new_text}
                                        {$smarty.const.ENTRY_PASSWORD_NEW_TEXT}
                                    {/if}
                                </small>
                            </div>
                                {$password_new_field}
                        </div>
                        {*******************PASSWORD CONFIRMATION****************}
                            <div class="inputHLD">
                                <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}
                                <span style="color:red">
                                    {if $entry_password_confirmation_text}
                                        {$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}
                                    {/if}
                                </span>
                                </div>
                                {$password_confirmation_field}
                            </div>  
                    </div>
                    <div class="clearer"></div>
                    <button type="reset" class="btn btn-primary btn-icon pull-left" style="margin-left:10px"><i class="fa fa-trash-o"></i></button>
                    <div class="inputHLD" style="float:right">
                        <button type="submit" class="btn btn-primary btn-icon">{$smarty.const.IMAGE_BUTTON_CONTINUE}</button>
                    </div>
                    <div class="clearer"></div>
                </div>
            </div>
        </div>
    </div>
</section>