<ol class="steps">
    <li class="step1 done"><span><a
                    href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span>
    </li>
    <li class="step2 done"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span>
    </li>
    <li class="step3 current"><span><a href="{$checkout_payment_href}">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></span>
    </li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>
<a href="{$checkout_shipping_href}" class="btn btn-primary btn-icon"
   style="margin-bottom:20px">&larr;&nbsp;&nbsp; {$smarty.const.IMAGE_BUTTON_BACK}</a>
<div style="margin-top:20px">
    {$checkout_address_form}
    <div class="row">
        <div class="col-xs-6">
            {if $is_checkout_address}
                <address>
                    {$checkout_address}
                </address>
            {/if}
            {if $process == false}
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{$smarty.const.TABLE_HEADING_PAYMENT_ADDRESS}</h3>
                    </div>
                    <div class="panel-body">
                        {$smarty.const.TEXT_SELECTED_PAYMENT_DESTINATION}
                        {$smarty.const.TITLE_PAYMENT_ADDRESS}
                        {$billto_address_label}
                    </div>
                </div>
                {if $addresses_count > 1}
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">{$smarty.const.TABLE_HEADING_ADDRESS_BOOK_ENTRIES}</h3>
                            {$smarty.const.TEXT_SELECT_OTHER_PAYMENT_DESTINATION}
                            {$smarty.const.TITLE_PLEASE_SELECT}
                        </div>
                        <div class="panel-body">
                            {section name=current loop=$addresseslist}

                                {if $addresseslist[current].address_book_id == $billto}
                                    <span id="defaultSelected" class="moduleRowSelected"
                                          onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)"
                                          onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})"></span>
                                {else}
                                    <span class="moduleRow" onmouseover="rowOverEffect(this)"
                                          onmouseout="rowOutEffect(this)"
                                          onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})"></span>
                                {/if}
                                <address>
                                    {$addresseslist[current].address_radio}
                                    {$addresseslist[current].firstname}
                                    {$addresseslist[current].lastname}
                                    {$addresseslist[current].formated_address}
                                </address>
                            {/section}
                        </div>
                    </div>
                {/if}
            {/if}
        </div>
        {if $addresses_count < $smarty.const.MAX_ADDRESS_BOOK_ENTRIES}
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{$smarty.const.TABLE_HEADING_NEW_PAYMENT_ADDRESS}</h3>
                    </div>
                    <div class="panel-body">
                        <p class="text-center">{$smarty.const.TEXT_CREATE_NEW_PAYMENT_ADDRESS}</p>
                        {$CHECKOUT_NEW_ADDRESS_HTML}
                    </div>
                </div>
            </div>
        {/if}
        <div class="clearfix"></div>
        <button type="submit" class="btn btn-primary btn-icon pull-right"
                style="margin-right:15px">{$smarty.const.IMAGE_BUTTON_CONTINUE}</button>
    </div>
    </form>
</div>