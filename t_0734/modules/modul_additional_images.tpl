<!-- <ul class="inline product-info-additional-images"> -->
<!-- {if $arr_product_info.videolink != ""} -->
<!-- <li class="additional-image video-thumb"> -->
	<!-- <a href="{$arr_product_info.videolink}" class="modal-item">	
		<div class="additional-image-canvas" style="background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$arr_product_info.image_link}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$arr_product_info.image_link}', sizingMethod='scale');">
			<i class="icon-play-circle"></i>
		</div>
	</a> -->
<!-- </li> -->
{/if}

{foreach from=$arr_additional_images.products key=index item=product_additional name="addimages"}   
  {if $product_additional.image_src} 
  {php}
   {$product_additional = $this->get_template_vars('product_additional');}
   {$pattern_src = '/src="([^"]*)"/';}
   {preg_match($pattern_src, $product_additional['image_src'], $matches);}
   {$src = $matches[1];}
   {$this->assign('src', $src);}
  {/php}
  {else}
    {assign var=src value="http://placehold.it/150x150&text=No%20image"}
  {/if}
<a href="{$src}">
	<img src="{$src}" alt="" >
</a>
{/foreach}
