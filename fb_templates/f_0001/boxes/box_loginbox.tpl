{if $arr_loginbox}
	{if $arr_loginbox.boxtyp eq 'myaccount'}	
		<div class="box box-loginbox">
		  <div class="box-heading">
		    <h3 class="box-title">{$arr_loginbox.title}</h3>  
		  </div>
		  <div class="box-content">
		    <ul class="unstyled">    	
				 {foreach from=$arr_loginbox.items key=key item=item name=loginitems}  									
				 	<li><a href="{$item.target}">{$item.title}</a></li>
		      	{/foreach}
		    </ul>
		  </div>    
		 </div>   
	{/if}
{/if}