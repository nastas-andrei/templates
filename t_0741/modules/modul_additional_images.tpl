<div id="additional_images" class="owl-carousel">
    {if $arr_product_info.image_linkLARGE}
        <div class="item">
            <div class="grid cs-style-3" data-effect="slide-bottom">
                <a href="{$product_additional.popup_img}" class="zoom first" rel="prettyPhoto[product-gallery]">
                    <img src="{$arr_product_info.image_linkLARGE}" alt="" />
                </a>
            </div>
        </div>
    {else}
        <div class="item">
            <div class="grid cs-style-3" data-effect="slide-bottom">
                <a href="{$product_additional.popup_img}" class="zoom first" rel="prettyPhoto[product-gallery]">
                    <img src="{$arr_product_info.image_link}" alt="" />
                </a>
            </div>
        </div>
    {/if}
    {foreach from=$arr_additional_images.products key=index item=product_additional name="addimages"}
        {if $product_additional.image_src}
            {php}
                {$product_additional = $this->get_template_vars('product_additional');}
                {$pattern_src = '/src="([^"]*)"/';}
                {preg_match($pattern_src, $product_additional['image_src'], $matches);}
                {$src = $matches[1];}
                {$this->assign('src', $src);}
            {/php}
        {else}
            {assign var=src value=$imagepath|cat:"/empty-album.png"}
        {/if}
        <div class="item">
            <div class="grid cs-style-3" data-effect="slide-bottom">
                <a href="{$product_additional.popup_img}" class="zoom first" rel="prettyPhoto[product-gallery]">
                    <img src="{$src}" alt="" />
                </a>
            </div>
        </div>
    {/foreach}
</div>
