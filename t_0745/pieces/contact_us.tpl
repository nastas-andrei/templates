<section id="contact-page" class="container margin-top">
    {if $contact_message_exists}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">&times;</button>
            {$contact_message}
        </div>
    {/if}
    <div class="row margin-bottom">
        <div class="col-sm-12">
            <h2 class="title text-center">{$smarty.const.NAVBAR_TITLE}</h2>
            <iframe width="1170" height="450" frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyASNmy5aLU4Gv3ExDUfmCIbmOpE6MgXzmg"></iframe>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="contact-form">
                <h2 class="title text-center">{$smarty.const.CONTACT_RESPONSE_TIME}</h2>
                {$contact_us_form}
                {if $is_action_success}

                {$smarty.const.TEXT_SUCCESS}
                <a href="{$button_continue_url}">{$button_continue_img}</a>
                {else}
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>{$smarty.const.ENTRY_NAME}</label>
                        {$name_input|replace:'class="general_inputs"':'class="form-control"'}
                    </div>
                    <div class="form-group col-md-6">
                        <label>{$smarty.const.ENTRY_EMAIL}</label>
                        {$email_input|replace:'class="general_inputs"':'class="form-control"'}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <label>{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>
                        {$anliegen_pull_down|replace:'name="anliegen"':'name="anliegen" class="form-control"'}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12" id="artikel" style="display: none">
                        <label id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</label>
                        {$artikel_input|replace:'general_inputs':'form-control'}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12" id="bestellung" style="display: none">
                        <label id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>
                        {$bestellung_input|replace:'general_inputs':'form-control'}
                    </div>
                    <div class="form-group col-md-12">
                        {$enquiry_textarea|replace:'class="textarea"':'class="form-control" id="message"'}
                    </div>
                </div>
                {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                    <div class="form-group col-md-12">
                          <span>
                              Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                          </span>
                        <select name="pf_antispam" id="pf_antispam" class="form-control">
                            <option value="1" selected="selected">Ja</option>
                            <option value="2">Nein</option>
                        </select>
                    </div>
                    <script language="javascript" type="text/javascript">

                        var sel = document.getElementById('pf_antispam');
                        var opt = new Option('Nein', 2, true, true);
                        sel.options[sel.length] = opt;
                        sel.selectedIndex = 1;

                    </script>
                    <noscript>
                        <div class="antistalker">
                            <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                            <select name="pf_antispam" id="pf_antispam">
                                <option value="1" selected="selected">Ja</option>
                                <option value="2">Nein</option>
                            </select>
                        </div>
                    </noscript>
                {/if}
                <button type="submit" class="btn btn-primary margin-bottom">{$smarty.const.CONTACT_BOTTOM_TEXT}</button>
            </div>
            {/if}
            </form>
        </div>
        <div class="col-sm-4">
            <div class="contact-info">
                <h2 class="title text-center">{$smarty.const.HEADING_TITLE}</h2>
                <address>
                    <p>{$contact_data.shop_streat.value}</p>

                    <p>{$contact_data.shop_city.value}, {$contact_data.shop_country.value}</p>

                    <p>Phone: {$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</p>

                    <p>Email: {$smarty.const.HEAD_REPLY_TAG_ALL}</p>
                </address>
            </div>
        </div>
    </div>
</section>