<div class="account-container">
<a  href="{$addressbook_href}"><i class="icon-caret-left"></i> {$smarty.const.IMAGE_BUTTON_BACK}</a>
{if !$is_delete}
	
	{$addressbook_edit_form}

{/if}
	 
{if $is_addressbook_message}
	{$addressbook_message}
{/if}

{if $is_delete}
	{$smarty.const.DELETE_ADDRESS_TITLE}
	{$smarty.const.DELETE_ADDRESS_DESCRIPTION}
    {$smarty.const.SELECTED_ADDRESS}
    {$delete_address_label}
    <a href="{$addressbook_href}" >{$smarty.const.IMAGE_BUTTON_BACK}</a>
	<a href="{$addressbook_delete_href}" class="btn">{$smarty.const.IMAGE_BUTTON_DELETE}</a>	
{else}
	<div class="offset1">
		{$ADDRESS_BOOK_DETAILS_HTML}
	</div>

	{if $is_edit}
		<div class="text-center">			
			{$edit_hidden_fields}		
			<input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_UPDATE}">	
		</div>
		
	{else}		
		<div class="text-center">			
			{$action_hidden_field}
			<input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
		</div>
	{/if}
{/if}

{if !$is_delete}
	</form>
{/if}
</div>
