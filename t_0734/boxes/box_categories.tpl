<!-- BEGIN Categories -->
<div class="collapse navbar-collapse" id="offcanvas">
    <ul class="nav navbar-nav categories-menu list-group list-categ">
        {foreach from=$categories key=key item=category}
            <li class="dropdown">
                <a href="{$category.link}" class="list-group-item">
                    {$category.name}
                    {if $category.children|@count > 0}
                        <i class="fa fa-caret-right float-right-ico-arrow"></i>
                    {/if}
                </a>
                {if $category.children|@count > 0}
                    <ul class="dropdown-menu">
                        {foreach from=$category.children key=key item=categorylevel2}
                            <li class="dropdown-submenu">
                                <a href="{$categorylevel2.link}" class="submenu" >
                                    {$categorylevel2.name}
                                </a>
                                {if $categorylevel2.children|@count > 0}
                                    <i class="fa fa-caret-right arrow-level2"></i>
                                    <ul class="dropdown-menu overflow_3_level">
                                        {foreach from=$categorylevel2.children key=key item=categorylevel3}
                                            <li>
                                                <a class="submenu" href="{$categorylevel3.link}">{$categorylevel3.name}</a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </li>
        {/foreach}
    </ul>
</div>
<p class="visible-xs clearfix">
<button type="button" class="btn btn-primary btn-xs col-xs-12 button_display_categories" data-toggle="collapse" data-target="#offcanvas">
 <span class="caret"></span>
</button>
</p> 
<div class="clearfix"></div>
<!-- End Categories -->


