{include file="header.tpl"}
<section class="postwrapper clearfix">
  <div class="container">
    <div class="checkout-container">
        {$content}
    </div>
  </div>
</section>
{include file="footer.tpl"}