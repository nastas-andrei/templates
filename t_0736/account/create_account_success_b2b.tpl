<div class="account-container">
<div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <p>{$smarty.const.TEXT_ACCOUNT_CREATED}</p>
</div>

<a href="{$origin_href}">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
</div>