<tr>
	<td>
		<div id="news_listerHOLDERindex">			
    		<div id="news_listerHOLDER">
      			<div id="news_listerNAVI">
        			<div id="news_listerHEADER" class="headerfarbe">{$smarty.const.BOX_HEADING_SHOPNEWS}</div>
        			<div class="listerfarbe news_listerSPLIT">
          				<div class="news_listerLNKLIST">
            				<a href="/product_info.php?news_id=0&action=all" class="news_listerLNK"><span>zu den News</span></a>
          				</div>
          				<div class="clearer"></div>
        			</div>
      			</div>
      			<div id="news_lister">
      				{section name=current loop=$newslist}      				
      					<div class="news_listerARTICLE{if $newslist[current].news_is_last_row} noBorder{/if}">      					
      					<div class="news_listerDATE"><span>{$newslist[current].news_date}</span></div>      					
      					<div class="news_listerHEAD"><h2><a href="{$newslist[current].news_seo_link}">{$newslist[current].news_title}</a></h2></div>
      					<div class="news_listerSHORTY" style="width:{if $newslist[current].news_picture}73%;{else}100%;{/if}">
            				{$newslist[current].news_teaser_short}
            				<a href="{$newslist[current].news_seo_link}">{$newslist[current].more}</a>
          				</div>
          				{if $newslist[current].news_picture}<div class="news_listerPIC"><img src="{$newslist[current].news_picture}" border="0" /></div>{/if}
          				<div class="clearer"></div>
        			</div>
      				{/section}      				
      			</div>
    		</div>
 		</div>
	</td>
</tr>
