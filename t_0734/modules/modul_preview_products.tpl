{if $arr_modul_previewproducts.products}
    <!-- Featured -->
    <div class="clearfix"></div>
    <div class="col-lg-12 col-sm-12">
        <span class="title">{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</span>
    </div>
    {foreach from=$arr_modul_previewproducts.products key=key item=product name=productpreviewitems}
        {if $smarty.foreach.productpreviewitems.index < 6}
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3 text-center min-width-block-preview">
                <div class="thumbnail">
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value="http://placehold.it/300x300&text=No%20image"}
                    {/if}
                    <a href="{$product.link}" class="link-p">
                        <div class="image_view_product_list" style="background-image: url('{$src}')"></div>
                    </a>

                    <div class="caption prod-caption">
                        <h4>
                            <a href="{$product.link}">{$product.name|truncate:35:"...":true}</a>
                        </h4>

                        <div class="btn-group">
                            <span class="btn btn-default" onclick="location.href='{$product.buy_now_link}'">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                            <a href="{$product.buy_now_link}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    {/foreach}
{/if}
