<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            	<button type="button" class="btn btn-default">
            		<i class="fa fa-arrow-left"></i>
            		<a href="{$LOGIN_URL}" style="color:#000"> {$smarty.const.IMAGE_BUTTON_BACK}</a></button>

                <div class="content-box">
                    {$password_forgotten_form}
                        <fieldset>
						    {if $password_forgotten_stack_size}
								<div class="alert alert-danger fade in alert-error">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								  	{$password_forgotten_message}
								</div>
					  		{/if} 
                            <legend>{$smarty.const.TEXT_MAIN}</legend>
                            <div class="form-group">
                                <label  class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
                                {$email_address_input}
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success">{$smarty.const.IMAGE_BUTTON_CONTINUE}</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>