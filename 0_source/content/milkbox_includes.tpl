{if $DESIGN_VARIANT != ""}
  <link href="{$templatepath}mt_milkbox/milkbox_{$DESIGN_VARIANT}.css" rel="stylesheet" type="text/css" />
{else}
  <link href="{$templatepath}mt_milkbox/milkbox.css" rel="stylesheet" type="text/css" />
{/if}
<script type="text/javascript" src="templates/0_source/lib/milkbox/milkbox.js"></script>
<script type="text/javascript" src="templates/0_source/lib/milkbox/milkbox_thumb.js"></script>