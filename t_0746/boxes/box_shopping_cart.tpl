{if $arr_shopping_cart}
    {if $SCRIPT_NAME !== '/shopping_cart.php'}
        <a type="button" class="btn btn-cart dropdown-toggle" data-toggle="dropdown">
            {$arr_shopping_cart.title|strip_tags:false} {$arr_shopping_cart.count_contents}
        </a>
        <ul class="box-dropdown dropdown-menu" role="menu">
            <li>
                <form name="cart_quantity" action="shopping_cart.php?action=update_product" method="post">
                    <table class="table table-bordered">
                        {foreach from=$arr_shopping_cart.products key=key item=product}
                            <tr>
                                <td class="image"><a href="{$product.link}">{$product.image}</a></td>
                                <td class="name"><a
                                            href="{$product.link}">{$product.name|truncate:20:"...":true}</a></td>
                                <td class="quantity">
                                    <p class="quantity-container">  {$product.final_price}</p>

                                    <p class="quantity-container">
                                        <input type="text" name="cart_quantity[]" value="{$product.quantity}"
                                               size="4" maxlength="10"
                                               class="validate-zero-or-greater validate-number qty"
                                               onblur="if (checkValue('cartadd0')) document.forms.cart_quantity.submit()"
                                               id="cartadd0">
                                        <input type="hidden" name="products_id[]" value="{$product.id}">
                                        <input type="hidden" name="old_qty[]" value="{$product.quantity}">
                                    </p>
                                </td>
                                <td class="remove">
                                    <input id="cart-product-{$product.id}" type="checkbox" name="cart_delete[]" value="{$product.id}"
                                           style="display: none;">
                                    <a href="javascript: del_item_shoppingcart('cart-product-{$product.id}');"
                                       title="Remove This Item"
                                       onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');">
                                        <i class="fa fa-times"></i>
                                    </a>
                                </td>
                            </tr>
                        {/foreach}
                    </table>
                </form>
                <table class="table table-bordered">
                    <tr>
                        <td><b>{$arr_shopping_cart.cart_summe}</b></td>
                        <td>{$arr_shopping_cart.gesamtsumme}</td>
                    </tr>
                </table>
                <button class="btn btn-default shop-cart" onclick="window.location.href = ('shopping_cart.php')">
                    {$arr_shopping_cart.title|strip_tags:false}
                </button>
                <button class="btn btn-default shop-cart" onclick="location.reload();">
                    {$smarty.const.IMAGE_BUTTON_UPDATE_CART}
                </button>
            </li>
        </ul>
    {/if}
{/if}
