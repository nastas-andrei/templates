{$checkout_address_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}		
		{include file="pieces/page_title_section.tpl"}		        
		{$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
      		<tr>
        		<td>
    				{include file="pieces/page_title_section.tpl"}
        		</td>
      		</tr>
      		{$smarty.const.BETWEEN_HL_CONTENT}
 	{/if}
    <tr>
        <td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="checkout_area">
        		{if $checkout_address_exists}        		
      			<tr>
        			<td>{$checkout_address_message}</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			{/if}      
      			{if !$process}       
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="0">
          					<tr>
            					<td class="main"><b>{$smarty.const.TABLE_HEADING_SHIPPING_ADDRESS}</b></td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="0">
              							<tr>
                							<td>{$pixel_trans_10_1}</td>
                							<td class="main" width="50%" valign="top">{$smarty.const.TEXT_SELECTED_SHIPPING_DESTINATION}</td>
                							<td align="right" width="50%" valign="top">
                								<table border="0" cellspacing="0" cellpadding="2">
                  									<tr>
                    									<td class="main" align="center" valign="top">
                    										<b>{$smarty.const.TITLE_SHIPPING_ADDRESS}</b>
                    										<br>
                    										{$arrow_south_east_img}
                    									</td>
                    									<td>{$pixel_trans_10_1}</td>
														<td class="main" valign="top">{$sendto_address_label}</td>
                    									<td>{$pixel_trans_10_1}</td>
                  									</tr>
                								</table>
                							</td>
              							</tr>
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>      				
      			{if $addresses_count > 1}
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="0">
          					<tr>
            					<td class="main">
            						<b>{$smarty.const.TABLE_HEADING_ADDRESS_BOOK_ENTRIES}</b>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="0">
              							<tr>
                							<td>{$pixel_trans_10_1}</td>
                							<td class="main" width="50%" valign="top">{$smarty.const.TEXT_SELECT_OTHER_SHIPPING_DESTINATION}</td>
                							<td class="main" width="50%" valign="top" align="right">
                								<b>{$smarty.const.TITLE_PLEASE_SELECT}</b>
                								<br>{$arrow_east_south}
                							</td>
                							<td>{$pixel_trans_10_1}</td>
              							</tr>
              							{section name=current loop=$addresseslist}              								
										<tr>
											<td>{$pixel_trans_10_1}</td>
											<td colspan="2">
												<table border="0" width="100%" cellspacing="0" cellpadding="0">
													{if $addresseslist[current].is_sendto}
													<tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})">
													{else}
													<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})">
													{/if}
				                    					<td width="10">{$pixel_trans_10_1}</td>
				                    					<td class="main" colspan="2"><b>{$addresseslist[current].firstname} {$addresseslist[current].lastname}</b></td>
				                    					<td class="main" align="right">{$addresseslist[current].address_radio}</td>
				                    					<td width="10">{$pixel_trans_10_1}</td>
				                  					</tr>
	                  								<tr>
	                    								<td width="10">{$pixel_trans_10_1}</td>
	                    								<td colspan="3">
	                    									<table border="0" cellspacing="0" cellpadding="2">
	                      										<tr>
	                        										<td width="10">{$pixel_trans_10_1}</td>                        											
	                        										<td class="main">{$addresseslist[current].address_format}</td>
	                        										<td width="10">{$pixel_trans_10_1}</td>
	                      										</tr>
	                    									</table>
	                    								</td>
	                    								<td width="10">{$pixel_trans_10_1}</td>
	                  								</tr>
	                							</table>
                							</td>
                							<td>{$pixel_trans_10_1}</td>
              							</tr>
              							{/section}								
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
	        		<td>{$pixel_trans_100_10}</td>
      			</tr>
      			{/if}
      			{/if}
	
				{if $addresses_count < $smarty.const.MAX_ADDRESS_BOOK_ENTRIES}
	      		<tr>
	        		<td>
	        			<table border="0" width="100%" cellspacing="0" cellpadding="0">
	          				<tr>
	            				<td class="main"><b>{$smarty.const.TABLE_HEADING_NEW_SHIPPING_ADDRESS}</b></td>
	          				</tr>
	        			</table>
	        		</td>
	      		</tr>
	      		<tr>
	        		<td>
	        			<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
	          				<tr>
	            				<td>
	            					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	              						<tr>
	                						<td>{$pixel_trans_10_1}</td>
	                						<td class="main" width="100%" valign="top">{$smarty.const.TEXT_CREATE_NEW_SHIPPING_ADDRESS}</td>
	                						<td>{$pixel_trans_10_1}</td>
	              						</tr>
	              					<tr>
	                					<td>{$pixel_trans_10_1}</td>
	                					<td>
	                						<table border="0" width="100%" cellspacing="0" cellpadding="0">
	                  							<tr>
	                    							<td width="10">{$pixel_trans_10_1}</td>
	                    							<td>{$CHECKOUT_NEW_ADDRESS_HTML}</td>
	                    							<td width="10">{$pixel_trans_10_1}</td>
	                  							</tr>
	                						</table>
	                					</td>
	                					<td>{$pixel_trans_10_1}</td>
	              					</tr>
	            				</table>
	            			</td>
	          			</tr>
	        		</table>
	        	</td>
	      	</tr>
			{/if}
	
	      	<tr>
	        	<td>{$pixel_trans_100_10}</td>
	      	</tr>
	      	<tr>
	        	<td>
	        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
	          			<tr>
	            			<td>
	            				<table border="0" width="100%" cellspacing="0" cellpadding="0">
	              					<tr>
	                					<td width="10">{$pixel_trans_10_1}</td>
	                					<td class="main"><b>{$smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}</b><br>{$smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}</td>
	                					<td class="main" align="right">{$action_hidden}{$button_continue_img_submit}</td>
	                					<td width="10">{$pixel_trans_10_1}</td>
	              					</tr>
	            				</table>
	            			</td>
	          			</tr>
	        		</table>
	        	</td>
	      	</tr>
		  	{if $process}
			<tr>
				<td>{$pixel_trans_100_10}</td>
	      	</tr>
	      	<tr>
	        	<td><a href="{$checkout_shipping_address_href}">{$button_back_img_button}</a></td>
	      	</tr>
			{/if}
	      	<tr>
	        	<td>{$pixel_trans_100_10}</td>
	      	</tr>
	      	<tr>
	        	<td>
	        		<table border="0" width="100%" cellspacing="0" cellpadding="0">
	          			<tr>
	            			<td width="25%">
	            				<table border="0" width="100%" cellspacing="0" cellpadding="0">
	              					<tr>
	                					<td width="50%" align="right">{$checkout_bullet_img}</td>
	                					<td width="50%">{$pixel_trans_100_1}</td>
	              					</tr>
	            				</table>
	            			</td>
	            			<td width="25%">{$pixel_trans_100_1}</td>
	            			<td width="25%">{$pixel_trans_100_1}</td>
	            			<td width="25%">
	            				<table border="0" width="100%" cellspacing="0" cellpadding="0">
	              					<tr>
	                					<td width="50%">{$pixel_trans_100_1}</td>
	                					<td width="50%">{$pixel_trans_1_5}</td>
	              					</tr>
	            				</table>
	            			</td>
	          			</tr>
	          		<tr>
	            		<td align="center" width="25%" class="checkoutBarCurrent">{$smarty.const.CHECKOUT_BAR_DELIVERY}</td>
	            		<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_PAYMENT}</td>
	            		<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</td>
	            		<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_FINISHED}</td>
	          		</tr>
	        	</table>
        	</td>
		</tr>
	</table>
        </td>
      </tr>
    </table>
    
	{if !$smarty.const.PH_INSIDE}		
				</td>
			</tr>
		</table>		  
	{/if}
	{$smarty.const.AFTER_CONTENT}
</form>