{if $arr_newsdetails}
    <a href="{$HEADER_NEWS_ALL_LINK}">{$arr_shopnewsbox.title}</a>
    <div class="clearfix">
        {if $found == 'true'}
            {if $first_link}<a href="{$first_link}" class="pull-left"></a>{/if}
            {if $next_link}<a href="{$next_link}" class="pull-right"></a>{/if}
        {/if}
    </div>
    <h1>{$arr_newsdetails.news_title}</h1>
    <div class="clearfix">
        <time>{$arr_newsdetails.news_date}</time>
    </div>
    {if $arr_newsdetails.news_picture}
        <img src="images/user/{$user_id}/{$arr_newsdetails.news_picture}" alt=""/>
    {/if}
    <div class="clearfix">{$arr_newsdetails.news_article}</div>
    {if $arr_newsdetails.news_author_id}
        {if $arr_newsdetails.picture_src}
            <img src="{$arr_newsdetails.picture_src}"/>
        {/if}
        <div class="clearfix">
            <div>{$arr_newsdetails.news_author_name}</div>
            {if $arr_newsdetails.news_author_position}
                <div>{$arr_newsdetails.news_author_position}</div>
            {/if}
            {if $arr_newsdetails.news_author_signatur}
                <div>{$arr_newsdetails.news_author_signatur}</div>
            {/if}
        </div>
    {/if}
    {if $arr_products_list}
        <h6>Zugehörige Produkte</h6>
        <table>
            {foreach from=$arr_products_list key=products_id item=arr_product name=productslister}
                <tr>
                    <td><a href="{$arr_product.seolink}">{$arr_product.products_image}</a></td>
                    <td><a href="{$arr_product.seolink}" class="productListingName">{$arr_product.products_name}</a>
                    </td>
                    <td>
                        {if $arr_product.special_price_brutto}
                            {$arr_product.special_price_brutto}
                            {$arr_product.products_brutto}
                        {else}
                            {$arr_product.products_brutto_price}
                        {/if}
                    </td>
                    <td>
                        <form action="/product_info.php?action=add_product&cPath={$arr_product.cPath}&products_id={$arr_product.products_id}">
                            <input type="hidden" name="products_id" value="{$arr_product.products_id}"/>
                            <input type="image" border="0" title="In den Warenkorb" alt="In den Warenkorb"
                                   src="{$imagepath}/buttons/{$language}/button_in_cart2.gif"/>
                        </form>
                    </td>
                </tr>
            {/foreach}
        </table>
    {/if}
{/if}
{if $arr_newslistview}
    <div class="text-right">
        <ul class="inline">
            {foreach from=$arr_newslistview.arr_pages_links key=linkkey item=link}
                {if $arr_newslistview.current_page == $linkkey}
                    <li class="muted">{$linkkey}</li>
                {else}
                    <li><a href="{$link}">{$linkkey}</a></li>
                {/if}
            {/foreach}
        </ul>
    </div>
    {foreach from=$arr_newslistview.arr_current_news key=newskey item=newsitem name=tegos}
        <div class="media">
            {if $newsitem.news_picture}
                <a href="{$newsitem.seolink}" class="pull-left">
                    <div class="media-object image-canvas"
                         style="width:100px;height:100px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$newsitem.news_picture}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$newsitem.news_picture}', sizingMethod='scale');"></div>
                </a>
            {/if}
            <div class="media-body">
                <h4 class="media-heading news-list-media-heading">
                    <a href="{$newsitem.seolink}">{$newsitem.news_title}</a>
                </h4>
                <small class="muted">{$newsitem.news_date}</small>
                <p>{$newsitem.news_teaser|truncate:230:"...":true}<a
                            href="{$newsitem.seolink}">{$arr_smartyconstants.TEXT_NEWS_MORE}</a></p>
            </div>
        </div>
    {/foreach}
    <div class="text-right">
        <ul class="inline">
            {foreach from=$arr_newslistview.arr_pages_links key=linkkey item=link}
                {if $arr_newslistview.current_page == $linkkey}
                    <li class="muted">{$linkkey}</li>
                {else}
                    <li><a href="{$link}">{$linkkey}</a></li>
                {/if}
            {/foreach}
        </ul>
    </div>
{/if}
{if $TEXT_NEWS_NOT_EXISTS}
    {$TEXT_NEWS_NOT_EXISTS}
{/if}