{if $arr_searchbox.form}
    <div id="search-wrap" class="pull-right">
        <div class="search">
            {$arr_searchbox.form}
                <input id="search" type="text" name="keywords" value="{$arr_searchbox.inputtextfield}" placeholder="{$arr_searchbox.title}">
            </form>
        </div>
    </div>
{/if}
