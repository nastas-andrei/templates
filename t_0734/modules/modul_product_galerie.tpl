{if $arr_modul_productlisting.products}

    {if $SCRIPT_NAME ne "/advanced_search_result.php"}
    {literal}
        <script type="text/javascript">
            function goHin(das) {
                var slink = document.getElementById('sortlink').value;
                var sort = document.getElementById('sortierung').selectedIndex;
                var wert = das[sort].value;
                var max = document.getElementById('max').value;
                var link = slink + '&sort=' + wert + '&max=' + max;
                location.href = link;
            }
        </script>
    {/literal}
    {/if}
    <div class="row">
        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            <div class="col-xs-12 col-sm-6 col-lg-3 text-center" style="min-width: 292px;">
                <div class="thumbnail">
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value="http://placehold.it/300x300&text=No%20image"}
                    {/if}
                    <a href="{$product.link}" class="link-p">
                        <div class="image_view_product_list" style="background-image: url('{$src}')"></div>
                    </a>

                    <div class="caption prod-caption">
                        <h4>
                            <a href="{$product.link}">{$product.products_name|truncate:36:"...":true}</a>
                        </h4>

                        <div class="btn-group">
                            <span class="btn btn-default" onclick="location.href='{$product.buy_now_link}'">{$product.newprice}</span>
                            <a href="{$product.buy_now_link}" class="btn btn-primary">
                                <i class="fa fa-shopping-cart"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}
        <div class="clearfix"></div>
        {if $arr_modul_productlisting.pagination|@count > 1}
        <div class="text-center">
            <ul class="pagination catalogue-pagination">
                {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                    {if $page.previous}
                        <li class="pag-prev">
                            <a href="{$page.link}" title="{$page.title}">← prev</a>
                            {elseif $page.active}
                        <li class="active">
                            <a href="javascript:void(0)">{$page.text}</a>
                            {elseif $page.next}
                        <li class="pag-next">
                            <a href="{$page.link}" title="{$page.title}">next →</a>
                            {else}
                        <li>
                        <a href="{$page.link}" title="{$page.title}">
                            {$page.text}
                        </a>
                    {/if}
                    </li>
                {/foreach}
            </ul>
        </div>
        {/if}
    </div>
{/if}
  
