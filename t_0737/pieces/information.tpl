<div class="container row">
    {if empty($custom_heading_title)}
        <h1 class="text-center h1-information">{$HEADING_TITLE}</h1>
    {else}
        <h1 class="text-center h1-information">{$custom_heading_title}</h1>
    {/if}
    <div class="page-info-description">
        {$INFO_DESCRIPTION}
    </div>
</div>