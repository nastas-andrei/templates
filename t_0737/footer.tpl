</div>
<!-- footer -->
<footer>
    <div class="container">
        <div class="row">
            <!-- 1box information -->
            <div class="col-lg-3 col-md-3  col-sm-6 col-xs-12 footer-box">
                <!-- <div class="column"> -->
                {include file="boxes/box_information.tpl"}
                <!-- </div> -->
            </div>
            {php}
                $i = 0;
            {/php}

            {foreach from=$boxes_pos.left key=key item=boxname name=current}
                {if $boxname eq 'htmlbox'}
                    {php}
                        $i++;
                        $this->assign('counter_boxes', $i);
                    {/php}
                    <div class="col-lg-3 col-md-3  col-sm-6 col-xs-12 footer-box">
                        {include file="boxes/box_$boxname.tpl"}
                    </div>
                    {if $counter_boxes == 3}
                        <div class="clearfix" style="margin-top:10px;"></div>
                    {/if}

                    {if $counter_boxes % 4 == 0 and $counter_boxes > 5}
                        <div class="clearfix" style="margin-top:10px;"></div>
                    {/if}
                {/if}
            {/foreach}
            {if $smarty.const.FACEBOOK_DISPLAY_FOOTER }
                {$text_custom_footer}
            {/if}
        </div>
        <div class="text-center copyright">
            {if $smarty.const.FACEBOOK_DISPLAY_FOOTER }
                {$text_custom_footer}
            {/if}
            <div class="inline text-center">
                <small>&copy; {$arr_footer.year} </small>
                {foreach from=$arr_footer.partner key=key item=partner}
                    <div style="display:inline-block;">
                        <small><a class="footer_partner" href="http://{$partner.link}" target="_blank" class="muted">{$partner.text} {$partner.name}</a></small>
                        <a href="http://{$partner.link}" target="_blank">
                            <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" alt="" title="{$partner.name}" style="vertical-align: middle" />
                        </a>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</footer>
<!-- footer end -->
</div>
</div>
</div>

</div>
</div>
</body>
</html>

  
    
    
    
