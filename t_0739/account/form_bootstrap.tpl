<div class="account-container">
<form id="createForm" class="form-login-page" name="create" action="{$login_form_url_create}" method="post">
{if $LoginCreateMessageStackSize == true}
<div class="alert">  
  {$logincreate_message}
</div>
{/if}


<div class="panel panel-default">
	<div class="panel-heading">
    <div class="panel-title">
    {$smarty.const.HEADING_NEW_CUSTOMER}
    </div>
  </div>
  <div class="panel-body">
   <div class="span10 offset1">
  <div class="form-inline">  
    <div class="control-group">
    <div class="controls">
      <label>{$smarty.const.ENTRY_GENDER_TEXT}</label>
      <label for="m" class="radio">
        {$gender_radio_m|replace:'name="reg_title"':'name="reg_title" class="required validate-reqchk-byname groupName:\'reg_title\'"'}
        {$smarty.const.MALE}
      </label>
      <label for="f" class="radio">
        {$gender_radio_f|replace:'name="reg_title"':'name="reg_title" class="required validate-reqchk-byname groupName:\'reg_title\'"'}  
        {$smarty.const.FEMALE}
      </label>      
      </div>
    </div>
  </div>
 
  
 <!--
    {if $smarty.const.ACCOUNT_COMPANY == 'true'}
    <div class="control-group">
      <label class="control-label">{$smarty.const.ENTRY_COMPANY}</label>
      <div class="controls">
      {$company_input}
      </div>
    </div>    
	  {/if} -->
    
    <div class="row">
      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_FIRST_NAME}</label>
        <div class="controls">
          <div class="input-append">
        {$firstname_input|replace:'class="inputfield"':'class="span4 required"'}
         <span class="add-on">+</span>
         </div>
        </div>
      </div>
      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_LAST_NAME}</label>
        <div class="controls">
          <div class="input-append">
        {$lastname_input|replace:'class="inputfield"':'class="span4 required"'}
         <span class="add-on">+</span>
         </div>
        </div>
      </div>
    </div>


     <div class="row">
      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_STREET_ADDRESS}</label>
        <div class="controls">
          <div class="input-append">
        {$street_address_input|replace:'class="inputfield"':'class="span4 required"'}
        <span class="add-on">+</span>
         </div>
        </div>
      </div>

      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_POST_CODE}</label>
        <div class="controls">
        <div class="input-append">
        {$zipcode|replace:'inputfield':'span2 zipcode required'}
        <span class="add-on">+</span>
         </div>
        </div>
      </div>
      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_CITY}</label>
        <div class="controls">
          <div class="input-append">
          {$city_input|replace:'inputfield':'span2 city required'}
          <span class="add-on">+</span>
         </div>
        </div>
      </div>
    </div> 
    <div class="row">

      
      {if $smarty.const.ACCOUNT_STATE == 'true'}
      <div class="span">
          <label class="control-label">{$smarty.const.ENTRY_STATE}</label>
          <div class="controls">        
            <div id="DivRegions" class="span2">                				
              {$state_field_html}&nbsp;
              <span class="inputRequirement">{$smarty.const.ENTRY_STATE_TEXT}</span>
            </div>
          </div>
      </div>
      <div class="span">
          <label class="control-label">{$smarty.const.ENTRY_COUNTRY}</label>
          <div class="controls">
            {$country_list|replace:'validate-custom-required':'span2 required'}
            {if $entry_country_text}
              <span class="inputRequirement">{$smarty.const.ENTRY_COUNTRY_TEXT}</span>
            {/if}
          </div>
      </div>
      {else}
        <div class="span">
          <label class="control-label">{$smarty.const.ENTRY_COUNTRY}</label>
          <div class="controls">
            <div class="input-prepend">
              <span class="add-on">&nbsp;</span>
              {$country_list|replace:'validate-custom-required':'span4 required'}              
            </div>
            {if $entry_country_text}
              <span class="inputRequirement">{$smarty.const.ENTRY_COUNTRY_TEXT}</span>
            {/if}
          </div>
        </div>
  		{/if}    
      
      <div class="span">
        <label class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
        <div class="controls">
          <div class="input-append">
        {$email_input|replace:'class="inputfield"':'class="span4 required validate-email"'}
         <span class="add-on">+</span>
         </div>
        </div>
      </div>
    </div>
    
    <div class="row">
    <div class="span">
      <label class="control-label">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</label>
      <div class="controls">
        <div class="input-append">
      {$fon_input|replace:'inputfield':'span4'}
       <span class="add-on">&nbsp;</span>
         </div>
      </div>
    </div>
    
    <div class="span">
      <label class="control-label">{$smarty.const.ENTRY_FAX_NUMBER}</label>
      <div class="controls">
        <div class="input-append">
      {$fax_input|replace:'inputfield':'span4'}
      <span class="add-on">&nbsp;</span>
         </div>
      </div>
    </div>
    </div>
    
    <div class="row">
    <div class="span">
      <label class="control-label">{$smarty.const.ENTRY_PASSWORD}</label>
      <div class="controls">
        <div class="input-append">
      {$create_password_input|replace:'class="inputfield"':'class="span4 required"'}
       <span class="add-on">+</span>
         </div>
      </div>
    </div>

    <div class="span">
      <label class="control-label">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</label>
      <div class="controls"> 
        <div class="input-append">
      {$password_confirmation_input|replace:'class="inputfield"':'class="span4 required"'}
       <span class="add-on">+</span>
         </div>
      </div>
    </div>
   </div>
    <div class="control-group">
    <div class="controls">

		  <label for="check-1" class="checkbox">
        <input type="checkbox" name="reg_terms_news" class="inputcheckbox" id="check-1" />
        {$smarty.const.TEXT_NEWS_TERM}
      </label>
         
		  <label for="check-2" class="inputcheckboxTXT">
        <input type="checkbox" name="reg_terms_ds" class="inputcheckbox required validate-required-check" id="check-2" />
        {$smarty.const.ENTRY_DATENSCHUTZ_TEXT}
      </label>
      <input type="submit" id="submit-register" class="btn btn-large pull-right" title="{$smarty.const.IMAGE_BUTTON_LOGIN}" value="{$smarty.const.IMAGE_BUTTON_LOGIN}"/>
    </div>
    </div>
  </div><!-- end span -->
  </div>
</div>         
</form>
</div>