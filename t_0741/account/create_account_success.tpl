<div class="account-container">
    <div class="alert alert-success">
       <button type="button" class="btn btn-primary btn-icon close" data-dismiss="alert" aria-hidden="true">×</button>
       <p>{$smarty.const.TEXT_STANDARD_WELCOME}</p>
    </div>
    <a href="{$origin_href}">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
</div>