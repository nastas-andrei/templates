    <footer>
        <div class="container footer-box">
            <div class="row">
                <div class="col-sm-3 contacts">
                    {include file="boxes/box_information.tpl"}
                </div>
                {php}
                    $i = 0;
                {/php}
                {foreach from=$boxes_pos.left key=key item=boxname name=current}
                    <div class="col-sm-3 info">
                        {if $boxname eq 'htmlbox' && $key < 79}
                            {include file="boxes/box_$boxname.tpl"}
                        {/if}
                    </div>
                {/foreach}
            </div>
        </div>
        <section class="copyrighttext">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <span>
                            <a href="sitemap.php" title="{$smarty.const.TEXT_SITEMAP}">{$smarty.const.TEXT_SITEMAP}</a> |
                            <a href="advanced_search.php" title="{$smarty.const.NAVBAR_TITLE_1}">{$smarty.const.BOX_SEARCH_ADVANCED_SEARCH}</a>
                        </span>
                        <p>&copy; {$arr_footer.year}
                            {foreach from=$arr_footer.partner key=key item=partner}
                                <span>
                                    <a href="http://{$partner.link}" target="_blank" class="muted">
                                        {$partner.text} {$partner.name}
                                    </a>
                                </span>
                                <a href="http://{$partner.link}" target="_blank">
                                    <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" title="{$partner.name}"/>
                                </a>
                            {/foreach}
                        </p>
                    </div>
                </div>
            </div>
        </section>
    </footer>
    <div id="gotoTop" class="fa fa-angle-up"></div>
    <div class="social">
        <ul>
            <li><a class="facebook" title="facebook" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
            <li><a class="twitter" title="twitter" href="https://www.twitter.com/"><i class="fa fa-twitter"></i></a></li>
            <li><a class="google" title="google" href="https://www.google.com/"><i class="fa fa-google-plus"></i></a></li>
            <li><a class="linkedin" title="linkedin" href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="pinterest" title="pinterest"  href="http://pinterest.com"><i class="fa fa-pinterest"></i></a></li>
            <li><a class="vimeo" title="vimeo"  href="http://vimeo.com"><i class="fa fa-vimeo-square"></i></a></li>
            <li><a class="youtube" title="youtube"  href="http://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
            <li><a class="flickr" title="flickr" href="http://www.flickr.com/"><i class="fa fa-flickr"></i></a></li>
        </ul>
    </div>

<script type="application/javascript" src="{$templatepath}lib/js/jquery.bxslider.min.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/jquery.easing.1.3.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/jquery.flexslider.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/custom.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/script.js"></script>

</body>
</html>