<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}" />
    <meta name="description" content="{$meta_description}" />
    <meta name="robots" content="index, follow" />
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}" />
    {/if}
    <link rel="stylesheet" href="{$templatepath}custom.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/owl-carousel.css">
    <link rel="stylesheet" href="{$templatepath}lib/css/flexslider.css">
    <link rel="stylesheet" href="{$templatepath}lib/css/animate.min.css">
    <link href="{$templatepath}lib/css/prettyPhoto.css" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,300italic,700,700italic,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Exo:400,300,600,500,400italic,700italic,800,900' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$templatepath}lib/css/royalslider.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/rs-default-inverted.css" />
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/settings.css" media="screen" />
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{$templatepath}lib/js/bootstrap.min.js"></script>
    <script src="includes/general.js"></script>
    <script src="{$templatepath}lib/js/categories.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/jquery.jqtransform.js"></script>
</head>
<body>
<div id="topbar">
    <div class="container">
        <div class="col-xs-12">
            <div class="callus clearfix">
                {include file="boxes/box_languages.tpl"}
                <span class="topbar-email"><i class="fa fa-envelope"></i>
                    <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a>
                </span>
                <span class="topbar-login"><i class="fa fa-user"></i>
                    <a href="{$domain}/login.php">{$arr_loginbox.title}</a>
                    {if $arr_loginbox.items}
                        <a href="{$arr_loginbox.items.4.target}"
                           title="{$arr_loginbox.items.4.text}">| {$arr_loginbox.items.4.text}</a>
                    {/if}
                </span>
                <span class="topbar-cart"><i class="fa fa-shopping-cart"></i>
                <a href="{$arr_shopping_cart.cart_link}">{$arr_shopping_cart.count_contents} -
                    <span class="price">{$arr_shopping_cart.gesamtsumme}</span></a>
                </span>
                <span class="top-search" onclick="toggleSearch()"><i class="fa fa-search"></i></span>

                <div class="search" id="form-search-container" style="display: none">
                    {include file="boxes/box_search.tpl"}
                </div>
            </div>
        </div>
    </div>
</div>
<header id="header-style-1">
    <div class="container">
        <nav class="navbar yamm navbar-default">
            <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {$cataloglogo}
            </div>
            <div id="categories-box"></div>
        </nav>
    </div>
</header>
{if $breadcrumb_array|@count > 1}
    <section class="post-wrapper-top jt-shadow clearfix">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="row">
                    <div class="col-md-12">
                        {assign var=last value=$breadcrumb_array|@end}
                        <h1>{$last.title}</h1>
                    </div>
                </div>
                <ul class="breadcrumb pull-right">
                    {foreach from=$breadcrumb_array key=key item=crumb}
                        {assign var=last value=$breadcrumb_array|@end}
                        {if $last.title == $crumb.title}
                            <li>{$crumb.title}</li>
                        {else}
                            <li><a href="{$crumb.link}">{$crumb.title}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
    </section>
{/if}
