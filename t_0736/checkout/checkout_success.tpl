<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 done"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3 done"><span><a href="{$checkout_payment_href}">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></span></li>
    <li class="step4 done"><span><a href="{$checkout_confirmation_href}">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</a></span></li>
</ol>

<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {$text_success}
</div>

<div>
	{$paypal_button}
	{$finanzkauf_button}
	<p class="text-center">{$text_see_orders}</p>						
	<p class="text-center">{$smarty.const.TEXT_THANKS_FOR_SHOPPING}</p>
	{if $is_money_order}
		<h4>{$text_money_order_success_edit}</h4>
	{/if}
	
	{$tracking_output}					
	{$DOWNLOADS_HTML}
	<div class="text-center">

		 <a class="button" href="{$seo_index_link}">{$smarty.const.HEADER_TITLE_HOME}</a>
	</div>
</div>
