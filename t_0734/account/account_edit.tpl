<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                {if $account_message_exists} 
                   <div class="alert alert-danger text-center">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</i>
                        </button>
                        {$account_message}
                   </div>
                {/if}
                <!-- button back -->               
                <div class="row">
                    <div class="pull-left shopping"> 
                        <a class="btn btn-primary" href="{$account_href}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>
                    </div>
                </div>
                <!-- button back end --> 
                <div class="row">
                    {$account_edit_form}
                        <div class="inputHLDbox">
                            <div class="inputHLDhead">{$smarty.const.MY_ACCOUNT_TITLE}</div>
                            <div class="inputHLDinner" style="margin-top:10px">
                                {*******************GENRE****************}
                                <div class="inputHLD">
                                    <div class="logradioHLD">
                                        {if $smarty.const.ACCOUNT_GENDER == 'true'}
                                            <label class="control-label" style="margin-right:10px">
                                                {$gender_radio_m}
                                                {$smarty.const.MALE}
                                            </label>                    
                                            <label class="control-label">
                                                {$gender_radio_f}
                                                {$smarty.const.FEMALE}                  
                                            </label>    
                                            <span style="color:red">
                                                {if $entry_gender_text}
                                                    {$smarty.const.ENTRY_GENDER_TEXT}
                                                {/if}
                                            </span>                 
                                        {/if}
                                    </div>
                                </div>
                                <div class="clearer"></div>
                                {*******************FIRST NAME****************}
                                <div class="inputHLD">
                                    <div class="inputTXT">{$smarty.const.ENTRY_FIRST_NAME}
                                        <small style="color:red">
                                        {if $entry_firstname_text}
                                            {$smarty.const.ENTRY_FIRST_NAME_TEXT}
                                        {/if}
                                            {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                                            {$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH}
                                        {/if}
                                        </small>
                                    </div>
                                    {$firstname_input}
                                </div>
                                {*******************LAST NAME****************}
                                <div class="inputHLD">
                                    <div class="inputTXT">{$smarty.const.ENTRY_LAST_NAME}
                                        <small style="color:red">
                                           {if $entry_lastname_text}
                                                {$smarty.const.ENTRY_LAST_NAME_TEXT}
                                            {/if}

                                            {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                                                {$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})
                                            {/if}   
                                        </small>
                                    </div>
                                        {$lastname_input}
                                </div>
                                <div class="clearer"></div>
                                {*******************DATE OF BIRTH****************}
                                {if $smarty.const.ACCOUNT_DOB == 'true'}
                                    <div class="inputHLD">
                                        <div class="inputTXT">{$smarty.const.ENTRY_DATE_OF_BIRTH}
                                        <span style="color:red">
                                            {if $entry_dob_text}
                                                {$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}
                                            {/if}
                                        </span>
                                        </div>
                                        {$dob_input}
                                    </div>
                                {/if}
                                {*******************EMAIL ADDRESS****************}
                                <div class="inputHLD">
                                    <div class="inputTXT" style="white-space: nowrap;">{$smarty.const.ENTRY_EMAIL_ADDRESS}
                                        <span style="color:red">
                                            {if $entry_email_address_text}
                                                {$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}
                                            {/if}
                                        </span>
                                    </div>
                                    {$email_address_input}
                                </div>
                                {*******************TELEPHONE NUMBER****************}
                                <div class="inputHLD">
                                    <div class="inputTXT" style="white-space: nowrap;">{$smarty.const.ENTRY_TELEPHONE_NUMBER}
                                        <span style="color:red">
                                            {if $entry_telephone_text}
                                                {$smarty.const.ENTRY_TELEPHONE_NUMBER_ERROR}
                                            {/if}
                                        </span>
                                    </div>
                                    {$telephone_input}
                                </div>
                                {*******************FAX NUMBER****************}
                                <div class="inputHLD">
                                    <div class="inputTXT">{$smarty.const.ENTRY_FAX_NUMBER}</div>
                                        <span style="color:red">
                                           {if $entry_fax_text}
                                                {$smarty.const.ENTRY_FAX_NUMBER_TEXT}
                                            {/if}
                                        </span>
                                    {$fax_input}
                                </div>     

                                {if $is_active_b2b}
                                    <p style="text-align:center"><strong>{$smarty.const.MY_ACCOUNT_TAX_ID}</strong></p>
                                    {if $smarty.const.ACCOUNT_CF == 'true'}
                                        <div class="form-group">
                                            <label class="control-label">{$smarty.const.ENTRY_CF}
                                            </label>
                                            {$entry_cf}
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">{$smarty.const.ENTRY_PIVA}
                                            </label>
                                            {$entry_piva}
                                        </div>
                                    {/if}
                                {/if}    
                            </div>
                            <div class="clearer"></div>
                                <div class="inputHLD" style="float:right">
                                    <button type="submit" class="btn btn-primary" >{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i>
                                    </button>  
                                </div>
                            <div class="clearer"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
