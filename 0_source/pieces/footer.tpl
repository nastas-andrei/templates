<!-- start footer //-->

<nobr>
  {if $reseller eq 0 || $reseller eq 1}
      {if $arr_footer.partner}
      	{foreach from=$arr_footer.partner key=key item=partner}
      		{if $key neq 0} <img src="{$imagepath}/footer_tr.gif" border="0" align="absmiddle">{/if}
      		{if $partner.id eq 1}{*Text vor Logo*}
      			<a href="http://{$partner.link}" target="_blank">{$partner.text}</a>
      			{if $partner.logo}
      				<a href="http://{$partner.link}" target="_blank"><img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0" align="absmiddle"> {$partner.name} </a>
      			{else}
      				<a href="http://{$partner.link}" target="_blank">{$partner.name} </a>&nbsp;&nbsp;
      			{/if}
      		{else}
      			{if $partner.logo}
      				<a href="http://{$partner.link}" target="_blank">{$partner.text}<img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0" align="absmiddle"></a>
      			{else}
					<a href="http://{$partner.link}" target="_blank">{$partner.text}</a>      				
      			{/if}
      			<a href="http://{$partner.link}" target="_blank">{$partner.name} </a>&nbsp;&nbsp;
      		{/if}
      	{/foreach}
      {/if}
  {elseif $reseller eq 3}
      {if $arr_footer.partner}
      	{foreach from=$arr_footer.partner key=key item=partner}
      		{if $key neq 0}
      			<img src="{$imagepath}/footer_tr.gif" border="0">&nbsp;&nbsp;
      		{/if}
      		{if $partner.id eq 6}{*Text vor Logo*}
      			{$text_dubli_footer}
      			{if $partner.logo}
      				<a href="http://{$partner.link}" target="_blank"><img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0" align="absbottom"></a>&nbsp;
      			{else}
      				<a href="http://{$partner.link}" target="_blank">{$partner.name}</a>&nbsp;&nbsp;
      			{/if}
      		{else}
      			{if $partner.logo}
      				<img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0">&nbsp;
      			{/if}
      			<a href="http://{$partner.link}" target="_blank">{$partner.name}</a>&nbsp;&nbsp;
      		{/if}
      	{/foreach}
      {/if}
  {else}
    	&copy; {$arr_footer.year}&nbsp;
      {if $arr_footer.partner}
      	{foreach from=$arr_footer.partner key=key item=partner}
      		{if $key neq 0}
      			<img src="{$imagepath}/footer_tr.gif" border="0">&nbsp;&nbsp;
      		{/if}
      		{if $partner.logo}
      			<img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0" align="absbottom">&nbsp;
      		{/if}
      		<a href="http://{$partner.link}" target="_blank">{$partner.name}</a>&nbsp;&nbsp;
      	{/foreach}
      {/if}
    {/if}
</nobr>
{$text_global_script} 
<!-- start footer_eof //-->