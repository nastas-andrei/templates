{if $boxes_content.$key}
<div class="box-html-box">    
	<div class="box-heading">
		<h5>{$boxes_content.$key.boxes_title} &nbsp;</h5>     			
	</div>
	<div class="box-content clearfix">
		{$boxes_content.$key.boxes_content}	
	</div>				
</div>
{/if}