<div class="footer">
  {if $reseller eq 0 || $reseller eq 1}
      {if $arr_footer.partner} 
      	{foreach from=$arr_footer.partner key=key item=partner}
      		{if $key neq 0} <{/if}
      		{if $partner.id eq 1}{*Text vor Logo*}
      			<a href="http://{$partner.link}" target="_blank">{$text_hotdigital_footer}</a>
      			{if $partner.logo}
      				<a href="http://{$partner.link}" target="_blank"><img src="" alt="{$partner.name}" title="{$partner.name}" border="0" align="absmiddle"> {$partner.name} </a>
      			{else}
      				<a href="http://{$partner.link}" target="_blank"> {$partner.name} </a>&nbsp;&nbsp;
      			{/if}
      		{else}
      			{if $partner.logo}
      				<a href="http://{$partner.link}" target="_blank"><img src="" alt="{$partner.name}" title="{$partner.name}" border="0" align="absmiddle"></a>
      			{/if}
      			<a href="http://{$partner.link}" target="_blank"> {$partner.name} </a>&nbsp;&nbsp;
      		{/if}
      	{/foreach}
      {/if}	
  {else}
	&copy; {$arr_footer.year}&nbsp;
	{if $arr_footer.partner}
		{foreach from=$arr_footer.partner key=key item=partner}
			{if $key neq 0}
			<&nbsp;&nbsp;
			{/if}
			{if $partner.logo}
			<img src="{$imagepath}/{$partner.logo}" alt="{$partner.name}" title="{$partner.name}" border="0" align="absmiddle">&nbsp;
			{/if}
			<a href="http://{$partner.link}" target="_blank">{$partner.name}</a>&nbsp;&nbsp;
		{/foreach}
	{/if}	
  {/if}	
{$text_global_script}
</div>