{if $arr_loginbox}
<div class="btn-group">
    <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
        <i class="fa fa-user"></i>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li><a href="{$arr_topmenu.4.link}" title="{$arr_topmenu.4.name}" >{$arr_topmenu.4.name}</a></li>
        {if $arr_loginbox.boxtyp == "myaccount"}
            <li ><a href="{$arr_loginbox.items.4.target}">{$arr_loginbox.items.4.text}</a></li>
        {else}
            <li ><a href="{$domain}/login.php">{$arr_smartyconstants.LOGIN_BOX_REGISTER}</a></li>
        {/if}
        <li><a href="{$arr_topmenu.2.link}">{$arr_topmenu.2.name}</a></li>
        {if $arr_loginbox.boxtyp == "login"}
            <li><a href="{$arr_topmenu.4.link}" class="red">{$smarty.const.TEXT_NOT_CUSTOMER_YET}</a></li>
        {/if}
    </ul>
</div>
{/if}