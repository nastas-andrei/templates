<div class="col-md-4">
{$advanced_search_form}
	
	{if $is_search_message}
   <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {$search_message}
    </div>
  {/if}
 <fieldset>
    <legend>{include file="pieces/page_title_section.tpl"}</legend>
      <div class="form-group">
        {*$info_box_content*} 
        <span class="col-md-9"><input type="text" class="form-control" name="keywords"></span>
        <button type="submit" target="payment" title="suchen" alt="suchen" class="btn btn-primary" style="padding:6px 10px">Suchen</button>
      </div>
      <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="search_in_description" checked="checked" id="1">Auch in den Beschreibungen suchen
            </label>
        </div>
      </div>
      <div class="form-group">
        <div class="checkbox">
            <label>
                {$inc_subcat_field}{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}
            </label>
        </div>
      </div>
      <div class="form-group">
        {$smarty.const.ENTRY_CATEGORIES}
        {$categories_id_field}
      </div>
      <div class="form-group">
        {$smarty.const.ENTRY_MANUFACTURERS}
        {$manufacturers_id_field}
      </div>
       <div class="form-group">
        <label class="control-label">{$smarty.const.ENTRY_PRICE_FROM}</label>
        {$pfrom_field}
      </div>
      <div class="form-group">
        <label class="control-label">{$smarty.const.ENTRY_PRICE_FROM}</label>
        {$pto_field}
      </div>
  </fieldset>      
</form>
</div>
