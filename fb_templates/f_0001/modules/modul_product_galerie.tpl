  {assign var=arr1 value=" "|explode:$arr_modul_productlisting.count_products_info}
  {assign var=total_items value=$arr1[0]|regex_replace:"/[^0-9]+/":""}  

  {literal}

    <script>
      var category = '{/literal}{$cPath}{literal}';
      var items_per_page = {/literal}{$max}{literal}      
      var total_items = {/literal}{$total_items}{literal}    
      var current_page = 1 
      var total_pages = Math.ceil(parseInt(total_items)/items_per_page)

      $(document).ready(function($) {
        var fired = false
        $('#wrapper').scroll(function () { 
          $('#loader').remove();
          if(current_page <= total_pages){ 

          if ($('#wrapper').scrollTop() >= $('.row-fluid').height() - $('#wrapper').height() - $('.footer').height()) {                 
              if (fired ===false){
                  fired = true
                  $('#items-listing').append('<img id="loader" src="https://www.facebook.com/images/loaders/indicator_blue_medium.gif" style="display:block;margin:0 auto;"/>')                   
                    current_page++                                     
                    $.ajax({
                      type: "GET",
                      url: "product_listing_request.php?fb=1",
                      data: { page: current_page,max:items_per_page,cPath:category}
                    }).done(function(result) {
                        $('.products-listing').append(result)                                          
                        fired = false
                    });                
                }
              }  
            }               
          });  
      });      
    </script>

 {/literal}


{if $arr_modul_productlisting.products}  
 <div id="items-listing">

      <div class="products-listing">
      {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}

        <div class="media"> 
       
                  {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                  {/if}

                  
                    <a href="{$product.link}" class="pull-left">
                      <!--<img class="media-object" src="{$src}" alt="" style="width:168px;height:168px;">-->
                        <div class="img-canvas" style="width:168px;height:168px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
                    </a>
                  
        
                <div class="media-body">       
                  
                    <a href="{$product.link}" class="media-heading">
                      {$product.products_name|truncate:150:"...":true}
                    </a>
                     <p class="product-category">{$cat_name}</p>
                    <p class="product-price">        
                      {if $product.oldprice > 0}
                        <span class="product-old-price">{$product.oldprice}</span>
                       {/if}   
                        <span class="product-new-price">{$product.newprice|regex_replace:"/[()]/":""}</span>  
                        &nbsp;<span class="product-new-price" style="display:block">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                    </p>
                      <p>
                      <div class="fb-like" data-href="{$product.link}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                    </p>
                                       
                              
                </div>
        </div>

      {/foreach}
      </div> 

</div>
{/if}