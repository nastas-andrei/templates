<div id="sns_content2" class="wrap" style="margin-bottom: 10px">
    <div class="container">
        <div class="row-fluid">
            <div class="alert alert-info">
                <button type="button" class="btn btn-primary btn-icon close" data-dismiss="alert" aria-hidden="true">×
                </button>
                {$smarty.const.TEXT_MAIN}
            </div>
            <div class="text-center">
                <a href="{$DEFAULT_URL}" class="btn btn-primary btn-icon">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
            </div>
        </div>
    </div>
</div>