<div id="content" class="box-wrap">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {$smarty.const.TEXT_MAIN}
    </div>
    <div class="text-center">
        <a href="{$DEFAULT_URL}" class="button">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
    </div>
</div>