{if $boxes_content.$key}
<div class="box box-html-box">    
	<div class="box-heading">
		<h3>{$boxes_content.$key.boxes_title}</h3>     			
	</div>
	<div class="box-content clearfix">
		{$boxes_content.$key.boxes_content}	
	</div>				
</div>
{/if}