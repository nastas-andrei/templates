{if $arr_languagebox}
	<div class="btn-group">
		{foreach from=$arr_languagebox.items key=key item=language}
			{if ($languageview neq $language.directory)}
				<a href="{$language.link}" class="btn lang {if $arr_languagebox.title == $language.name}topmenu_selected{/if}">
					{$language.name|truncate:3:" "}
				</a>
			{/if}
		{/foreach}
	</div>
{/if}