{if $arr_bestsellersbox}
    <!--Begin Best Content -->
    <ul class="list-popular">
        {foreach from=$arr_bestsellersbox.products key=i item=product name=previewitems}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
            {/if}
            <li>
                <figure class="img-polaroid1">
                    <a href="#">
                        <img src="{$src}" alt="" width="70"></a>
                </figure>
                <div>
                    <h5>
                        <a href="{$product.link}" title="{$product.name}">{$product.name}</a>
                    </h5>
                    <span href="#" class="v_price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                </div>
            </li>
        {/foreach}
    </ul>
    <!--END Best Content -->
{/if}

