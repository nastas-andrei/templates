<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    <meta charset="utf-8">
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}

    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css"
          href="{$templatepath}lib/skin/default/sns_nova/assets/bootstrap/css/responsive.css" media="all"/>
    {*<link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/css/widgets.css" media="all"/>*}
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/sns/ajaxcart/css/style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/fractionsliderbanner/css/fractionslider.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/fractionsliderbanner/css/style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/sns/megamenu/css/horizontal/megamenu.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/sns/quickview/css/quickview.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/quickview/css/jquery.fancybox-1.3.4.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/slider/css/slider.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/sns/twitter/css/style.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/producttabs/css/styles.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/default/sns/producttabs/css/effects.css" media="all"/>
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/skin/default/sns_nova/css/print.css" media="print"/>
    <link rel="stylesheet" href="{$templatepath}lib/skin/default/sns_nova/css/browsers/chrome.css" type="text/css"/>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="{$templatepath}lib/skin/default/sns_nova/css/cloud-zoom.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}lib/skin/default/sns_nova/assets/prettyPhoto/css/prettyPhoto.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}lib/skin/default/sns_nova/assets/plg_jqtransform/css/jqtransform.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}lib/skin/default/sns_nova/css/sns-responsive.css" type="text/css"/>
    {if $DESIGN_VARIANT != ""}
        <link rel="stylesheet" type="text/css" href="{$templatepath}stylesheet_news_{$DESIGN_VARIANT}.css"/>
    {/if}
    <link rel="stylesheet" href="{$templatepath}custom.css" type="text/css"/>

    <script type="text/javascript" src="{$templatepath}lib/js/prototype/prototype.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/lib/ccard.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/prototype/validation.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/scriptaculous/builder.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/scriptaculous/effects.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/scriptaculous/dragdrop.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/scriptaculous/controls.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/scriptaculous/slider.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/varien/js.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/varien/form.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/varien/menu.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/mage/translate.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/mage/cookies.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/varien/product.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/calendar/calendar.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/calendar/calendar-setup.js"></script>

    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/sns/ajaxcart/js/jquery.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/sns/ajaxcart/js/sns.noconflict.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/ajaxcart/js/sns-ajaxcart.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/fractionsliderbanner/js/jquery.fractionslider.js"></script>
    {*<script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/sns/megamenu/js/mega-menu.js"></script>*}
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/quickview/js/jquery.fancybox-1.3.4.pack.js"></script>
     {*<script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/quickview/js/quickview.js"></script> *}
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/slider/js/jquery.carouFredSel-6.2.1-packed.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/slider/js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/slider/js/jquery.touchSwipe.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/default/sns/slider/js/jquery.transit.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/sns/twitter/js/twitterfetcher-min.js"></script>

    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/js/cloud-zoom.1.0.2.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/assets/prettyPhoto/js/jquery.prettyPhoto.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/js/sns-script.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/js/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/js/jquery.accordion_snyderplace.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/assets/plg_jqtransform/js/jquery.jqtransform.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/skin/default/sns_nova/js/sns-extend.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/categories.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/device_script_style.js"></script>
    <script type="text/javascript" src="includes/general.js"></script>
    {literal}
        <script type="text/javascript">
            //<![CDATA[
            var isCOPage = 0;
            var isWLPage = 0;
            var isCPPage = 0;
            var isLoggedIn = "0";
            var skiplink = '';
            var ajax_for_compare = 1;
            var ajax_for_wishlist = 1;
            var ajax_for_update = 1;
            var show_confirm = 1;
            var time_cd = 8;
            var show_product_in_confirm = 1;
            var loader = '<img alt="Loading..." src="templates/t_0736/images/camera-loader.gif"/><div>Please wait...</div>';
            var usingImgFly = 1;
            var scrollToCart = 1;

            function getConfirm(result) {
                if (!show_confirm) {
                    return false;
                }
                $("ajax_overlay").onclick = function (e) {
                    $("confirmbox").innerHTML = '';
                    $("ajax_content").setStyle({display: "none"});
                    $("sns_ajaxwrap").setStyle({display: "none"});
                    $("ajax_overlay").setStyle({display: "none"});
                };
                cfaction = "";
                cfaction_text = "";
                cfaddedto_text = (closeCFirm == 1 || deletePCompare == 1) ? "Item deleted from " : " added to ";

                if (result.addtype == '0') {
                    ac_text = (isCOPage == 1) ? "Proceed to Checkout" : "Go to Cart";
                    ac_href = (isCOPage == 1) ? "index.php/checkout/onepage/" : "index.php/checkout/cart/";
                    cfaction = '<li><a id="btn_cfaction" title="Checkout" class="btn view-cart" onclick="$(\'confirmbox\').innerHTML=\'\';$(\'ajax_content\').setStyle({display:\'none\'});$(\'sns_ajaxwrap\').setStyle({display:\'none\'});window.location=\'' + ac_href + '\';"  href="' + ac_href + '">  <span>' + ac_text + '</span></a></li>';
                    cfaction_text = "your shopping cart.";
                } else if (result.addtype == '1') {
                    cfaction = '<li><a id="btn_cfaction" title="View Wish List" class="btn view-wishlist" onclick="$(\'confirmbox\').innerHTML=\'\';$(\'ajax_content\').setStyle({display:\'none\'});$(\'sns_ajaxwrap\').setStyle({display:\'none\'});window.location=\'index.php/wishlist/index/\';"  href="index.php/wishlist/index/">  <span>My Wishlist</span></a></li>';
                    cfaction_text = "your wishlist.";
                } else if (result.addtype == '2') {
                    strPop = result.co_sb;
                    ac_href = strPop.substring(strPop.indexOf("popWin('"), strPop.indexOf(")", strPop.indexOf("popWin('")) + 1);
                    if (trim(ac_href) != '') {
                        cfaction = '<li><a id="btn_cfaction" title="View Comparision" class="btn view-compare" href="javascript:void(\'0\')" onclick="$(\'confirmbox\').innerHTML=\'\';$(\'ajax_content\').setStyle({display:\'none\'});$(\'sns_ajaxwrap\').setStyle({display:\'none\'});' + ac_href + '">  <span>Compare</span></a></li>';
                    } else {
                        cfaction = '';
                    }
                    cfaction_text = "comparison list.";
                }

                cfhead = '<span id="cout_down">' + time_cd + 's</span><p class="head-cfbox ' + ((result.addtype == '1' || result.addtype == '2') ? 'show' : '') + '"><span class="product-title">' + result.pdname + '</span>' + cfaddedto_text + cfaction_text + '</p>';
                pdinfo = (typeof result.pdinfo != "undefined") ? result.pdinfo : "";
                confirmcontent = cfhead + ((typeof pdinfo.item != "undefined" && pdinfo.item != "" && show_product_in_confirm) ? (pdinfo.item) : "") + ((typeof pdinfo.other != "undefined" && pdinfo.other != "") ? pdinfo.other : "") + '<div id="bottom-cfbox"><ul>' + cfaction + '</ul></div>';

                $("confirmbox").innerHTML = confirmcontent;
                setTimeout("getCDown(time_cd)", 1000);
                $("ajax_content").setStyle({display: "block"});
                $("sns_ajaxwrap").setStyle({display: "block"});
                fixPosWrap();
                if (time_cd == 0)
                    isCPPage = 0;
            }
            //]]>
        </script>
        <script type="text/javascript">
            // <![CDATA[
            $sns_jq(window).load(function () {
                $sns_jq('.sns-fraction-slider').fractionSlider({
                    'autoChange': true,
                    'timeout': 5000,
                    'fullWidth': true,
                    'dimensions': "1170,651",
                    'controls': true,
                    'pager': false,
                    'responsive': true,
                    'increase': false,
                    'pauseOnHover': false
                });
            });
            // ]]>
        </script>
        <style type="text/css">
            body#bd {
                font-family: arial, sans-serif;
                font-size: 12px;
                background-color: #FFFFFF;
            }

            .page-title, .page-head, ul.mainnav > li > a span, .megamenu-col.have-spetitle > .mega-title, .sns-slideshow-wrap .claim, .sns-slideshow-wrap .subclaim, .sns-slideshow-wrap .buttons-action .button, .block .block-title, input[type="button"], .button, button, .price-box .price, .pdt-nav > li, .pdt-loadmore .btn-loadmore, #bottom-cfbox .btn, .sns-product-detail ul.sns-tab-navi li a, .products-grid .item-title a, .product-name, .ico-product, #sns_twitter .user span span, .item-post .postTitle a, .sns-slider .title-slider, #sns_titlepage, .slide-banner a.get-it, .postWrapper .postTitle h2 {
                font-family: Open Sans, sans-serif;
                font-weight: 300 !important;
            }
        </style>
    {/literal}
</head>
<body class="sns_nova  cms-index-index cms-home" id="bd">
<section id="sns_wrapper">
    <header>
        <div id="sns_topheader" class="wrap">
            <div class="container">
                <div class="row-fluid">
                    <div class="topheader-left span5">
                        {if $smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT || $smarty.const.HEAD_REPLY_TAG_ALL}
                        <ul class="contact-us-now" id="contact-header">
                            {if $smarty.const.HEAD_REPLY_TAG_ALL}
                                <li class="c-email" id="contact-header-mail">
                                    <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a>
                                </li>
                            {/if}
                            {if $smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
                                <li class="c-phone" id="contact-header-phone">
                                    <label>Phone</label>: {$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
                                </li>
                            {/if}
                        </ul>
                        {/if}
                    </div>
                    <div class="topheader-right span7">
                        <div class="inner">
                            <div class="sns-quickaccess">
                                <div class="quickaccess-inner">
                                    {include file="boxes/box_loginbox.tpl"}
                                </div>
                            </div>
                            <div class="sns-switch">
                                <div class="switch-inner">
                                    <div class="language-switcher">
                                        {include file="boxes/box_languages.tpl"}
                                        {literal}
                                            <script type="text/javascript">
                                                $sns_jq(function ($) {
                                                    $(".language-switcher").jqTransform();
                                                    array_bgi = new Array();
                                                    array_bgr = new Array();
                                                    array_bgp = new Array();
                                                    array_pdr = new Array();

                                                    $i = 0;
                                                    $(".language-switcher select option").each(function () {
                                                        array_bgi[$i] = $(this).data('image')
                                                        $i++;
                                                    });
                                                    $i = 0;
                                                    $(".language-switcher li a").each(function () {
                                                        $text = $(this).text();
                                                        $(this).empty();
                                                        $(this).append($(document.createElement('img')).attr('src', array_bgi[$i]), $text);

                                                        if ($(this).hasClass('selected')) {
                                                            $text = $('#sns_topheader .language-switcher .jqTransformSelectWrapper div span').text();
                                                            $('#sns_topheader .language-switcher .jqTransformSelectWrapper div span').empty().append($(document.createElement('img')).attr('src', array_bgi[$i]), $text);
                                                        }
                                                        $(this).css('padding-left', array_pdr[$i]);
                                                        $i++;
                                                    });
                                                });
                                            </script>
                                        {/literal}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN: Header -->
        <div id="sns_header" class="wrap">
            <div class="container">
                <div class="row-fluid">
                    <!-- BEGIN: Logo -->
                    <div id="logo" class="span4">
                        {$cataloglogo}
                    </div>
                    <!-- END: Logo -->
                    <div class="header-right span8">
                        <div class="header-right-inner">
                            {include file="boxes/box_search.tpl"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--END BEGIN: Header -->
        <div id="sns_menu" class="wrap">
            <div class="inner">
                <div class="container">
                    <div class="row-fluid">
                        {*Ajax categories*}
                        <div id="categories-box"></div>
                        <div class="nav-right">
                            <div class="header-right-inner">
                                {include file="boxes/box_shopping_cart.tpl"}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BEGIN: breadcrumbs -->
        {if $breadcrumb_array|@count > 1}
            <div id="sns_breadcrumbs" class="wrap">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span12">
                            <div id="sns_titlepage">
                                {foreach from=$breadcrumb_array key=key item=bread_last name=foo}
                                    {if $smarty.foreach.foo.last}
                                        <h1>{$bread_last.title}</h1>
                                    {/if}
                                {/foreach}
                            </div>
                            <div id="sns_pathway" class="clearfix">
                                <div class="pathway-inner">
                                    <ul class="breadcrumbs">
                                        {foreach from=$breadcrumb_array key=key item=bread}
                                            <li class="last">
                                                <a href="{$bread.link}">{$bread.title}</a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/if}
        <!-- END: breadcrumbs -->
    </header>
        <!-- BEGIN: SlideShow -->
