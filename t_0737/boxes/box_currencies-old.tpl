{if $arr_currenciesbox}
<!-- start currencies box //-->
<table border="0" width="100%" cellspacing="0" cellpadding="0">
  <tr>
		<td align="left" valign="top"  class="box_set1_out_o"><img src="{$imagepath}/box_set1_out_o_li.gif" border="0" alt=""></td>
		<td valign="top"  class="box_set1_out_o" width="100%"><img src="{$imagepath}/box_set1_out_o.gif" border="0" alt=""></td>
		<td align="right" valign="top" class="box_set1_out_o"><img src="{$imagepath}/box_set1_out_o_re.gif" border="0" alt=""></td>
  </tr>
  <tr>
		<td align="left" valign="top"  class="box_set1_out_li"><img src="{$imagepath}/box_set1_out_li.gif" border="0" alt=""></td>
		<td align="left" valign="top" width="100%" class="box_set1_out_mi">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" valign="top" width="100%" class="box_set1_in_hl_mi">
					{* start *}
									<table border="0" width="100%" cellspacing="0" cellpadding="0" class="currenciesBoxTitelTabelle">
										<tr>
											<td class="currenciesBoxTitelInhalt" nowrap width="100%" align="left">{$arr_currenciesbox.title}</td>
										</tr>
									</table>
					{* ende *}
					</td>
				</tr>
			</table>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" valign="top" width="100%" class="box_set1b_in_mi">
					{* start *}
									<table border="0" width="100%"  cellspacing="0" cellpadding="0">
	  						<tr>
								<td>
									<table border="0" width="100%"  cellspacing="0" cellpadding="0">
						  				<tr>
											<td align="left">
												<form name="currencies" action="{$arr_currencies.formaction}" method="get">
													<select name="currency" onChange="this.form.submit();" style="width: 90%" class="currenciesBoxSel" >
														{html_options options=$arr_currenciesbox.items selected=$currency}
													</select>
													{foreach from=$arr_currenciesbox.hiddenfields key=key item=fieldname}
														<input type="hidden" name="{$key}" value="{$fieldname}">
													{/foreach}
												</form>
											</td>
						  				</tr>
									</table>
								</td>
  							</tr>
						</table>
					{* ende *}
					</td>
				</tr>
			</table>
		</td>
		<td align="right" valign="top" class="box_set1_out_re"><img src="{$imagepath}/box_set1_out_re.gif" border="0" alt=""></td>
  </tr>
  <tr>
		<td align="left" valign="top"  class="box_set1_out_u"><img src="{$imagepath}/box_set1_out_u_li.gif" border="0" alt=""></td>
		<td valign="top"   class="box_set1_out_u" width="100%"><img src="{$imagepath}/box_set1_out_u.gif" border="0" alt=""></td>
		<td align="right" valign="top" class="box_set1_out_u"><img src="{$imagepath}/box_set1_out_u_re.gif" border="0" alt=""></td>
  </tr>
</table>
<!-- end currencies box //-->
<img src="{$imagepath}/spacer.gif" border="0" alt=""  height="20" width="1"><br>
{/if}
