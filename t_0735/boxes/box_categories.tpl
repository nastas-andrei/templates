<!-- Brand and toggle get grouped for better mobile display -->
<div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse-menu">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
</div>
<!-- Collect the nav links, forms, and other content for toggling -->
<div class="collapse navbar-collapse" id="collapse-menu">
    <ul class="nav navbar-nav">
        {foreach from=$categories key=key item=category}
            <li class="catLevel_ dropdown">
                <a href="{$category.link}" class="menu-item dropdown-toggle">
                    {$category.name}
                </a>
                {if $category.children|@count > 0}
                    <ul class="dropdown-menu">
                        {foreach from=$category.children key=key item=categorySub}
                            <li {if $categorySub.children|@count > 0} class="dropdown-submenu"{/if}>
                                <a href="{$categorySub.link}" class="menu-item">{$categorySub.name}</a>
                                {if $categorySub.children|@count > 0}
                                    <ul class="dropdown-menu">
                                        {foreach from=$categorySub.children item=categorySub2}
                                            <li {if $categorySub2.children|@count > 0}class="dropdown-submenu"{/if}>
                                                <a href="{$categorySub2.link}" class="menu-item">{$categorySub2.name}</a>
                                                {if $categorySub2.children|@count > 0}
                                                    <ul class="dropdown-menu">
                                                        {foreach from=$categorySub2.children item=categorySub3}
                                                            <li {if $categorySub3.children|@count > 0} class="dropdown-submenu"{/if}>
                                                                <a href="{$categorySub3.link}">{$categorySub3.name}</a>
                                                            </li>
                                                        {/foreach}
                                                    </ul>
                                                {/if}
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                {/if}
            </li>
        {/foreach}
    </ul>
</div>
<!-- /.navbar-collapse -->