{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}
    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
        {*<section id="slider">*}
            {*<div class="container">*}
                {*<div class="row">*}
                    {*<div class="col-sm-12">*}
                        {*<div id="slider-carousel" class="carousel slide" data-ride="carousel">*}
                            {*{$user_html}*}
                        {*</div>*}
                    {*</div>*}
                {*</div>*}
            {*</div>*}
        {*</section>*}
    {*{/if}*}
    {if $slider_images != ""}
    <section id="slider">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            {foreach from=$slider_images item=slider name=name_slider key=key}
                                <li data-target="#slider-carousel" data-slide-to="{$key}" {if $smarty.foreach.name_slider.first}class="active"{/if}></li>
                            {/foreach}
                        </ol>

                        <div class="carousel-inner">
                            {foreach from=$slider_images item=slider name=name_slider}
                            <div class="item {if $smarty.foreach.name_slider.first}active{/if}">
                                <div class="col-sm-6">
                                    <h1>{$slider.title}</h1>
                                    {if $slider.link}
                                        <a href="{$slider.link}" class="btn btn-default get" >{$slider.button_title}</a>
                                    {/if}
                                </div>
                                <div class="col-sm-6">
                                    <img src="{$slider.image}?t[]=resize:width=500,height=500" class="girl img-responsive" alt="{$slider.title}" />
                                </div>
                            </div>
                            {/foreach}
                        </div>

                        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {/if}

    {*{$UPCOMING_PRODUCTS_HTML}*}
    <section id="content-index">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="left-sidebar">
                        <h2>{$smarty.const.BOX_HEADING_CATEGORIES}</h2>

                        <div class="panel-group category-products" id="categories"><!--category-productsr-->
                            <div id="categories-box"></div>
                        </div>
                        <!--/category-products-->
                           <!--manufacturer-->
                            <h2>{$smarty.const.BOX_HEADING_MANUFACTURERS}</h2>
                            {include file="boxes/box_manufacturers.tpl"}
                            <!--/manufacturer-->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9 padding-right">
                    {if $arr_modul_previewproducts}
                        <div class="features_items"><!--features_items-->
                            <h2 class="title text-center">{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</h2>
                            {$FILENAME_FEATURED_HTML}
                        </div>
                        <!--features_items-->
                    {/if}
                    <div class="clearfix"></div>
                    {if $arr_whatsnewbox}
                        <hr/>
                        <div class="recommended_items"><!--new_items-->
                            <h2 class="title text-center what-new">{$arr_whatsnewbox.title}</h2>

                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                {include file="boxes/box_whats_new.tpl"}
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div>
                        <!--/new_items-->
                    {/if}
                </div>
            </div>
        </div>
    </section>
    {*{if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}*}
        {*{$define_main_bottom}*}
    {*{/if}*}

{/if}

{if $is_products == 1}
    <section id="content-index" class="margin-top">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <div class="left-sidebar">
                        <h2>{$smarty.const.BOX_HEADING_CATEGORIES}</h2>

                        <div class="panel-group category-products" id="categories"><!--category-productsr-->
                            <div id="categories-box"></div>
                        </div>
                        <!--/category-products-->
                        <!--manufacturer-->
                        <h2>{$smarty.const.BOX_HEADING_MANUFACTURERS}</h2>
                        {include file="boxes/box_manufacturers.tpl"}
                        <!--/manufacturer-->
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9 padding-right">
                    {$smarty.const.HEADING_TITLE}

                    {if $filterlistisnotempty}
                        {$filter_form}
                        {$smarty.const.TEXT_SHOW}
                    {/if}

                    {if !empty($categories_html)}
                        {$categories_html}
                    {/if}

                    {if !empty($manufacturers_html_header)}
                        {$manufacturers_html_header}
                    {/if}

                    {if !empty($PRODUCT_LISTING_HTML)}
                        {$PRODUCT_LISTING_HTML}
                    {/if}

                    {if !empty($manufacturers_id)}
                        {$manufacturers_html_footer}
                    {/if}

                    {if !empty($categories_html_footer)}
                        {$categories_html_footer}
                    {/if}
                </div>
            </div>
        </div>
    </section>
{else}
    {if $is_default}
        &nbsp;
    {else}
        {*<p class="block">{$tep_image_category}</p>  *}
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}