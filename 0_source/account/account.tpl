{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}	

	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}
 
		<tr>
        	<td class="cont_abst">        		
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
					{if $account_message_exists} 
     				<tr>
        				<td>{$account_message}</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				{/if}
       
					{if $count_customer_orders > 0}
      				<tr>
        				<td>
        					<table border="0" cellspacing="0" cellpadding="2">
          						<tr>
            						<td class="main"><b>{$smarty.const.OVERVIEW_TITLE}</b></td>
            						<td class="main"><a href="{$account_hystory_href}"><u>{$smarty.const.OVERVIEW_SHOW_ALL_ORDERS}</u></a></td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" >
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
            								<tr>
            									<td class="main" align="center" valign="top" width="130">
            										<b>{$smarty.const.OVERVIEW_PREVIOUS_ORDERS}</b>
            										<br>
            										{$arrow_south_east_img}
            										</td>
            								</tr>
            							</table>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td>
                									<table border="0" width="100%" cellspacing="0" cellpadding="2">
														{section name=current loop=$orderslist}
										            	<tr class="moduleRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.location.href='{$orderslist[current].order_history_info_href}'">
										                	<td class="main" width="80">{$orderslist[current].date_purchased}</td>
										                	<td class="main">#{$orderslist[current].orders_id}</td>
										                	<td class="main">{$orderslist[current].order_name}, {$orderslist[current].order_country}</td>
										                	<td class="main">{$orderslist[current].orders_status_name}</td>
										                	<td class="main" align="right" nowrap>{$orderslist[current].order_total}</td>
										                	<td class="main" align="right"><a href="{$orderslist[current].order_history_info_href}">{$small_view_img}</a></td>
										                	{if $orderslist[current].payment_button}
										                		<td class="main" align="right" onclick="event.cancelBubble = true;">
										                			{$orderslist[current].paypalform}
										                				{$paypal_logo}
										                				{$orderslist[current].payment_button}
										                			</div></form>
										                			</span><div style='clear: both;'>
										                		</td>
										                	{/if}
										               	</tr>
										       			{/section}
                									</table>
                								</td> 
                								<td>{$pixel_trans_10_1}</td> 
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				{/if}
      				<tr>
        				<td>        				
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td class="main"><b>{$smarty.const.MY_ACCOUNT_TITLE}</b></td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" >
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
								                <td width="60">{$account_personal_img}</td>
								                <td width="10">{$pixel_trans_10_1}</td>
								                <td>
								                	<table border="0" width="100%" cellspacing="0" cellpadding="2">
			                  							<tr>
			                    							<td class="main">{$arrow_green_img} <a href="{$account_edit_href}">{$smarty.const.MY_ACCOUNT_INFORMATION}</a></td>
			                  							</tr>
			                  							<tr>
			                    							<td class="main">{$arrow_green_img} <a href="{$address_book_href}">{$smarty.const.MY_ACCOUNT_ADDRESS_BOOK}</a></td>
			                  							</tr>
			                  							<tr>
			                    							<td class="main">{$arrow_green_img} <a href="{$account_password_href}">{$smarty.const.MY_ACCOUNT_PASSWORD}</a></td>
			                  							</tr>
			                						</table>
                								</td>
                								<td width="10" align="right">{$pixel_trans_10_1}</td>
              								</tr>
			            				</table>
			            			</td>
			          			</tr>
			        		</table>
			        	</td>
			      	</tr>
			      	<tr>
			        	<td>{$pixel_trans_100_10}</td>
			      	</tr>
					<tr>
			        	<td>
			        		<table border="0" width="100%" cellspacing="0" cellpadding="2">
			          			<tr>
			            			<td class="main"><b>{$smarty.const.MY_ORDERS_TITLE}</b></td>
			          			</tr>
			        		</table>
			        	</td>
			      	</tr>
					<tr>
						<td>
				        	<table border="0" width="100%" cellspacing="1" cellpadding="2" >
				          		<tr>
				            		<td>
				            			<table border="0" width="100%" cellspacing="0" cellpadding="2">
				              				<tr>
								                <td width="10">{$pixel_trans_10_1}</td>
								                <td width="60">{$account_orders_img}</td>
								                <td width="10">{$pixel_trans_10_1}</td>
								                <td>
								                	<table border="0" width="100%" cellspacing="0" cellpadding="2">
				                  						<tr>
				                    						<td class="main">{$arrow_green_img} <a href="{$account_hystory_href}">{$smarty.const.MY_ORDERS_VIEW}</a></td>
				                  						</tr>
				                					</table>
				                				</td>
				                				<td width="10" align="right">{$pixel_trans_10_1}</td>
				         					</tr>
				       					</table>
				       				</td>
			          			</tr>
			        		</table>
			        	</td>
			      	</tr>			      	
			      	{if $is_newsletters_block}
					<tr>
			        	<td>{$pixel_trans_100_10}</td>
			      	</tr>
			      	<tr>
				        <td>
				        	<table border="0" width="100%" cellspacing="0" cellpadding="2">
			          			<tr>
									<!-- Newsletter Titel -->
            						<td class="main" id ="account_newsletter_title"><b>{$smarty.const.EMAIL_NOTIFICATIONS_TITLE}</b></td>
			          			</tr>
			        		</table>
			        	</td>
			      	</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" >
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>                								
                								<td width="60" id="img_notifications">{$account_notifications_img}</td>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td>
                									<table border="0" width="100%" cellspacing="0" cellpadding="2">
                  										<tr>
                  											<!-- Newsletter Link -->                    										
                    										<td id="account_newsletter" class="main">{$arrow_green_img} <a href="{$account_newsletters_href}">{$smarty.const.EMAIL_NOTIFICATIONS_NEWSLETTERS}</a></td>
                  										</tr>
                									</table>
                								</td>
                								<td width="10" align="right">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
			      	{/if}
			      	{if $is_use_points_system}
						<tr>
							<td>{$pixel_trans_100_10}</td>
						</tr>
						<tr>
							<td>
								<table border="0" width="100%" cellspacing="0" cellpadding="2">
					          		<tr>
										<!-- Bonuspunkte -->
					            			<td class="main" id ="account_newsletter_title"><b>{$smarty.const.POINTS_ACCOUNT_TITLE}</b></td>
					          		</tr>
					        	</table>
					        </td>
						</tr>
						<tr>
					    	<td>
					    		<table border="0" width="100%" cellspacing="1" cellpadding="2" >
					          		<tr>
					            		<td>
					            			<table border="0" width="100%" cellspacing="0" cellpadding="2">
					              				<tr>
					                				<td width="10">{$pixel_trans_100_10}</td>
					                				<td width="60" id="img_notifications">{$account_points_img}</td>
					                				<td width="10">{$pixel_trans_10_1}</td>
					                				<td>
					                					<table border="0" width="100%" cellspacing="0" cellpadding="2">
					                  						<tr>
					                  							<!-- Bonuspunkte Link -->
					                    						<td id="account_bonuspunkte" class="main">{$arrow_green_img} <a href="{$my_points_url}">{$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS}</a></td>
					                  						</tr>
					                  						<tr>
					                    						<td class="main">{$arrow_green_img} <a href="{$my_points_url_umpf}">{$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS_COUPON}</a></td>
					                  						</tr>					
					                					</table>
					                				</td>
					                				<td width="10" align="right">{$pixel_trans_10_1}</td>
					              				</tr>
					            			</table>
					            		</td>
					          		</tr>
					        	</table>
					        </td>
						</tr>			      		
			      	{/if}			      		      		
	      		</table>
	        </td>
		</tr>	
	</table>
    
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}