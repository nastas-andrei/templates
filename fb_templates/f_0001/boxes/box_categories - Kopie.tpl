{if $title == "SETLEFTCAT"}
  {foreach from=$arr_categorybox.items key=key item=category}
    {assign var=tmpPOSL value=$category.catid|strpos:'_'}
    {if $tmpPOSL == 0 && $category.level == "0_on"}
      {assign var=masterID value=$category.catid}
      {assign var=showit value=true}
    {/if}
  {/foreach}
  <div class="filterBOX">
    <ul id="catLEFTBox" class="filterBOXitems">
    {assign var=ulgesetz value=0}
    {foreach from=$arr_categorybox.items key=key item=category name=catlistL}
    	{if $category.level !="0_on"}
          {assign var=tmpPOSL value=$category.catid|strpos:'_'}
          {if $category.catid|regex_replace:'/_.*/':'' == $masterID}
          	<li class="filterITEM_{$category.level} fCAT"><a href="{$category.link}"><span>{$category.name}</span></a></li>
          {/if}
      {/if}
    {/foreach}
    </ul>
	</div>
{else}
  <ul id="catTOPBox">
    {* <!-- <li class="{if $SCRIPT_NAME == "/index.php" && $cPath == ""}catLevel_home_on{else}catLevel_home{/if}"><a href="{$domain}"><span></span></a></li> --> *}

  {assign var=colcat_li_width value=180} {* <!-- // Breite der <li>'s --> *}
  {assign var=colcat_li_margin value=10} {* <!-- // Breite Margin rechts der <li>'s --> *}
  {assign var=count_ppr value=25} {* <!-- // Anzahl der produkte einer Spalte -1 --> *}

  {assign var=ulgesetz value=0}
  {assign var=popeye value=0}
  {assign var=popeyefirst value=0}
  {foreach from=$arr_categorybox.items key=key item=category name=catlist}
  {assign var=tmpPOS value=$category.catid|strpos:'_'}
  
  {if $category.level gt 0}
  	{assign var=popeye value=$popeye+1}
  {/if}
    {if $tmpPOS == 0}
      {if $ulgesetzt == 1}
      
       </ul>
       {if $popeye <= $count_ppr}
        </td></tr></table>
         <div style="clear:both; width:{$colcat_li_width}px!important;">&nbsp;</div>
        {elseif $popeye <= $count_ppr * 2}
         </td></tr></table>
         <div class="vert_nav_width" style="clear:both; width:{$colcat_li_width*2+$colcat_li_margin*2}px!important;">&nbsp;</div>
        {else}
         </td></tr></table>
         <div class="vert_nav_width" style="clear:both; width:{$colcat_li_width*3+$colcat_li_margin*3}px!important;">&nbsp;</div> {* <!-- // Subtext if u want ;) --> *} 
       {/if}

      </li>
       
         </ul>
        </li>
      {/if}
      {assign var=popeye value=0}
      <li class="catLevel_{$category.level}"><a href="{$category.link}"><span>{$category.name}</span></a>
     
      {if $ulgesetzt == 1}
        {if $category.havesub}
          <ul class="shadowbox"><li>
            <table cellpadding="0" cellspacing="0" border="0">
              <tr><td class="nav_trenner first_nav_row" style="width:{$colcat_li_width}px">
               <ul style="margin:0px {$colcat_li_margin}px 0px 0px;" class="vert_nav_styles">
        {/if}	
        {assign var=ulgesetz value=0}
      {/if}

    {else}
      {if $ulgesetzt == 0} 
        <ul class="shadowbox"><li>
        	<table cellpadding="0" cellspacing="0" border="0">
        		<tr><td class="nav_trenner first_nav_row" style="width:{$colcat_li_width}px">
        			<ul style="margin:0px {$colcat_li_margin}px 0px 0px;" class="vert_nav_styles">
        {assign var=ulgesetzt value=1}
      {/if}
 

       {if $popeye % $count_ppr == 0}
         </ul></td><td class="nav_trenner" style="width:{$colcat_li_width}px">
         <ul style="margin:0px {$colcat_li_margin}px 0px 0px;" class="vert_nav_styles">
       {else}
    
    
       <li class="catLevel_{$category.level}" style="width:{$colcat_li_width}px">
          <a href="{$category.link}">
          	<span>{$category.name}</span>
          </a>
       </li>
       {/if}
    {/if}

     
    {if $smarty.foreach.catlist.last}
     
     </ul>
        {if $popeye <= $count_ppr}
        </td></tr></table>
         <div style="clear:both; width:{$colcat_li_width}px!important;">&nbsp;</div>
        {elseif $popeye <= $count_ppr * 2}
        </td></tr></table>
         <div class="vert_nav_width" style="clear:both; width:{$colcat_li_width*2+$colcat_li_margin*2+2}px!important;">&nbsp;</div>
        {else}
        </td></tr></table>
         <div class="vert_nav_width" style="clear:both; width:{$colcat_li_width*3+$colcat_li_margin*3+3}px!important;">&nbsp;</div> {* <!-- // Subtext if u want ;) --> *} 
       {/if}
     
     </li>
       </ul>
     </li>
    {/if}
    


    
  {/foreach}
  </ul>
  <div class="clearer"></div>
{/if}