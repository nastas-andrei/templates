{if $arr_languagebox}  
	<span>{$smarty.const.BOX_HEADING_LANGUAGES} </span>
	<a id="sellanguage" data-toggle="dropdown" href="#" style="background:url('{$arr_languagebox.language.image}') no-repeat 5px center; padding-left:35px;">{$arr_languagebox.title}</a>
    <ul class="dropdown-menu select-language" role="menu" aria-labelledby="sellanguage">
    	{foreach from=$arr_languagebox.items key=key item=language}	
			{if ($languageview neq $language.directory)}		
				<li><a href="{$language.link}" style="background:url('{$language.image}') no-repeat 5px center; padding-left:40px;">{$language.name}</a></li>
			{/if}
		{/foreach}
    </ul>
 {/if}
