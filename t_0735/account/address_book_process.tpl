<div class="container accountMarginBottom">
<a  href="{$addressbook_href}" class="btn btn-default" style="margin-bottom:20px">&larr;&nbsp;&nbsp; {$smarty.const.IMAGE_BUTTON_BACK}</a>
{if !$is_delete}
	
	{$addressbook_edit_form}

{/if}
	 
{if $is_addressbook_message}
	{$addressbook_message}
{/if}

{if $is_delete}
	{$smarty.const.DELETE_ADDRESS_TITLE}
	{$smarty.const.DELETE_ADDRESS_DESCRIPTION}
    {$smarty.const.SELECTED_ADDRESS}
    {$delete_address_label}
    <a href="{$addressbook_href}" >{$smarty.const.IMAGE_BUTTON_BACK}</a>
	<a href="{$addressbook_delete_href}" class="btn">{$smarty.const.IMAGE_BUTTON_DELETE}</a>	
{else}
	<div class="row">
		{$ADDRESS_BOOK_DETAILS_HTML}
	</div>

	{if $is_edit}
		<div class="col-xs-6">			
			{$edit_hidden_fields}		
			<button type="submit" class="btn btn-primary pull-right">
				<i class="fa fa-check"></i>&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_UPDATE}
			</button>	
		</div>
		
	{else}		
		<div class="col-xs-6">			
			{$action_hidden_field}
			<button type="submit" class="btn btn-primary pull-right" >
				<i class="fa fa-check"></i>&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_CONTINUE}
			</button>
		</div>
	{/if}
{/if}

{if !$is_delete}
	</form>
{/if}
</div>
