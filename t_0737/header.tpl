<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{$templatepath}lib/css/jquery.bxslider.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/styles.css" media="all"/>
    <link href="{$templatepath}lib/css/base.css" rel="stylesheet">
    <link href="{$templatepath}lib/css/dresscode.css" rel="stylesheet">
    <link href="{$templatepath}custom.css" rel="stylesheet">
    <link href="{$templatepath}lib/css/cloud-zoom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/responsive_styles.css" media="all"/>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="includes/general.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{$templatepath}lib/bootstrap.min.js"></script>

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/cloud-zoom.1.0.2.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/categories.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/custom.js"></script>

    <script type="text/javascript" src="{$templatepath}lib/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/jquery.selectbox-0.2.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/responsiveslides.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/jquery-ui.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/jquery.elastislide.js"></script>

    {literal}
        <style>
            #search input[type="submit"], #search_dresscode input[type="submit"],
            #footer_button, .carousel-nav a:hover, ul.megamenu a:hover, ul.megamenu a.content-link.mm-item-link-hover, #nav li.over a, #nav a:hover, .es-nav span,
            #shopping_cart_mini .button, .newsletter_footer_submit button, .products-list .product-shop button.button, .cart-table button.btn-continue, .cart-table button.btn-empty, .cart-table button.btn-update, .product-view .add-to-cart button.button, .box-reviews .form-add button.button, .cart .checkout-types button.btn-checkout, #shipping-zip-form .buttons-set button.button, #discount-coupon-form .discount-form .buttons-set button.button, .totals .checkout-types button.btn-checkout, #login-form .col2-set button.button .block-poll .actions button.button {
                background-color: #d80000
            }

        </style>
    {/literal}
</head>

<body class="cms-index-index">
<div class="wrapper">
    <div class="page">
        <div id="header">
            <div class="container padding-1">
                <div id="logo" class="five columns">
                    <h1 class="logo">
                        {$cataloglogo}
                    </h1>
                </div>
                <div id="top_link" class="eleven columns">
                    <div class="row padding-2">
                        <div class="phone">{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</div>
                        <div class="language">
                            {include file="boxes/box_languages.tpl"}
                        </div>
                        <div class="shopping_cart">
                            <div class="shopping_cart_b">
                                {include file="boxes/box_shopping_cart.tpl"}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="nav_first">
                            <div id="nav_block_head">
                                <div class="nav_block_head_field">
                                    Service Tools
                                </div>
                                <span class="nav_block_head_button">&nbsp;</span>

                                <div class="clear"></div>
                            </div>
                            <div class="nav_block_dropdown">
                                <ul class="links">
                                    {include file="boxes/box_loginbox.tpl"}
                                </ul>
                            </div>
                        </div>
                        <div class="select_wrapper" id="select_top">
                            {include file="boxes/box_search.tpl"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav-container">
            <div class="container">
                <div class="sixteen columns">
                    <div id="menu_block_head">
                        <div class="nav_block_head_field">
                            Select category...
                        </div>
                        <span class="menu_block_head_button">&nbsp;</span>

                        <div class="clear"></div>
                    </div>
                    {*Ajax categories*}
                    <div id="categories-box"></div>
                </div>
            </div>
        </div>
    </div>
