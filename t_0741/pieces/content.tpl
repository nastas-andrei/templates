{if $is_default}
    {*    {if $smarty.const.TEXT_DEFINE_MAIN != ""}
            <section class="sliderwrapper clearfix">
                <div class="tp-banner-container">
                    <div class="tp-banner">
                        {$user_html}
                    </div>
                </div>
            </section>
        {/if}*}
    {if $slider_images != ""}
        <section class="sliderwrapper clearfix">
            <div class="tp-banner-container">
                <div class="tp-banner">
                    <ul>
                        {foreach from=$slider_images item=slider}
                            <li class="bluebg" data-transition="fadetobottomfadefromtop" data-slotamount="7" data-masterspeed="1500">
                                <!-- MAIN IMAGE -->
                                <!-- LAYERS -->

                                <!-- LAYER NR. 2 -->
                                <div class="tp-caption customin customout"
                                     data-x="right" data-hoffset="130"
                                     data-y="center" data-voffset="0"
                                     data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="800"
                                     data-start="700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="500"
                                     data-endeasing="Power4.easeIn"
                                     style="z-index: 3"><img src="{$slider.image|replace:'.jpg':'.png'}" alt="{$slider.title}">
                                </div>

                                <!-- LAYER NR. 3 -->
                                <div class="tp-caption pacifico4 skewfromleft customout"
                                     data-x="left" data-hoffset="10"
                                     data-y="200"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="800"
                                     data-start="1500"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 6">{$slider.title}
                                </div>

                                <!-- LAYER NR. 5 -->
                                <div class="tp-caption minidesc3 skewfromleft customout"
                                     data-x="left" data-hoffset="20"
                                     data-y="265"
                                     data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                     data-speed="800"
                                     data-start="1700"
                                     data-easing="Power4.easeOut"
                                     data-endspeed="300"
                                     data-endeasing="Power1.easeIn"
                                     data-captionhidden="on"
                                     style="z-index: 6"><p>{$slider.description}</p>
                                </div>
                                {if $slider.button_title}
                                    <!-- LAYER NR. 6 -->
                                    <div class="tp-caption skewfromleft customout"
                                         data-x="left" data-hoffset="30"
                                         data-y="355"
                                         data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="800"
                                         data-start="2100"
                                         data-easing="Power4.easeOut"
                                         data-endspeed="300"
                                         data-endeasing="Power1.easeIn"
                                         data-captionhidden="on"
                                         style="z-index: 6"><a href="{$slider.link}" class="jtbtn-big1" title="">{$slider.button_title}</a>
                                    </div>
                                {/if}
                            </li>
                            {*<li class="yellowbg" data-transition="fadetobottomfadefromtop" data-slotamount="7" data-masterspeed="1500" >*}
                            {*<!-- MAIN IMAGE -->*}

                            {*<!-- LAYER NR. 3 -->*}
                            {*<div class="tp-caption pacifico4 skewfromleft customout"*}
                            {*data-x="center" data-hoffset="0"*}
                            {*data-y="100"*}
                            {*data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"*}
                            {*data-speed="800"*}
                            {*data-start="700"*}
                            {*data-easing="Power4.easeOut"*}
                            {*data-endspeed="300"*}
                            {*data-endeasing="Power1.easeIn"*}
                            {*data-captionhidden="on"*}
                            {*style="z-index: 6">{$slider.title}*}
                            {*</div>*}

                            {*<!-- LAYER NR. 2 -->*}
                            {*<div class="tp-caption customin customout"*}
                            {*data-x="center" data-hoffset="0"*}
                            {*data-y="center" data-voffset="0"*}
                            {*data-customin="x:50;y:150;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.5;scaleY:0.5;skewX:0;skewY:0;opacity:0;transformPerspective:0;transformOrigin:50% 50%;"*}
                            {*data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"*}
                            {*data-speed="800"*}
                            {*data-start="1000"*}
                            {*data-easing="Power4.easeOut"*}
                            {*data-endspeed="500"*}
                            {*data-endeasing="Power4.easeIn"*}
                            {*style="z-index: 3"><img src="{$slider.image}" alt="{$slider.title}">*}
                            {*</div>*}

                            {*<!-- LAYER NR. 6 -->*}
                            {*<div class="tp-caption skewfromleft customout"*}
                            {*data-x="440"*}
                            {*data-y="455"*}
                            {*data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"*}
                            {*data-speed="800"*}
                            {*data-start="1200"*}
                            {*data-easing="Power4.easeOut"*}
                            {*data-endspeed="300"*}
                            {*data-endeasing="Power1.easeIn"*}
                            {*data-captionhidden="on"*}
                            {*style="z-index: 6"><a href="{$slider.link}" class="jtbtn-big1" title="">{$sldier.button_title}</a>*}
                            {*</div>*}
                            {*</li>*}
                        {/foreach}
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </section>
    {/if}
    <section class="whitewrapper greybg bordertop clearfix">
        <div class="container">
            <div class="row">
                {$FILENAME_FEATURED_HTML}
            </div>
        </div>
    </section>
    <section class="whitewrapper bordertop clearfix">
        <div class="container">
            <div class="row">
                {include file="boxes/box_whats_new.tpl"}
                {$UPCOMING_PRODUCTS_HTML}
            </div>
        </div>
    </section>
{/if}
<section class="whitewrapper">
    {if $products_not_found_message}
        {$products_not_found_message}
    {/if}
    {if $is_nested}
        {if !empty($categories_html)}
            {$categories_html}
        {/if}
        {if isset($categorieslist)}

        {else}
            {$FILENAME_FEATURED_HTML}
        {/if}
        {if !empty($categories_html_footer)}
            {$categories_html_footer}
        {/if}
    {/if}
    {if $is_products == 1}
        {$smarty.const.HEADING_TITLE}
        {if $filterlistisnotempty}
            {$filter_form}
            {$smarty.const.TEXT_SHOW}
        {/if}
        {if !empty($categories_html)}
            {$categories_html}
        {/if}
        {if !empty($manufacturers_html_header)}
            {$manufacturers_html_header}
        {/if}
        {if !empty($manufacturers_html_header) || isset($categorieslist)}
            {if isset($categorieslist)}
                {if $categorieslist|@count > 0}
                {/if}
            {else}
                {if !empty($manufacturers_html_header)}
                {/if}
            {/if}
        {/if}
        {if !empty($PRODUCT_LISTING_HTML)}
            <div class="container">
                {$PRODUCT_LISTING_HTML}
                <div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    {include file="boxes/box_best_sellers.tpl"}
                </div>
            </div>
        {/if}
        {if !empty($manufacturers_id)}
            {$manufacturers_html_footer}
        {/if}
        {if !empty($categories_html_footer)}
            {$categories_html_footer}
        {/if}
    {else}
        {if $is_default}
            &nbsp;
        {else}
            <div class="row">
                <p class="block">{$tep_image_category}</p>

                <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
            </div>
        {/if}
    {/if}
</section>