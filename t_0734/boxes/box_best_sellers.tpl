<!-- Best Seller -->
<div class="col-lg-12 col-md-12 col-sm-6">
    <div class="no-padding">
        <span class="title">BEST SELLER</span>
    </div>
    <div class="hero-feature">
        <div class="thumbnail text-center">
            <a href="detail.html" class="link-p">
                <img src="http://21075.dev01.hotdigital.org/images/user/21075/produkte/0_7_img.jpg" alt="">
            </a>

            <div class="caption prod-caption">
                <h4><a href="#">Gothic black corset</a></h4>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, minima!</p>

                <p>

                <div class="btn-group">
                    <a href="#" class="btn btn-default">$ 528.96</a>
                    <a href="#" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
                </div>
                </p>
            </div>
        </div>
    </div>
    <div class="hero-feature hidden-sm">
        <div class="thumbnail text-center">
            <a href="#" class="link-p">
                <img src="http://21075.dev01.hotdigital.org/images/user/21075/produkte/0_6_img.jpg" alt="">
            </a>

            <div class="caption prod-caption">
                <h4><a href="detail.html">Pink corset</a></h4>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut, minima!</p>

                <p>

                <div class="btn-group">
                    <a href="#" class="btn btn-default">$ 924.25</a>
                    <a href="#" class="btn btn-primary">
                        <i class="fa fa-shopping-cart"></i>
                    </a>
                </div>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- End Best Seller -->
<!-- {if $arr_bestsellersbox}
<div class="box box-best-sellers">
  <div class="box-heading">
  <h3 class="box-title">{$arr_bestsellersbox.title}</h3>  
</div>
  <div class="products-media-gallery">
  {foreach from=$arr_bestsellersbox.products key=i item=product name=previewitems}
    
  <div class="media">
    <a class="pull-left" href="{$product.link}">
                  {if $product.image} 
                    {php}
                       {$product = $this->get_template_vars('product');}
                       {$pattern_src = '/src="([^"]*)"/';}          
                       {preg_match($pattern_src, $product['image'], $matches);}
                       {$src = $matches[1];}
                       {$this->assign('src', $src);}          
                    {/php}
                  {else}
                    {assign var=src value="http://placehold.it/400x300&text=No%20image"}
                  {/if}        
        <div class="media-object image-canvas" style="width:64px;height:64px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
    </a>
    <div class="media-body">
        <a href="{$product.link}" class="product-title-link media-heading">{$product.name}</a>
        {if $product.preisalt > 0}
          <span class="product-old-price">{$product.preisalt}</span>
          <span class="product-new-price">{$product.preisneu}</span>
        {/if}    
    </div>

   </div>             
  {/foreach}
  </div>
</div>
{/if} -->