{if $arr_modul_productlisting.products}  
  {foreach from=$arr_modul_productlisting.products key=key item=product name=modpp}
        <li class="span3">  
              <div class="thumbnail">
       
                  {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                  {/if}

                  
                    <a href="{$product.link}">                      
                      <div class="img-canvas" style="width:210px;height:168px;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
                    </a>
                  
        
                     <div class="caption">       
                  
                    <a href="{$product.link}" class="product-title">
                      {$product.name|truncate:150:"...":true}
                    </a>
                     
                    <p class="product-price"><i class="icon-eur"></i>                    
                      {if $product.oldprice  > 0}
                        <span class="product-old-price">{$product.oldprice}</span>
                       {/if}   
                        <span class="product-new-price">{$product.newprice|regex_replace:"/[()]/":""}</span>  
                        <span class="product-base-price" style="display:block">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                    </p>
                                                                       
                    </div>

                </div>
        </li>
  {/foreach}
{/if}