{if $boxes_content.$key}
    <div class="box-html-box col-lg-12 col-md-12  col-sm-12 col-xs-12">
        {if $boxes_content.$key.boxes_title}
            <div class="page-head">
                <div class="page-title">
                    <h2>{$boxes_content.$key.boxes_title}</h2>
                </div>
            </div>
        {/if}

        <div class="box-content clearfix">
            <ul>
                {$boxes_content.$key.boxes_content}
            </ul>
        </div>
    </div>
{/if}