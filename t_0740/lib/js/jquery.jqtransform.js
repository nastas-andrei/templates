(function($){
    var defaultOptions = {preloadImg:true};
    var jqTransformImgPreloaded = false;
    var jqTransformGetLabel = function(objfield){
        var selfForm = $(objfield.get(0).form);
        var oLabel = objfield.next();
        if(!oLabel.is('label')) {
            oLabel = objfield.prev();
            if(oLabel.is('label')){
                var inputname = objfield.attr('id');
                if(inputname){
                    oLabel = selfForm.find('label[for="'+inputname+'"]');
                }
            }
        }
        if(oLabel.is('label')){return oLabel.css('cursor','pointer');}
        return false;
    };
    /* Hide all open selects */
    var jqTransformHideSelect = function(oTarget){
        var ulVisible = $('.jqTransformSelectWrapper ul:visible');
        ulVisible.each(function(){
            var oSelect = $(this).parents(".jqTransformSelectWrapper:first").find("select").get(0);
            //do not hide if click on the label object associated to the select
            if( !(oTarget && oSelect.oLabel && oSelect.oLabel.get(0) == oTarget.get(0)) ){$(this).hide();}
        });
    };
    var jqTransformCheckExternalClick = function(event) {
        if ($(event.target).parents('.jqTransformSelectWrapper').length === 0) { jqTransformHideSelect($(event.target)); }
    };
    var jqTransformAddDocumentListener = function (){
        $(document).mousedown(jqTransformCheckExternalClick);
    };
    var jqTransformReset = function(f){
        var sel;
        $('.jqTransformSelectWrapper select', f).each(function(){sel = (this.selectedIndex<0) ? 0 : this.selectedIndex; $('ul', $(this).parent()).each(function(){$('a:eq('+ sel +')', this).click();});});
        $('a.jqTransformCheckbox, a.jqTransformRadio', f).removeClass('jqTransformChecked');
        $('input:checkbox, input:radio', f).each(function(){if(this.checked){$('a', $(this).parent()).addClass('jqTransformChecked');}});
    };
    $.fn.jqTransSelect = function(){
        return this.each(function(index){
            var $select = $(this);
            if($select.hasClass('jqTransformHidden')) {return;}
            if($select.attr('multiple')) {return;}
            var oLabel  =  jqTransformGetLabel($select);
            var $wrapper = $select
                    .addClass('jqTransformHidden')
                    .wrap('<div class="drop-box"></div>')
                    .parent()
                    .css({zIndex: 10-index, height: 35})
                ;
            $wrapper.prepend('<a href="#" class="dropdown-toggles"></a>' +
                '<ul class="box" role="menu" aria-labelledby="dLabel"></ul>');
            var $ul = $('ul', $wrapper).css('width',$select.width()).hide();
            $('option', this).each(function(i){
                var oLi = $('<li><a role="menuitem" href="#" index="'+ i +'">'+ $(this).html() +'</a></li>');
                $ul.append(oLi);
            });
            $ul.find('a').click(function(){
                $('a.selected', $wrapper).removeClass('selected');
                $(this).addClass('selected');
                if ($select[0].selectedIndex != $(this).attr('index') && $select[0].onchange) { $select[0].selectedIndex = $(this).attr('index'); $select[0].onchange(); }
                $select[0].selectedIndex = $(this).attr('index');
                $('a:eq(0)', $wrapper).html($(this).html());
                $ul.hide();
                return false;
            });
            $('a:eq('+ this.selectedIndex +')', $ul).click();
            oLabel && oLabel.click(function(){$("a.dropdown-toggles",$wrapper).trigger('click');});
            this.oLabel = oLabel;
            var oLinkOpen = $('a.dropdown-toggles', $wrapper).click(function(){
                        if( $ul.css('display') == 'none' ) {jqTransformHideSelect();}
                        if($select.attr('disabled')){return false;}
                        $ul.slideToggle('fast', function(){
                            var offSet = ($('a.selected', $ul).offset().top - $ul.offset().top);
                            $ul.animate({scrollTop: offSet});
                        });
                        return false;
                    })
                ;
            $ul.css({display:'block',visibility:'hidden'});
            var iSelectHeight = ($('li',$ul).length)*($('li:first',$ul).height());
            (iSelectHeight < $ul.height()) && $ul.css({height:iSelectHeight,'overflow':'hidden'});
            $ul.css({display:'none',visibility:'visible'});
        });
    };
    $.fn.jqTransform = function(options){
        var opt = $.extend({},defaultOptions,options);
        return this.each(function(){
            var selfForm = $(this);
            if(selfForm.hasClass('jqtransformdone')) {return;}
            selfForm.addClass('jqtransformdone');
            if( $('select', this).jqTransSelect().length > 0 ){jqTransformAddDocumentListener();}
            selfForm.bind('reset',function(){var action = function(){jqTransformReset(this);}; window.setTimeout(action, 10);});
        });
    };
})(jQuery);
