{$smarty.const.BEFORE_CONTENT}

{if !$smarty.const.PH_INSIDE}	
	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
     	<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}
		<tr>
        	<td class="cont_abst accountCNT">
<div class="accountBox">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td class="main"><b>{$smarty.const.HEADING_ORDER_NUMBER|replace:" %s":": $order_id"}</b> {* <small>({$order_info.orders_status})</small>*}</td>
            						<td class="smallText" align="right">{$smarty.const.HEADING_ORDER_DATE} {$order_info.date_purchased|date_format:"%d.%m.%Y"}</td></tr>
                                    <tr>
            						<td class="smallText" colspan="2" align="right" style="height:10px;">{*$smarty.const.HEADING_ORDER_TOTAL} {$order_info.total*}</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td width="50%" valign="top">
            							<table border="0" cellspacing="0" cellpadding="2">
              								<tr>
                								<td class="main"><b>{$smarty.const.HEADING_DELIVERY_ADDRESS}</b></td>
              								</tr>
              								<tr>
                								<td class="main">
                                                {*$delivery_format_id*}

                                                {if $order_customer_delivery.name}{$order_customer_delivery.name}<br />{/if}
                                                {if $order_customer_delivery.company}{$order_customer_delivery.company}<br />{/if}
                                                {if $order_customer_delivery.street_address}{$order_customer_delivery.street_address}<br />{/if}
                                                {if $order_customer_delivery.suburb}{$order_customer_delivery.suburb}<br />{/if}
                                                {if $order_customer_delivery.postcode}
                                                	{$order_customer_delivery.postcode}{$order_customer_delivery.city}<br />
                                                {/if}
                                                {if $order_customer_delivery.state}{$order_customer_delivery.state}<br />{/if}
                                                {if $order_customer_delivery.country}{$order_customer_delivery.country}<br />{/if}

                                            	</td>
              								</tr>
											{if is_shipping_method}
              								<tr>
                								<td class="main" style="padding-top:15px;"><b>{$smarty.const.HEADING_SHIPPING_METHOD}</b></td>
              								</tr>
              								<tr>
                								<td class="main">{$order_info.shipping_method}</td>
              								</tr>
											{/if}
            							</table>
            						</td>

            						
                                    <td width="50%" valign="top">
                                            <table border="0" cellspacing="0" cellpadding="2">
                                                <tr>
                                                    <td class="main"><b>{$smarty.const.HEADING_BILLING_ADDRESS}</b></td>
                                                </tr>
                                                <tr>
                                                    <td class="main">
                                                    	{*$billing_format_id*}
                                                        
                                                {if $order_customer_billing.name}{$order_customer_billing.name}<br />{/if}
                                                {if $order_customer_billing.company}{$order_customer_billing.company}<br />{/if}
                                                {if $order_customer_billing.street_address}{$order_customer_billing.street_address}<br />{/if}
                                                {if $order_customer_billing.suburb}{$order_customer_billing.suburb}<br />{/if}
                                                {if $order_customer_billing.postcode}
                                                	{$order_customer_billing.postcode}{$order_customer_billing.city}<br />
                                                {/if}
                                                {if $order_customer_billing.state}{$order_customer_billing.state}<br />{/if}
                                                {if $order_customer_billing.country}{$order_customer_billing.country}<br />{/if}
                                                    
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="main" style="padding-top:15px;"><b>{$smarty.const.HEADING_PAYMENT_METHOD}</b></td>
                                                </tr>
                                                <tr> 
                                                    <td class="main">{$order_info.payment_method}</td>
                                                </tr>
                                            </table>
        							</td>
      				</tr>      				
      				<tr>
        				<td colspan="2">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td colspan="2" class="main" style="padding-top:20px;"><b>{$smarty.const.HEADING_BILLING_INFORMATION}</b></td>
      				</tr>
      				<tr>
        				<td colspan="2">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
                    
                    
                    <td width="100%" colspan="2" valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="0">
              								<tr>
                								<td>
                									<table border="0" width="100%" cellspacing="0" cellpadding="2">
														{if $is_multi_tax_groups}
                  										<tr>
                    										<td class="main" colspan="2"><b>{$smarty.const.HEADING_PRODUCTS}</b></td>
                    										<td class="smallText" align="right"><b>{$smarty.const.HEADING_TAX}</b></td>
                    										<td class="smallText" align="right"><b>{$smarty.const.HEADING_TOTAL}</b></td>
                  										</tr>
														{else}
                  										<tr>
                    										<td class="main" colspan="3"><b>{$smarty.const.HEADING_PRODUCTS}</b></td>
                  										</tr>                  										
														{/if}
														{section name=current loop=$order_products}
														<tr>
         													<td class="main" align="right" valign="top" width="30">{$order_products[current].qty}&nbsp;x</td>
         													<td class="main" valign="top">
         														{$order_products[current].name}
         														{if $order_products[current].attributes}
         															{assign var=attlist value=$order_products[current].attributes}
         															{foreach from=$attlist key=key item=attr name=attrlister}
																		<br>
         																<nobr>
         																	<small>&nbsp;<i> - {$attr.option}: {$attr.value}</i></small>
         																</nobr>         															

         															{/foreach}
         														{/if}
         														{section name=attr_current loop=$historyattributeslist}
         														<br>
         														<nobr>
         															<small>&nbsp;<i> - {$historyattributeslist[attr_current].option}: {$historyattributeslist[attr_current].value}</i></small>
         														</nobr>
         														{/section}
														    </td>
														    {if $is_multi_tax_groups}
														    	<td class="main" valign="top" align="right">{$order_products[current].tax}%</td>
														    {/if}
														    <td class="main" align="right" valign="top" style="border-bottom:1px solid;padding-bottom:10px;" nowrap>
														    	{* $order_products[current].final_price *}
                                                                {math equation="x * (y / 100 + 1)" x=$order_products[current].final_price y=$order_products[current].tax format="%.2f"} {$order_info.currency|replace:"EUR":"&euro;"}
														    </td>
														</tr>
														{/section}
                									</table>
                								</td>
              								</tr>
            							</table>
            						</td>
									</tr>
                                    <tr>
            						<td width="100%" valign="top" align="right" colspan="2">
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
            								{section name=current loop=$order_totals}
											<tr>
												<td class="main" align="right" width="100%">{$order_totals[current].title}</td>
												<td class="main" align="right" nowrap>{$order_totals[current].text}</td>
											</tr>
											{/section}
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td colspan="2">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td colspan="2"  style="padding-top:20px;" class="main"><b>{$smarty.const.HEADING_ORDER_HISTORY}</b></td>
      				</tr>
      				<tr>
        				<td colspan="2">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td colspan="2" style="padding-bottom:20px;">
        					
                            <table border="0" width="100%" cellspacing="2" cellpadding="2">
                                {foreach from=$orderhistorylist key=key item=history name=historylister}
                                    <tr>
                                        <td class="main" valign="top" nowrap width="70">{$history.date_added}</td>
                                        <td class="main" valign="top" width="70">{$history.orders_status_name}</td>
                                        <td class="main" valign="top">{$history.comments}</td>
                                    </tr>
                                {/foreach}
                            </table>

        				</td>
      				</tr>
      				{$DOWNLOADS_HTML}      				
      				<tr>
        				<td colspan="2">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td colspan="2" align="right">

                            {* <a href="{$account_history_href}">{$button_back_img_button}</a> *}
                            <a href="javascript:history.back()"><img src="{$imagepath}/buttons/{$language}/button_back.gif" /></a>
                                                
        				</td>
      				</tr>
        		</table>
          </div>
        	</td>
      	</tr>
	</table>

{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}