{if $arr_searchbox}
    {$arr_searchbox.form}
        <div class="form-search">
            <input type="text" class="input-text" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}"
                   value="{$arr_searchbox.inputtextfield}"/>
            <button type="submit" title="Search entire store here..." class="button"></button>
            {$arr_searchbox.hidden_sessionid}
        </div>
    </form>
{/if}

