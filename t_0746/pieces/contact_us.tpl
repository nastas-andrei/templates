<section class="container">
    <div class="row content-div v_contact_form2 v_contact_form m_sm_bottom_20">
        <article class="col-xs-12 col-md-6">
            <h3>{$smarty.const.HEADING_TITLE}</h3>

            <div class="photoframe map_container">
                <iframe width="1170" height="450" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyASNmy5aLU4Gv3ExDUfmCIbmOpE6MgXzmg"></iframe>
            </div>
            <div class="row">
                <ul class="row v_drop m_top_20">
                    <li class="col-xs-6">
                        <section class="block-features">
                            <div><a href="#"><span><i class="fa fa-map-marker"></i></span></a>

                                <p class="txt-2">{$contact_data.shop_streat.value}, <br>
                                    {$contact_data.shop_city.value}, {$contact_data.shop_country.value}</p>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </li>
                    <li class="col-xs-6">
                        <section class="block-features">
                            <div><a href="#"><span><i class="fa fa-envelope"></i></span></a>

                                <p class="txt-2">
                                    <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a> <br>
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </li>
                    <div class="clearfix"></div>
                    <li class="col-xs-6 ms_p_bottom_10">
                        <section class="block-features">
                            <div><a href="#"><span><i class="fa fa-phone"></i></span></a>

                                <p class="txt-2">
                                    {$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
                                </p>
                            </div>
                            <div class="clearfix"></div>
                        </section>
                    </li>
                </ul>
            </div>
        </article>
        <article class="col-xs-12 col-md-6">
            {$contact_us_form|replace:'name="contact_us"':'name="contact_us" class="v_form"'}
            {if $is_action_success}

            {$smarty.const.TEXT_SUCCESS}
            <a href="{$button_continue_url}">{$button_continue_img}</a>
            {else}
            <ul>
                <li class="clearfix">
                    <ul class="v_h_fild">
                        <li>
                            <div class="full_width">
                                <label>{$smarty.const.ENTRY_NAME}</label>
                                {$name_input|replace:'class="general_inputs"':'class="form-control"'}
                            </div>
                        </li>
                        <li>
                            <div class="full_width">
                                <label>{$smarty.const.ENTRY_EMAIL}</label>
                                {$email_input|replace:'class="general_inputs"':'class="form-control"'}
                            </div>
                        </li>
                        <li>
                            <div class="full_width">
                                <label>{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>
                                {$anliegen_pull_down|replace:'name="anliegen"':'name="anliegen" class="form-control"'}
                            </div>
                        </li>
                        <li>
                            <div class="full_width" id="artikel_title" style="display: none">
                                <label id="artikel">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</label>
                                {$artikel_input|replace:'general_inputs':'form-control'}
                            </div>
                        </li>
                        <li>
                            <div class="full_width m_bottom_20" id="bestellung_title" style="display: none">
                                <label id="bestellung">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>
                                {$bestellung_input|replace:'general_inputs':'form-control'}
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="clearfix">
                    {$enquiry_textarea|replace:'class="textarea"':'class="form-control" id="message"'}
                </li>
                <li class="clearfix">
                    <div class="fleft third_column">
                        {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                            <label>
                                Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                            </label>
                            <select name="pf_antispam" id="pf_antispam" class="form-control">
                                <option value="1" selected="selected">Ja</option>
                                <option value="2">Nein</option>
                            </select>
                            <script language="javascript" type="text/javascript">

                                var sel = document.getElementById('pf_antispam');
                                var opt = new Option('Nein', 2, true, true);
                                sel.options[sel.length] = opt;
                                sel.selectedIndex = 1;

                            </script>
                            <noscript>
                                <label>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</label>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </noscript>
                        {/if}
                    </div>
                </li>
                <li class="clearfix m_top_20">
                    <button type="submit" class="button-send btn-small">{$smarty.const.CONTACT_BOTTOM_TEXT}</button>
                </li>
            </ul>
            {/if}
        </article>
    </div>
</section>