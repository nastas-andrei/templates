<div class="clearfix"></div>
<section class="blog-wrapper">
    <div class="container">
    <iframe width="100%" height="350" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyBzBxYxCJDus32apdAMyDlkvNX3qNYgOsE">
    </iframe>
        <div class="contact_form">
            {if $contact_message_exists}
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {$contact_message}
                </div>
            {/if}
            {$contact_us_form}
            {if $is_action_success}
                {$smarty.const.TEXT_SUCCESS}
                <button onclick="setLocation('{$button_continue_url}')" type="button" class="btn btn-primary btn-icon">
                    {$IMAGE_BUTTON_CONTINUE_SHOPPING}
                </button>
            {else}
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <strong>{$smarty.const.ENTRY_NAME}</strong>
                    {$name_input}
                    <strong>{$smarty.const.ENTRY_EMAIL}</strong>
                    {$email_input}
                    <strong>{$smarty.const.ENTRY_CONTACT_SUBJECT}</strong>
                    <div class="input-box">
                        <div style="position: relative;z-index: 1;margin-right: -4px">{$anliegen_pull_down}</div>
                    </div>
                    <div class="field" id="artikel" style="display: none">
                        <span id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</span>
                        <div class="clearfix">
                            {$artikel_input}
                        </div>
                    </div>
                    <div class="field" id="bestellung" style="display: none">
                        <span id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</span>
                        <div class="clearfix">
                            {$bestellung_input}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <strong>{$smarty.const.ENTRY_ENQUIRY}</strong>
                    {$enquiry_textarea}
                    <div class="buttons-set">
                        <button type="submit" class="btn btn-lg btn-primary pull-right">{$IMAGE_ANI_SEND_EMAIL}</button>
                    </div>
                    {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                        <div class="form-group">
                                    <span>
                                        Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                                    </span>
                            <select name="pf_antispam" id="pf_antispam">
                                <option value="1" selected="selected">Ja</option>
                                <option value="2">Nein</option>
                            </select>
                        </div>
                        <script language="javascript" type="text/javascript">
                            var sel = document.getElementById('pf_antispam');var opt = new Option('Nein',2,true,true);sel.options[sel.length] = opt;
                            sel.selectedIndex = 1;
                        </script>
                        <noscript>
                            <div class="antistalker">
                                <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </div>
                        </noscript>
                    {/if}
                </div>
            {/if}
            </form>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section id="one-parallax" class="parallax" style="background-image: url('{$templatepath}/images/parallax_06.jpg');" data-stellar-background-ratio="0.6" data-stellar-vertical-offset="20">
    <div class="overlay">
        <div class="container">
            <div class="row padding-top margin-top">
                <div class="contact_details">
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="text-center">
                            <div class="wow swing">
                                <div class="contact-icon">
                                    <a href="#" class=""> <i class="fa fa-map-marker fa-3x"></i> </a>
                                </div>
                                <p>{$contact_data.shop_streat.value}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="text-center">
                            <div class="wow swing">
                                <div class="contact-icon">
                                    <a href="#" class=""> <i class="fa fa-phone fa-3x"></i> </a>
                                </div>
                                <p>{$contact_data.shop_mobilephone.value}
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="text-center">
                            <div class="wow swing">
                                <div class="contact-icon">
                                    <a href="#" class=""> <i class="fa fa-envelope-o fa-3x"></i> </a>
                                </div>
                                <p><a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br><br>
        </div>
    </div>
</section>
