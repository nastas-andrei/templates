<section class="blog-wrapper">
    <div class="container">
        {if $arr_modul_productlisting.products|@count lt 1}
            <p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}
                <a href="{$arr_searchbox.advanced_search_link}" class="btn btn-primary btn-icon">{$arr_searchbox.advanced_search_text}</a>
            </p>
            <a href="javascript:history.go(-1)" class="btn btn-primary btn-icon">{$smarty.const.IMAGE_BUTTON_BACK}</a>
        {else}

        {if $is_search_message}
            <p class="text-center">{$search_message}</p>
        {/if}
        <div class="shop_wrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="row">
                <div class="shop-top">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <p class="amount">{$arr_modul_productlisting.count_products_info|strip_tags:false}</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="pull-right">
                            <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink"/>
                            <input type="hidden" value="{$max}" id="max" name="max"/>
                            <input type="hidden" value="{$standard_template}" id="standard_template"
                                   name="standard_template"/>
                            <select name="sortierung" id="sortierung" class="custom-select form-control"
                                    onchange="goHin(this)">
                                {foreach from=$arr_sort_new key=key item=sort}
                                    {if $sort.class == "p"}
                                        {if $sort_select eq $sort.id}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {else}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {/if}
                                    {else}
                                        {if $sort_select == $sort.id}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {else}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {/if}
                                    {/if}
                                {/foreach}
                            </select>
                        </div>
                        <div class="pull-right">
                            <div class="view-mode">
                                {if $standard_template == "liste"}
                                    <a href="{$templ_link}" title="Grid" class="box glyphicon grid"></a>
                                    <strong title="List" class="box glyphicon list"></strong>
                                {/if}
                                {if $standard_template == "galerie"}
                                    <strong title="Grid" class="box glyphicon grid"></strong>
                                    <a href="{$templ_link}" title="List" class="box glyphicon list"></a>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="product-list-container" class="">
                    <div class="col-md-12">
                        {$PRODUCT_LISTING_HTML}
                    </div>
                </div>
                {if $arr_modul_productlisting.pagination|@count > 1}
                <div class="pagination_wrapper">
                        <div class="pager text-center ">
                            <ol>
                                {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                                {if $page.active}
                                <li class="active current">
                                    <a>{$page.text}</a>
                                    {elseif $page.previous}
                                <li>
                                    <a class="previous" href="{$page.link}" title="{$page.title}">
                                        &laquo;
                                    </a>
                                    {elseif $page.next}
                                <li>
                                    <a class="next" href="{$page.link}" title="{$page.title}">
                                        &raquo;
                                    </a>
                                    {else}
                                <li>
                                    <a href="{$page.link}" title="{$page.title}">
                                        {$page.text}
                                    </a>
                                    {/if}
                                    {/foreach}
                                </li>
                            </ol>
                        </div>
                </div>
                {/if}
            </div>
        </div>
        <div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            {include file="boxes/box_best_sellers.tpl"}
        </div>
        {/if}
    </div>
</section>

