<link rel="stylesheet" type="text/css" href="templates/0_source/css/foxrate_shopmodule.css"/>
{$clink}
{if $FAVICON_LOGO && $FAVICON_LOGO != ''}
    <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
{/if}
{if $DESIGN_VARIANT != ""}
    <link rel="stylesheet" type="text/css" href="{$templatepath}stylesheet_{$DESIGN_VARIANT}.css"/>
{/if}
{if $news_atindex || $arr_newslistview || $arr_newsdetails}
    {if $DESIGN_VARIANT != ""}
        <link rel="stylesheet" type="text/css" href="{$templatepath}stylesheet_news_{$DESIGN_VARIANT}.css"/>
    {else}
        <link rel="stylesheet" type="text/css" href="{$templatepath}stylesheet_news.css">
    {/if}
{/if}
<link rel="stylesheet" type="text/css" href="templates/0_source/lib/tooltip/ToolTip.css">
<script type="text/javascript" src="includes/general.js"></script>
{* SHOPZILLA SETUP SCRIPT BEGIN *}
{if $TEMPLATE_USERDEFINED3 == "shopzilla" && $arr_modul_productlisting}
{literal}
    <script type="text/javascript">
        var pr_style_sheet = "http://cdn.shopzilla.com/aux/{/literal}{$TEMPLATE_USERDEFINED4}{literal}/css/express.css";
    </script>
    <script type="text/javascript"
            src="http://cdn.shopzilla.com/repos/{/literal}{$TEMPLATE_USERDEFINED4|regex_replace:'/\/(.*)$/':''}{literal}/pr/pwr/engine/js/full.js"></script>
{/literal}
{/if}
{* SHOPZILLA SETUP SCRIPT END *}
<!--<script type="text/javascript" src="includes/mootools/mootools-core.js"></script>-->
<script type="text/javascript" src="templates/0_source/lib/mootools-core-1.4.js"></script>
<!--<script type="text/javascript" src="includes/mootools/mootools-more.js"></script>-->
<script type="text/javascript" src="templates/0_source/lib/mootools-more-1.4.js"></script>
<script type="text/javascript" src="templates/0_source/lib/tooltip/ToolTip.js"></script>
<script type="text/javascript" src="templates/0_source/lib/foxrate_shopmodule_tooltip.js"></script>