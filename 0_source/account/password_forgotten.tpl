{$password_forgotten_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}                		
			{include file="pieces/page_title_section.tpl"}
		    {$smarty.const.BETWEEN_HL_CONTENT}
		    <table border="0" width="100%" cellspacing="0" cellpadding="0">
		    	<tr>
		        	<td class="pageContentTable">
		            	<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
            	<tr>
                	<td>
                    	{include file="pieces/page_title_section.tpl"}
                  	</td>
				</tr>
                {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}
		<tr>
			<td class="cont_abst">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" class="login_area">
        			{if $password_forgotten_stack_size}
            		<tr>
                		<td>{$password_forgotten_message}</td>
            		</tr>
            		<tr>
                		<td>{$pixel_trans_10}</td>
            		</tr>
        			{/if}        
      				<tr>
        				<td>
        					<table border="0" width="100%" height="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td>{$pixel_trans_10}</td>
              								</tr>
              								<tr>
                								<td class="main" colspan="2">{$smarty.const.TEXT_MAIN}</td>
              								</tr>
              								<tr>
                								<td>{$pixel_trans_10}</td>
              								</tr>
              								<tr>
                								<td class="main"><b>{$smarty.const.ENTRY_EMAIL_ADDRESS}</b> {$email_address_input}</td>
              								</tr>
              								<tr>
                								<td>{$pixel_trans_10}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_1}</td>
                								<td><a href="{$LOGIN_URL}">{$button_back_img}</a></td>
                								<td align="right">{$button_continue_img}</td>
                								<td width="10">{$pixel_trans_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
        		</table>
        	</td>
      	</tr>
    </table>

    {if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
    {/if}
    {$smarty.const.AFTER_CONTENT}

</form>    