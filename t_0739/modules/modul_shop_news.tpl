<div class="box box-news">
    <div class="box-heading">
        <h3 class="box-title">{$smarty.const.BOX_HEADING_SHOPNEWS}
            <small><a href="{$all_news_seo_link}" class="pull-right muted show-all-news-link">{$smarty.const.BOX_FOOTER_NEWS_NEXT}</a></small>
        </h3>
    </div>
    <div class="products-media-gallery">
        {section name=current loop=$newslist}
            <div class="media">
                {if $newslist[current].news_picture}
                    <a class="pull-left" href="{$newslist[current].news_seo_link}">
                        <div class="media-object image-canvas" style="width:64px;height:64px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$newslist[current].news_picture}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$newslist[current].news_picture}', sizingMethod='scale');"></div>
                    </a>
                {/if}
                <div class="media-body">
                    <a class="news-title-item" href="{$newslist[current].news_seo_link}" class="product-title-link media-heading">{$newslist[current].news_title}</a>
                    <p class="news-small-description"><small>{$newslist[current].news_teaser_short}</small></p>
                </div>
            </div>
        {/section}
    </div>
</div>
