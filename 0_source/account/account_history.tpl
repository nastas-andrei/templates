{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}	
	{include file="pieces/page_title_section.tpl"}
	{$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
   		<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
        	</td>
      	</tr>
		{$smarty.const.BETWEEN_HL_CONTENT}
 {/if}
 
	<tr>
		<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
      			<tr>
        			<td>
        			{if $orders_total > 0}
          				{section name=current loop=$historylist}          				
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
            				<tr>
              					<td class="main"><b>{$smarty.const.TEXT_ORDER_NUMBER}</b> {$historylist[current].orders_id}</td>              					
              					<td class="main" align="right"><b>{$smarty.const.TEXT_ORDER_STATUS}</b> {$historylist[current].orders_status_name}</td>
            				</tr>            				
          				</table>
          				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
            				<tr>
              					<td>
              						<table border="0" width="100%" cellspacing="2" cellpadding="4">
                						<tr>
                  							<td class="main" width="50%" valign="top">
                  								<b>{$smarty.const.TEXT_ORDER_DATE}</b> {$historylist[current].date_purchased} 
                  								<br>
                  								<b>{$historylist[current].order_type}</b> {$historylist[current].order_name}
                  							</td>
                  							<td class="main" width="30%" valign="top">
                  								<b>{$smarty.const.TEXT_ORDER_PRODUCTS}</b> {$historylist[current].count}
                  								<br>
                  								<b>{$smarty.const.TEXT_ORDER_COST}</b> {$historylist[current].order_total}
                  							</td>
                  							<td class="main" width="20%"><a href="{$historylist[current].account_history_info_href}">{$historylist[current].small_view_img_button}</a></td>
                						</tr>
              						</table>
              					</td>
            				</tr>
          				</table>
          				<table border="0" width="100%" cellspacing="0" cellpadding="2">
            				<tr>
              					<td>{$pixel_trans_1_10}</td>
            				</tr>
          				</table>
          				{/section}        			
        			{else}        			
          				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
            				<tr>
              					<td>
              						<table border="0" width="100%" cellspacing="2" cellpadding="4">
                						<tr>
                  							<td class="main">{$smarty.const.TEXT_NO_PURCHASES}</td>
                						</tr>
              						</table>
              					</td>
            				</tr>
          				</table>        			
        			{/if}
        			</td>
      			</tr>
      			{if $orders_total > 0}
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="smallText" valign="top">{$pager_count}</td>
            					<td class="smallText" align="right">{$smarty.const.TEXT_RESULT_PAGE} {$pager_links}</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			{/if}
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
              							<tr>
                							<td width="10">{$pixel_trans_10_1}</td>
                							<td><a href="{$account_href}">{$button_back_img}</a></td>
                							<td width="10">{$pixel_trans_10_1}</td>
              							</tr>
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
        	</table>
        </td>
	</tr>
</table>

{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>		
{/if}
{$smarty.const.AFTER_CONTENT}
