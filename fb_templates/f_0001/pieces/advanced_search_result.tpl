

{if $arr_modul_productlisting.products|@count lt 1}
<p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}</p>
<a href="javascript:history.go(-1)" class="btn">{$smarty.const.IMAGE_BUTTON_BACK}</a>
{/if}



{if $is_search_message}
 <p class="text-center">{$search_message}</p>
 {/if}

{$PRODUCT_LISTING_HTML}

