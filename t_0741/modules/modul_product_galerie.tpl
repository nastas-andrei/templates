{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort='+wert+'&max='+max;
            location.href = link;
        }
    </script>
{/literal}
    <div class="shop_wrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="row">
            <div class="shop-top">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <h4>{$arr_modul_productlisting.count_products_info|strip_tags:false}</h4>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="pull-right">
                        {if $manufacturer_select}
                            {$manufacturer_select}&nbsp;
                        {/if}
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="pull-right">
                        <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink"/>
                        <input type="hidden" value="{$max}" id="max" name="max"/>
                        <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template"/>
                        <select name="sortierung" id="sortierung" class="custom-select form-control" onchange="goHin(this)">
                            {foreach from=$arr_sort_new key=key item=sort}
                                {if $sort.class == "p"}
                                    {if $sort_select eq $sort.id}
                                        <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                    {else}
                                        <option value="{$sort.id}">{$sort.text}</option>
                                    {/if}
                                {else}
                                    {if $sort_select == $sort.id}
                                        <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                    {else}
                                        <option value="{$sort.id}">{$sort.text}</option>
                                    {/if}
                                {/if}
                            {/foreach}
                        </select>
                    </div>
                    <div class="pull-right">
                        <div class="view-mode">
                            {if $standard_template == "liste"}
                                <a href="{$templ_link}" title="Grid" class="glyphicon glyphicon-th-large"></a>
                                <span class="glyphicon glyphicon-list"></span>
                            {/if}
                            {if $standard_template == "galerie"}
                                <span class="glyphicon glyphicon-th-large"></span>
                                <a href="{$templ_link}" title="List" class="glyphicon glyphicon-list"></a>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div id="product-list-container">
                <div class="col-md-12">
                    <div id="product-grid-container" class="paddingtop clearfix text-center">
                        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
                            {if $product.image}
                                {php}
                                    $product = $this->get_template_vars('product');
                                    $pattern_src = '/src="([^"]*)"/';
                                    preg_match($pattern_src, $product['image'], $matches);
                                    $src = $matches[1];
                                    $this->assign('src', $src);
                                {/php}
                            {else}
                                {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                            {/if}
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 grid cs-style-3" data-effect="slide-bottom">
                                <div class="shopstyle1">
                                    <div class="media_element">
                                        <figure>
                                            <a href="{$product.link}">
                                                <div class="image_view_product" style="background-image:url('{$src}')"></div>
                                            </a>
                                            <figcaption>
                                                <a class="external" rel="prettyPhoto" href="{$src}">
                                                    <i class="fa fa-search"></i>
                                                </a>
                                                <a class="zoom" href="{$product.link}"><i class="fa fa-eye"></i></a>
                                                <a class="addtocart" href="{$product.buy_now_link}">
                                                    <i class="fa fa-shopping-cart"></i> {$smarty.const.IMAGE_BUTTON_IN_CART}
                                                </a>
                                            </figcaption>
                                        </figure>
                                    </div>
                                    <div class="product_title title">
                                        <h3><a href="{$product.link}" title="{$product.products_name}">{$product.products_name|truncate:20:"...":true}</a></h3>
                                    </div>
                                    <div class="price-detail">{$product.newprice|regex_replace:"/[()]/":""}</div>
                                </div>
                            </div>
                        {/foreach}
                    </div>
                </div>
            </div>
            {if $arr_modul_productlisting.pagination|@count > 1}
                <div class="pagination_wrapper text-center">
                    <ul class="pagination">
                        {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                        <li class="active current">
                            <a>{$page.text}</a>
                            {elseif $page.previous}
                        <li>
                            <a class="previous" href="{$page.link}" title="{$page.title}">
                                &laquo;
                            </a>
                            {elseif $page.next}
                        <li>
                            <a class="next" href="{$page.link}" title="{$page.title}">
                                &raquo;
                            </a>
                            {else}
                        <li>
                            <a href="{$page.link}" title="{$page.title}">
                                {$page.text}
                            </a>
                            {/if}
                            {/foreach}
                        </li>
                    </ul>
                </div>
            {/if}
        </div>
    </div>
{/if}