<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}" />
    <meta name="description" content="{$meta_description}" />
    <meta name="robots" content="index, follow" />
    <meta charset="utf-8">
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}" />
    {/if}

    <link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="{$templatepath}custom.css" />
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/jquery-ui-1.8.16.custom.css" />
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/slideshow.css" media="screen" />
    <link rel="stylesheet" href="{$templatepath}lib/css/cloud-zoom.css" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/cloud-zoom.1.0.2.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/common.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/custom.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/bootstrap-tooltip.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/jquery.nivo.slider.pack.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/jquery.cycle.js"></script>
    <script type="text/javascript" src="includes/general.js"></script>

    {literal}
        <script type="text/javascript">
            $(document).ready(function () {
                $('#slideshow0').nivoSlider({
                    effect: 'fade',
                    pauseTime: 9000
                });
            });
        </script>
    {/literal}
</head>
<body>
<div id="container">
    <div id="header">
        <div id="logo">
            {$cataloglogo}
        </div>
        <div id="search">
            {include file="boxes/box_search.tpl"}
        </div>
        <div class="shopping-cart-mobile">
            {$arr_shopping_cart.title}
        </div>
        <div id="welcome">
            <ul class="align-loginbox">
                {include file="boxes/box_loginbox.tpl"}
                <li id="cart">
                    <div >
                        {include file="boxes/box_shopping_cart.tpl"}
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div id="menu">
        {*Ajax categories*}
        <div id="categories-box"></div>
    </div>
    <p id="back-top">
        <a href="#top"><i class="fa fa-angle-double-up"></i></a>
    </p>
    {literal}
        <script type="text/javascript">
            $(document).ready(function () {
                $("[rel=tooltip]").tooltip();
            });
        </script>
    {/literal}

