<div class="account-container">

<a href="javascript:history.back()"><i class="icon-caret-left"></i> {$smarty.const.IMAGE_BUTTON_BACK}</a>	
<div class="clearfix">
	<p class="pull-left">{$smarty.const.HEADING_ORDER_NUMBER|replace:" %s":": $order_id"}&nbsp;&nbsp;&nbsp;{$smarty.const.HEADING_ORDER_DATE}{$order_info.date_purchased|date_format:"%d.%m.%Y"}</p>	

</div>		

<div class="clearfix">
	<div class="span3">
		<address>
			<strong>{$smarty.const.HEADING_DELIVERY_ADDRESS}</strong><br>
			{if $order_customer_delivery.name}
				{$order_customer_delivery.name}<br>
			{/if}
			{if $order_customer_delivery.company}
				{$order_customer_delivery.company}<br>
			{/if}
			{if $order_customer_delivery.street_address}
				{$order_customer_delivery.street_address}<br>
			{/if}

			{if $order_customer_delivery.suburb}
				{$order_customer_delivery.suburb}<br>
			{/if}

			{if $order_customer_delivery.postcode}
				{$order_customer_delivery.postcode}
				{$order_customer_delivery.city}<br>
			{/if}
			{if $order_customer_delivery.state}
				{$order_customer_delivery.state}<br>
			{/if}
			{if $order_customer_delivery.country}
				{$order_customer_delivery.country}<br>
			{/if}
		</address>
	</div>
	<div class="span3">
		<address>
			<strong>{$smarty.const.HEADING_BILLING_ADDRESS}</strong><br> 
			{*$billing_format_id*}

			{if $order_customer_billing.name}
				{$order_customer_billing.name} <br>
			{/if}

			{if $order_customer_billing.company}
				{$order_customer_billing.company}<br>
			{/if}

			{if $order_customer_billing.street_address}
				{$order_customer_billing.street_address}<br>
			{/if}

			{if $order_customer_billing.suburb}
				{$order_customer_billing.suburb}<br>
			{/if}

			{if $order_customer_billing.postcode}
				{$order_customer_billing.postcode}{$order_customer_billing.city}<br>
			{/if}

			{if $order_customer_billing.state}
				{$order_customer_billing.state}<br>
			{/if}

			{if $order_customer_billing.country}
				{$order_customer_billing.country}<br>
			{/if}
		</address>
	</div>
	<div class="span3">
		{if is_shipping_method}
			<strong>{$smarty.const.HEADING_SHIPPING_METHOD}</strong>
			<p>{$order_info.shipping_method}</p>
		{/if}
	</div>
	<div class="span3">
		<strong>{$smarty.const.HEADING_PAYMENT_METHOD}</strong>
		{$order_info.payment_method}
	</div>
</div>




<h4>{$smarty.const.HEADING_BILLING_INFORMATION}</h4>

{if $is_multi_tax_groups}
	{$smarty.const.HEADING_PRODUCTS}
	{$smarty.const.HEADING_TAX}
	{$smarty.const.HEADING_TOTAL}
{else}
	{$smarty.const.HEADING_PRODUCTS}									
{/if}

<table class="table table-bordered">
{section name=current loop=$order_products}
	<tr>
	<td>{$order_products[current].qty}</td>
	<td>{$order_products[current].name}</td>

	
	{if $order_products[current].attributes}
	<td>
		{assign var=attlist value=$order_products[current].attributes}
		{foreach from=$attlist key=key item=attr name=attrlister}
			{$attr.option}: {$attr.value}
		{/foreach}
	</td>
	{/if}
	
	{if $historyattributeslist}
	<td>
		{section name=attr_current loop=$historyattributeslist}
			{$historyattributeslist[attr_current].option}: {$historyattributeslist[attr_current].value}
		{/section}
	</td>
	{/if}

	
		{if $is_multi_tax_groups}
		<td>
			{$order_products[current].tax}
		</td>
		{/if}
	
	<td>
	{math equation="x * (y / 100 + 1)" x=$order_products[current].final_price y=$order_products[current].tax format="%.2f"} {$order_info.currency|replace:"EUR":"&euro;"}
	</td>
	</tr>
{/section}
</table>



<div>
	<h4>{$smarty.const.HEADING_ORDER_HISTORY}</h4>
	<p>
		{foreach from=$orderhistorylist key=key item=history name=historylister}
			{$history.date_added}&nbsp;{$history.orders_status_name}{$history.comments}
		{/foreach}
		{$DOWNLOADS_HTML}      				
	</p>	
</div>	

<div>
	<table class="table table-bordered">
	{section name=current loop=$order_totals}
	<tr>
		<td>{$order_totals[current].title}</td>
		<td>{$order_totals[current].text}</td>
	</tr>
	{/section}
	</table>
</div>				

</div>