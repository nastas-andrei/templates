<section class="content-wrapper">
    <div class="container">
        <div class="row">
		    <button type="button" class="button" style="margin:20px 0" onclick="window.location='{$account_href}';">
		        <span>
		          <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
		        </span>
		    </button> 
			{if $orders_total > 0}
				<table class="table table-bordered table-condensed table-text-center">
					<td>{$smarty.const.TEXT_ORDER_NUMBER}</td>
					<td>{$smarty.const.TEXT_ORDER_STATUS}</td>
					<td>{$smarty.const.TEXT_ORDER_DATE} </td>
					<td>{$smarty.const.TEXT_ORDER_SHIPPED_TO} </td>
					<td>{$smarty.const.TEXT_ORDER_PRODUCTS} </td>
					<td>{$smarty.const.TEXT_ORDER_COST} </td>
					<td>&nbsp;</td>
						
					{section name=current loop=$historylist}      		
						<tr class="text-center">
							<td>{$historylist[current].orders_id}</td>
							<td>{$historylist[current].orders_status_name}</td>	
							<td>{$historylist[current].date_purchased}</td>	
							<td>{$historylist[current].order_name}</td>	
							<td>{$historylist[current].count}</td>	
							<td>{$historylist[current].order_total}</td>
							<td><a href="{$historylist[current].account_history_info_href}">{$smarty.const.SMALL_IMAGE_BUTTON_VIEW}</a></td>	
						</tr>
					{/section}        				
				</table>
			{else}         			
			   <div class="alert alert-warning text-center">    
			     {$smarty.const.TEXT_NO_PURCHASES}  
			  </div>
			{/if}

			{if $orders_total > 0}
			  {$pager_count}
			  {$smarty.const.TEXT_RESULT_PAGE} {$pager_links}
			{/if}
		</div>
	</div>
</section>