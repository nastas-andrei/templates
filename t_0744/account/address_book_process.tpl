<div class="container accountMarginBottom">
    <button type="button" class="btn btn-default btn-icon" style="margin:20px 0"
            onclick="window.location='{$addressbook_href}';">
        <span>
          <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
        </span>
    </button>
    {if !$is_delete}
        {$addressbook_edit_form}
    {/if}
    {if $is_addressbook_message}
        {$addressbook_message}
    {/if}
    {if $is_delete}
        {$smarty.const.DELETE_ADDRESS_TITLE}
        {$smarty.const.DELETE_ADDRESS_DESCRIPTION}
        {$smarty.const.SELECTED_ADDRESS}
        {$delete_address_label}
        <a href="{$addressbook_href}">{$smarty.const.IMAGE_BUTTON_BACK}</a>
        <a href="{$addressbook_delete_href}" class="btn btn-default btn-icon">{$smarty.const.IMAGE_BUTTON_DELETE}</a>
    {else}
        <div class="form_tem">
            {$ADDRESS_BOOK_DETAILS_HTML}
        </div>
        {if $is_edit}
            <div class="col-xs-6">
                {$edit_hidden_fields}
                <button type="submit" class="btn btn-default btn-icon pull-right">
                    <i class="fa fa-check"></i>&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_UPDATE}
                </button>
            </div>
        {else}
            <div class="col-xs-6">
                {$action_hidden_field}
                <button type="submit" class="btn btn-default btn-icon pull-right">
                    <i class="fa fa-check"></i>&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_CONTINUE}
                </button>
            </div>
        {/if}
    {/if}
    {if !$is_delete}
        </form>
    {/if}
</div>
