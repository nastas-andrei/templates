jQuery(document).ready(function () {
    jQuery.post('categories/tree', function (data) {
        jQuery('#categories-box').empty().append(data);
        $(".panel a").click(function(e){
            e.preventDefault();
            var style = $(this).attr("class");
            $(".jetmenu").removeAttr("class").addClass("jetmenu").addClass(style);
        });
        $().jetmenu();
    });
});
