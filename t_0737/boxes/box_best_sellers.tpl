<!-- Best Seller -->
{if $arr_bestsellersbox}
  {foreach from=$arr_bestsellersbox.products key=i item=product name=previewitems}
    {if $product.image} 
      {php}
       {$product = $this->get_template_vars('product');}
       {$pattern_src = '/src="([^"]*)"/';}
       {$pattern_width = '/width="([^"]*)"/';}
       {$pattern_height = '/height="([^"]*)"/';}
       {preg_match($pattern_src, $product['image'], $matches);}
       {$src = $matches[1];}
       {preg_match($pattern_width, $product['image'], $matches);}
       {$width = $matches[1];}
       {preg_match($pattern_height, $product['image'], $matches);}
       {$height = $matches[1];}
       {$this->assign('src', $src);}
       {$this->assign('img_width', $width);}
       {$this->assign('img_height', $height);}
      {/php}
    {else}
      {assign var=src value=$imagepath|cat:"/empty-album.png"}
    {/if}
  <li class="four columns product" >
    <div class="product-image-wrapper" id="productImageWrapID_182" onclick="location.href='{$product.link}'">
      <a href="{$product.link}"   title="{$product.name}">
        <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
        <div class="product-image-wrapper-hover"></div>
      </a>
    </div>
    <div class="wrapper-hover">
      <div class="product-price">
        <div class="price-box">
          <span class="regular-price" style="color: rgb(34, 34, 34);">
            <span class="price" style="color: rgb(34, 34, 34);"> {$product.preisneu|regex_replace:"/[()]/":""}</span>               
          </span> 
        </div>
      </div>
      <div class="product-name">
        <div class="clearfix">
          <a class="icon_cart_title" href="{$product.link}"  onclick="location.href='{$product.link}'" title="{$product.name}">
            {$product.name|truncate:45:"...":true}
          </a>
          <div class="icon_cart_rollover">
            <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}"  onclick="location.href='{$product.buy_now_link}'"></button>
            <span class='ajax_loader182' style='display:none'>
              <img src='/templates/t_0737/images/ajax-loader.gif'/>
            </span>
          </div>
        </div>
      </div>
    </div>
  </li>
  {/foreach}
{/if}