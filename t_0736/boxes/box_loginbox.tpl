{if $arr_loginbox}
    <ul class="links" id="header-login">
        {if $arr_loginbox.boxtyp == "myaccount"}
            <li class="first"><a href="{$arr_topmenu.4.link}" title="{$arr_topmenu.4.name}">{$arr_topmenu.4.name}</a></li>
            <li><a href="{$arr_loginbox.items.4.target}">{$arr_loginbox.items.4.text}</a></li>
        {else}
            <li><a href="{$domain}/login.php">{$arr_smartyconstants.LOGIN_BOX_REGISTER}</a></li>
        {/if}
        <li class="last"><a href="{$arr_topmenu.2.link}">{$arr_topmenu.2.name}</a></li>
    </ul>
{/if}