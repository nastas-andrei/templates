{foreach from=$arr_modul_previewproducts.products key=key item=product name=productpreviewitems}
    {if $product.image}
        {php}
            $product = $this->get_template_vars('product');
            $pattern_src = '/src="([^"]*)"/';
            preg_match($pattern_src, $product['image'], $matches);
            $src = $matches[1];
            $this->assign('src', $src);
        {/php}
    {else}
        {assign var=src value=$imagepath|cat:"/empty-album.png"}
    {/if}
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <a href="{$product.link}" title="{$product.name}">
                        <div class="width-image-product" style="background-image: url('{$src}');"></div>
                    </a>

                    <h2>{$product.preisneu|regex_replace:"/[()]/":""}</h2>

                    <p class="overflow-title-product">
                        <a href="{$product.link}" title="{$product.name}">
                            {$product.name}
                        </a>
                    </p>
                    <a href="{$product.buy_now_link}" class="btn btn-default add-to-cart">
                        <i class="fa fa-shopping-cart"></i>{$smarty.const.IMAGE_BUTTON_IN_CART}
                    </a>
                </div>
                {if $preisalt > 0}
                    <img src="{$imagepath}/sale.png" class="new" alt="">
                {/if}
            </div>
        </div>
    </div>
{/foreach}
