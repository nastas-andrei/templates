{include file="header.tpl"}

<div id="content">

    {$content}

    {if $content_template}
        {include file="$content_template"}
    {/if}

</div>

{include file="footer.tpl"}