<div class="container">
    <form id="loginForm" class="formHTML5" name="login" action="{$login_form_url}" method="post">
        {if $LoginMessageStackSize == true}
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {$login_message}
            </div>
        {/if}
        {* LOGIN *}
        <div class="inputHLDbox">
            <div class="inputHLDhead">{$smarty.const.TEXT_RETURNING_CUSTOMER}</div>
            <div class="inputHLDinner">
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_EMAIL_ADDRESS}</div>
                    <input type="email" name="email_address" class="inputfield" maxlength="128" required />
                </div>
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD}</div>
                    <input type="password" name="password" class="inputfield" maxlength="128" required />
                </div>
                <div class="clearer"></div>
                <div class="inputHLD">

                    <button type="submit" class="btn button_add">{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>

                    <div class="clearer"></div>
                    <a href="{$PASSWORD_FORGOTTEN_URL}" class="linkpwf">{$smarty.const.TEXT_PASSWORD_FORGOTTEN}</a>
                </div>
                <div class="clearer"></div>
            </div>
        </div>
    </form>

    {* REGISTER *}
    <form id="createForm" class="formHTML5" name="create" action="{$login_form_url_create}" method="post">
        {if $LoginCreateMessageStackSize == true}
            <div class="errorMessages">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">?</button>
                {$logincreate_message}
            </div>
        {/if}
        <div class="inputHLDbox">
            <div class="inputHLDhead">{$smarty.const.HEADING_NEW_CUSTOMER}</div>
            <div class="inputHLDinner">
                {*****************RADIO***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_GENDER_TEXT}</div>
                    <div class="logradioHLD" style="display: inline-flex;">
                        {$gender_radio_m|replace:'name="reg_title"':'name="reg_title" class="required validate-reqchk-byname groupName:\'reg_title\'"'}
                        <label for="m">{$smarty.const.MALE}</label>
        <span style="margin-left:20px">
          {$gender_radio_f|replace:'name="reg_title"':'name="reg_title" class="required validate-reqchk-byname groupName:\'reg_title\'"'}
        </span>
                        <label for="f">{$smarty.const.FEMALE}</label>
                    </div>
                </div>
                <div class="clearer"></div>
                {**********************COMPANY IF ISSET*********************************}
                {if $smarty.const.ACCOUNT_COMPANY == 'true'}
                    <div class="inputHLD">
                        <div class="inputTXT">{$smarty.const.ENTRY_COMPANY}</div>
                        {$company_input}
                    </div>
                    <div class="clearer"></div>
                {/if}
                {*****************FIRST NAME***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_FIRST_NAME}</div>
                    {$firstname_input}
                    <!-- |replace:'class="inputfield"':'class="inputfield required"' -->
                </div>
                {*****************LAST NAME***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_LAST_NAME}</div>
                    {$lastname_input}
                </div>
                <div class="clearer"></div>
                {*****************STREET ADRESS***********************}
                <div class="inputHLD width_streed">
                    <div class="inputTXT">{$smarty.const.ENTRY_STREET_ADDRESS}</div>
                    {$street_address_input}
                </div>
                <div class="clearer"></div>
                {*****************COUNTRY***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_COUNTRY}</div>
                    <div class="inputselectHLD">
                        {$country_list}&nbsp;
                        {if $entry_country_text}
                            <span class="inputRequirement">{$smarty.const.ENTRY_COUNTRY_TEXT}</span>
                        {/if}
                    </div>
                </div>
                {*****************POST CODE***********************}
                <div class="inputHLD" style="margin-right:10px;width:100px;">
                    <div class="inputTXT">{$smarty.const.ENTRY_POST_CODE}</div>
                    {$zipcode|replace:'inputfield':'inputfield zipcode'}
                </div>
                {*****************CITY***********************}
                <div class="inputHLD" style="margin-right:0">
                    <div class="inputTXT">{$smarty.const.ENTRY_CITY}</div>
                    {$city_input|replace:'inputfield':'inputfield city'}
                </div>
                {*****************TATE IF ISSET***********************}
                {if $smarty.const.ACCOUNT_STATE == 'true'}
                    <div class="inputHLD">
                        <div class="inputTXT">{$smarty.const.ENTRY_STATE}</div>
                        <div class="inputfieldHLD">
                            <div id="DivRegions">
                                {$state_field_html}&nbsp;
                                <span class="inputRequirement">{$smarty.const.ENTRY_STATE_TEXT}</span>
                            </div>
                        </div>
                    </div>
                {/if}
                {*****************EMAIL ADRESS***********************}
                <div class="inputHLD width_streed">
                    <div class="inputTXT">{$smarty.const.ENTRY_EMAIL_ADDRESS}</div>
                    {$email_input}
                </div>
                <div class="clearer"></div>
                {*****************TELEPHONES NUMBER***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</div>
                    {$fon_input}
                </div>
                {*****************FAX NUMBER***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_FAX_NUMBER}</div>
                    {$fax_input}
                </div>
                <div class="clearer"></div>
                {*****************PASSWORD***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD}</div>
                    {$create_password_input}
                </div>
                {*****************PASSWORD CONFIRMATION***********************}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</div>
                    {$password_confirmation_input}
                </div>
                <div class="clearer"></div>
                <div class="inputHLD">
                    <div class="req_label"><span>*</span> Pflichtfelder
                        {*<p>Gleichzeitig erhalten Sie per E-Mail regelma?ig ausgesuchte Angebote zu ahnlichen Produkten<br /> unseres Shops, die Sie interessieren konnten. Den Newsletter konnen Sie jederzeit kostenfrei<br /> durch formlose E-Mail oder durch anklicken des Links "Abmelden", am Ende eines Newsletters,<br /> abbestellen.</p>*}
                    </div>
                    <input type="checkbox" name="reg_terms_news" class="inputcheckbox" style="margin-right:5px" id="check-1"/>
                    <label for="check-1" class="inputcheckboxTXT">{$smarty.const.TEXT_NEWS_TERM}</label>

                    <div class="clearer"></div>
                    <input type="checkbox" name="reg_terms_ds" class="inputcheckbox" style="margin-right:5px" id="check-2" required />
                    <label for="check-2" class="inputcheckboxTXT">{$smarty.const.ENTRY_DATENSCHUTZ_TEXT}</label>
                </div>
                <div class="clearer"></div>

                <button type="submit" class="btn button_add">{$smarty.const.IMAGE_BUTTON_LOGIN}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </form>
</div>
