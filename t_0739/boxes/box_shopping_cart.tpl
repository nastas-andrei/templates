{if $arr_shopping_cart}	
    {if $SCRIPT_NAME !== '/shopping_cart.php'}
        {if $arr_shopping_cart.count_contents == ''}
            <div class="heading">
                <a href="{$arr_shopping_cart.cart_link}">
                    <span id="cart-total">{$arr_shopping_cart.count_contents} - {$arr_shopping_cart.gesamtsumme}</span>
                </a>
            </div>
            <div class="content">
                <div class="empty">{$arr_shopping_cart.BOX_SHOPPING_CART_EMPTY}</div>
            </div>
        {else}
            <div class="heading">
                <a><span id="cart-total">{$arr_shopping_cart.count_contents} - {$arr_shopping_cart.gesamtsumme}</span></a>
            </div>
            <div class="content">
                <div class="mini-cart-info">
                    <form name="cart_quantity" action="shopping_cart.php?action=update_product" method="post">
                        <table>
                            {foreach from=$arr_shopping_cart.products key=key item=product}
                                <tr>
                                    <td class="image">
                                        <a href="{$product.link}">
                                            {$product.image}
                                        </a>
                                    </td>
                                    <td class="name">
                                        <a href="{$product.link}">{$product.name|truncate:50:"...":true}</a>
                                    </td>
                                    <input id="{$product.id}" type="checkbox" name="cart_delete[]" value="{$product.id}" style="display: none;">
                                    <td class="quantity"><input type="text" name="cart_quantity[]" value="{$product.quantity}" size="4" maxlength="10" class="validate-zero-or-greater validate-number qty input-text" onblur="if (checkValue('cartadd0')) document.forms.cart_quantity.submit()" id="cartadd0"></td>
                                    <td class="total">{$product.final_price}</td>
                                    <td class="remove">
                                        <input id="cart-product-{$product.id}" type="checkbox" name="cart_delete[]" value="{$product.id}" style="display: none;">
                                        <a href="javascript: del_item_shoppingcart('cart-product-{$product.id}');" title="Remove This Item" class="btn-remove">
                                            <i class="fa fa-times" alt="Remove" title="Remove"></i>
                                        </a>
                                        <input type="hidden" name="products_id[]" value="{$product.id}">
                                        <input type="hidden" name="old_qty[]" value="{$product.quantity}">
                                    </td>
                                </tr>
                            {/foreach}
                        </table>
                    </form>
                </div>
                <div class="mini-cart-total">
                    <table>
                        <tr>
                            <td class="right">{$arr_shopping_cart.cart_summe}</td>
                            <td class="right"><b>{$arr_shopping_cart.gesamtsumme}</b></td>
                        </tr>
                    </table>
                </div>
                <div class="checkout">
                    <a href="{$arr_topmenu.3.link}">{$arr_shopping_cart.title|strip_tags:false}</a>
                    <a href="{$arr_topmenu.5.link}">{$arr_topmenu.5.name}</a>
                </div>
            </div>
        {/if}


      {*<div class="block mini-cart sns-ajaxcart have-item">*}
        {*<div class="block-title">*}
          {*{if $arr_shopping_cart.count_contents > 0}*}
          {*<a href="javascript:void(0)" class="ico-view"></a>*}
          {*<div class="cart-status">*}
            {*<span class="label">{$arr_shopping_cart.title}</span>*}
            {*<span class="subtotal">*}
              {*<span class="amount">{$arr_shopping_cart.count_contents}</span>*}
              {*<span>{$arr_shopping_cart.cart_prod} </span>*}
              {*<span class="price">{$arr_shopping_cart.gesamtsumme}</span>*}
            {*</span>*}
          {*</div>*}
          {*{/if}*}
        {*</div>*}
        {*{if $arr_shopping_cart.count_contents > 0}*}
        {*<div class="block-content">*}
          {*<div class="block-inner">*}
            {*<p class="block-subtitle">*}
              {*{$smarty.const.BOX_SHOPPING_CART_PROD}*}
            {*</p>*}
            {*<form name="cart_quantity" action="shopping_cart.php?action=update_product" method="post">*}
              {*<ol id="minicart-sidebar" class="mini-products-list">*}
              {*{foreach from=$arr_shopping_cart.products key=key item=product}*}
                {*<li class="item last odd">*}
                  {*<a href="{$product.link}" title="{$product.name}" class="product-image">*}
                    {*<div class="img_mini_cart">{$product.image}</div>*}
                  {*</a>*}
                  {*<div class="product-details">*}
                    {*<p class="product-name">*}
                      {*<a href="{$product.link}">{$product.name|truncate:50:"...":true}</a>*}
                    {*</p>*}
                  {*</div>*}
                  {*<div class="product-details-bottom">*}
                    {*<input id="{$product.id}" type="checkbox" name="cart_delete[]" value="{$product.id}" style="display: none;">*}
                    {*<a href="javascript: del_item_shoppingcart('{$product.id}');" title="Remove This Item" onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');" class="btn-remove">*}
                    {*</a>*}

                    {*<div class="price-box">*}
                      {*<span class="price">{$product.final_price}</span>*}
                    {*</div>*}
                    {*<p class="quantity-container" style="margin-left:70px">*}
                      {*<span class="label-qty">{$smarty.const.TABLE_HEADING_QUANTITY }</span>*}
                      {*<input type="text" name="cart_quantity[]" value="{$product.quantity}" size="4" maxlength="10" class="validate-zero-or-greater validate-number qty input-text" onblur="if (checkValue('cartadd0')) document.forms.cart_quantity.submit()" id="cartadd0">*}
                      {*<input type="hidden" name="products_id[]" value="{$product.id}">*}
                      {*<input type="hidden" name="old_qty[]" value="{$product.quantity}">*}
                    {*</p>*}
                  {*</div>*}
                {*</li>*}
                {*{/foreach}*}
              {*</ol>*}
            {*</form>*}
            {*<div class="bottom-action actions">*}
              {*<div class="shopping_cart_mini">{$arr_shopping_cart.cart_summe} {$arr_shopping_cart.gesamtsumme}</div>*}
              {*<button data-original-title="Go to cart" data-toggle="tooltip" type="button" title="Go to cart" class="button" onclick="setLocation('shopping_cart.php')">*}
                {*{$arr_shopping_cart.title|strip_tags:false}*}
              {*</button>*}
              {*<button data-original-title="{$arr_topmenu.5.name}" data-toggle="tooltip" type="button" title="{$arr_topmenu.5.name}" class="button btn-gocheckout" onclick="setLocation('login.php')">*}
                {*{$arr_topmenu.5.name}*}
              {*<button data-original-title="Update Shopping Cart" data-toggle="tooltip" type="button" name="update_cart_action" value="update_qty" onclick="location.reload();" title="Update Shopping Cart" class="button btn-update">*}
                {*{$smarty.const.IMAGE_BUTTON_UPDATE_CART}*}
              {*</button>*}
            {*</div>*}
          {*</div>*}
        {*</div>*}
        {*{/if}*}
      {*</div>*}

    {/if}
{/if} 

