<h3>{$arr_whatsnewbox.title}</h3>
{foreach from=$arr_whatsnewbox.products key=key item=product name=newproducts}
    {if $product.image}
        {php}
            $product = $this->get_template_vars('product');
            $pattern_src = '/src="([^"]*)"/';
            preg_match($pattern_src, $product['image'], $matches);
            $src = $matches[1];
            $this->assign('src', $src);
        {/php}
    {else}
        {assign var=src value=$imagepath|cat:"/empty-album.png"}
    {/if}
    <article class="col-xs-12 col-sm-6 col-md-3 product-block">
        <div class="block-info maxheight">
            <div class="box_inner">
                <div class="inner-block">
                    <a href="{$product.link}">
                        <div style="background: url('{$src}') center no-repeat;background-size:cover;width: 100%;height: 200px"></div>
                    </a>

                    <div class="box-text-small">
                        <h4>
                            <a href="{$product.link}">{$product.name|truncate:45:"..."}</a>
                        </h4>
                        <div class="btn-small">
                            <span class="price">{$product.preisneu}</span>
                            <div class="block-btns">
                                <a href="{$product.buy_now_link}" class="button-cart"> {$smarty.const.IMAGE_BUTTON_IN_CART}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </article>
{/foreach}

