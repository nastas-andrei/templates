    <footer class="footer-wrapper">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="widget">
                    <h3 class="title">{$arr_informationbox.title}</h3>
                    <ul>
                        {include file="boxes/box_information.tpl"}
                    </ul>
                </div>
            </div>
            {foreach from=$boxes_pos.left key=key item=boxname name=current}
                {if $boxname eq 'htmlbox'}
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        {include file="boxes/box_$boxname.tpl"}
                    </div>
                {/if}
            {/foreach}
        </div>
    </footer>
    <div id="copyright" class="col-full">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div id="copyrighttext">
                    <p>
                        <a href="sitemap.php" title="{$smarty.const.TEXT_SITEMAP}">{$smarty.const.TEXT_SITEMAP}</a> |
                        <a href="advanced_search.php" title="{$smarty.const.NAVBAR_TITLE_1}">{$smarty.const.BOX_SEARCH_ADVANCED_SEARCH}</a>
                    </p>
                    <p>&copy; {$arr_footer.year}
                        {foreach from=$arr_footer.partner key=key item=partner}
                            <span>
                                <a href="http://{$partner.link}" target="_blank" class="muted">
                                    {$partner.text} {$partner.name}
                                </a>
                            </span>
                            <a href="http://{$partner.link}" target="_blank">
                                <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" title="{$partner.name}"/>
                            </a>
                        {/foreach}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>{*END wrapper*}
<div class="dmtop">Scroll to Top</div>
<script src="{$templatepath}lib/js/jetmenu.js"></script>
<script src="{$templatepath}lib/js/jquery.unveilEffects.js"></script>
<script src="{$templatepath}lib/js/jquery.bxslider.js"></script>
<script src="{$templatepath}lib/js/jquery.nav.js"></script>
<script src="{$templatepath}lib/js/jquery.hoverex.min.js"></script>
<script src="{$templatepath}lib/js/jquery.prettyPhoto.js"></script>
<script src="{$templatepath}lib/js/application.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="{$templatepath}lib/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="{$templatepath}lib/js/jquery.themepunch.revolution.min.js"></script>
<script src="{$templatepath}lib/js/custom.js"></script>
</body>
</html>