<div class="account-container">
{$account_password_form}
<div class="form-horizontal">
<fieldset>
  {if $is_account_password_message}
    <p class="text-center">{$account_password_message}</p>
  {/if}


<legend class="text-center">{$smarty.const.MY_PASSWORD_TITLE}</legend>
<a href="{$account_href}" ><i class="icon-caret-left"></i> {$smarty.const.IMAGE_BUTTON_BACK}</a>
<div class="control-group">
  <label for="">&nbsp;</label>
  <div class="controls">
  <p >{$smarty.const.FORM_REQUIRED_INFORMATION}</p>  
  </div>
</div>



<div class="control-group">
  <label for="" class="control-label">{$smarty.const.ENTRY_PASSWORD_CURRENT}</label>
  <div class="controls">
    {$password_current_field}
    <span class="help-inline">
    {if $entry_password_current_text}
      {$smarty.const.ENTRY_PASSWORD_CURRENT_TEXT}
    {/if}
  </span>
  </div>
</div>


<div class="control-group">
  <label for="" class="control-label">{$smarty.const.ENTRY_PASSWORD_NEW}</label>
  <div class="controls">
    {$password_new_field}
     <span class="help-inline">
    {if $entry_password_new_text}
      {$smarty.const.ENTRY_PASSWORD_NEW_TEXT}
    {/if}
  </span>
  </div> 
</div>

<div class="control-group">
  <label for="" class="control-label">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</label>
  <div class="controls">
    {$password_confirmation_field}
    <span class="help-inline">
    {if $entry_password_confirmation_text}
      {$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}
    {/if}
  </span>
  </div>
</div>


<div class="text-center">
  <input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
</div>
</fieldset>
</div>
</form>

</div>