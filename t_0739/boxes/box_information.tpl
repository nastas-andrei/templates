{if $arr_informationbox}
    <h3>{$arr_informationbox.title}</h3>
    <ul>
        {foreach from=$arr_informationbox.items key=key item=link name=element}
            <li><a href="{$link.target}">{$link.text}</a></li>
        {/foreach}
    </ul>
{/if}