</section>
<footer>
  <!-- BEGIN WIDGET BOTTOM -->
  <div class="container">
      <div class="row">
          <div class="col-md-12">
              <section class="wdgt-bottom">
                  <div class="row">
                    {include file="boxes/box_information.tpl"}

                    <!--footer-->
                     {php}
                      $i = 0;
                    {/php}

                    {foreach from=$boxes_pos.left key=key item=boxname name=current} 
                    {if $boxname eq 'htmlbox'}
                      {php}
                        $i++;
                        $this->assign('counter_boxes', $i);
                      {/php}
                    <div class="col-md-3">
            <div class="widget-box">
            {include file="boxes/box_$boxname.tpl"}
            </div>
                    </div>
                    {/if}   
                    {/foreach} 
                  </div>
              </section>
          </div>
      </div>
  </div>
  <div class="container">
    <ul class="inline text-center widget">
          <li><small>&copy; {$arr_footer.year}</small></li>
        {foreach from=$arr_footer.partner key=key item=partner}
            <li>
              <small>
                <a href="http://{$partner.link}" target="_blank" class="muted">{$partner.text} {$partner.name}</a>
              </small>
                <a href="http://{$partner.link}" target="_blank">
                  <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" alt="" title="{$partner.name}"/>
                </a>
           </li>
        {/foreach}
    </ul>    
  </div>
<!-- END WIDGET BOTTOM -->
</footer>
</div>    
</body>
</html>