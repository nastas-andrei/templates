<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 done"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3 done"><span><a href="{$checkout_payment_href}">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></span></li>
    <li class="step4 current"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>

<div class="row">
    <div class="span6">
        <h5>{$smarty.const.TEXT_SHIPPING_INFORMATION}</h5>
        {if $sendto != false}
            {if $smarty.const.HEADING_DELIVERY_ADDRESS}
                {$smarty.const.HEADING_DELIVERY_ADDRESS}
                <a href="{$checkout_shipping_address_href}">
                    ({$smarty.const.TEXT_EDIT})
                </a>
            {/if}
            <br>
            {$formated_address_delivery}
            {if $shipping_method}
                {if $smarty.const.HEADING_SHIPPING_METHOD}
                    {$smarty.const.HEADING_SHIPPING_METHOD}
                {/if}
                <br />
                <a href="{$checkout_shipping_href}">({$smarty.const.TEXT_EDIT})</a>
                <h4>{$shipp_online_title}</h4>
                {$shipp_online_desc}
            {/if}
        {/if}
    </div>

    <div class="span6">
        {if $smarty.const.HEADING_BILLING_INFORMATION}
            <h5>{$smarty.const.HEADING_BILLING_INFORMATION}</h5>
            {$smarty.const.HEADING_BILLING_ADDRESS}
            <a href="{$checkout_payment_address_href}">({$smarty.const.TEXT_EDIT})</a>
            <br>
            {$formated_address_billing}
            {$smarty.const.HEADING_PAYMENT_METHOD}
            <a href="{$checkout_payment_href}">({$smarty.const.TEXT_EDIT})</a>
        {/if}
        {$payment_method}
    </div>
</div>

{if $is_payment_modules_confirmation}
    <h4>{$smarty.const.HEADING_PAYMENT_INFORMATION}</h4>
    <div class="">
        <h5>{$confirmation_title}</h5>
        {section name=current loop=$confirmationlist}
            {$confirmationlist[current].title}
            {$confirmationlist[current].field}
        {/section}
    </div>
{/if}

{$checkout_confirmation_form}
<!-- beginning checkBOX //-->
<label class="checkbox">
    {$terms_checkbox}  {$seo_link}
</label>
{if $is_terms_error}
    <p class="text-error">{$terms_error}</p>
{/if}

<table class="table table-bordered table-confirmation-checkout">
    <thead>
    <tr>
        <td colspan="2"><strong>{$smarty.const.HEADING_PRODUCTS}
                <a href="{$shipping_card_href}">({$smarty.const.TEXT_EDIT})</a></strong>
        </td>

        {if $is_multiple_tax_groups}
            <td>
                <strong>{$smarty.const.HEADING_TAX}</strong>
            </td>
        {/if}

        <td><strong>Pcs</strong></td>
        <td><strong>{$smarty.const.TABLE_HEADING_PRICE}</strong></td>
        <td><strong>{$smarty.const.HEADING_TOTAL}</strong></td>
    </tr>
    </thead>
    <tbody>
    {section name=current loop=$productslist}
        <tr>
            <td class="checkout-confirmation-image-cell">
                {if $productslist[current].image neq ''}
                    <img src="{$productslist[current].image}">
                {/if}
            </td>
            <td>
                {$productslist[current].name}
                {section name=current_attr loop=$productslist[current].attributes}
                    {$productslist[current].attributes[current_attr].option}: {$productslist[current].attributes[current_attr].value}
                {/section}
            </td>
            <td>
                {$productslist[current].qty}
                {if $smarty.const.STOCK_CHECK == 'true'}
                    {$productslist[current].products_check_stock}
                {/if}
            </td>
            {if $is_multiple_tax_groups}
                <td>
                    {$productslist[current].tax}%
                </td>
            {/if}

            <td class="price-unit">{$productslist[current].price_single}</td>
            <td class="price-total"><strong>{$productslist[current].price_common}</strong></td>
        </tr>
    {/section}
    </tbody>
</table>

{if $CHECKOUT_ALTERNATIVE_PRODUCT}
    <div class="clearfix"> {$CHECKOUT_ALTERNATIVE_PRODUCT } </div>
{/if}

<div class="clearfix">
    {if $smarty.const.MODULE_ORDER_TOTAL_INSTALLED}
        <p class="order-total text-left">{$order_total_modules}</p>
    {/if}
</div>

{if $is_comments}
    <p><strong>{$smarty.const.HEADING_ORDER_COMMENTS}</strong></p>
    <a href="{$checkout_payment_href}">({$smarty.const.TEXT_EDIT})</a>
    <p>{$comments} {$comments_hidden}</p>
{/if}

{if $button_iframe}
    {$button_iframe}
{/if}

{if $foxrate_checkbox}
    {$foxrate_checkbox}
{/if}


{if $MONEYBOOKER_NOTE_TEXT_CONFIRM}
    {$MONEYBOOKER_NOTE_TEXT_CONFIRM}
{/if}

{$ORDER_CONFIRMATION_ADDITIONAL_COSTS}
<div id="confirmation_button">
    <div id="confirmt" class="text-right">
        <input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONFIRM_ORDER}">
    </div>
</div>
{$payment_modules_process_button}



{if !empty($temporderid_hidden)}
    {$temporderid_hidden}
{/if}

{$CHECKOUT_TEXTS_HTML}

</form>