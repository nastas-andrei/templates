{$smarty.const.BEFORE_CONTENT}
{literal}
<style type="text/css">
#pageOL {
  background: black;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
}

#qpayfr {
  position:absolute;
  width:658px;
  height:650px;
  background:#FFF;
  border:3px solid #8f9faa;
  padding:5px;
  display:none;
  z-index:1001;
}

/* foxxTip */
.tip-wrap {
  padding:8px;
  border:2px solid #000;
  background:#CCC;
  visibility:hidden;
}


/* NEW view products desc */
a:hover {
  cursor:pointer !important;
}
#view_desc {
  position:absolute;
  top:0px;
  left:0px;
  width:600px;
  height:20px;
  background:#FFF;
  border:1px solid #8f9faa;
  visibility:hidden;
  z-index:1001;
}
#view_desc_content {
  padding:10px;
  height:auto;
}
.view_desc_close {
  position:absolute;
  right:-12px;
  top:-13px;
  width:26px;
  height:26px;
  background:url(/templates/0_source/images/view_desc_close.png) no-repeat;
}
.view_desc_close:hover {
  background-position:0px -26px;
}
.checkout_img {
  width:30px;
  height:auto;
}
.checkoutOTLeft {
  padding:0 15px 0 0;
}
.order_details_0 {
  background:#f0f0f0;
  color: #000;
}
.order_details_0 a {
  color: #686868;
}
.order_details_0 a:hover {
  color: #000;
}
.payment_m, .shipping_m {
  margin-bottom:0px;
}
#payment {
  height:0px;
}
/* NEW view products desc */



</style>
<script type="text/javascript">
var pageOL = null;
var xBox =0;
var yBox = 0;
var act = null;

function framebild_auf_nein() {
  var view = document.getElementById('framebild').style.display;
  if ( view == 'none') {
    document.getElementById('framebild').style.display = 'block';
  } else {
    document.getElementById('framebild').style.display = 'none';
  }
}

window.addEvent('domready', function() {
  var foxxTips = new Tips($('foxspam'), {
      hideDelay: 100,
      showDelay: 100,
      fixed:  true,
      offset: {x:-26, y:-45}
  });
  foxxTips.addEvents({
      'show': function(tip) {
        tip.fade('in');
      },
      'hide': function(tip) {
        tip.fade('out');
      }
  });


  if($('qpayfr') != undefined) {
      $$('input.openfr').addEvent('click', function(e){
        //e.stop();
        if(testagb_noalert()!=false){
          xBox = ($(window).getSize().x / 2 - $('qpayfr').getStyle('width').toInt()/2);
          yBox = ($(window).getSize().y + $(window).getScroll().y) / 2;
          pageOL = new Element('div', { 'id':'pageOL','styles':{ 'opacity':0.5,'visibility':'visible','overflow':'hidden' }}).inject($(document.body));
          $('qpayfr').setStyles({ 'top': yBox, 'left':xBox, 'display':'block' });
          $('payment').setStyles({ 'height': '100%' });
      }
      });
   }


  /* view products desc */
  $$('a.viewd').each(function(el) {
      el.addEvent('click',function(e){
        e.stop();
          pageOL = new Element('div', { 'id':'pageOL','styles':{ 'opacity':0.5,'visibility':'visible','overflow':'hidden'}}).inject($(document.body));
          pageOL.addEvent('click',function(e){
             $('view_desc').setStyles({'visibility':'hidden', 'height':'0px'})
            pageOL.destroy();
          });
          var req = new Request({
                method: 'post',
                url: 'ajax_prod_desc.php',
                data: { 'pId' : el.getProperty('rel') },
                onRequest: function() {  },
                onComplete: function(response) {
                  $('view_desc_content').set('html',response);
                  $('view_desc').setStyle('height', ''+($('view_desc_content').getCoordinates().height + 20)+'px');
                  xBox = ($(window).getSize().x / 2 - $('view_desc').getStyle('width').toInt()/2);
                  yBox = ($(window).getSize().y / 2 + $(window).getScroll().y) - $('view_desc').getCoordinates().height/2;
                  if (yBox < 0) { yBox = 20; }
                  $('view_desc').setStyles({ 'top': yBox, 'left':xBox, 'visibility':'visible'});
                }
          }).send();
          $$('a.view_desc_close').addEvent('click', function(e){
            e.stop();
            $('view_desc').setStyles({'visibility':'hidden', 'height':'0px'})
            pageOL.destroy();
          })

        })
  });
  /* view products desc */





});

window.addEvent('resize',function(){
  if($('qpayfr')) {
      xBox = ($(window).getSize().x / 2 - $('qpayfr').getStyle('width').toInt()/2);
      yBox = ($(window).getSize().y + $(window).getScroll().y) / 2;
      $('qpayfr').setStyles({'left':xBox, 'top':yBox });
  }
}.bindWithEvent(this));

</script>
{/literal}

<!--view products desc -->
<table id="view_desc" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <div style="position:relative;">
        <a href="javascript:;" class="view_desc_close">&nbsp;</a>
        <div id="view_desc_content"> </div>
      </div>
    </td>
  </tr>
</table>
<!--view products desc -->

{if !$smarty.const.PH_INSIDE}
  {include file="pieces/page_title_section.tpl"}
  {$smarty.const.BETWEEN_HL_CONTENT}
  <table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td class="pageContentTable">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">

{else}

  <table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
      <td>{include file="pieces/page_title_section.tpl"}</td>
    </tr>
   {$smarty.const.BETWEEN_HL_CONTENT}

{/if}

        <tr>
          <td class="cont_abst" align="left">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="checkout_area">
              <tr>
                <td>
                  <div class="checkoutFrameHead"><b>{$smarty.const.TEXT_SHIPPING_INFORMATION}</b></div>
                  <div class="checkoutFrame">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
                      <tr> {if $sendto != false}
                        <td width="30%" valign="top">
                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="main"> <b>{$smarty.const.HEADING_DELIVERY_ADDRESS}</b> <a href="{$checkout_shipping_address_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a> </td>
                            </tr>
                            <tr>
                              <td class="main">{$formated_address_delivery}</td>
                            </tr>
                            {if $shipping_method}
                            <tr>
                              <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
                            </tr>
                            <tr>
                              <td class="main"> <b>{$smarty.const.HEADING_SHIPPING_METHOD}</b> <a href="{$checkout_shipping_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a> </td>
                            </tr>
                            <tr>
                              <td class="main">{* $shipping_method *}<span>{$shipp_online_title}</span><span>{$shipp_online_desc}</span></td>
                            </tr>
                            {/if}
                          </table>
                        </td>
                        {/if} </tr>
                    </table>
                  </div>
                </td>
                <td >
                  <div class="checkoutFrameHead"><b>{$smarty.const.HEADING_BILLING_INFORMATION}</b></div>
                  <div class="checkoutFrame">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
                      <tr>
                        <td width="30%" valign="top">
                          <table border="0" width="100%" cellspacing="0" cellpadding="0">
                            <tr>
                              <td class="main"> <b>{$smarty.const.HEADING_BILLING_ADDRESS}</b> <a href="{$checkout_payment_address_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a> </td>
                            </tr>
                            <tr>
                              <td class="main">{$formated_address_billing}</td>
                            </tr>
                            <tr>
                              <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
                            </tr>
                            <tr>
                              <td class="main"> <b>{$smarty.const.HEADING_PAYMENT_METHOD}</b> <a href="{$checkout_payment_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a> </td>
                            </tr>
                            <tr>
                              <td class="main">{$payment_method}</td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        {if $is_payment_modules_confirmation}
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td class="main" align="left">
            <div class="checkoutFrameHead"><b>{$smarty.const.HEADING_PAYMENT_INFORMATION}</b></div>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td align="left">
            <div class="checkoutFrame">
              <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
                <tr>
                  <td>
                    <table border="0" cellspacing="0" cellpadding="0">
                      {if $payment_method neq 'Nachnahme'}
	                      <tr>
	                        <td class="main" colspan="4">{$confirmation_title}</td>
	                      </tr>
                      {/if}
                      {section name=current loop=$confirmationlist}
                      <tr>
                        <td width="10">{$pixel_trans_10_1}</td>
                        <td class="main">{$confirmationlist[current].title}</td>
                        <td width="10">{$pixel_trans_10_1}</td>
                        <td class="main">{$confirmationlist[current].field}</td>
                      </tr>
                      {/section}
                    </table>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        {/if}
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
            </table>
          </td>
        </tr>
        {$checkout_confirmation_form}
        <tr>
          <td align="left" class="main">
            <!-- beginning checkBOX //-->
            <div class="checkoutBoxBlock"> {$terms_checkbox}
              {$seo_link}
              <div class="clearer"></div>
            </div>
            {if $is_terms_error} <br />
            <br />
            <font color="red"><em>{$terms_error}</em></font> <br>
            <br>
            {/if}
            <!-- ending checkBOX //-->
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td class="cont_abst" align="left">
            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="checkout_area">
              <tr>
                <td>
                  <div class="checkoutFrame">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
                      <tr>
                        <td width="{if $sendto != false}70%{else}100%{/if}" valign="top">

                          <!-- new -->
                          <table border="0" width="100%" cellspacing="0" cellpadding="5" class="order_details_0">
                            {if $is_multiple_tax_groups}
                            <tr>
                              <td></td>
                              <td class="main"><b>{$smarty.const.HEADING_PRODUCTS}</b> <a href="{$shipping_card_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a></td>
                              <td class="smallText" align="right"><b>{$smarty.const.HEADING_TAX}</b></td>
                              <td class="smallText" align="right">&nbsp;</td>
                              <td class="smallText" align="right"><b>{$smarty.const.TABLE_HEADING_PRICE}</b></td>
                              <td align="right"><span class="smallText"><b>{$smarty.const.HEADING_TOTAL}</b></span></td>
                            </tr>
                            {else}
                            <tr>
                              <td colspan="3"><b>{$smarty.const.HEADING_PRODUCTS}</b> <a href="{$shipping_card_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a></td>
                              <td class="main" align="right">&nbsp;</td>
                              <td class="main" align="right"><b>{$smarty.const.TABLE_HEADING_PRICE}</b></td>
                              <td align="right"><span class="main"><b>{$smarty.const.TABLE_HEADING_TOTAL}</b></span></td>
                              <td></td>
                            </tr>
                            {/if}
                            {section name=current loop=$productslist}
                            <tr>
                              <td>{$productslist[current].qty}&nbsp;x</td>
                              <td class="main" align="left" valign="top"> {if $productslist[current].image neq ''} <img src="{$productslist[current].image}" class="checkout_img"> {/if} </td>
                              <td class="main" valign="top" align="left">
                                <div style="width:148px;"> <a class="viewd" rel="{$productslist[current].id}" title="{$VIEW_PROD_DESC_CONFIRM}">{$productslist[current].name}</a> {if $smarty.const.STOCK_CHECK == 'true'}
                                  {$productslist[current].products_check_stock}
                                  {/if}
                                  {section name=current_attr loop=$productslist[current].attributes} <br>
                                  <small>&nbsp;<i> - {$productslist[current].attributes[current_attr].option}: {$productslist[current].attributes[current_attr].value}</i></small> {/section} </div>
                              </td>
                              <td class="main" valign="top" align="right">{if $is_multiple_tax_groups}{$productslist[current].tax}%{/if}</td>
                              <td class="main" align="right" valign="top" nowrap>{$productslist[current].price_single}</td>
                              <td class="main" align="right" valign="top" nowrap>{$productslist[current].price_common}</td>
                            </tr>
                            {/section}
                          </table>
                          <!-- new -->

                        </td>
                      </tr>
                    </table>
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        {if $CHECKOUT_ALTERNATIVE_PRODUCT}
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td align="left">
            <div class="checkoutFrame"> {$CHECKOUT_ALTERNATIVE_PRODUCT } </div>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        {/if}
        <tr>
          <td align="left">
            <div class="checkoutFrame">
              <table border="0" width="100%" cellspacing="0" cellpadding="10" class="infoBox order_details_0">
                <tr>
                  <td width="30%" valign="top">
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="main">&nbsp; </td>
                      </tr>
                      <tr>
                        <td class="main">&nbsp;</td>
                      </tr>
                      <tr>
                        <td class="main">&nbsp; </td>
                      </tr>
                    </table>
                  </td>
                  <td width="70%" valign="top" align="right"> {if $smarty.const.MODULE_ORDER_TOTAL_INSTALLED}
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td colspan="2"> {$pixel_trans_100_1} </td>
                      </tr>
                      {$order_total_modules}
                    </table>
                    {/if} </td>
                </tr>

				<tr><td colspan="2">
	            	{$pixel_trans_100_1}<br><br>
	            </td></tr>
          		<tr><td colspan="2">
          			{$ORDER_CONFIRMATION_ADDITIONAL_COSTS}
          		</td></tr>
				{if $payment_method eq 'Nachnahme'}
          			<tr><td colspan="2">
          				{$confirmation_title}<br><br>
       				</td></tr>
       			{/if}	
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        {if $is_comments}
        <tr>
          <td class="main" align="left">
            <div class="checkoutFrameHead"> <b>{$smarty.const.HEADING_ORDER_COMMENTS}</b> <a href="{$checkout_payment_href}"><span class="orderEdit">({$smarty.const.TEXT_EDIT})</span></a> </div>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td align="left">
            <div class="checkoutFrame">
              <table border="0" width="100%" cellspacing="0" cellpadding="10" class="infoBox">
                <tr>
                  <td>
                    <table border="0" width="100%" cellspacing="0" cellpadding="0">
                      <tr>
                        <td class="main"> {$comments} {$comments_hidden} </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </div>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        {/if}
        <tr>
          <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              {if $button_iframe}
              {$button_iframe}
              {/if}

              {if $foxrate_checkbox}
              <tr>
                <td align="left" class="main">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" class="main">
                  <!-- beginning checkBOX Foxrate//-->
                  <div class="foxratebox"> {$foxrate_checkbox}
                  </div>
                  <!-- ending checkBOX Foxrate//-->
                </td>
              </tr>
              {/if}

              {if $MONEYBOOKER_NOTE_TEXT_CONFIRM}
              <tr>
                <td align="left" valign="middle">
                  <div> {* $MONEYBOOKER_NOTE_TEXT_CONFIRM *} </div>
                </td>
              </tr>
              {/if}

              <tr>
                <td width="100%" align="rigth" valign="middle" id="confirmation_button">
                  <div id="confirmt" style="display: block;"> {$button_confirm_order_img_button}
                    {* $test_button *} </div>
                  <div id="confirmed" style="display: none; height: 40px;"> {$button_wait_img_button} </div>
                </td>
              </tr>
              <tr>
                <td align="left" class="main"> {$payment_modules_process_button} </td>
              </tr>
              <tr>
                <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
              </tr>
              {literal}
              <script typw="text/javscript">
              var dies = document.getElementById('confirmation_button');
              dies.style.display = 'block';
              var das = document.getElementById('confirmt');
              das.style.display = 'block';
              var jenes = document.getElementById('terms');
              jenes.style.display = 'inline-block';
              //var anderes = document.getElementById('moneyfr');
              //anderes.style.display = 'none';
              //var pay = document.getElementById('payment');
              //pay.style.display = 'none';

              function setframeM()
              {
              //var anderes = document.getElementById('moneyfr');
              //    anderes.height = '450';
              var pay = document.getElementById('payment');
              pay.style.height = '450px';

              var dies = document.getElementById('confirmation_button');
              dies.style.display = 'none';
              var das = document.getElementById('confirmt');
              das.style.display = 'block';
              var jenes = document.getElementById('terms');
              jenes.style.display = 'none';
              return false;

              }
              </script>
              {/literal}
              {if !empty($temporderid_hidden)}
              <tr>
                <td>{$temporderid_hidden}</td>
              </tr>
              {/if}
              <tr>
                <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
              </tr>
              {$CHECKOUT_TEXTS_HTML}

                  </form>

            </table>
          </td>
        </tr>
        <tr>
          <td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
        </tr>
        <tr>
          <td>
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
              <tr>
                <td width="25%">
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="50%" align="right">{$pixel_trans_1_5}</td>
                      <td width="50%">{$pixel_trans_100_1}</td>
                    </tr>
                  </table>
                </td>
                <td width="25%">{$pixel_trans_100_1}</td>
                <td width="25%">
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="50%">{$pixel_trans_100_1}</td>
                      <td>{$checkout_bullet_img}</td>
                      <td width="50%">{$pixel_trans_100_1}</td>
                    </tr>
                  </table>
                </td>
                <td width="25%">
                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="50%">{$pixel_trans_100_1}</td>
                      <td width="50%">{$pixel_trans_1_5}</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" width="25%" class="checkoutBarFrom"><a href="{$checkout_shipping_href}" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></td>
                <td align="center" width="25%" class="checkoutBarFrom"><a href="{$checkout_payment_href}" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></td>
                <td align="center" width="25%" class="checkoutBarCurrent">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</td>
                <td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_FINISHED}</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
{$smarty.const.AFTER_CONTENT}