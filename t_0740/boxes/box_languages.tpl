<div class="drop-box">
{if $arr_languagebox}
    <a href="#" class="dropdown-toggles"  data-toggle="dropdown">{$arr_languagebox.language.name}
        <ul class="box lang" role="menu" aria-labelledby="dLabel">
            {foreach from=$arr_languagebox.items key=key item=language}
		        {if ($languageview neq $language.directory)}
		        	<li><a role="menuitem" tabindex="-1" href="{$language.link}">{$language.name}</a></li>
		        {/if}
	        {/foreach}
        </ul>
    </a>
{/if}
</div>
{literal}
    <script type="text/javascript">
        $(function(){
            $('.drop-box').click(function() {
                $('ul.box.lang').slideToggle( "fast" );
            });
        });
    </script>
{/literal}