{if $arr_bestsellersbox == ''}
    <div class="widget">
        <div class="title1">
            <h1>{$arr_whatsnewbox.title}</h1>
            <hr>
        </div>
        <div class="popular_items">
            {foreach from=$arr_whatsnewbox.products key=key item=product name=productpreviewitems}
                <div class="popular_items_div">
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <div class="recent_post_img">
                        <a href="{$product.link}">
                            <div class="image_sm" style="background-image:url('{$src}')"></div>
                        </a>
                    </div>
                    <div class="overflow-top-best">
                        <a href="{$product.link}">{$product.name}</a>
                    </div>
                    <div class="price1">
                        {$product.preisneu|regex_replace:"/[()]/":""}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{else}
    <div class="widget">
        <div class="title1">
            <h1>{$arr_bestsellersbox.title}</h1>
            <hr>
        </div>
        <div class="popular_items">
            {foreach from=$arr_bestsellersbox.products key=key item=product name=productpreviewitems}
                <div class="popular_items_div">
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <div class="recent_post_img">
                        <a href="{$product.link}">
                            <div class="image_sm" style="background-image:url('{$src}')"></div>
                        </a>
                    </div>
                    <div class="overflow-top-best">
                        <a href="{$product.link}">{$product.name}</a>
                    </div>
                    <div class="price1">
                        {$product.preisneu|regex_replace:"/[()]/":""}
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
{/if}