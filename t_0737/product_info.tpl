{include file="header.tpl"}
<div class="container">
    <div class="col1-layout">
        <div class="main">
            <div class="sixteen columns">
                <div class="breadcrumb">
                    {$breadcrumb|replace:'|':''}
                </div>
            </div>
            {if $pAnfrage}
                {if $pAnfrage == 1}
                    {include file="modules/modul_additional_price_query.tpl"}
                {else}
                    {include file="modules/modul_additional_price_query.tpl"}
                {/if}
            {else}
                {if $arr_product_info.product_check == true}
                    <div class="col-main sixteen columns">
                        <div id="messages_product_view"></div>
                        <div class="product-view">
                            <div itemscope itemtype="http://data-vocabulary.org/Product" class="product-essential">
                                <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post">
                                    <div class="product-img-box">
                                        <p class="product-image">
                                            {if $arr_product_info.image_source}
                                                {if $arr_product_info.image_linkLARGE}
                                                    <a href="{$arr_product_info.image_linkLARGE}" class='cloud-zoom' id='zoom1' rel="position:'right',showTitle:0,titleOpacity:0.5,lensOpacity:0.5,adjustX: 10,adjustY:-4">
                                                        <img src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" style="width:280px">
                                                    </a>
                                                {else}
                                                    <a href="{$arr_product_info.image_link}" class='cloud-zoom' id='zoom1' rel="position:'right',showTitle:0,titleOpacity:0.5,lensOpacity:0.5,adjustX: 10,adjustY:-4">
                                                        <img src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" style="width:280px">
                                                    </a>
                                                {/if}
                                            {else}
                                                <a href="#" class="mfp-image">
                                                    <img src="{$imagepath}/empty-album.png" style="width:130px;height:130px;" style="width:280px" />
                                                </a>
                                            {/if}
                                        </p>

                                        <div class="more-views">
                                            <ul>
                                                <li>
                                                    {if $arr_product_info.image_source}
                                                        {if $arr_product_info.image_linkLARGE}
                                                            <a href="{$arr_product_info.image_linkLARGE}" class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{$arr_product_info.image_linkLARGE}'">
                                                                <img src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" width="80">
                                                            </a>
                                                        {else}
                                                            <a href="{$arr_product_info.image_link}" class='cloud-zoom-gallery' rel="useZoom: 'zoom1', smallImage: '{$src}'">
                                                                <img src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" width="80">
                                                            </a>
                                                        {/if}
                                                    {else}
                                                        <a href="#" class="mfp-image">
                                                            <img src="{$imagepath}/empty-album.png" width="80" />
                                                        </a>
                                                    {/if}
                                                </li>
                                                {include file="modules/modul_additional_images.tpl"}
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-shop">
                                        <div class="clearfix">
                                            <div class="left_info">
                                                <div class="product-name">
                                                    <h1 itemprop="name">{$arr_product_info.products_name}</h1>
                                                </div>
                                                <div class="right_info">
                                                    <div class="price-box">
                                                        <span class="regular-price pull-left">
                                                            <span class="price">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</span>
                                                        </span>
                                                        <div class="clearfix"></div>
                                                        {if $arr_product_info.arr_products_options}
                                                            <div class="form-group">
                                                                <label class="control-label">
                                                                    {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                                                </label>
                                                                {if $arr_product_info.product_options_javascript}
                                                                    {$arr_product_info.product_options_javascript}
                                                                {/if}
                                                                <table>
                                                                    {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                                                        <tr>
                                                                            <td>{$productoption.name}:</td>
                                                                            <td>{$productoption.pulldown}</td>
                                                                        </tr>
                                                                    {/foreach}
                                                                </table>
                                                            </div>
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $opt_count > 0}
                                                            {if $var_options_array}
                                                                <input type="hidden" id="master_id" value="{$master_id}">
                                                                <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                                                <input type="hidden" id="cPath" value="{$cPath}">
                                                                <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                                                {if $Prodopt}
                                                                    <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                                                {/if}
                                                                {if $prod_hidden}
                                                                    {foreach from=$prod_hidden key=key item=hidden_input}
                                                                        {$hidden_input}
                                                                    {/foreach}
                                                                {/if}
                                                                {if $select_ids}
                                                                    {foreach from=$select_ids key=key item=select_input}
                                                                        {$select_input}
                                                                    {/foreach}
                                                                {/if}
                                                                {if $javascript_hidden}
                                                                    {$javascript_hidden}
                                                                {/if}
                                                                {foreach from=$var_options_array key=key item=options}
                                                                    <label>{$options.title}:</label>
                                                                    {$options.dropdown}
                                                                {/foreach}
                                                            {/if}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $arr_product_info.base_price}
                                                            {$arr_product_info.base_price}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $arr_product_info.shipping_value gt 0}
                                                            <label class="control-label pull-left">{$TEXT_SHIPPING}: </label>
                                                            <span class="pull-left" style="margin-left:5px">({$arr_product_info.shipping})</span>
                                                            <div class="clearfix"></div>
                                                        {/if}

                                                        {*---------------------------------------------*}
                                                        {if $arr_product_info.products_uvp}
                                                            {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                                            {$arr_product_info.products_uvp}
                                                            {$arr_product_info.TEXT_YOU_SAVE}:
                                                            {$arr_product_info.diff_price}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $count_bestprice gt 0}
                                                            {$staffelpreis_hinweis}

                                                            {foreach from=$bestprice_arr key=key item=bestprice}
                                                                {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                                                {$bestprice.ausgabe_stueck}
                                                                {$bestprice.ausgabe_preis}
                                                            {/foreach}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $PRICE_QUERY_ON eq 'true'}
                                                            <div class="price_inquiry pull-left">
                                                                {$PRICE_QUERY}:
                                                                <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $PRODUCTS_QUERY_ON eq 'true'}
                                                            <div class="price_inquiry pull-left">
                                                                {$PRODUCTS_INQUIRY_TEXT}
                                                                <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        {/if}

                                                        {*-------------------------------------------*}

                                                        {if count($arr_product_info.availability)}

                                                            {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                                            {$arr_product_info.availability.text}

                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if count($arr_product_info.delivery_time)}
                                                            {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                                            {$arr_product_info.delivery_time.text} *
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $arr_product_info.text_date_available}
                                                            &nbsp;
                                                            {$arr_product_info.text_date_available}
                                                        {/if}
                                                        {*-------------------------------------------*}
                                                        {if $arr_product_info.products_url}
                                                            &nbsp;
                                                            {$arr_product_info.products_url}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $SANTANDER_LOANBOX}
                                                            <span id="santandertitle_pinfo">
                                                                {$SANTANDER_LOANBOX_TITLE}:
                                                            </span>
                                                            &nbsp;
                                                            {$SANTANDER_LOANBOX}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $PRODUCTS_WEIGHT_INFO}
                                                            {if $arr_product_info.products_weight > 0}
                                                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                                                {$arr_product_info.products_weight}
                                                                &nbsp;
                                                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                                            {/if}
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if count($arr_product_info.products_model)}
                                                            <label class="pull-left">Model: </label>
                                                            <span class="pull-left"> {$arr_product_info.products_model}</span>
                                                            <br />
                                                        {/if}

                                                        {*-------------------------------------------*}
                                                        {if $arr_manufacturer_info.manufacturers_name}
                                                            <label class="pull-left">{$manufacturer}: </label>
                                                            {if $arr_manufacturer_info.manufacturers_image neq ""}
                                                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                                    <a href="{$arr_manufacturer_info.manufacturers_url}">
                                                                        <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                                                    </a>
                                                                {else}
                                                                    <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                                                {/if}
                                                            {else}
                                                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                                    <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                                                {else}
                                                                    <span class="pull-left"> {$arr_manufacturer_info.manufacturers_name}</span>
                                                                {/if}
                                                            {/if}
                                                        {/if}
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="add-to-box">
                                                        <div class="add-to-cart">
                                                            <p class="text-center">
                                                                {if $LOAD_CLIENT}
                                                                    <button type="submit" title="Add to Cart" class="button btn-cart" style="width:100%">
                                                                        <span>
                                                                            <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                                        </span>
                                                                    </button>
                                                                    <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                                                                {else}
                                                                    <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button btn-cart disabled" style="width:100%" disabled>
                                                                        <span>
                                                                            <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                                        </span>
                                                                    </button>
                                                                {/if}
                                                            </p>
                                                            <br />
                                                            <span class='ajax_loader' style='display:none'>
                                                                <img src='/templates/t_0737/images/ajax-loader.gif' />
                                                            </span>
                                                            <span id='ajax_loader' style='display:none'>
                                                                <img src='/templates/t_0737/images/opc-ajax-loader.gif' />
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="clearer"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-8">


                                                {*---------------------------------------------*}
                                                {if $widget_view && $widget_view eq 1}
                                                    <div id="widget_views"></div>
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearer"></div>
                                </form>
                                <ul class="product-tabs">
                                    <li id="product_tabs_description" class=" active first">
                                        <a href="#">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                                    </li>
                                </ul>
                                <div class="product-tabs-content" id="product_tabs_description_contents">
                                    <div class="std">
                                        {$arr_product_info.products_description}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {if $foxrate && $foxrate->GetVariable('counts') != 0}
                        {*include file="../0_source/modules/modul_foxrate_products_info.tpl"*}
                    {else}
                    {/if}

                    {include file="modules/modul_xsell_products.tpl"}
                    <br>
                    {include file="modules/modul_also_purchased_products.tpl"}
                {else}
                    {$TEXT_PRODUCT_NOT_FOUND}
                {/if}
            {/if}
        </div>
    </div>
</div>
{include file="footer.tpl"}