{if $arr_modul_also_purchased_products}
 <div class="product-info-also-purchased-container clearfix">    
<h4 class="text-center">{$arr_modul_also_purchased_products.title}</h4>
<div class="modul-also-purchased-products">               
        <div class="slider-also-purchased">          
          {foreach from=$arr_modul_also_purchased_products.products key=key item=product}          
             <div class="slide">
                <a href="{$product.link}" rel="{$product.id}" class="thumbnail">
 
                    {if $product.image} 
                    {php}
                     {$product = $this->get_template_vars('product');}
                     {$pattern_src = '/src="([^"]*)"/';}
                     {$pattern_width = '/width="([^"]*)"/';}
                     {$pattern_height = '/height="([^"]*)"/';}
                     {preg_match($pattern_src, $product['image'], $matches);}
                     {$src = $matches[1];}
                     {preg_match($pattern_width, $product['image'], $matches);}
                     {$width = $matches[1];}
                     {preg_match($pattern_height, $product['image'], $matches);}
                     {$height = $matches[1];}
                     {$this->assign('src', $src);}
                     {$this->assign('img_width', $width);}
                     {$this->assign('img_height', $height);}
                    {/php}
                    {else}
                      {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                 
                    <div class="img-canvas" style="width:90px;height:90px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>                   

                </a>
            </div>
         {/foreach}          
        </div>   
</div>  
</div>
{/if}