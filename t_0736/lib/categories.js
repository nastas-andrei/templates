jQuery(document).ready(function () {
    jQuery.post('categories/tree', function (data) {
        jQuery('#categories-box').empty().append(data);
    });
    setTimeout(function () {
        jQuery(".mega-content-wrap:last").css("right", "0");
    }, 1000);

    var container = jQuery('#sns_menu .container');
    setTimeout(function () {
        jQuery('.sns-megamenu-wrap').find('li').each(function () {
            var menucontent = jQuery(this).find(".mega-content-wrap:first");
            var li = jQuery(this);
            if ((container.outerWidth() + container.offset().left) < (li.offset().left + menucontent.outerWidth())) {
                menucontent.css({"left": (container.outerWidth() - menucontent.outerWidth() ) + "px"});
            }
        });
    }, 500);

});
