{if $arr_loginbox}
<ul class="nav navbar-nav" id="header-login">
    {if $arr_loginbox.boxtyp == "myaccount"}
        <li><a href="{$arr_loginbox.items.4.target}"><i class="fa fa-sign-out"></i> {$arr_loginbox.items.4.text}</a></li>
    {else}
        <li><a href="{$domain}/login.php"><i class="fa fa-lock"></i> {$arr_smartyconstants.LOGIN_BOX_REGISTER}</a></li>
    {/if}
        <li><a href="shopping_cart.php"><i class="fa fa-shopping-cart"></i> {$arr_shopping_cart.title|strip_tags:false}</a></li>
        <li class="last" ><a href="{$arr_topmenu.2.link}">{$arr_topmenu.2.name}</a></li>
</ul>
{/if}