
{if $arr_modul_previewproducts.products}  




<!-- Featured -->
          
            <div class="col-lg-12 col-sm-12">
                <span class="title">FEATURED PRODUCTS</span>
              </div>{foreach from=$arr_modul_previewproducts.products key=key item=product name=productpreviewitems}
        {if $smarty.foreach.productpreviewitems.index < 6} 
              <div class=" col-lg-4 col-md-4 col-sm-4 hero-feature text-center">


                  <div class="thumbnail">
                    {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                  {/if}
                    <a href="{$product.link}" class="link-p">
                        <div style="width: 230px; height: 280px; margin: 3px auto 0 auto; background-position: center; background-size: cover; background-image: url('{$src}')"></div>                  
                        <!-- <img src="{$src}" alt="" >     -->                                       
                  </a>
                    </a>
                      <div class="caption prod-caption">

                          <h4><a href="{$product.link}">{$product.name|truncate:150:"...":true}</a></h4>
                          <p>
                            <div class="btn-group">
                             <a href="#" class="btn btn-default">{$product.preisneu|regex_replace:"/[()]/":""}</a>

                             
                              <a href="{$product.buy_now_link}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
                            </div>
                          </p>
                      </div>
                  </div>
   {/if} </div>
      {/foreach}               
         
{/if}
