<div class="thumbnails clearfix">
    <div id="services" class="owl-carousel owl-theme" style="opacity: 1; display: block;">
        {foreach from=$arr_additional_images.products key=index item=product_additional name="addimages"}
            {if $product_additional.image_src}
                {php}
                    {$product_additional = $this->get_template_vars('product_additional');}
                    {$pattern_src = '/src="([^"]*)"/';}
                    {preg_match($pattern_src, $product_additional['image_src'], $matches);}
                    {$src = $matches[1];}
                    {$this->assign('src', $src);}
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
            {/if}
            <div class="owl-item" style="width: 133px;">
                <div class="item">
                    <div class="entry">
                        <img alt="" class="img-responsive" src="{$src}">

                        <div class="magnifier">
                            <div class="buttons">
                                <a href="{$product_additional.popup_img}" class="sf" title=""
                                   data-gal="prettyPhoto[product-gallery]"><span class="fa fa-search"></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>