<div class="whitewrapper bordertop">
    <div class="container">
        <div class="row">
            <div class="half-width clearfix">
                <div id="content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="bs-example bs-example-tabs">
                        <div class="my-account">
                            <h1 class="short">
                                <strong>{$smarty.const.BOX_HEADING_LOGIN_BOX_MY_ACCOUNT }</strong>
                            </h1>
                            {if $account_message_exists}
                                <p>{$account_message}</p>
                            {/if}
                        </div>
                        <ul class="nav nav-tabs" role="tablist" id="myTab">
                            <li class="active"><a href="#account" role="tab" data-toggle="tab">{$smarty.const.MY_ACCOUNT_TITLE}</a></li>
                            <li><a href="#comands" role="tab" data-toggle="tab">{$smarty.const.MY_ORDERS_TITLE}</a></li>
                            {if $is_newsletters_block}
                                <li><a href="#notifications" role="tab" data-toggle="tab">{$smarty.const.EMAIL_NOTIFICATIONS_TITLE}</a></li>
                            {/if}
                            {if $is_use_points_system}
                                <li><a href="#messages" role="tab" data-toggle="tab">{$smarty.const.POINTS_ACCOUNT_TITLE}</a></li>
                            {/if}
                            {if $count_customer_orders > 0}
                                <li><a href="#customer" role="tab" data-toggle="tab">{$smarty.const.OVERVIEW_TITLE}</a></li>
                            {/if}
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="account">
                            <p>
                                <a href="{$account_edit_href}"><i class="fa fa-user"></i> {$smarty.const.MY_ACCOUNT_INFORMATION}</a>
                            </p>
                            <p>
                                <a href="{$address_book_href}"><i class="fa fa-home"></i> {$smarty.const.MY_ACCOUNT_ADDRESS_BOOK}</a>
                            </p>
                            <p>
                                <a href="{$account_password_href}"><i class="fa fa-lock"></i> {$smarty.const.MY_ACCOUNT_PASSWORD}</a>
                            </p>
                        </div>
                        <div class="tab-pane" id="comands">
                            <p>
                                <a href="{$account_hystory_href}"><i class="fa fa-shopping-cart"></i> {$smarty.const.MY_ORDERS_VIEW}</a>
                            </p>
                        </div>
                        <div class="tab-pane" id="notifications">
                            <p>
                                <a href="{$account_newsletters_href}">
                                    <i class="fa fa-envelope"></i> {$smarty.const.EMAIL_NOTIFICATIONS_NEWSLETTERS}
                                </a>
                            </p>
                        </div>
                        <div class="tab-pane" id="messages">
                            <p>
                                <a href="{$my_points_url}"> {$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS}</a>
                            </p>
                            <p>
                                <a href="{$my_points_url_umpf}"> {$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS_COUPON}</a>
                            </p>
                        </div>
                        <div class="tab-pane" id="customer">
                            <a href="{$account_hystory_href}" style="text-decoration:underline">
                                {$smarty.const.OVERVIEW_SHOW_ALL_ORDERS}
                            </a>
                            {$smarty.const.OVERVIEW_PREVIOUS_ORDERS}
                            <table class="table account-history-orders table-hover table-condensed table-bordered">
                                <tbody>
                                {section name=current loop=$orderslist}
                                    <tr onClick="document.location.href='{$orderslist[current].order_history_info_href}'">
                                        <td><p>{$orderslist[current].date_purchased}</p></td>
                                        <td><p>#{$orderslist[current].orders_id}</p></td>
                                        <td><p>{$orderslist[current].order_name}</p></td>
                                        <td><p>{$orderslist[current].order_country}</p></td>
                                        <td><p>{$orderslist[current].orders_status_name}</p></td>
                                        <td><p>{$orderslist[current].order_total}</p></td>
                                        <td>
                                            <a href="{$orderslist[current].order_history_info_href}">{$smarty.const.SMALL_IMAGE_BUTTON_VIEW}</a>
                                        </td>
                                    </tr>
                                {/section}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
