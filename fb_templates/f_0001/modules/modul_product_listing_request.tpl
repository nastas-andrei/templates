{if $arr_modul_productlisting.products}  
  {foreach from=$arr_modul_productlisting.products key=key item=product name=modpp}
    <div class="media ajax-loaded">         
              {if $product.image} 
              {php}
               {$product = $this->get_template_vars('product');}
               {$pattern_src = '/src="([^"]*)"/';}
               {$pattern_width = '/width="([^"]*)"/';}
               {$pattern_height = '/height="([^"]*)"/';}
               {preg_match($pattern_src, $product['image'], $matches);}
               {$src = $matches[1];}
               {preg_match($pattern_width, $product['image'], $matches);}
               {$width = $matches[1];}
               {preg_match($pattern_height, $product['image'], $matches);}
               {$height = $matches[1];}
               {$this->assign('src', $src);}
               {$this->assign('img_width', $width);}
               {$this->assign('img_height', $height);}
              {/php}
              {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
              {/if}
              
            <a href="{$product.link}" class="pull-left">                    
               <div class="img-canvas" style="width:168px;height:168px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
            </a>
                    
            <div class="media-body">                         
                <a href="{$product.link}" class="media-heading">
                  {$product.products_name|truncate:150:"...":true}
                </a>
                 <p class="product-category">&bull; {$cat_name}</p>
                <p class="product-price">
                  {if $product.oldprice > 0}
                    <span class="product-old-price">{$product.oldprice}</span>
                   {/if}   
                    <span class="product-new-price">{$product.newprice|regex_replace:"/[()]/":""}</span>  
                    <span class="product-new-price" style="display:block">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                </p>
                <p>
                   <iframe src="//www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21&amp;appId=341633249301316" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:21px;" allowTransparency="true"></iframe>
                </p>                                                                   
            </div>
    </div>
  {/foreach}
{/if}