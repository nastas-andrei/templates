{if $arr_languagebox}
	<div class="muted">&copy; {$smarty.const.HEAD_TITLE_TAG_DEFAULT} 2013</div>
	<ul class="inline">
	{foreach from=$arr_languagebox.items key=key item=language}	
		{if ($languageview neq $language.directory)}		
			<li><span class="muted">&#183;</span> <a href="{$language.link}">{$language.name}</a></li>
		{/if}
	{/foreach}
	</ul>
{/if}