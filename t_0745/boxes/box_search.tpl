{if $arr_searchbox}
    {$arr_searchbox.form}
        <div class="search_box">
            <input type="text" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}" value="{$arr_searchbox.inputtextfield}"/>
            <button class="fa fa-search form-control-feedback search" type="submit"></button>
            {$arr_searchbox.hidden_sessionid}
        </div>
    </form>
{/if}

