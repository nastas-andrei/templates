{include file="header.tpl"}
<section class="blog-wrapper">
    <div class="container">
        {if $arr_product_info.product_check == true}
            <div class="shop_wrapper col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="general_row">
                    <div class="shop-left shop_item col-lg-6">
                        <div class="entry">
                            <a {if $arr_additional_images.products|@count > 0}data-gal="prettyPhoto[product-gallery]" {else} data-gal="prettyPhoto"{/if} href="{$arr_product_info.image_link}" class="sf">
                                {if $arr_product_info.image_source}
                                    {if $arr_product_info.image_linkLARGE}
                                        <div class="image_view_product"
                                             style="background-image:url('{$arr_product_info.image_link}')"></div>
                                    {else}
                                        <div class="image_view_product"
                                             style="background-image:url('{$arr_product_info.image_link}')"></div>
                                    {/if}
                                {else}
                                    <div class="image_view_product" style="background-image:url('{$imagepath}/empty-album.png')"></div>
                                {/if}
                            </a>
                            {*<div class="magnifier">*}
                            {*<div class="buttons">*}
                            {*<a {if $arr_additional_images.products|@count > 0}data-gal="prettyPhoto[product-gallery]" {else} data-gal="prettyPhoto"{/if}*}
                            {*href="{$arr_product_info.image_link}"class="sf">*}
                            {*<i class="fa fa-search"></i>*}
                            {*</a>*}
                            {*<a class="sg" rel="bookmark" href="{$arr_product_info.shopping_card_link}">*}
                            {*<span class="fa fa-shopping-cart"></span>*}
                            {*</a>*}
                            {*</div>*}
                            {*</div>*}
                        </div>
                        {include file="modules/modul_additional_images.tpl"}
                    </div>
                    <div class="shop-right col-lg-6">
                        <div class="title">
                            <h2>{$arr_product_info.products_name}</h2>
                            <span class="price">{if $arr_product_info.preisalt > 0}{$arr_product_info.preisalt}{/if}</span>
                            <span class="price" id="pricefield">
                                <span id="products_price">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</span>
                            </span>
                        </div>
                        <div>
                            {if $arr_product_info.arr_products_options}
                                <div class="form-group">
                                    <label class="control-label">
                                        {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                    </label>
                                    {if $arr_product_info.product_options_javascript}
                                        {$arr_product_info.product_options_javascript}
                                    {/if}
                                    <table>
                                        {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                            <tr>
                                                <td>{$productoption.name}:</td>
                                                <td>{$productoption.pulldown}</td>
                                            </tr>
                                        {/foreach}
                                    </table>
                                </div>
                            {/if}
                            {*-------------------------------------------*}
                            {if $opt_count > 0}
                                {if $var_options_array}
                                    <input type="hidden" id="master_id" value="{$master_id}">
                                    <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                    <input type="hidden" id="cPath" value="{$cPath}">
                                    <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                    {if $Prodopt}
                                        <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                    {/if}
                                    {if $prod_hidden}
                                        {foreach from=$prod_hidden key=key item=hidden_input}
                                            {$hidden_input}
                                        {/foreach}
                                    {/if}
                                    {if $select_ids}
                                        {foreach from=$select_ids key=key item=select_input}
                                            {$select_input}
                                        {/foreach}
                                    {/if}
                                    {if $javascript_hidden}
                                        {$javascript_hidden}
                                    {/if}
                                    {foreach from=$var_options_array key=key item=options}
                                        <label>{$options.title}:</label>
                                        {$options.dropdown}
                                    {/foreach}
                                {/if}
                            {/if}
                            {*-------------------------------------------*}
                            {if $arr_product_info.base_price}
                                {$arr_product_info.base_price}
                            {/if}
                            {*-------------------------------------------*}
                            {if $arr_product_info.shipping_value gt 0}
                                <label class="control-label">{$TEXT_SHIPPING}:</label>
                                ({$arr_product_info.shipping})
                            {/if}
                            {*---------------------------------------------*}
                            {if $arr_product_info.products_uvp}
                                {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                {$arr_product_info.products_uvp}
                                {$arr_product_info.TEXT_YOU_SAVE}:
                                {$arr_product_info.diff_price}
                            {/if}
                            {*-------------------------------------------*}
                            {if $count_bestprice gt 0}
                                {$staffelpreis_hinweis}
                                {foreach from=$bestprice_arr key=key item=bestprice}
                                    {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                    {$bestprice.ausgabe_stueck}
                                    {$bestprice.ausgabe_preis}
                                {/foreach}
                            {/if}
                            {*-------------------------------------------*}

                            {if $PRICE_QUERY_ON eq 'true'}
                                <div class="clearfix"></div>
                                <div style="font-size:14px;">
                                    {$PRICE_QUERY}:
                                    <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                </div>
                            {/if}

                            {*-------------------------------------------*}
                            {if $PRODUCTS_QUERY_ON eq 'true'}
                                <div class="clearfix"></div>
                                <div style="font-size:14px;font-weight: bold">
                                    {$PRODUCTS_INQUIRY_TEXT}: <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                </div>
                            {/if}
                            <div class="clearfix"></div>
                            {*-------------------------------------------*}
                            {if count($arr_product_info.availability)}
                                {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                {$arr_product_info.availability.text}
                            {/if}
                            {*-------------------------------------------*}
                            {if count($arr_product_info.delivery_time)}
                                {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                {$arr_product_info.delivery_time.text} *
                            {/if}
                            {*-------------------------------------------*}
                            {if $arr_product_info.text_date_available}
                                &nbsp;
                                {$arr_product_info.text_date_available}
                            {/if}
                            {*-------------------------------------------*}
                            {if $arr_product_info.products_url}
                                &nbsp;
                                {$arr_product_info.products_url}
                            {/if}
                            {*-------------------------------------------*}
                            {if $SANTANDER_LOANBOX}
                                <span id="santandertitle_pinfo">
                                                                {$SANTANDER_LOANBOX_TITLE}:
                                                            </span>
                                &nbsp;
                                {$SANTANDER_LOANBOX}
                            {/if}
                            {*-------------------------------------------*}
                            {if $PRODUCTS_WEIGHT_INFO}
                                {if $arr_product_info.products_weight > 0}
                                    {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                    {$arr_product_info.products_weight}
                                    &nbsp;
                                    {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                {/if}
                            {/if}
                            {*-------------------------------------------*}
                            {if count($arr_product_info.products_model)}
                                <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">Model: </span>
                                {$arr_product_info.products_model}
                                <br />
                            {/if}
                            {*-------------------------------------------*}
                            {if $arr_manufacturer_info.manufacturers_name}
                                <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">{$manufacturer}: </span>
                                {if $arr_manufacturer_info.manufacturers_image neq ""}
                                    {if $arr_manufacturer_info.manufacturers_url neq ""}
                                        <a href="{$arr_manufacturer_info.manufacturers_url}">
                                            <img src="{$arr_manufacturer_info.manufacturers_image}"
                                                 alt="{$arr_manufacturer_info.manufacturers_name}">
                                        </a>
                                    {else}
                                        <img src="{$arr_manufacturer_info.manufacturers_image}"
                                             alt="{$arr_manufacturer_info.manufacturers_name}">
                                    {/if}
                                {else}
                                    {if $arr_manufacturer_info.manufacturers_url neq ""}
                                        <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                    {else}
                                        {$arr_manufacturer_info.manufacturers_name}
                                    {/if}
                                {/if}
                            {/if}
                            {*---------------------------------------------*}
                            {if $widget_view && $widget_view eq 1}
                                <div id="widget_views"></div>
                            {/if}
                        </div>
                        {if $widget_view && $widget_view eq 1}
                            <tr>
                                <td colspan="2">
                                    <div id="widget_views"></div>
                                </td>
                            </tr>
                        {/if}
                        </table>
                        <div class="shop_meta">
                            <div class="pull-left">
                                <div class="btn-shop">
                                    <a href="{$arr_product_info.shopping_card_link}" class="btn woo_btn btn-primary">{$smarty.const.IMAGE_BUTTON_IN_CART}</a>
                                    <a href="{$arr_product_info.shopping_card_link}">
                                        <span><i class="fa fa-shopping-cart"></i></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="general_row">
                    <div id="shop_features" class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active dm-icon-effect-1"><a href="#tab1" data-toggle="tab">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab1">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    {$arr_product_info.products_description}
                                    {*{if $foxrate && $foxrate->GetVariable('counts') != 0}*}
                                    {*{else}*}
                                    {*{/if}*}
                                    {*{if $foxrate_rating}*}
                                    {*{/if}*}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {include file="modules/modul_also_purchased_products.tpl"}
                {include file="modules/modul_xsell_products.tpl"}
            </div>
        {/if}
        <div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            {include file="boxes/box_best_sellers.tpl"}
        </div>
    </div>
</section>
{include file="footer.tpl"}
