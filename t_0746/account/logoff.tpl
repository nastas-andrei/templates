<section id="logoff" class="margin-bottom">
  	<div class="container">
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {$smarty.const.TEXT_MAIN}
        </div>
        <div class="text-center">
            <a href="{$DEFAULT_URL}" class="btn btn-primary">{$smarty.const.IMAGE_BUTTON_BACK }</a>
        </div>
    </div>
</section>