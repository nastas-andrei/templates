{if $products_not_found_message}
  {$products_not_found_message}    
{/if}

{if $is_default}

  {if $smarty.const.TEXT_DEFINE_MAIN != ""}             
      {*$user_html*}
  {/if}

  {$FILENAME_FEATURED_HTML}
  
  {$UPCOMING_PRODUCTS_HTML}
  
  
  {if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}
   {$define_main_bottom}
  {/if}
          
{/if}

{if $is_nested}

      {if !empty($categories_html)} 
        {$categories_html} 
      {/if}                            
     

      {if isset($categorieslist)}         
           <div class="row-fluid subcategories-gallery">
          {section name=current loop=$categorieslist}  
            <div class="span4"> 
             <div class="thumbnail">   
                <div class="image-container text-center"> 
                <a href="{$categorieslist[current].seolink}">                          
                  
                  {if $categorieslist[current].categories_image}    
                  <img src="{$categorieslist[current].categories_image}" alt="" style="width:147px;height:147px;">
                  {else}
                  <img src="{$imagepath}/folder.png" alt="">  
                  {/if}
                </a>          
                </div>
                 </div>
                <div class="caption text-center">
                    <a href="{$categorieslist[current].seolink}"  class="product-title-link" style="display:block;height:37px;overflow:hidden;">
                    {$categorieslist[current].categories_name}                     
                    </a>    
                </div>
             
            </div>
          {/section}                   
        </div>            
      {else}
          {$FILENAME_FEATURED_HTML}
      {/if}    

      {if !empty($categories_html_footer)}
       {*$categories_html_footer*}
      {/if}     
{/if}


{if $is_products == 1}

  {$smarty.const.HEADING_TITLE}

    {if $filterlistisnotempty} 
      {*$filter_form*}  
      {$smarty.const.TEXT_SHOW}      
    {/if}                          

    {if !empty($categories_html)} 
     {$categories_html}
    {/if}

    {if !empty($manufacturers_html_header)} 
      {*$manufacturers_html_header*}    
    {/if}
        

    {if !empty($manufacturers_html_header) || isset($categorieslist)}
      {if isset($categorieslist)}    
        {if $categorieslist|@count > 0}
         <div class="row-fluid subcategories-gallery">
            {assign var=row value=0}
            {section name=current loop=$categorieslist}                                                


            <div class="span4"> 
             <div class="thumbnail" style="height:180px;">   
                <div class="image-container text-center"> 
                   <a href="{$categorieslist[current].seolink}">
                            {$categorieslist[current].CATE}
                  </a>        
                </div>
                 </div>
        
             
            </div>

            {/section}     
          </div> 

        {/if}
      {else}
        {if !empty($manufacturers_html_header)} 
            {*$tep_manufacturers_html_header*}          
        {/if}                          
      {/if}             
    {/if}        


    {if !empty($PRODUCT_LISTING_HTML)}     
      {$PRODUCT_LISTING_HTML}                 
    {/if}

    {if !empty($manufacturers_id)} 
      {*$manufacturers_html_footer*}
    {/if}

    {if !empty($categories_html_footer)}  
      {*$categories_html_footer*}
    {/if}

{else}      
        {if $is_default}
          &nbsp;
        {else}
          <p class="block">{$tep_image_category}</p> 
          <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
        {/if}
{/if}