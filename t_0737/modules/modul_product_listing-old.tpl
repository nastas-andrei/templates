  {if $arr_modul_productlisting.products}  

  {assign var=page_i value=$arr_modul_productlisting.page_info|regex_replace:'/\[[^\]]*(&gt;)\]/':'<div class="modul_pLister_next modN"></div>'}
  {assign var=page_i value=$page_i|regex_replace:'/\[(&lt;)[^\]]*\]/':'<div class="modul_pLister_previous modN"></div>'}
  {assign var=page_i value=$page_i|replace:"imagepath":$imagepath}
  {assign var=page_i value=$page_i|regex_replace:'/&nbsp;/':''}  

<!--   {assign var=nr_of_items value=$arr_modul_productlisting.products|@count}
  {assign var=arr1 value=" "|explode:$arr_modul_productlisting.count_products_info}
  {assign var=total_items value=$arr1[0]|regex_replace:"/[^0-9]+/":""}   -->

  <script type="text/javascript" src="{$templatepath}lib/infinityScroll.js"></script>
  {if $SCRIPT_NAME ne "/advanced_search_result.php"}
    {literal} 
    <script type="text/javascript">
    var category = '{/literal}{$cPath}{literal}';

    var items_per_page = 20
    var nr_of_items = {/literal}{$nr_of_items}{literal}
    var total_items = {/literal}{$total_items}{literal}
    
    var current_p = Math.ceil(parseInt(nr_of_items)/parseInt(items_per_page))

    var remained_pages = Math.ceil(parseInt(total_items)/items_per_page-current_p)+1
   
    window.addEvent('domready',function(){     
        var infScroll = new InfScroll('scroll_wrapper', {
          url: 'product_listing_request.php',
          method: 'get',
          maxPage: remained_pages,
          loadClassName: 'scrollLoad',
          gifPath:'http://www.femee.com/templates/c_0496_femee/images/loader_white.gif',
          data: {
            page: current_p+1,
            max:items_per_page,
            cPath:category
          }
        });      
      }); 
    </script>
  
 
    <script type="text/javascript">
      function goHin(das) {
      var slink = document.getElementById('sortlink').value;
      var sort = document.getElementById('sortierung').selectedIndex;
      var wert = das[sort].value;
      var max = document.getElementById('max').value;
      var link = slink + '&sort='+wert+'&max='+max;
      location.href = link;
      }
    </script>
 {/literal}
 {/if}
         <div class="row" id="scroll_wrapper">  
 
           {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            <div class="col-lg-4 col-sm-4 hero-feature text-center">
              
                <div class="thumbnail" >

                  {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                  {/if}
                  <a href="{$product.link}">  
                    <div style="width: 230px; height: 280px; margin: 3px auto 0 auto; background-position: center; background-size: cover; background-image: url('{$src}')"></div>                  
                        <!-- <img src="{$src}" alt="" >     -->                                       
                  </a>
                </div>
                <div class="caption prod-caption">
                    <a href="{$product.link}">
                        <h4 class="product-name">{$product.products_name}</h4>
                    </a>
                                 
            <div class="btn-group">

                             <a href="#" class="btn btn-default">{$product.newprice|regex_replace:"/[()]/":""}</a>
                              <a href="{$product.buy_now_link}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
             </div>                                             
                  <!-- <div class="product-price">
                    {if $product.oldprice  > 0}
                    <span class="oldprice">{$product.oldprice}</span>
                    {/if} 
                    <span>{$product.newprice|regex_replace:"/[()]/":""}</span>
                    <span class="newprice">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                  </div>
                  <div class="product-action">
                        <a href="{$product.link}" class="addcart">
                            <i class="icon-basket"></i>
                            Add to cart
                        </a>
                   </div>  -->
                
                    <!-- <div class="product-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i> 
                    </div> -->
                </div>
            </div>
       
        {/foreach}
    </div>
    {/if}
  
