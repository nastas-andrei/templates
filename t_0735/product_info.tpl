{include file="header.tpl"}
<section class="content-wrapper accountMarginBottom">
    <div class="container">
        {if $pAnfrage}
        {if $pAnfrage == 1}
            {include file="modules/modul_additional_price_query.tpl"}
        {else}
            {include file="modules/modul_additional_price_query.tpl"}
        {/if}
        {else}
        {if $arr_product_info.product_check == true}
        <div class="row">
            <div class="col-xs-4">
                <div class="content-box">
                    <!-- Begin Img Thumbnail -->
                    <div class="sp-wrap">
                        {if $arr_product_info.image_source}
                            {if $arr_product_info.image_linkLARGE}
                                <a href="{$arr_product_info.image_linkLARGE}">
                                    <img src="{$arr_product_info.image_link}" alt="">
                                </a>
                            {else}
                                <a href="{$arr_product_info.image_link}">
                                    <img src="{$arr_product_info.image_link}" alt="">
                                </a>
                            {/if}
                            {include file="modules/modul_additional_images.tpl"}
                        {else}
                            <a href="http://placehold.it/400x400&text=No%20image" class="mfp-image">
                                <img src="http://placehold.it/360x360&text=No%20image" />
                            </a>
                        {/if}
                    </div>
                    <!-- end Img Thumbnail -->
                </div>
            </div>

            <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post">
                <div class="col-xs-8">
                    <div class="content-box">
                        <div class="product-meta">
                            <h1 class="product-title">{$arr_product_info.products_name}</h1>
                            <!--                   <div class="product-rating">
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star-half-o"></i>
                                    </div> -->
                        </div>
                        <div class="product-desc">
                            <div class="product-price">
                                {if $arr_product_info.preisalt > 0}
                                    <div class="price-old">
                                        <span>{$arr_product_info.preisalt}</span>
                                    </div>
                                {/if}

                                <span class="price-base">
                  {$arr_product_info.preisneu|regex_replace:"/[()]/":""}
                </span>

                                <!--      <div class="price-sale">Sale
                                   <span>50%</span>
                                 </div> -->
                            </div>
                            <p>
                                {if $LOAD_CLIENT}
                                    <button type="submit" class="btn btn-primary">
                                        {$smarty.const.IMAGE_BUTTON_IN_CART}
                                    </button>
                                    <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                                {else}
                                    <button class="btn btn-primary disabled" disabled>
                                        {$smarty.const.IMAGE_BUTTON_IN_CART}
                                    </button>
                                    <input type="hidden" name="products_id" value="{$arr_product_info.products_id}" />
                                    <!-- <span id="hinweis_variante" class="hinweis_variante">{$PRODUCTS_VARIANTS_TIP}</span> -->
                                {/if}
                            </p>
                            <div class="col-xs-12">
                                {if $arr_product_info.arr_products_options}
                                    <div class="form-group">
                                        <label class="control-label">
                                            {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                        </label>
                                        {if $arr_product_info.product_options_javascript}
                                            {$arr_product_info.product_options_javascript}
                                        {/if}
                                    </div>
                                    <div class="form-group">
                                        {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                            <label class="control-label">{$productoption.name}:</label>
                                            {$productoption.pulldown}
                                        {/foreach}
                                    </div>
                                {/if}

                                {*-------------------------------------------*}
                                {if $opt_count > 0}
                                    {if $var_options_array}
                                        <input type="hidden" id="master_id" value="{$master_id}">
                                        <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                        <input type="hidden" id="cPath" value="{$cPath}">
                                        <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                        {if $Prodopt}
                                            <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                        {/if}
                                        {if $prod_hidden}
                                            {foreach from=$prod_hidden key=key item=hidden_input}
                                                {$hidden_input}
                                            {/foreach}
                                        {/if}
                                        {if $select_ids}
                                            {foreach from=$select_ids key=key item=select_input}
                                                {$select_input}
                                            {/foreach}
                                        {/if}
                                        {if $javascript_hidden}
                                            {$javascript_hidden}
                                        {/if}
                                        {foreach from=$var_options_array key=key item=options}
                                            <label class="control-label">{$options.title}:</label>
                                            {$options.dropdown}
                                        {/foreach}
                                    {/if}
                                {/if}
                                {*-------------------------------------------*}
                                {if $arr_product_info.base_price}
                                    {$arr_product_info.base_price}
                                {/if}
                                {*-------------------------------------------*}
                                {if $arr_product_info.shipping_value gt 0}
                                    <label class="control-label">{$TEXT_SHIPPING}:</label>
                                    ({$arr_product_info.shipping})
                                {/if}
                                {*---------------------------------------------*}
                                {if $arr_product_info.products_uvp}
                                    {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                    {$arr_product_info.products_uvp}
                                    {$arr_product_info.TEXT_YOU_SAVE}:
                                    {$arr_product_info.diff_price}
                                {/if}
                                {*-------------------------------------------*}
                                {if $count_bestprice gt 0}
                                    {$staffelpreis_hinweis}
                                    {foreach from=$bestprice_arr key=key item=bestprice}
                                        {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                        {$bestprice.ausgabe_stueck}
                                        {$bestprice.ausgabe_preis}
                                    {/foreach}
                                {/if}
                                {*-------------------------------------------*}
                                {if $PRICE_QUERY_ON eq 'true'}
                                    {$PRICE_QUERY}:
                                    <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                {/if}
                                {*-------------------------------------------*}
                                {if $PRODUCTS_QUERY_ON eq 'true'}
                                    <ul class="list-date-description">
                                        <li>
                                            {$PRODUCTS_INQUIRY_TEXT}: <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                        </li>
                                    </ul>
                                {/if}
                                {*-------------------------------------------*}
                                {* $arr_product_info.availability.text => z.b. "Sofort lieferbar"*}
                                {* $arr_product_info.availability.value => 0-3 => 0 = Keine Angabe, 1 = Sofort lieferbar, 2 = beschr�nkt lieferbar, 3 = nicht lieferbar *}
                                {if count($arr_product_info.availability)}

                                    {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                    {$arr_product_info.availability.text}
                                {/if}
                                {*-------------------------------------------*}
                                {if count($arr_product_info.delivery_time)}
                                    {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                    {$arr_product_info.delivery_time.text} *
                                {/if}
                                {*-------------------------------------------*}
                                {if $arr_product_info.text_date_available}
                                    &nbsp;
                                    {$arr_product_info.text_date_available}
                                {/if}
                                {*-------------------------------------------*}
                                {if $arr_product_info.products_url}
                                    &nbsp;
                                    {$arr_product_info.products_url}
                                {/if}
                                {*-------------------------------------------*}
                                {if $SANTANDER_LOANBOX}
                                    <span id="santandertitle_pinfo">
                {$SANTANDER_LOANBOX_TITLE}:
              </span>
                                    &nbsp;
                                    {$SANTANDER_LOANBOX}
                                {/if}
                                {*-------------------------------------------*}
                                {if $PRODUCTS_WEIGHT_INFO}
                                    {if $arr_product_info.products_weight > 0}
                                        {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                        {$arr_product_info.products_weight}
                                        &nbsp;
                                        {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                    {/if}
                                {/if}
                                {*-------------------------------------------*}
                                {if count($arr_product_info.products_model)}
                                    <label class="control-label">Model:</label>
                                    {$arr_product_info.products_model}
                                    <br />
                                {/if}
                                {*-------------------------------------------*}
                                {if $arr_manufacturer_info.manufacturers_name}
                                    <label class="control-label">{$manufacturer}:</label>
                                    {if $arr_manufacturer_info.manufacturers_image neq ""}
                                        {if $arr_manufacturer_info.manufacturers_url neq ""}
                                            <a href="{$arr_manufacturer_info.manufacturers_url}">
                                                <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                            </a>
                                        {else}
                                            <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                        {/if}
                                    {else}
                                        {if $arr_manufacturer_info.manufacturers_url neq ""}
                                            <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                        {else}
                                            {$arr_manufacturer_info.manufacturers_name}
                                        {/if}
                                    {/if}
                                {/if}
                                {*---------------------------------------------*}
                                {if $widget_view && $widget_view eq 1}
                                    <div id="widget_views"></div>
                                {/if}
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="col-xs-12">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#desc" data-toggle="tab">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                    </li>
                    <!--  {if $smarty.const.FACEBOOK_DISPLAY_FOOTER }
        <li>
          <a href="#reviews" data-toggle="tab">FoxRate</a>
        </li>
        {/if}  -->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="desc">
                        <p>
                            {$arr_product_info.products_description}
                        </p>
                    </div>
                    <div class="tab-pane fade" id="reviews">
                        {if $smarty.const.FACEBOOK_DISPLAY_FOOTER }
                            {$text_custom_footer}
                        {else}
                            <div style="display:none">{$text_custom_footer}</div>
                        {/if}
                    </div>
                </div>
            </div>
            <!-- END TAB -->

            {if $foxrate && $foxrate->GetVariable('counts') != 0}
                {*include file="../0_source/modules/modul_foxrate_products_info.tpl"*}
            {else}
            {/if}

            {include file="modules/modul_xsell_products.tpl"}

            {include file="modules/modul_also_purchased_products.tpl"}
            {else}
            {$TEXT_PRODUCT_NOT_FOUND}
            {/if}
            {/if}
        </div>
    </div>
</section>
{include file="footer.tpl"}
