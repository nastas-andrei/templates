{if $arr_shopping_cart}
    <div class="btn-group btn-group-cart">
        <button type="button" class="btn btn-default dropdown-toggle" onclick="location.href='{$arr_shopping_cart.cart_link}'">
            <span class="pull-left"><i class="fa fa-shopping-cart icon-cart"></i></span>
            <span class="pull-left">&nbsp;&nbsp;{$arr_shopping_cart.title|strip_tags:false} &nbsp;:&nbsp;{$arr_shopping_cart.count_contents}&nbsp;&nbsp;&nbsp;&nbsp;</span>
            <span class="pull-right"><i class="fa fa-caret-down"></i></span>
        </button>
        {if $arr_shopping_cart.products|@count > 0 }
            <ul class="dropdown-menu cart-content" role="menu">
                {foreach from=$arr_shopping_cart.products key=key item=product}
                    <li>
                        <a href="{$product.link}">
                            <b>{$product.name}</b>
                            <span>{$product.final_price}</span>
                        </a>
                    </li>
                {/foreach}
                <li class="divider"></li>
                <li><a href="{$arr_shopping_cart.cart_link}">{$arr_shopping_cart.cart_summe} {$arr_shopping_cart.gesamtsumme}</a></li>
            </ul>
        {/if}
    </div>
{/if} 