<div class="bordertop">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <nav class="navbar navbar-inverse fhmm" role="navigation">
                    <div class="navbar-header">
                        <button type="button" data-toggle="collapse" data-target="#defaultmenu" class="navbar-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="defaultmenu" class="navbar-collapse menu-blue collapse container">
                        <ul class="nav navbar-nav">
                            {foreach from=$categories key=key item=category name=catlist}
                                <li class="dropdown fhmm-fw">
                                    <a href="{$category.link}">
                                        {$category.name} {if $category.children|@count > 0}<i class="fa fa-angle-down"></i>{/if}
                                    </a>
                                    {if $category.children|@count > 0}
                                        <ul class="dropdown-menu fullwidth">
                                            <li class="fhmm-content withoutdesc nobg">
                                                <div class="row">
                                                    {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                                                        <div class="col-sm-3">
                                                            <h4>
                                                                <a href="{$categorySub.link}">{$categorySub.name|truncate:20:"...":true}
                                                                </a>
                                                            </h4>
                                                            <ul class="scroll_list">
                                                                <li>
                                                                    {if $categorySub.children|@count > 0}
                                                                        {foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}
                                                                            <ul class="custommenu">
                                                                                <li><a href="{$categorySub2.link}">{$categorySub2.name|truncate:15:"...":true}</a>
                                                                            </ul>
                                                                        {/foreach}
                                                                    {/if}
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    {/foreach}
                                                </div>
                                            </li>
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>
