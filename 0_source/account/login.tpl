 
<form name="login" action="{$login_form_url}" method="post">
        {$smarty.const.BEFORE_CONTENT}	    	
   		    	
        {if !$smarty.const.PH_INSIDE}    	    				
			{include file="pieces/page_title_section.tpl"}
        	{$smarty.const.BETWEEN_HL_CONTENT}
			<table class="pageContentTable" border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td>  
						<table border="0" width="100%" cellspacing="0" cellpadding="0">								
		{else}
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
      			<tr>
        			<td>        
        				{include file="pieces/page_title_section.tpl"}               
        			</td>
      			</tr>
      			{$smarty.const.BETWEEN_HL_CONTENT}      					
		{/if}

		<tr>
			<td class="cont_abst">        
			   	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="login_area">
		    		{if $LoginMessageStackSize}
		        	<tr>
		            	<td>{$login_message}</td>
		            </tr>
		            <tr>
		            	<td>{$pixel_trans_10}</td>
		            </tr>
		            {/if}
		            {if $CountContents}
					<!--
		            <tr>
		            	<td class="smallText">{$smarty.const.TEXT_VISITORS_CART}</td>
		            </tr>
		            -->
		            <tr>
		            	<td>{$pixel_trans_10}</td>
		            </tr>
		            {/if}
		      		<tr>
		        		<td>        
					        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="borderinfo">
					  			<tr>
					    			<td class="main2" width="100%"><b>{$smarty.const.HEADING_RETURNING_CUSTOMER}</b></td>
					    			<td  width="480" valign="top"></td>
					  			</tr>
					  			<tr>
					    			<td height="90" width="80%" colspan="2" valign="top">    
							    		<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2" class="infoBox">
							              	<tr >
							                	<td>
							                		<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
							                  			<tr>
							                    			<td colspan="2">{$pixel_trans_2}</td>
							                  			</tr>
							                  			<tr>
							                    			<td class="main" colspan="2">{$smarty.const.TEXT_RETURNING_CUSTOMER}</td>
							                  			</tr>
							                  			<tr>
							                    			<td colspan="2">{$pixel_trans_2}</td>
							                  			</tr>
							                  			<tr>
							                    			<td class="main"><b>{$smarty.const.ENTRY_EMAIL_ADDRESS}</b></td>
							                    			<td class="main">{$email_address_input}</td>
							                  			</tr>
							                  			<tr>
							                    			<td class="main"><b>{$smarty.const.ENTRY_PASSWORD}</b></td>
							                    			<td class="main">{$password_input}</td>
							                  			</tr>
							                  			<tr>                  
							                    			<td class="smallText" colspan="2"><a href="{$PASSWORD_FORGOTTEN_URL}">{$smarty.const.TEXT_PASSWORD_FORGOTTEN}</a></td>
							                  			</tr>
							                  			<tr>
							                    			<td colspan="2"></td>
							                  			</tr>
							                		</table>
							                	</td>
							              	</tr>
							            </table>            
					            	</td>
					    			<td width="131">
					    				<table border="0" width="100%" cellspacing="0" cellpadding="0">
					                      	<tr>
						                        <td align="right">{$button_login}</td>
					                      	</tr>
					                    </table>
					                 </td>
					  			</tr>
							</table>     
							
							{if $mode_retail_active}	
								<br>
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="borderinfo">
							  		<tr>
							    		<td class="main2" width="100%"><b>{$smarty.const.HEADING_NEW_CUSTOMER}</b></td>
							    		<td  width="480" colspan="2" valign="top"></td>
							  		</tr>
							  		<tr>
							    		<td height="90" width="80%" valign="top">
							    			<table border="0" width="100%" height="100%" cellspacing="1" cellpadding="2" class="infoBox">
							              		<tr>
							                		<td>
							                			<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
															<tr>
							                    				<td class="main" valign="top">{$smarty.const.TEXT_NEW_CUSTOMER_INTRODUCTION}</td>
							                  				</tr>
							                			</table>
							                		</td>
							              		</tr>
							            	</table>
							            </td>
							    		<td width="131">
							    			<table border="0" width="100%" cellspacing="0" cellpadding="2" align="right" valign="midlle">
							                	<tr>
							                        <td align="right" valign="middle"><a href="{$CREATE_ACCOUNT_URL}">{$button_new_customer_img}</a></td>
							                     </tr>
							                </table>
							            </td>
							  		</tr>
								</table>	
							{/if}
							
							{if $mode_merchant_active}
								<br />
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="borderinfo">
							  		<tr>
							    		<td class="main2" width="100%"><b>{$smarty.const.HEADING_NEW_MERCHANT}</b></td>
							    		<td  width="480" colspan="2" valign="top"></td>
							  		</tr>
							  		<tr>
							    		<td height="90" width="80%" valign="top">
							    			<table border="0" width="100%" height="100%" cellspacing="1" cellpadding="2" class="infoBox">
							              		<tr>
							                		<td>
							                			<table border="0" width="100%" height="100%" cellspacing="0" cellpadding="2">
															<tr>
							                    				<td class="main" valign="top">{$smarty.const.TEXT_NEW_MERCHANT_INTRODUCTION}</td>
							                  				</tr>
							                			</table>
							                		</td>
							              		</tr>
							            	</table>
							            </td>
							    		<td width="131">
							    			<table border="0" width="100%" cellspacing="0" cellpadding="2" align="right" valign="midlle">
							                      <tr>
							                        <td align="right" valign="middle"><a href="{$CREATE_ACCOUNT_B2B_URL}">{$button_new_customer_img}</a></td>
							                      </tr>
							                </table>
							            </td>
							  		</tr>
								</table>
	
							{/if}
			        	</td>
		      		</tr>
		        </table>
			</td>
		</tr>
	</table>      					

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}

</form>    
    	    		    	