{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    <div id="sns_content2" class="wrap">
        <div class="container">
            <div class="row-fluid">
                <div class="category-products">
                    <div class="toolbar clearfix">
                        <div class="toolbar-inner">
                            <p class="view-mode">
                                <label>View as</label>
                                <strong title="Grid" class="grid"></strong>
                                {if $standard_template == "galerie"}
                                    <a href="{$templ_link}" title="List" class="list"></a>
                                {/if}
                            </p>
                            {if $manufacturer_select}
                                {$manufacturer_select}&nbsp;
                            {/if}
                            <div class="sort-by">
                                <label>{$TABLE_HEADING_SEARCH_TO}</label>

                                <div class="select-new">
                                    <div class="select-inner">
                                        <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink" />
                                        <input type="hidden" value="{$max}" id="max" name="max" />
                                        <input type="hidden" value="{$standard_template}" id="standard_template"
                                               name="standard_template" />
                                        <select name="sortierung" id="sortierung" class="select-sort-by" onchange="goHin(this)">
                                            {foreach from=$arr_sort_new key=key item=sort}
                                                {if $sort.class == "p"}
                                                    {if $sort_select eq $sort.id}
                                                        <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                                    {else}
                                                        <option value="{$sort.id}">{$sort.text}</option>
                                                    {/if}
                                                {else}
                                                    {if $sort_select == $sort.id}
                                                        <option value="{$sort.id}">{$sort.text}</option>
                                                    {else}
                                                        <option value="{$sort.id}">{$sort.text}</option>
                                                    {/if}
                                                {/if}
                                            {/foreach}
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="pager">
                                <p class="amount">
                                    {$arr_modul_productlisting.count_products_info|strip_tags:false}
                                </p>
                                {if $arr_modul_productlisting.pagination|@count > 1}
                                    <div class="pages">
                                        <strong>Page:</strong>
                                        <ol>
                                            {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                                                {if $page.active}
                                                    <li class="current">
                                                        {$page.text}
                                                        {elseif $page.previous}
                                                    <li>
                                                        <a class="previous" href="{$page.link}" title="{$page.title}">
                                                        </a>
                                                        {elseif $page.next}
                                                    <li>
                                                        <a class="next" href="{$page.link}" title="{$page.title}">
                                                        </a>
                                                        {else}
                                                    <li>
                                                    <a href="{$page.link}" title="{$page.title}">
                                                        {$page.text}
                                                    </a>
                                                {/if}
                                                </li>
                                            {/foreach}
                                        </ol>
                                    </div>
                                {/if}
                            </div>
                        </div>
                    </div>
                    {literal}
                        <script type="text/javascript">
                            $sns_jq(function ($) {
                                // Transforming the form's Select control using jqTransform Plugin.
                                $(".toolbar .limiter .select-new .select-inner").jqTransform();
                                $(".toolbar .sort-by .select-new .select-inner").jqTransform();
                            });
                        </script>
                    {/literal}
                    <div class="sns-products-container clearfix row" style="margin-right:-16px">
                        <ul class="products-grid clearfix">
                            {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
                                {if $product.image}
                                    {php}
                                        $product = $this->get_template_vars('product');
                                        $pattern_src = '/src="([^"]*)"/';
                                        preg_match($pattern_src, $product['image'], $matches);
                                        $src = $matches[1];
                                        $this->assign('src', $src);
                                    {/php}
                                {else}
                                    {assign var=src value="http://placehold.it/300x300&text=No%20image"}
                                {/if}
                                <!-- Modal -->
                                <div class="modal fade hide" id="{$product.products_id}">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="fancybox-close">&times;</button>
                                            <div class="modal-body">
                                                <div class="product-img-box span5">
                                                    <a title="{$product.name}" href="{$product.link}">
                                                        <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                                                    </a>
                                                </div>
                                                <div id="product-shop" class="product-shop span7" data-tablet="product-shop span8"
                                                     data-mobile="product-shop span8">
                                                    <div class="title-modal">{$product.products_name}</div>
                                                    <div class="price-box text-left">
                                                        <span class="regular-price" id="product-price-274">
                                                            <span class="price">{$product.newprice|regex_replace:"/[()]/":""}</span>             .
                                                        </span>
                                                    </div>
                                                    <div class="add-to-cart text-left">
                                                        <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}"
                                                                class="button btn-cart have-borderinset" onclick="setLocation('{$product.buy_now_link}')">
                                                            <span>
                                                                <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                            </span>
                                                        </button>
                                                    </div>
                                                    <div class="clearer"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->
                                <li class="span3_span4_max item show-addtocart"
                                    data-normal="col-md-3 item show-addtocart"
                                    data-tablet="col-sm-3 item show-addtocart"
                                    data-mobile="col-xs-11 row item show-addtocart">
                                    <div class="item-inner clearfix">
                                        <div class="item-img have-additional clearfix">
                                            <div class="cart-wrap">
                                                <button title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="btn-cart"
                                                        onclick="setLocation('{$product.buy_now_link}')" data-toggle="tooltip"
                                                        data-original-title="{$smarty.const.IMAGE_BUTTON_IN_CART}">{$smarty.const.IMAGE_BUTTON_IN_CART}
                                                </button>
                                            </div>
                                            <div class="item-img-info">
                                                <a href="{$product.image_link}" title="Virtual Product" class="product-image">
                                                    <div class="img-main">
                                                        <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                                                    </div>
                                                </a>

                                                <div class="item-box-hover number-buttom3 has-btn-qv">
                                                    <div class="box-inner">
                                                        <div class="quickview-wrap">
                                                            <a class="sns-btn-quickview" aria-hidden="true" data-dismiss="modal"
                                                               data-toggle="modal" href="#{$product.products_id}">
                                                                <span>Quick View</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item-info">
                                            <div class="info-inner">
                                                <div class="item-title">
                                                    <a href="{$product.name_link}"
                                                       title="{$product.products_name|truncate:150:"...":true}">
                                                        {$product.products_name|truncate:35:"...":true}
                                                    </a>
                                                </div>
                                                <div class="item-content">
                                                    <div class="item-price">
                                                        <div class="price-box">
                                                            <span class="regular-price" id="product-price-28816069218061400503052">
                                                                <span class="price">{$product.newprice}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                    <div class="toolbar-bottom clearfix">
                        <div class="toolbar clearfix">
                            <div class="toolbar-inner">
                                <p class="view-mode">
                                    <label>View as</label>
                                    <strong title="Grid" class="grid"></strong>
                                    {if $standard_template == "galerie"}
                                        <a href="{$templ_link}" title="List" class="list"></a>
                                    {/if}
                                </p>

                                <div class="sort-by">
                                    <label>{$TABLE_HEADING_SEARCH_TO}</label>

                                    <div class="select-new">
                                        <div class="select-inner">
                                            <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink" />
                                            <input type="hidden" value="{$max}" id="max" name="max" />
                                            <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template" />
                                            <select name="sortierung" id="sortierung" class="select-sort-by" onchange="goHin(this)">
                                                {foreach from=$arr_sort_new key=key item=sort}
                                                    {if $sort.class == "p"}
                                                        {if $sort_select eq $sort.id}
                                                            <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                                        {else}
                                                            <option value="{$sort.id}">{$sort.text}</option>
                                                        {/if}
                                                    {else}
                                                        {if $sort_select == $sort.id}
                                                            <option value="{$sort.id}">{$sort.text}</option>
                                                        {else}
                                                            <option value="{$sort.id}">{$sort.text}</option>
                                                        {/if}
                                                    {/if}
                                                {/foreach}
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="pager">
                                    <p class="amount">
                                        {$arr_modul_productlisting.count_products_info|strip_tags:false}
                                    </p>
                                    {if $arr_modul_productlisting.pagination|@count > 1}
                                        <div class="pages">
                                            <strong>Page:</strong>
                                            <ol>
                                                {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                                                    {if $page.active}
                                                        <li class="current">
                                                            {$page.text}
                                                            {elseif $page.previous}
                                                        <li>
                                                            <a class="previous" href="{$page.link}" title="{$page.title}">
                                                            </a>
                                                            {elseif $page.next}
                                                        <li>
                                                            <a class="next" href="{$page.link}" title="{$page.title}">
                                                            </a>
                                                            {else}
                                                        <li>
                                                        <a href="{$page.link}" title="{$page.title}">
                                                            {$page.text}
                                                        </a>
                                                    {/if}
                                                    </li>
                                                {/foreach}
                                            </ol>
                                        </div>
                                    {/if}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}