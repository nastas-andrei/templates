{if $arr_modul_also_purchased_products}
    <div class="block block-related">
        {*<div class="block-title" style="text-align:center">*}
        {*<span>{$arr_modul_also_purchased_products.title}</span>*}
        {*</div>*}
        <div class="block-content clearfix">
            <ul class="mini-products-list products-grid" id="block-related">
                {foreach from=$arr_modul_also_purchased_products.products key=key item=product}
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value="http://placehold.it/300x300&text=No%20image"}
                    {/if}
                    <!-- Modal -->
                    <div class="modal fade" id="{$product.pid}" style="display:none">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="fancybox-close">&times;</button>
                                <div class="modal-body">
                                    <div class="product-img-box span5">
                                        <div class="product-image-zoom" style="width:260px">
                                            <a id="sns_cloudzoom" class="cloud-zoom" rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1" title="{$product.name}" href="{$src}">
                                                <img id="image" src="{$src}" alt="{$product.name}" title="{$product.name}" />
                                            </a>
                                        </div>
                                    </div>
                                    <div id="product-shop" class="product-shop span7" data-tablet="product-shop span8" data-mobile="product-shop span8">
                                        <div class="title-modal">{$product.name}</div>
                                        <div class="price-box" style="text-align:left">
                                              <span class="regular-price" id="product-price-274">
                                                <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>             .
                                              </span>
                                        </div>
                                        <div class="add-to-cart" style="text-align:left">
                                            <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button btn-cart have-borderinset" onclick="setLocation('{$product.buy_now_link}')">
                                                <span>
                                                  <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                </span>
                                            </button>
                                        </div>
                                        <div class="clearer"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <li class="item show-addtocart" style="width: 250px;margin: 15px;">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="cart-wrap">
                                    <button title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="btn-cart" onclick="setLocation('{$product.buy_now_link}')" data-toggle="tooltip" data-original-title="v">{$smarty.const.IMAGE_BUTTON_IN_CART}</button>
                                </div>
                                <div class="item-img-info">
                                    <a href="{$product.link}" title="{$product.name}" class="product-image">
                                        <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                                    </a>

                                    <div class="item-box-hover number-buttom3 has-btn-qv">
                                        <div class="box-inner">
                                            <div class="quickview-wrap">
                                                <a href="{$product.link}" title="{$product.name}" style="display:none"></a>
                                                <a class="sns-btn-quickview" aria-hidden="true" data-dismiss="modal" data-toggle="modal" href="#{$product.pid}"><span>Quick View</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title">
                                        <a href="{$product.link}">{$product.name|truncate:35:"...":true}</a>
                                    </div>
                                    <div class="item-content">
                                        <div class="item-price">
                                            <div class="price-box">
                                                <span class="regular-price" id="product-price-284-related">
                                                  <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
{/if}