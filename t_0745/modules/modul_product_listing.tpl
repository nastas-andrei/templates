{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
        {if $product.image}
            {php}
                $product = $this->get_template_vars('product');
                $pattern_src = '/src="([^"]*)"/';
                preg_match($pattern_src, $product['image'], $matches);
                $src = $matches[1];
                $this->assign('src', $src);
            {/php}
        {else}
            {assign var=src value=$imagepath|cat:"/empty-album.png"}
        {/if}
        <div class="col-xs-12 col-sm-6 col-md-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <a href="{$product.link}" title="{$product.products_name}">
                            <div class="width-image-product" style="background-image: url('{$src}');"></div>
                        </a>

                        <h2>{$product.preisneu|regex_replace:"/[()]/":""}</h2>

                        <p>
                            <a href="{$product.link}" title="{$product.products_name}">
                                {$product.products_name|truncate:35:"...":true}
                            </a>
                        </p>
                        <a href="{$product.buy_now_link}" class="btn btn-default add-to-cart">
                            <i class="fa fa-shopping-cart"></i>{$smarty.const.IMAGE_BUTTON_IN_CART}
                        </a>
                    </div>
                    {if $preisalt > 0}
                        <img src="{$imagepath}/sale.png" class="new" alt="">
                    {/if}
                </div>
            </div>
        </div>
    {/foreach}
    <div class="clearfix"></div>
    <ul class="pagination">
        {foreach from=$arr_modul_productlisting.pagination key=key item=page}
            {if $page.active}
                <li class="active">
                    <a href="javascript:void(0)">{$page.text}</a>
                    {elseif $page.previous}
                <li>
                    <a href="{$page.link}" title="{$page.title}">
                        &laquo;
                    </a>
                    {elseif $page.next}
                <li>
                    <a href="{$page.link}" title="{$page.title}">
                        &raquo;
                    </a>
                    {else}
                <li>
                <a href="{$page.link}" title="{$page.title}">
                    {$page.text}
                </a>
            {/if}
            </li>
        {/foreach}
    </ul>
{/if}
