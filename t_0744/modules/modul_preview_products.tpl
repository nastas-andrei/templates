{if $arr_modul_previewproducts.products}
    <h2 class="widget-title">
        <span>{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</span>
    </h2>
    <div class="sidebar-line"><span></span></div>
    <div class="groom">
        {foreach from= $arr_modul_previewproducts.products key=key item=product}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
            {/if}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="product1 image-hover he-wrap tpl6">
                    <div class="image_view_product" style="background-image:url('{$src}')"></div>
                    <div class="he-view" onclick="location.href='{$product.link}'">
                        <div class="bg a0" data-animate="fadeIn">
                            <h3 class="a1" data-animate="fadeInDown">{$product.name|truncate:20:"...":true}</h3>
                            <a href="{$product.buy_now_link}" class="btn a2" data-animate="rotateInLeft"><i class="fa fa-shopping-cart"></i> {$smarty.const.IMAGE_BUTTON_IN_CART}</a>
                            <a href="{$product.link}" class="btn a2" data-animate="rotateInRight"><i class="fa fa-search"></i> {$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                        </div>
                    </div>
                </div>
                <div class="product-details">
                    <a href="{$product.link}" class="overflow-title-product">
                        <h4>{$product.name}</h4>
                    </a>
                    <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                </div>
            </div>
        {/foreach}
    </div>
{/if}
