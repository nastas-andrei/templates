<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
			<!-- button back -->               
			    <div class="pull-left shopping"> 
			        <a class="btn button_add" href="{$account_href}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>  
			    </div>
				<div class="clearfix"></div>
				<!-- button back end -->  
				{if $is_addressbook_message}
					<p class="text-center">{$addressbook_message}</p>
				{/if}
	            <div class="panel panel-default" style="margin-top:20px">
	                <div class="panel-heading">
	                    <h3 class="panel-title">{$smarty.const.PRIMARY_ADDRESS_TITLE}</h3>
	                </div>
	                <div class="panel-body">
	                    {$customer_address}
	                </div>
	            </div>
	            <div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title">{$smarty.const.ADDRESS_BOOK_TITLE}</h3>
	                </div>
	                <div class="panel-body">
	                    {section name=current loop=$addresseslist}
						<div class="span6">
						<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onClick="document.location.href='{$addresseslist[current].address_book_edit_row_href}'">
						{$addresseslist[current].firstname} 
						{$addresseslist[current].lastname}
						{if $addresseslist[current].is_customer_default_address}
							<i>{$smarty.const.PRIMARY_ADDRESS}</i>
						{/if}

						
						<address>
							{$addresseslist[current].formated_address}
						</address>
						<p style="margin:20px">
							<a href="{$addresseslist[current].address_book_edit_row_href}" class="button_add" style="padding:10px">
								<i class="fa fa-pencil-square-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_EDIT}
							</a> 
							<a href="{$addresseslist[current].address_book_delete_row_href}" class="button_dell" style="padding:10px">
								<i class="fa fa-trash-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_DELETE}
							</a>	
						</p>
						</div>
						{/section}
                	</div>
            	</div>
	            <p class="text-center">
					{if $is_allow_add}
					<a href="{$address_book_add_href}" class="button_add" style="padding:10px">
						<i class="icon-plus"></i> {$smarty.const.IMAGE_BUTTON_ADD_ADDRESS}
					<a/>
					{/if}
				</p>
		    </div>
        </div>
    </div>
</section>
