{if $arr_modul_xsell}
<section class="featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-top:50px">
                <div class="box-title">
                    <h2>{$arr_modul_xsell.TEXT_XSELL_PRODUCTS}</h2>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div id="featured-slider" class="owl-carousel">
              {foreach from=$arr_modul_xsell.products key=key item=product}
                <div class="item">
                    <!-- PRODUCT -->
                  <div class="col-md-12">
                    <div class="product-block">
                  {if $product.image} 
                    {php}
                     {$product = $this->get_template_vars('product');}
                     {$pattern_src = '/src="([^"]*)"/';}
                     {$pattern_width = '/width="([^"]*)"/';}
                     {$pattern_height = '/height="([^"]*)"/';}
                     {preg_match($pattern_src, $product['image'], $matches);}
                     {$src = $matches[1];}
                     {preg_match($pattern_width, $product['image'], $matches);}
                     {$width = $matches[1];}
                     {preg_match($pattern_height, $product['image'], $matches);}
                     {$height = $matches[1];}
                     {$this->assign('src', $src);}
                     {$this->assign('img_width', $width);}
                     {$this->assign('img_height', $height);}
                    {/php}
                    {else}
                      {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                      <div class="product-image" style="height: 200px;">
                        <a href="{$product.link}">
                            <figure class="product-display">
<!--                                 <span class="product-label-special label">
                                    <i>Sale</i>
                                    <span class="special">50%</span>
                                </span> -->
                                <div class="product-mainpic" style="width:260px;height:200px;background-image:url('{$src}');background-position: center;;background-size: cover;"></div>
                               <div class="product-secondpic" style="width:260px;height:200px;background-image:url('{$src}');background-position: center;;background-size: cover;"></div>
<!--                                 <img data-src="{$src}" alt="" class="lazyOwl product-mainpic" src="{$src}" />
                                <img src="{$src}" alt="" class="product-secondpic"/> -->
                            </figure>
                        </a>
                      </div>
                      <div class="product-meta">
                          <div class="product-action">
                              <a href="{$product.buy_now_link}" class="addcart">
                                  <i class="icon-basket"></i>
                                 {$smarty.const.IMAGE_BUTTON_IN_CART}
                              </a>
               <!--                <a href="javascript:;" class="wishlist">
                                  <i class="fa fa-heart"></i>
                              </a>
                              <a href="javascript:;" class="compare">
                                  <i class="fa fa-retweet"></i>
                              </a> -->
                          </div>
                      </div>
                          <div class="product-info">
                              <a href="{$product.link}">
                                  <h5 class="product-name">{$product.name|truncate:150:"...":true}</h5>
                              </a>
                              <div class="product-price">
                                  <span>{$product.preisneu|regex_replace:"/[()]/":""}</span>
                              </div>
                          </div>
                          <div class="product-rating">
<!--                               <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i> -->
                          </div>
                    </div>
                  </div>        
                </div>
                {/foreach}
                <!--END Owl -->
              </div>
            </div>
          </div>
        </div>
    </div>
    <!--/.container -->
</section>
{/if}