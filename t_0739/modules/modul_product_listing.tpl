{assign var=arr1 value=" "|explode:$arr_modul_productlisting.count_products_info}
{assign var=total_items value=$arr1[0]|regex_replace:"/[^0-9]+/":""}
{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
        var slink = document.getElementById('sortlink').value;
        var sort = document.getElementById('sortierung').selectedIndex;
        var wert = das[sort].value;
        var max = document.getElementById('max').value;
        var link = slink + '&sort='+wert+'&max='+max;
        location.href = link;
        }
    </script>
{/literal}
<div id="content">
    <div class="product-filter">
        <span style="float: left;margin: 8px">{$arr_modul_productlisting.count_products_info|strip_tags:false}</span>
        <div class="display">
            <span class="text-styling">Display:</span>
            <div class="list-view-active">
                <span class="list-text">List</span>
            </div>
            {if $standard_template == "liste"}
                <a href="{$templ_link}"><div class="grid-view"><span class="grid-text">Grid</span></div></a>
            {/if}
        </div>
        {if $manufacturer_select}
            {$manufacturer_select}&nbsp;
        {/if}
        <div class="sort">
            <span class="text-styling">Sort By:</span>
            <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink" />
            <input type="hidden" value="{$max}" id="max" name="max" />
            <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template" />
            <select name="sortierung" id="sortierung" class="select-sort-by" onchange="goHin(this)">
                {foreach from=$arr_sort_new key=key item=sort}
                    {if $sort.class == "p"}
                        {if $sort_select eq $sort.id}
                            <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                        {else}
                            <option value="{$sort.id}">{$sort.text}</option>
                        {/if}
                    {else}
                        {if $sort_select == $sort.id}
                            <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                        {else}
                            <option value="{$sort.id}">{$sort.text}</option>
                        {/if}
                    {/if}
                {/foreach}
            </select>
        </div>
    </div>
    <div class="product-list">
        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/nopicture.gif"}
            {/if}
            <div>
                <div class="left">
                    <div class="image">
                        <a href="{$product.link}">
                            <div class="image_view_product_list" style="background-image: url('{$src}')"></div>
                        </a>
                    </div>
                    <div class="name">
                        <a href="{$product.link}">{$product.products_name}</a>
                    </div>
                    <div class="price">
                        <span style="float: right; margin-right: 50px">{$product.newprice|regex_replace:"/[()]/":""}</span>
                            <br>
                        <div class="cart">
                            <input type="button" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" onclick="addToCart('{$product.buy_now_link}');" class="button">
                        </div>
                    </div>
                </div>
            </div>
        {/foreach}
        <div class="pagination" style="border-bottom: 0">
            <div class="results">
                <ul class="links">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <li class="current">
                                <b>{$page.text}</b>
                        {elseif $page.previous}
                            <li>
                                <a class="previous" href="{$page.link}" title="{$page.title}">
                                    <i class="fa fa-angle-double-left"></i>
                                </a>
                        {elseif $page.next}
                            <li>
                                <a class="next" href="{$page.link}" title="{$page.title}">
                                    <i class="fa fa-angle-double-right"></i>
                                </a>
                        {else}
                            <li>
                                <a href="{$page.link}" title="{$page.title}">
                                    {$page.text}
                                </a>
                        {/if}
                            </li>
                    {/foreach}
                </ul>
            </div>
        </div>
    </div>
{/if}