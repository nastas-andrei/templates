{$smarty.const.BEFORE_CONTENT}
		
{if !$smarty.const.PH_INSIDE}		
    {include file="pieces/page_title_section.tpl"}        
	{$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}		
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
      		<tr>
        		<td>
        			{include file="pieces/page_title_section.tpl"}
        		</td>
      		</tr>
      		{$smarty.const.BETWEEN_HL_CONTENT}
{/if}
      	<tr>
        	<td class="cont_abst">
	        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
	 				<tr>
	      				<td class="main">{$smarty.const.TEXT_STANDARD_WELCOME}</td>
					</tr>
	      			<tr>
	        			<td>
	        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
	          					<tr>
	            					<td>
	            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
	              							<tr>
	                							<td width="10">{$pixel_trans_1}</td>
	                							<td align="right"><a href="{$origin_href}">{$button_continue}</a></td>
	                							<td width="10">{$pixel_trans_1}</td>
	              							</tr>
	            						</table>
	            					</td>
	          					</tr>
	        				</table>
	        			</td>
	      			</tr>
	        	</table>
        	</td>
     	</tr>
    </table>

{if !$smarty.const.PH_INSIDE}		
			</td>
		</tr>
	</table>		  
{/if}

{$smarty.const.AFTER_CONTENT}
