{if $DESIGN_VARIANT != ""}
<link href="{$templatepath}mt_review2preview/review2preview_{$DESIGN_VARIANT}.css" rel="stylesheet" type="text/css" />
{else}
<link href="{$templatepath}mt_review2preview/review2preview.css" rel="stylesheet" type="text/css" />
{/if}
{literal}
<script type="text/javascript">
window.addEvent('domready', function(){
  function initViewer() {
    $('qPVTloader').destroy();
    $('viewerStage').removeClass('VSpos');
    var vStage = $('viewerStage').getSize().x;
    $('viewerMask').set({styles:{'width': vStage, 'visibility':'visible'}});
    
		var p2pStat = "{/literal}{$PRODUCTS_SLIDER_OPEN}{literal}";
    var ttTimer;
    var vImages;
    var imgW = 0;
    var imgH = 0;
    var xtrMargin = 0;
    var clicked = 0;
    var clickMax = 0;
    var increment = 0;
    var totIncrement = 0;
    //var maxRightIncrement = 0;
    var ImgSubPart = 0;
    var imgCount = 0;
        vImages = Math.floor(vStage / (70 + 10));
        xtrMargin = Math.round((vStage - (vImages * (70 + 10))) / vImages) + 10;

    $$('.subproductholder').each(function(el,index) {
            imgW = el.getSize().x;
            imgH = el.getSize().y;
            imgCount = index + 1;
            ImgSubPart += el.getSize().x + xtrMargin;
            el.set({styles:{'width': el.getSize().x + xtrMargin}});
    });

    if (imgCount > vImages) {
      $('scroll2next').set({styles:{'background':'url({/literal}{$templatepath}{literal}mt_review2preview/arrow_next.png) no-repeat'}});
      $('scroll2previous').set({styles:{'background':'url({/literal}{$templatepath}{literal}mt_review2preview/arrow_previous.png) no-repeat'}});
    } else {
			$('scroll2next').set({styles:{'display':'none'}});
      $('scroll2previous').set({styles:{'display':'none'}});
		}
    
    $('viewerImages').set({styles:{'width': ImgSubPart}});
    clickMax = Math.ceil(ImgSubPart / vStage - 1);
      //
      //
        if (Math.floor(imgCount / vImages) == 1){
            increment = (imgCount % vImages) * (imgW + xtrMargin);
        } else {
            increment = vStage;
        };
        //maxRightIncrement = imgCount *(imgW + xtrMargin)*(-1);
       //
       //
    var fx = new Fx.Tween('viewerImages', {
        duration: 1200,
        transition:'sine:in:out'
    });

      $('scroll2next').addEvent('click', function(){
      if (clicked < clickMax){
          //
         // if (window.console) console.log("Math increment NEXT: ",Math.floor(imgCount / vImages));
          //
          if (Math.floor(imgCount / vImages) == 1) {
              increment = (imgCount % vImages) * (imgW + xtrMargin);
              //
              // anders abfangen!!!
              //$('scroll2previous').setStyle('background-position','0% 0%');
              //
              //
              //this.setStyle('background-position','0% 100%');
          } else {
              increment = vStage;
              //$('scroll2previous').setStyle('background-position','0% 0%');

          };
          totIncrement -= increment;
          fx.cancel();
          fx.start('margin-left',totIncrement);
          clicked++;
          //
          imgCount -= vImages;
          //
      }
    });
    //$('scroll2previous').setStyle('background-position','0% 100%');
    $('scroll2previous').addEvent('click', function(){
      if (totIncrement < 0){
          //
          //if (window.console) console.log("Math increment PREV: ",Math.floor(imgCount / vImages));
          //
          if (Math.floor(imgCount / vImages) == 0) {
              increment = (imgCount % vImages) * (imgW + xtrMargin);
              //$('scroll2next').setStyle('background-position','0% 0%');
          } else {
              increment = vStage;
              //this.setStyle('background-position','0% 100%');
          };
         totIncrement += increment;
         fx.cancel();
         fx.start('margin-left',totIncrement);
         clicked--;
         //
         imgCount += vImages;
         //
      }
    });

    var activeEl;
    var arrowDiv = new Element('div', {'id': 'qPVTarrow', 'styles': {'position':'absolute', 'z-index':'1000', 'margin-left':(imgW+xtrMargin)/2-35, 'background-position':'50% 0', 'margin-top':'-2px', 'width':'70px', 'height':'10px'},
                              events: {
                                 mouseleave: function(){
                                   ttTimer = deleteDiv.delay(500);
                                   activeEl.setStyle('opacity',0.5);
                                 },
                                 mouseenter: function(){
                                   $clear(ttTimer);
                                   //
                                   // -----------
                                   $('qPVTarrow').setStyle('display','block');
                                   $('qPVTinner').setStyle('display','block');  
                                   // -----------
                                   //
                                   activeEl.setStyle('opacity',1);
                                 }
                              }
                            });
    var innerDiv = new Element('div', {'id': 'qPVTinner',  'styles': {'position':'absolute', 'z-index':'999', 'margin-top':'7px', 'margin-left':'-100px'}});
    innerDiv.cloneEvents(arrowDiv);
    var contentDiv = new Element('div', {'id': 'qPVTcontent', 'styles': {'padding': '10px'}});

    function deleteDiv() {
      //
      // -----------
      $('qPVTarrow').setStyle('display','none');
      $('qPVTinner').setStyle('display','none');  
      // -----------
      //
    }

    $$('.col').each(function(el,index){
      el.setStyle('opacity',0.5);
      el.store('origRel',el.getElement('a').get('rel'));
      var myRel = el.retrieve('origRel');
      if($chk(myRel)){
          var optArr = myRel.split(',');
      };

      el.addEvents({
        'mouseenter':function(e){          
            $clear(ttTimer);
            el.setStyle('opacity',1);
            arrowDiv.inject($(this), 'after');
            innerDiv.inject($(arrowDiv), 'after');
            contentDiv.inject($(innerDiv)); 
            //
            // -----------
            $('qPVTarrow').setStyle('display','block');
            $('qPVTinner').setStyle('display','block');  
            // -----------
            //
            if (this !== activeEl) {
                var req = new Request.HTML({
                  method: 'get',
                  async: true,
                  url:'/review2preview_request.php',
                  data: {
                    product_id: ''+optArr[0]+'',
                    image: '1',
                    price: '1',
                    shipping: '1',
                    availability: '1',
                    manufacturer: '1',
                    buynow: '1',
                    cPath: '{/literal}{$cPath}{literal}'
                  },
                  onRequest: function(item) {
                    contentDiv.set('html','<center><img src="{/literal}{$templatepath}{literal}mt_review2preview/ajax-loader.gif" /></center>');
                  },
                  onSuccess: function(html) {
                    contentDiv.set('text', '');
                    contentDiv.adopt(html);
                  },
                  onFailure: function(item) {                
                    contentDiv.set('text', item.responseText);                
                  }
                }).send();
            }
            activeEl = el;
        }.bind(el),
        'mouseleave':function(){
            ttTimer = deleteDiv.delay(500);
            el.setStyle('opacity',0.5);
        }.bind(el)
      });
    });

    var slideWrap = $('slide_wrap');
    var slideElm = new Fx.Slide($('quickProductViewer'), {
        wrapper: slideWrap,
        link: 'cancel',
        transition: 'expo:in:out',
        duration: 600,
        onStart: function(){
          // function anwenden wenn per css die b�hne geschlossen wurde...
          //$('mydiv').getParent().setStyle('height', 'auto');
					//slideWrap.setStyle('height', 'auto');
        },
        onCancel: function(){},
        onComplete: function(){
          if ($('qPVtoggle').getStyle('background-position') == '0% -14px') {
              $('qPVtoggle').setStyle('background-position', '0% 0%');
              $$('.qPVFBottom').removeClass('onTop');
          } else {
              $('qPVtoggle').setStyle('background-position', '0% -14px');
              $$('.qPVFBottom').addClass('onTop');
          }
        }
    }).{/literal}{$PRODUCTS_SLIDER_OPEN}{literal}();
    //}).show();
    //});
    
    $('qPVtoggle').addEvent('click', function(el){
        el.stop();
        slideElm.toggle();
    });
  }
  initViewer.delay(2500);
});
</script>
{/literal}