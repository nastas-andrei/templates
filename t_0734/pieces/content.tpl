{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}

    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
    {*{$user_html}*}
    {*{/if}*}
    {if $slider_images != ""}
        <!-- Slider -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            {*<ol class="carousel-indicators">*}
                {*{foreach from=$slider_images item=slider name=name_slider key=key}*}
                    {*<li data-target="#carousel-example-generic" data-slide-to="{$key}" {if $smarty.foreach.name_slider.first}class="active"{/if}></li>*}
                {*{/foreach}*}
            {*</ol>*}
            <div class="carousel-inner">
                {foreach from=$slider_images item=slider name=name_slider}
                    {if $slider.active == 1}
                        <div class="item {if $smarty.foreach.name_slider.first} active {/if}">
                            <img alt="{$slider.title}" src="{$slider.image|replace:'.jpg':'.png'}">
                        </div>
                    {/if}
                {/foreach}
            </div>
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
        <!-- End Slider -->
    {/if}

    {$FILENAME_FEATURED_HTML}

    {$UPCOMING_PRODUCTS_HTML}

{/if}

{if $is_products == 1}

    {$smarty.const.HEADING_TITLE}

    {if $filterlistisnotempty}
        {$filter_form}
        {$smarty.const.TEXT_SHOW}
    {/if}

    {*{if !empty($categories_html)}*}
    {*{$categories_html}*}
    {*{/if}*}

    {if !empty($manufacturers_html_header)}
        {$manufacturers_html_header}
    {/if}

    {if !empty($PRODUCT_LISTING_HTML)}
        {$PRODUCT_LISTING_HTML}
    {/if}

    {if !empty($manufacturers_id)}
        {$manufacturers_html_footer}
    {/if}

    {if !empty($categories_html_footer)}
        {$categories_html_footer}
    {/if}

{else}
    {if $is_default}

    {else}
        <p class="block">{$tep_image_category}</p>
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}

