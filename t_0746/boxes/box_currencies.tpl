{if $arr_currenciesbox}
    <form id="drop-currencies" name="currencies" action="{$arr_currencies.formaction}" method="get" style="display: inline-block;">
        <span class="language-name">{$arr_currenciesbox.title}:</span>
        <select id="basic" name="currency" onChange="this.form.submit();" class="selectpicker" data-size="5">
            {html_options options=$arr_currenciesbox.items selected=$currency}
        </select>
        {foreach from=$arr_currenciesbox.hiddenfields key=key item=fieldname}
            <input type="hidden" name="{$key}" value="{$fieldname}">
        {/foreach}
    </form>
{/if}
