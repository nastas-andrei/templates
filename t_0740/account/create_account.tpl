<div class="account-container">
{$create_account_frm}
    <div class="form-horizontal">
        <fieldset>
            <span class="help-block text-center">{$smarty.const.FORM_REQUIRED_INFORMATION}</span>
            {if $messages_exists}
                <div class="alert">
                    <button type="button" class="btn btn-primary btn-icon close" data-dismiss="alert" aria-hidden="true"><i
                                class="icon-remove"></i></button>
                    {$message_create_account}
                </div>
            {/if}
            {if $smarty.const.ACCOUNT_COMMERCIAL_CODE == 'true'}
                {$smarty.const.COMMERCIALCODE_OPTIONS}
                {$commercialcode_input}
                {$smarty.const.ENTRY_COMMERCIALCODE}
            {/if}
            {if $smarty.const.ACCOUNT_GENDER == 'true'}
                <div class="control-group">
                    <label class="control-label">{$smarty.const.ENTRY_GENDER_TEXT}</label>

                    <div class="controls form-inline">
                        <label class="radio">{$gender_radio_m} {$smarty.const.MALE} </label>
                        <label class="radio" style="padding-top:5px;"> {$gender_radio_f} {$smarty.const.FEMALE}</label>
                    </div>
                </div>
            {/if}
            <span class="help-block text-center">{$smarty.const.CATEGORY_PERSONAL} </span>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_FIRST_NAME}</label>

                <div class="controls">
                    {$firstname_input}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_LAST_NAME}</label>

                <div class="controls">
                    {$lastname_input}
                </div>
            </div>
            <div class="control-group">
                {if $smarty.const.ACCOUNT_DOB == 'true'}
                    <label class="control-label">{$smarty.const.ENTRY_DATE_OF_BIRTH}</label>
                    <div class="controls">
                        {$dob_input}
                        <span class="help-inline">
                  {if $entry_dob_text}
                      {$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}
                  {/if}
                  </span>
                    </div>
                {/if}
            </div>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>

                <div class="controls">
                    {$email_address_input}
                    <span class="help-inline">
                  {if $entry_email_address_text}
                      {$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}
                  {/if}
                  </span>
                </div>
            </div>
            {if $ACCOUNT_MAIL_CONFIRM}
                <div class="control-group">
                    <label class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS_CONFIRMATION}</label>

                    <div class="controls">
                        {$email_address_confirmation_input}
                        <span class="help-inline">
                    {if $entry_email_address_confirmation_text}
                        {$smarty.const.ENTRY_EMAIL_ADDRESS_CONFIRMATION_TEXT}
                    {/if}
                   </span>
                    </div>
                </div>
            {/if}
            {if $smarty.const.ACCOUNT_COMPANY == 'true'}
                <span class="help-block text-center">{$smarty.const.CATEGORY_COMPANY}</span>
                <div class="control-group">
                    <label class="control-label"> {$smarty.const.ENTRY_COMPANY}</label>

                    <div class="controls">
                        {$company_input}
                    </div>
                <span class="help-inline">
                  {if $entry_company_text}
                      {$smarty.const.ENTRY_COMPANY_TEXT}
                  {/if}
                </span>
                </div>
            {/if}
            <span class="help-block text-center">{$smarty.const.CATEGORY_ADDRESS}</span>
            {if $smarty.const.ACCOUNT_SUBURB == 'true'}
                <div class="control-group">
                    <label for="" class="control-label">{$smarty.const.ENTRY_SUBURB}</label>

                    <div class="controls">
                        {$suburb_input}
                        <span class="help-inline">
                    {if $entry_suburb_text}
                        {$smarty.const.ENTRY_SUBURB_TEXT}
                    {/if}
                  </span>
                    </div>
                </div>
            {/if}
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_STREET_ADDRESS}</label>

                <div class="controls">
                    {$street_address_input}
                    <span class="help-inline">
                  {if $entry_street_address_text}
                      {$smarty.const.ENTRY_STREET_ADDRESS_TEXT}
                  {/if}
                </span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_POST_CODE}</label>

                <div class="controls">
                    {$postcode_input}
                    <span class="help-inline">
                {if $entry_postcode_text}
                    {$smarty.const.ENTRY_POST_CODE_TEXT}
                {/if}
              </span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_CITY}</label>

                <div class="controls">
                    {$city_input}
                    <span class="help-inline">
                {if $entry_city_text}
                    {$smarty.const.ENTRY_CITY_TEXT}
                {/if}
              </span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_COUNTRY}</label>

                <div class="controls">
                    {$country_list}
                    <span class="help-inline">
                {if $entry_country_text}
                    {$smarty.const.ENTRY_COUNTRY_TEXT}
                {/if}
              </span>
                </div>
            </div>
            {if $smarty.const.ACCOUNT_STATE == 'true'}
                <div class="control-group">
                    <label class="control-label">{$smarty.const.ENTRY_STATE}</label>

                    <div class="controls">
                        {$state_field_html}
                        <span class="help-inline">
               {if $entry_state_text}
                   {$smarty.const.ENTRY_STATE_TEXT}
               {/if}
              </span>
                    </div>
                </div>
            {/if}
            <span class="help-block text-center">{$smarty.const.CATEGORY_CONTACT}</span>
            <div class="control-group">
                <label class="control-label">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</label>

                <div class="controls">
                    {$telephone_input}
                    <span class="help-inline">
                {if $entry_telephone_text}
                    {$smarty.const.ENTRY_TELEPHONE_NUMBER_ERROR}
                {/if}
              </span>
                </div>
            </div>
            {if $ACCOUNT_TEFAXNUMBER}
                <div class="control-group">
                    <label class="control-label">{$smarty.const.ENTRY_FAX_NUMBER}</label>

                    <div class="controls">
                        {$fax_input}
                        <span class="help-inline">
                {if $entry_fax_text}
                    {$smarty.const.ENTRY_FAX_NUMBER_TEXT}
                {/if}
              </span>
                    </div>
                </div>
            {/if}
            <!--<span class="help-block text-center">{$smarty.const.CATEGORY_OPTIONS}</span> -->
            <span class="help-block text-center"> {$smarty.const.CATEGORY_PASSWORD}</span>
            <div class="control-group">
                <label for="" class="control-label">{$smarty.const.ENTRY_PASSWORD}</label>

                <div class="controls">
                    {$password_input}
                    <span class="help-inline">
              {if $entry_password_text}
                  {$smarty.const.ENTRY_PASSWORD_TEXT}
              {/if}
              </span>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</label>

                <div class="controls">
                    {$password_confirmation_input}
                    <span class="help-inline">
                  {if $entry_password_confirmation_text}
                      {$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}
                  {/if}
              </span>
                </div>
            </div>
            <div class="control-group">
                <label for="" class="control-label">{$smarty.const.ENTRY_NEWSLETTER}</label>

                <div class="controls">
                    <label class="checkbox">
                        {$newsletter_checkbox} {$smarty.const.TEXT_NEWS_TERM}
                    </label>
                </div>
              <span class="help-inline">
                {if $entry_newsletter_text}
                    {$smarty.const.ENTRY_NEWSLETTER_TEXT}
                {/if}
              </span>
            </div>
            {if $is_data_security}
                <div class="control-group">
                    <label class="control-label">{$smarty.const.ENTRY_DATENSCHUTZ}</label>

                    <div class="controls">
                        <label class="checkbox">
                            {$datenschutz_checkbox}  {$smarty.const.ENTRY_DATENSCHUTZ_TEXT} <br>
                            <a href="#modal-terms" role="button" data-toggle="modal">{$smarty.const.TEXT_DATENSCHUTZ_NOTICE}</a>
                        </label>
                    </div>
                    <!-- Modal -->
                    <div id="modal-terms" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="modalTermsLabel"
                         aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="btn btn-primary btn-icon close" data-dismiss="modal" aria-hidden="true">×
                            </button>
                            <h3 id="modalTermsLabel">{$smarty.const.ENTRY_DATENSCHUTZ}</h3>
                        </div>
                        <div class="modal-body">
                            <p>
                                {$data_security_inf_desc}
                            </p>
                        </div>
                    </div>
                </div>
            {/if}
            {if $smarty.const.USE_POINTS_SYSTEM == 'true'}
                {$smarty.const.TEXT_NEWSLETTER_NOTICE}
            {/if}
            {if $is_dubli_modul}
                {$smarty.const.ENTRY_DUBLI}:
                {$dubli_checkbox}
                {$entry_newsletter_points}
            {/if}
            <div class="control-group text-center">
                <input type="submit" class="btn btn-primary btn-icon" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
            </div>
        </fieldset>
    </div>
</form>
</div>