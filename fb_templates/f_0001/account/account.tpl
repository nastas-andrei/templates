<div class="account-container">
	

{if $account_message_exists} 
  <p>{$account_message}</p>
{/if}


	<h5>{$smarty.const.MY_ACCOUNT_TITLE}</h5>
	<p><a href="{$account_edit_href}"><i class="icon-user"></i> {$smarty.const.MY_ACCOUNT_INFORMATION}</a></p>
	<p><a href="{$address_book_href}"><i class="icon-building"></i> {$smarty.const.MY_ACCOUNT_ADDRESS_BOOK}</a></p>
	<p><a href="{$account_password_href}"><i class="icon-lock"></i> {$smarty.const.MY_ACCOUNT_PASSWORD}</a></p>
	<h5>{$smarty.const.MY_ORDERS_TITLE}</h5>
	<p><a href="{$account_hystory_href}"><i class="icon-shopping-cart"></i> {$smarty.const.MY_ORDERS_VIEW}</a></p>
	{if $is_newsletters_block}
		<h5>{$smarty.const.EMAIL_NOTIFICATIONS_TITLE}</h5>
		<p> <a href="{$account_newsletters_href}"><i class="icon-envelope"></i> {$smarty.const.EMAIL_NOTIFICATIONS_NEWSLETTERS}</a></p>
	{/if}
	{if $is_use_points_system}
		<h5>{$smarty.const.POINTS_ACCOUNT_TITLE}</h5>
		<p><a href="{$my_points_url}">{$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS}</a></p>
		<p><a href="{$my_points_url_umpf}">{$smarty.const.POINTS_ACCOUNT_NOTIFICATIONS_COUPON}</a></p>
	{/if}	


{if $count_customer_orders > 0}
<h5 class="text-center">{$smarty.const.OVERVIEW_TITLE}</h5>
<a href="{$account_hystory_href}" style="text-decoration:underline">
	{$smarty.const.OVERVIEW_SHOW_ALL_ORDERS}
</a>

{$smarty.const.OVERVIEW_PREVIOUS_ORDERS}

<table class="table account-history-orders table-hover table-condensed table-bordered">
	<tbody>
	{section name=current loop=$orderslist}
	<tr onClick="document.location.href='{$orderslist[current].order_history_info_href}'">
	  <td><p>{$orderslist[current].date_purchased}</p></td>
	  <td><p>#{$orderslist[current].orders_id}</p></td>
	  <td><p>{$orderslist[current].order_name}</p></td>
	  <td><p>{$orderslist[current].order_country}</p></td>
	  <td><p>{$orderslist[current].orders_status_name}</p></td>
	  <td><p>{$orderslist[current].order_total}</p></td>
	  <td><a href="{$orderslist[current].order_history_info_href}">{$smarty.const.SMALL_IMAGE_BUTTON_VIEW}</a></td>
	 </tr>
	{/section}
	</tbody>
</table>
{/if}

</div>