{$BEFORE_CONTENT} 
{$BETWEEN_HL_CONTENT}

{* ------------------------------- NEWS DETAIL VIEW --------------------------------------- *}
{if $arr_newsdetails}
<div id="news_detailsHOLDER">
    <!-- NEWS DETAIL NAVI -->
    <div id="news_detailsNAVI" class="headerfarbe">
      <div id="news_detailsTOLIST"><a href="{$HEADER_NEWS_ALL_LINK}"><span>{$arr_shopnewsbox.title}&nbsp;</span></a></div>
      <div id="news_detailsSINGLE">
        {if $found == 'true'}
          {if $first_link}<a href="{$first_link}" id="news_prev"><div></div></a>{/if}
          <div id="news_detailsSINGLEtxt">News</div>
          {if $next_link}<a href="{$next_link}" id="news_next"><div></div></a>{/if}
        {/if}
      </div>
			<div class="clearer"></div>
    </div>
    <!-- NEWS DETAIL NAVI -->
		<!-- NEWS DETAIL MAIN -->
    <div id="news_details">
      <div id="news_detailsHEADER" class="listerfarbe"><span>{$arr_newsdetails.news_date}</span></div>
      <div id="news_detailsCNT">
        <h1>{$arr_newsdetails.news_title}</h1>
        {if $arr_newsdetails.news_picture}<div id="news_detailsPIC"><img src="images/user/{$user_id}/{$arr_newsdetails.news_picture}" border="0" /></div>{/if}
        <div id="news_detailsARTICLE">{$arr_newsdetails.news_article}</div>
        <div class="clearer"></div>
      </div>
    </div>
		<!-- NEWS DETAIL MAIN -->

		{if $arr_newsdetails.news_author_id}
    <!-- NEWS DETAIL AUTHOR -->
    <div id="news_detailsAUTHOR">
			{if $arr_newsdetails.picture_src}
      <div id="news_detailsAUTHORpic"><img src="{$arr_newsdetails.picture_src}" border="0" /></div>
			{/if}
      <div id="news_detailsAUTHORinfo">
        <div id="news_detailsAUTHORname">{$arr_newsdetails.news_author_name}</div>
        {if $arr_newsdetails.news_author_position}
        <div id="news_detailsAUTHORpos">{$arr_newsdetails.news_author_position}</div>
        {/if}
				{if $arr_newsdetails.news_author_signatur}
				<div id="news_detailsAUTHORsign">{$arr_newsdetails.news_author_signatur}</div>
				{/if}
      </div>
			<div class="clearer"></div>
    </div>
    <!-- NEWS DETAIL AUTHOR -->
		{/if}
			
		<!-- NEWS DETAIL PRODUCTLISTER -->
	  {if $arr_products_list}
		<div id="news_detailsPRODUCTS">
      <div id="news_detailsPRODUCTShead" class="headerfarbe"><h6>Zugehörige Produkte</h6></div>
      <div id="news_detailsPRODUCTSlist">
		 	<table width="100%" border="0" cellpadding="5" cellspacing="0" {*class="news_detailsPRODUCTSlist {if $smarty.foreach.productslister.last}noBorder{/if}"*}>
      {foreach from=$arr_products_list key=products_id item=arr_product name=productslister}	
        <tr>
          <td width="25%" class="productListing-datacol"><a href="{$arr_product.seolink}">{$arr_product.products_image}</a></td>	
          <td width="25%" align="left" valign="top" class="productListing-datacol"><a href="{$arr_product.seolink}" class="productListingName">{$arr_product.products_name}</a></td>
          <td width="35%" align="right" valign="top" class="productListing_pricenew productListing-datacol">
          {if $arr_product.special_price_brutto}
							{$arr_product.special_price_brutto}
              {$arr_product.products_brutto}
            {else}
              {$arr_product.products_brutto_price}
          {/if}
          </td>
          <td width="15%" align="right" valign="top" class="productListing-datacol">
            <form action="/product_info.php?action=add_product&cPath={$arr_product.cPath}&products_id={$arr_product.products_id}">
              <input type="hidden" name="products_id" value="{$arr_product.products_id}" />
              <input type="image" border="0" title="In den Warenkorb" alt="In den Warenkorb" src="{$imagepath}/buttons/{$language}/button_in_cart2.gif" />
            </form>
          </td>
        </tr>
        <tr>
          <td colspan="4" align="left" valign="top" class="prodlistLine"><img src="{$imagepath}/spacer.gif" width="1" height="1"></td>
        </tr>
      {/foreach}	
	 		</table>
      </div>
		</div>
		{/if}
		<!-- NEWS DETAIL PRODUCTLISTER -->
</div>
{/if}
{* ------------------------------- NEWS DETAIL VIEW --------------------------------------- *}

{* ------------------------------- NEWS LISTING VIEW --------------------------------------- *}
{if $arr_newslistview}
<div id="news_listerHOLDER">
    <div id="news_listerNAVI">
      <div id="news_listerHEADER" class="headerfarbe">{$arr_shopnewsbox.title}&nbsp;</div>
      <div class="listerfarbe news_listerSPLIT">
        <div class="news_listerLNKLIST">
        {foreach from=$arr_newslistview.arr_pages_links key=linkkey item=link}
          {if $arr_newslistview.current_page == $linkkey}<span class="news_listerACT">{$linkkey}</span>{else}<a href="{$link}" class="news_listerLNK"><span>{$linkkey}</span></a>{/if}
        {/foreach}
       </div>
	     <div class="clearer"></div>
     </div>
    </div>

    <div id="news_lister">
    {foreach from=$arr_newslistview.arr_current_news key=newskey item=newsitem name=tegos}
    	<div class="news_listerARTICLE {if $smarty.foreach.tegos.last}noBorder{/if}">
			<div class="news_listerDATE"><span>{$newsitem.news_date}</span></div>
    	<div class="news_listerHEAD"><h2><a href="{$newsitem.seolink}">{$newsitem.news_title}</a></h2></div>
			<div class="news_listerSHORTY" style="width:{if $newsitem.news_picture}73%;{else}100%;{/if}">
      {if $newsitem.news_teaser != ""}
        {$newsitem.news_teaser|truncate:230:"...":true}<a href="{$newsitem.seolink}">{$arr_smartyconstants.TEXT_NEWS_MORE}</a></div>
      {else}
        {$newsitem.news_article|truncate:230:"...":true}<a href="{$newsitem.seolink}">{$arr_smartyconstants.TEXT_NEWS_MORE}</a></div>
      {/if}
      {if $newsitem.news_picture}<div class="news_listerPIC"><img src="{$newsitem.news_picture}" border="0" /></div>{/if}
		  <div class="clearer"></div>
    </div>
    {/foreach}
</div>

<div class="listerfarbe news_listerSPLIT">
	<div class="news_listerLNKLIST">
  {foreach from=$arr_newslistview.arr_pages_links key=linkkey item=link}
    {if $arr_newslistview.current_page == $linkkey}<span class="news_listerACT">{$linkkey}</span>{else}<a href="{$link}" class="news_listerLNK"><span>{$linkkey}</span></a>{/if}
  {/foreach}
	</div>
	<div class="clearer"></div>
  </div>
</div>
{/if}

{* ------------------------------- NEWS LISTING VIEW --------------------------------------- *}		
	{if $TEXT_NEWS_NOT_EXISTS}
		 {$TEXT_NEWS_NOT_EXISTS}
	{/if}
	
{$AFTER_CONTENT}