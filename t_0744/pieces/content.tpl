{if $is_default}
    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
        {*<section class="firstcolon">*}
            {*<div class="container">*}
                {*<div class="tp-banner-container">*}
                    {*<div class="tp-banner" >*}
                        {*{$user_html}*}
                        {*<div class="tp-bannertimer"></div>*}
                    {*</div>*}
                {*</div>*}
            {*</div>*}
        {*</section>*}
    {*{/if}*}
    {if $slider_images != ""}
    <section class="firstcolon">
        <div class="container">
            <div class="tp-banner-container">
                <div class="tp-banner" >
                    <ul>
                        {foreach from=$slider_images item=slider name=name_slider}
                        <li data-transition="fade" data-slotamount="5" data-masterspeed="700" >
                            <img src="{$slider.image}" alt="{$slider.title}" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                            <div class="tp-caption large_bold_grey skewfromrightshort customout"
                                 data-x="center"
                                 data-hoffset="0"
                                 data-y="240"
                                 data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                 data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                 data-speed="1000"
                                 data-start="500"
                                 data-easing="Back.easeInOut"
                                 data-endspeed="300">{$slider.title}
                            </div>
                            {if $slider.description}
                            <div class="tp-caption medium_bg_asbestos fade"
                                 data-x="center"
                                 data-y="300"
                                 data-speed="500"
                                 data-start="800"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="300"
                                 data-endeasing="Power1.easeIn"
                                 data-captionhidden="off"
                                 style="z-index: 6">{$slider.description}
                            </div>
                            {/if}
                            {if $slider.button_title}
                            <div class="tp-caption fade"
                                 data-x="center"
                                 data-y="350"
                                 data-speed="500"
                                 data-start="1200"
                                 data-easing="Power4.easeOut"
                                 data-endspeed="300"
                                 data-endeasing="Power1.easeIn"
                                 data-captionhidden="off"
                                 style="z-index: 6"><a class="btn btn-default">{$slider.button_title}</a>
                            </div>
                            {/if}
                        </li>
                        {/foreach}
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </div>
    </section>
    {/if}
    <section class="secondcolon">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                {$FILENAME_FEATURED_HTML}
                {$UPCOMING_PRODUCTS_HTML}
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                {include file="boxes/box_whats_new.tpl"}
            </div>
            {if $arr_bestsellersbox}
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    {include file="boxes/best-sellers.tpl"}
                </div>
            {/if}
        </div>
    </section>
{/if}
{if $products_not_found_message}
    {$products_not_found_message}
{/if}
{if $is_nested}
    {*{if !empty($categories_html)}*}
        {*{$categories_html}*}
    {*{/if}*}
    {*{if isset($categorieslist)}*}

    {*{else}*}
        {*{$FILENAME_FEATURED_HTML}*}
    {*{/if}*}
    {*{if !empty($categories_html_footer)}*}
        {*{$categories_html_footer}*}
    {*{/if}*}
{/if}
{if $is_products == 1}
    {$smarty.const.HEADING_TITLE}
    {if $filterlistisnotempty}
        {$filter_form}
        {$smarty.const.TEXT_SHOW}
    {/if}
    {*{if !empty($categories_html)}*}
        {*{$categories_html}*}
    {*{/if}*}
    {if !empty($manufacturers_html_header)}
        {$manufacturers_html_header}
    {/if}
    {if !empty($manufacturers_html_header) || isset($categorieslist)}
        {if isset($categorieslist)}
            {if $categorieslist|@count > 0}
            {/if}
        {else}
            {if !empty($manufacturers_html_header)}
            {/if}
        {/if}
    {/if}
    {if !empty($PRODUCT_LISTING_HTML)}
        <div class="container">
            {$PRODUCT_LISTING_HTML}
        </div>
    {/if}
    {if !empty($manufacturers_id)}
        {$manufacturers_html_footer}
    {/if}
    {if !empty($categories_html_footer)}
        {$categories_html_footer}
    {/if}
{else}
    {if $is_default}
        &nbsp;
    {else}
        <div class="container">
            <p class="block">{$tep_image_category}</p>
            <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
        </div>
    {/if}
{/if}
