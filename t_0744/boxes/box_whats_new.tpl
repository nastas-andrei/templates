{if $arr_whatsnewbox}
    <h2 class="widget-title">
        <span>{$arr_whatsnewbox.title}</span>
    </h2>
    <div class="sidebar-line"><span></span></div>
    <div class="best-sellers">
        {foreach from= $arr_whatsnewbox.products key=key item=product}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
            {/if}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="product1 imagechange-3d image-hover he-wrap tpl5" style="width:245px;height:245px">
                    <div class="imagechange-3d-inner">
                        <div class="imgchange-1">
                            <div class="image_view_product" style="background-image:url('{$src}')"></div>
                        </div>
                        <div class="imgchange-2">
                            <div class="image_view_product" style="background-image:url('{$src}')"></div>

                        </div>
                    </div>
                    <div class="he-view" onclick="location.href='{$product.link}'">
                        <a href="{$product.buy_now_link}" class="price a1" data-animate="jellyInDown"><i class="fa fa-shopping-cart"></i></a>
                        <a href="{$product.link}" class="buy a0" data-animate="jellyInDown"><i class="fa fa-search"></i></a>
                    </div>
                </div>
                <div class="product-details">
                    <a href="{$product.link}">
                        <h4>{$product.name|truncate:15:"...":true}</h4>
                    </a>
                    <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                </div>
            </div>
        {/foreach}
    </div>
{/if}
