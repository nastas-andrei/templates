{include file="header.tpl"}
<div id="wrapper" class="container canvas main-container">

    <div class="navigation-controls clearfix">
      <div class="pull-left">
        <i class="icon-caret-left"></i> <a href="{$home_link}">Home</a>
      </div>
    </div>     
	
<div class="checkout-container">
	{$content}
</div>
	

{include file="footer.tpl"}