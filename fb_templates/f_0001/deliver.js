var month = '0'; 
var day = '+1';   
var dow = 0;     
var hour = 15;
var tz = 1;  
var lab = 'datecountdown';
var ddeliver = 'daydeliver';
// Make day number value correspond to actual day name
var ddd=new Date();
var dayName=new Array(7)
dayName[0]="Montag"; // Eigentlich Sonntag, small fix ;)
dayName[1]="Montag";
dayName[2]="Dienstag";
dayName[3]="Mittwoch";
dayName[4]="Donnerstag";
dayName[5]="Freitag";
dayName[6]="Samstag";

function start() {displayCountdown(setCountdown(month,day,hour,tz),lab);}
loaded(lab,start);

var pageLoaded = 0; window.onload = function() {pageLoaded = 1;}
function loaded(i,f) {if (document.getElementById && document.getElementById(i) != null) f(); else if (!pageLoaded) setTimeout('loaded(\''+i+'\','+f+')',100);
}

function setCountdown(month,day,hour,tz) {var m = month; if (month=='*') m = 0;  var c = setC(m,day,hour,tz); if (month == '*' && c < 0)  c = setC('*',day,hour,tz); return c;} function setC(month,day,hour,tz) {var toDate = new Date();if (day.substr(0,1) == '+') {var day1 = parseInt(day.substr(1));toDate.setDate(toDate.getDate()+day1);} else{toDate.setDate(day);}if (month == '*')toDate.setMonth(toDate.getMonth() + 1);else if (month > 0) { if (month <= toDate.getMonth())toDate.setYear(toDate.getYear() + 1);toDate.setMonth(month-1);}
if (dow >0) toDate.setDate(toDate.getDate()+(dow-1-toDate.getDay())%7);
toDate.setHours(hour);toDate.setMinutes(0-(tz*60));toDate.setSeconds(0);var fromDate = new Date();fromDate.setMinutes(fromDate.getMinutes() + fromDate.getTimezoneOffset());var diffDate = new Date(0);diffDate.setMilliseconds(toDate - fromDate);return Math.floor(diffDate.valueOf()/1000);}
function displayCountdown(countdn,lab) 
{
	if (countdn < 0) document.getElementById(lab).innerHTML = "Sorry, you are too late."; else 
		{
			var secs = countdn % 60; 
			if (secs < 10) secs = '0'+secs;var countdn1 = (countdn - secs) / 60;
			var mins = countdn1 % 60; 
			if (mins < 10) mins = '0'+mins;
			countdn1 = (countdn1 - mins) / 60;
			var hours = countdn1 % 24;
			var days = (countdn1 - hours) / 24;
			document.getElementById(lab).innerHTML = hours+' Std. '+mins+' Min. ';
			setTimeout('displayCountdown('+(countdn-1)+',\''+lab+'\');',999);
			if (hours > hour && mins > 0) {
			   document.getElementById(ddeliver).innerHTML = dayName[ddd.getDay()+parseInt(day)+1];
			}	else {
				  document.getElementById(ddeliver).innerHTML = dayName[ddd.getDay()+parseInt(day)];
			}
		}
}