<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html {$smarty.const.HTML_PARAMS}>

{include file="pieces/popup_head.tpl"}

<body marginwidth="0" marginheight="0" topmargin="0" bottommargin="0" leftmargin="0" rightmargin="0" >
	<table width="100%" style="background-color:#ffffff">
		<tr>
			<td style="padding:5px;">
				{$info_box_heading}
				{$info_box_content}
				<p class="smallText" align="right"><a href="javascript:window.close()">{$smarty.const.TEXT_CLOSE_WINDOW}</a></p>
			</td>
		</tr>
	</table>
</body>
</html>

			