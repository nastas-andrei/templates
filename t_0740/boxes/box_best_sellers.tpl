{if $arr_bestsellersbox == ''}
    <div class="widget">
        <div class="title">
            <h2>{$arr_whatsnewbox.title}</h2>
        </div>
        <ul class="recent_posts_widget">
            {foreach from=$arr_whatsnewbox.products key=key item=product}
                <li>
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <a href="{$product.link}">
                        <img src="{$src}" height="75px" width="75" />
                    </a>

                    <div class="overflow-top-best">
                        <a href="{$product.link}" title="{$product.name}">
                            {$product.name}
                        </a>
                    </div>
                    <span class="price readmore">{$product.preisneu}</span>
                </li>
            {/foreach}
        </ul>
    </div>
{else}
    <div class="widget">
        <div class="title">
            <h2>{$arr_bestsellersbox.title}</h2>
        </div>
        <ul class="recent_posts_widget">
            {foreach from=$arr_bestsellersbox.products key=key item=product}
                <li>
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <a href="{$product.link}">
                        <img src="{$src}" height="75px" width="75"/>
                    </a>

                    <div class="overflow-top-best">
                        <a href="{$product.link}" title="{$product.name}">
                            {$product.name}
                        </a>
                    </div>
                    <span class="price readmore">{$product.preisneu}</span>
                </li>
            {/foreach}
        </ul>
    </div>
{/if}