{$advanced_search_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}
      	<tr>
        	<td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0">
        			{if $is_search_message}
      				<tr>
        				<td>{$search_message}</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				{/if}
      				<tr>
        				<td width="175">
							{$info_box_content}
       					</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_10_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td class="smallText"><a href="javascript:popupWindow('{$popup_search_help_href}')">{$smarty.const.TEXT_SEARCH_HELP_LINK}</a></td>
            						<td class="smallText" align="right">{$button_search_img_submit}</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_10_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
							              	<tr>
							              		<td class="fieldKey">{$smarty.const.ENTRY_CATEGORIES}</td>
							                	<td class="fieldValue">{$categories_id_field}</td>
							              	</tr>
							              	<tr>
								                <td class="fieldKey">&nbsp;</td>
							                	<td class="smallText">{$inc_subcat_field} {$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}</td>
							              	</tr>
							              	<tr>
							                	<td colspan="2">{$pixel_trans_100_10}</td>
							              	</tr>
							              	{if $central_user_id  != "12581"}<!-- itter ohg -->											
              								<tr>
                								<td class="fieldKey">{$smarty.const.ENTRY_MANUFACTURERS}</td>
                								<td class="fieldValue">{$manufacturers_id_field}</td>
              								</tr>
              								<tr>
                								<td colspan="2">{$pixel_trans_100_10}</td>
              								</tr>
              								{/if}
              								<tr>
                								<td class="fieldKey">{$smarty.const.ENTRY_PRICE_FROM}</td>
                								<td class="fieldValue">{$pfrom_field}</td>
              								</tr>
              								<tr>
                								<td class="fieldKey">{$smarty.const.ENTRY_PRICE_TO}</td>
                								<td class="fieldValue">{$pto_field}</td>
              								</tr>
              								<tr>
                								<td colspan="2">{$pixel_trans_100_10}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
        		</table>
        	</td>
      	</tr>
	</table>
	
	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
</form>    