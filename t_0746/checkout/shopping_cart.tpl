<ol class="steps">
    <li class="step1 current">
        <span>
            <a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a>
        </span>
    </li>
    <li class="step2"><span>{$smarty.const.CHECKOUT_BAR_DELIVERY}</span></li>
    <li class="step3"><span>{$smarty.const.CHECKOUT_BAR_PAYMENT}</span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>

{$cart_quantity_form}

{if $any_out_of_stock == 1}
    {if $smarty.const.STOCK_ALLOW_CHECKOUT == 'true'}
        <div id="stockWarning" class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                <i class="fa fa-remove"></i>
            </button>
            {$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}
        </div>
    {else}
        <div id="stockWarning" class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                <i class="fa fa-remove"></i>
            </button>
            {$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}
        </div>
    {/if}

{/if}


{if $count_contents > 0}
    {$hidden_fields}
    {$table_box_content}
    <div class="pull-right m_bottom_20">
        <span>{$smarty.const.SUB_TITLE_SUB_TOTAL}</span>
        <span class="total-amount-shopping_cart">{$formated_products_price}</span>
    </div>
    <div class="clearfix"></div>
    <div class="pull-left">
        <button type="button" class="button-send btn-small" onclick="window.location='{$seo_link_back_href}';">
            &larr;&nbsp;&nbsp;{$arr_smartyconstants.IMAGE_BUTTON_CONTINUE_SHOPPING}
        </button>
    </div>
    <div class="pull-right">
        <button type="button" class="button-send btn-small" onclick="window.location='{$checkout_shipping_href}';">
              {$arr_smartyconstants.IMAGE_BUTTON_CHECKOUT}<i class="fa fa-shopping-cart"></i>
        </button>
    </div>
    <div class="clearfix m_bottom_20"></div>
{else}
    <div id="stockWarning" class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-remove"></i></button>
        {$smarty.const.TEXT_CART_EMPTY}
    </div>
    <div class="text-center">
        <a class="button-send btn-small" href="{$seo_index_link}">&larr;&nbsp;&nbsp;{$arr_smartyconstants.IMAGE_BUTTON_BACK}</a>
    </div>
{/if}
</form>