{include file="header.tpl"}
{if $DESIGN_VARIANT}
    {assign var=imagepathcss value=$imagepath|replace:'images':''}
    {assign var=imagepathcss value='images_'}
    {assign var=imagepathcss2 value='/buttons'}
    {assign var=imagepathcss value=$imagepathcss$DESIGN_VARIANT$imagepathcss2}

    {assign var=imagepathsubcat value='images_'}
    {assign var=imagepathsubcat2 value='/subcat.gif'}
    {assign var=imagepathsubcat value=$imagepathsubcat$DESIGN_VARIANT$imagepathsubcat2}

{/if}
<div id="sns_content2" class="wrap">
    <div class="container">
        {if $pAnfrage}
            {if $pAnfrage == 1}
                {include file="modules/modul_additional_price_query.tpl"}
            {else}
                {include file="modules/modul_additional_price_query.tpl"}
            {/if}
        {else}
            {if $arr_product_info.product_check == true}
                <div class="row-fluid">
                    <div id="sns_main" class="span12 col-main">
                        <!-- global messages -->
                        <div id="sns_mainmidle" class="span12 clearfix">
                            <!-- primary content -->
                            <script type="text/javascript" src="templates/t_0736/lib/tabulous.js"></script>
                            {literal}
                                <script type="text/javascript">
                                    var optionsPrice = new Product.OptionsPrice([]);
                                    $sns_jq(document).ready(function ($) {
                                        $('#sns_tab_products').tabulous({
                                            effect: 'slideLeft',
                                            tabcontainer: '.sns-tab-content'
                                        });
                                    });
                                </script>
                            {/literal}
                            <div class="product-view sns-product-detail">
                                <div class="product-essential clearfix">
                                    <div class="product-img-box span4">
                                        <div class="product-image-zoom">
                                            {if $arr_product_info.image_source}
                                                {if $arr_product_info.image_linkLARGE}
                                                    <a id="sns_cloudzoom" class="cloud-zoom"
                                                       rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1"
                                                       title="{$arr_product_info.products_name}" href="{$arr_product_info.image_linkLARGE}">
                                                        <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}"
                                                             title="{$arr_product_info.products_name}" />
                                                        <!-- <span class='ico-product ico-new'>New</span>     -->
                                                    </a>
                                                {else}
                                                    <a id="sns_cloudzoom" class="cloud-zoom"
                                                       rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1"
                                                       title="{$arr_product_info.products_name}" href="{$arr_product_info.image_link}">
                                                        <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}"
                                                             title="{$arr_product_info.products_name}" />
                                                        <!-- <span class='ico-product ico-new'>New</span>     -->
                                                    </a>
                                                {/if}
                                            {else}
                                                <a title="{$arr_product_info.products_name}">
                                                    <img id="image" src="http://placehold.it/500x500&text=No%20image" alt="{$arr_product_info.products_name}"
                                                         title="{$arr_product_info.products_name}" />
                                                </a>
                                            {/if}
                                            <div class="popup-btn">
                                                <a title="{$arr_product_info.products_name}" id="sns_popup" data-rel="prettyPhoto"
                                                   href="{$arr_product_info.image_linkLARGE}"></a>
                                            </div>
                                        </div>
                                        <!--<p class="a-center" id="track_hint"></p>-->
                                        <div class="more-views clearfix">
                                            <div class="nav-gallery clearfix">
                                                <span class="prev" style="display:none"></span>
                                                <span class="next" style="display:none"></span>
                                            </div>
                                            <div class="slide-gallery">
                                                <!--<h4></h4>-->
                                                <ul class="gallery" id="sns_gallery">
                                                    <li>
                                                        {if $arr_product_info.image_source}
                                                            {if $arr_product_info.image_linkLARGE}
                                                                <a class="cloud-zoom-gallery" rel="useZoom: 'sns_cloudzoom', smallImage: '{$arr_product_info.image_link}' "
                                                                    title="{$arr_product_info.products_name}" href="{$arr_product_info.image_linkLARGE}">
                                                            {else}
                                                                <a class="cloud-zoom-gallery" rel="useZoom: 'sns_cloudzoom', smallImage: '{$arr_product_info.image_link}' "
                                                                   title="{$arr_product_info.products_name}" href="{$arr_product_info.image_link}">
                                                            {/if}
                                                                <img src="{$arr_product_info.image_link}" alt="{$arr_additional_images.title}"
                                                                     title="{$arr_additional_images.title}" class="additional-image"/>
                                                            </a>
                                                        {else}
                                                            <img src="http://placehold.it/80x80&text=No%20image"/>
                                                        {/if}
                                                    </li>
                                                    {include file="modules/modul_additional_images.tpl"}
                                                </ul>
                                            </div>
                                        </div>
                                        {literal}
                                            <script type="text/javascript">
                                                $sns_jq(function ($) {
                                                    //$sns_jq(window).load(function(){
                                                    $sns_jq("a[data-rel^='prettyPhoto']").prettyPhoto({
                                                        /*theme: 'facebook',*/
                                                        social_tools: false,
                                                        show_title: false
                                                    });
                                                    $sns_jq('.cloud-zoom').CloudZoom({showTitle: false});
                                                    $sns_jq('a.cloud-zoom-gallery').bind('click', function () {
                                                        $sns_jq('a#sns_popup').attr('href', $sns_jq(this).attr('href'));
                                                        return false;
                                                    });
                                                    $sns_jq('#sns_gallery li').each(function () {
                                                        if ($sns_jq(this).find('a').attr('href') == $sns_jq('a#sns_popup').attr('href')) {
                                                            $sns_jq(this).addClass('active');
                                                        }
                                                    });
                                                    $sns_jq('#sns_gallery li a').bind('click', function () {
                                                        $sns_jq('#sns_gallery li').removeClass('active');
                                                        $sns_jq(this).parent().addClass('active');
                                                        return false;
                                                    });
                                                });
                                            </script>
                                            <script type="text/javascript">
                                                //$sns_jq(function($) {
                                                $sns_jq(window).load(function () {
                                                    width_ga = $sns_jq(".slide-gallery").outerWidth() / 4;

                                                    //var carousel = $sns_jq("ul#sns_gallery");
                                                    $sns_jq("ul#sns_gallery").carouFredSel({
                                                        onWindowResize: "throttle",
                                                        responsive: true,
                                                        auto: false,
                                                        width: '100%',
                                                        scroll: {
                                                            items: 1,
                                                            duration: 500
                                                        },
                                                        items: {
                                                            width: 100,
                                                            height: 'auto',  //  optionally resize item-height
                                                            visible: {
                                                                min: 1,
                                                                max: 100
                                                            }
                                                        },
                                                        prev: '.more-views .prev',
                                                        next: '.more-views .next'
                                                        /*,
                                                         onCreate : function () {
                                                         $sns_jq(window).on('resize', function(){
                                                         carousel.parent().add(carousel).css('height', carousel.children().first().height() + 'px');
                                                         }).trigger('resize');
                                                         }*/

                                                    });
                                                });
                                            </script>
                                        {/literal}
                                    </div>
                                    <div id="product-shop" class="product-shop span5" data-tablet="product-shop span8"
                                         data-mobile="product-shop span8">
                                        <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post"
                                              id="product_addtocart_form">
                                            <h1 class="title-modal">{$arr_product_info.products_name}</h1>

                                            <div class="price-box">
                                                <span class="regular-price" id="product">
                                                  <span class="price">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</span>             .
                                                </span>
                                            </div>
                                            <!-- AddThis Button BEGIN -->
                                            <div class="addthis_toolbox addthis_default_style ">
                                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                                <a class="addthis_button_tweet"></a>
                                                <a class="addthis_button_pinterest_pinit" pi:pinit:layout="horizontal"></a>
                                                <a class="addthis_counter addthis_pill_style"></a>
                                            </div>
                                            {literal}
                                                <script type="text/javascript">var addthis_config = {"data_track_addressbar": false};</script>
                                                <script type="text/javascript"
                                                        src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-507b2455057cfd5f"></script>
                                            {/literal}
                                            <!-- AddThis Button END -->
                                            <div class="add-to-cart">
                                                {if $LOAD_CLIENT}
                                                    <button type="submit" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button btn-cart have-borderinset"
                                                            onclick="productAddToCartForm.submit(this)">
                                                              <span>
                                                                <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                              </span>
                                                    </button>
                                                    <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                                                {else}
                                                    <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button btn-cart have-borderinset"
                                                            disabled onclick="productAddToCartForm.submit(this)">
                                                              <span>
                                                                <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                                                              </span>
                                                    </button>
                                                {/if}
                                            </div>
                                            <div style="margin:10px 0">
                                                {if $arr_product_info.arr_products_options}
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                                        </label>
                                                        {if $arr_product_info.product_options_javascript}
                                                            {$arr_product_info.product_options_javascript}
                                                        {/if}
                                                        <table>
                                                            {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                                                <tr>
                                                                    <td>{$productoption.name}:</td>
                                                                    <td>{$productoption.pulldown}</td>
                                                                </tr>
                                                            {/foreach}
                                                        </table>
                                                    </div>
                                                {/if}
                                                {*-------------------------------------------*}
                                                {if $opt_count > 0}
                                                    {if $var_options_array}
                                                        <input type="hidden" id="master_id" value="{$master_id}">
                                                        <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                                        <input type="hidden" id="cPath" value="{$cPath}">
                                                        <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                                        {if $Prodopt}
                                                            <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                                        {/if}
                                                        {if $prod_hidden}
                                                            {foreach from=$prod_hidden key=key item=hidden_input}
                                                                {$hidden_input}
                                                            {/foreach}
                                                        {/if}
                                                        {if $select_ids}
                                                            {foreach from=$select_ids key=key item=select_input}
                                                                {$select_input}
                                                            {/foreach}
                                                        {/if}
                                                        {if $javascript_hidden}
                                                            {$javascript_hidden}
                                                        {/if}
                                                        {foreach from=$var_options_array key=key item=options}
                                                            <label>{$options.title}:</label>
                                                            {$options.dropdown}
                                                        {/foreach}
                                                    {/if}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $arr_product_info.base_price}
                                                    {$arr_product_info.base_price}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $arr_product_info.shipping_value gt 0}
                                                    <label class="control-label pull-left">{$TEXT_SHIPPING}:</label>
                                                    ({$arr_product_info.shipping})
                                                    <div class="clearfix"></div>
                                                {/if}

                                                {*---------------------------------------------*}
                                                {if $arr_product_info.products_uvp}
                                                    {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                                    {$arr_product_info.products_uvp}
                                                    {$arr_product_info.TEXT_YOU_SAVE}:
                                                    {$arr_product_info.diff_price}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $count_bestprice gt 0}
                                                    {$staffelpreis_hinweis}

                                                    {foreach from=$bestprice_arr key=key item=bestprice}
                                                        {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                                        {$bestprice.ausgabe_stueck}
                                                        {$bestprice.ausgabe_preis}
                                                    {/foreach}
                                                    <div class="clearfix"></div>
                                                {/if}

                                                {*-------------------------------------------*}

                                                {if $PRICE_QUERY_ON eq 'true'}
                                                    <div style="font-size:15px;margin: 10px 0;">
                                                        {$PRICE_QUERY}:
                                                        <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $PRODUCTS_QUERY_ON eq 'true'}
                                                    <div style="font-size:15px;margin: 10px 0;">
                                                        {$PRODUCTS_INQUIRY_TEXT}: <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                {/if}

                                                {*-------------------------------------------*}

                                                {* $arr_product_info.availability.text => z.b. "Sofort lieferbar"*}
                                                {* $arr_product_info.availability.value => 0-3 => 0 = Keine Angabe, 1 = Sofort lieferbar, 2 = beschr�nkt lieferbar, 3 = nicht lieferbar *}
                                                {if count($arr_product_info.availability)}

                                                    {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                                    {$arr_product_info.availability.text}

                                                {/if}

                                                {*-------------------------------------------*}
                                                {if count($arr_product_info.delivery_time)}
                                                    {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                                    {$arr_product_info.delivery_time.text} *
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $arr_product_info.text_date_available}
                                                    &nbsp;
                                                    {$arr_product_info.text_date_available}
                                                {/if}
                                                {*-------------------------------------------*}
                                                {if $arr_product_info.products_url}
                                                    &nbsp;
                                                    {$arr_product_info.products_url}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $SANTANDER_LOANBOX}
                                                    <span id="santandertitle_pinfo">
                                                        {$SANTANDER_LOANBOX_TITLE}:
                                                      </span>
                                                    &nbsp;
                                                    {$SANTANDER_LOANBOX}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $PRODUCTS_WEIGHT_INFO}
                                                    {if $arr_product_info.products_weight > 0}
                                                        {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                                        {$arr_product_info.products_weight}
                                                        &nbsp;
                                                        {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                                    {/if}
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if count($arr_product_info.products_model)}
                                                    <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">Model: </span>
                                                    {$arr_product_info.products_model}
                                                    <br />
                                                {/if}

                                                {*-------------------------------------------*}
                                                {if $arr_manufacturer_info.manufacturers_name}
                                                    <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">{$manufacturer}: </span>
                                                    {if $arr_manufacturer_info.manufacturers_image neq ""}
                                                        {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                            <a href="{$arr_manufacturer_info.manufacturers_url}">
                                                                <img src="{$arr_manufacturer_info.manufacturers_image}"
                                                                     alt="{$arr_manufacturer_info.manufacturers_name}">
                                                            </a>
                                                        {else}
                                                            <img src="{$arr_manufacturer_info.manufacturers_image}"
                                                                 alt="{$arr_manufacturer_info.manufacturers_name}">
                                                        {/if}
                                                    {else}
                                                        {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                            <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                                        {else}
                                                            {$arr_manufacturer_info.manufacturers_name}
                                                        {/if}
                                                    {/if}
                                                {/if}

                                                {*---------------------------------------------*}
                                                {if $widget_view && $widget_view eq 1}
                                                    <div id="widget_views"></div>
                                                {/if}
                                            </div>
                                        </form>
                                    </div>
                                    <div class="span3 has-custom" data-tablet="hidden" data-mobile="hidden">
                                        <div class="block block-privacypolicy">
                                            <div class="block-title"><span>{$smarty.const.ENTRY_DATENSCHUTZ}</span></div>
                                            <div class="block-content">
                                                <ul class="custom-block">
                                                    <li class="shipping-policy"><span>{$smarty.const.TEXT_DELIVERY_HINT}</span></li>
                                                    <li class="money-back"><span>{$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}</span></li>
                                                    <li class="order-247"><span>{$smarty.const.TEXT_FREE_SHIPPING }</span></li>
                                                    <li class="special-event">
                                                        <span>{$smarty.const.TEXT_PRODUCT_POINTS}</span></li>
                                                    <li class="gift"><span>{$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}</span></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearer"></div>
                                    <!--Tabs Navigation-->
                                    <ul class="nav nav-tabs" style="margin:20px 0 -1px 0">
                                        {if $arr_product_info.products_description}
                                            <li class="active">
                                                <a href="#description" data-toggle="tab">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                                            </li>
                                        {/if}
                                        {if $arr_modul_also_purchased_products}
                                            <li {if $arr_product_info.products_description == ''}class="active"{/if}>
                                                <a href="#releated" data-toggle="tab">{$arr_modul_also_purchased_products.title}</a>
                                            </li>
                                        {/if}
                                        {if $arr_modul_xsell}
                                            <li {if $arr_modul_xsell == ''}class="active"{/if}>
                                                <a href="#accessories" data-toggle="tab">{$arr_modul_xsell.TEXT_XSELL_PRODUCTS}</a>
                                            </li>
                                        {/if}
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content ">
                                        {if $arr_product_info.products_description}
                                            <div class="tab-pane active" id="description">{$arr_product_info.products_description}</div>
                                        {/if}
                                        {if $arr_modul_also_purchased_products}
                                            <div class="tab-pane" id="releated">{include file="modules/modul_also_purchased_products.tpl"}</div>
                                        {/if}
                                        {if $arr_modul_xsell}
                                            <div class="tab-pane" id="accessories">{include file="modules/modul_xsell_products.tpl"}</div>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {*{if $foxrate && $foxrate->GetVariable('counts') != 0}*}
                        {*include file="../0_source/modules/modul_foxrate_products_info.tpl"*}
                        {*{else}*}
                        {*{/if}*}
                        {if $foxrate_rating}
                        {/if}
                    </div>
                </div>
            {else}
                {$TEXT_PRODUCT_NOT_FOUND}
            {/if}
        {/if}
    </div>
</div>
{include file="footer.tpl"}
