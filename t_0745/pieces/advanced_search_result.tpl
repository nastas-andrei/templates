<section id="advanced-search-result" class="container">
    {if $arr_modul_productlisting.products|@count lt 1}
        <p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}</p>
        <button type="button" class="btn btn-primary pull-left" onclick="window.location='javascript:history.go(-1)';">
		        <span>
		          <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
		        </span>
        </button>
    {/if}

    {if $is_search_message}
        <p class="text-center">{$search_message}</p>
    {/if}
    <div class="col-xs-12 col-sm-4 col-md-3">
        <div class="left-sidebar">
            <h2>{$smarty.const.BOX_HEADING_CATEGORIES}</h2>

            <div class="panel-group category-products" id="categories"><!--category-productsr-->
                <div id="categories-box"></div>
            </div>
            <!--/category-products-->
        </div>
    </div>
    <div class="col-xs-12 col-sm-8 col-md-9 padding-right">
        {$PRODUCT_LISTING_HTML}
    </div>
</section>