<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 done"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3 current"><span><a href="{$checkout_payment_href}">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>

{$checkout_payment_form}
<fieldset>
    {if $is_payment_error}
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
            <strong>{$payment_error_title}</strong>{$payment_error_description}
        </div>
    {/if}

    {if $is_error}
        <div class="alert alert-warning">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
            {$error_message}
        </div>
    {/if}

    <h5>{$smarty.const.TABLE_HEADING_BILLING_ADDRESS}</h5>
    <address>{$billto_address_label}</address>
    <a class="btn btn-primary btn-small" href="{$checkout_payment_address_href}">
        {$arr_smartyconstants.IMAGE_BUTTON_CHANGE_ADDRESS}
    </a>
    <h5>{$smarty.const.TABLE_HEADING_PAYMENT_METHOD}</h5>

    {if $is_multi_selection}
        <p>{$smarty.const.TEXT_SELECT_PAYMENT_METHOD}</p>
    {else}
        {if $smarty.const.TEXT_ENTER_PAYMENT_INFORMATION}
            <p>{$smarty.const.TEXT_ENTER_PAYMENT_INFORMATION}</p>
        {/if}
    {/if}
    <div class="span mam">
        {section name=current loop=$selectionlist}
        {if $selectionlist[current].is_payment_selection}
        <tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$selectionlist[current].radio_buttons}, '{$selectionlist[current].id}')">
            {else}
        <tr class="moduleRow"
            onmouseover="rowOverEffect(this)"
            onmouseout="rowOutEffect(this)"
            onclick="selectRowEffect(this, {$selectionlist[current].radio_buttons}, '{$selectionlist[current].id}')">
            {/if}

            <label class="radio main trenner anbieterbilder{$selectionlist[current].index} clearfix">
                {if $is_multi_selection}
                    {$selectionlist[current].payment_radio}
                {else}
                    {$selectionlist[current].payment_hidden}
                {/if}
                <strong>{$selectionlist[current].module}.</strong>
                {$selectionlist[current].info}
            </label>

            {if $selectionlist[current].is_error}
                <p class="text-error">{$selectionlist[current].error}</p>
            {/if}

            {if $selectionlist[current].is_fields}
                <div class="paymentsTable_{$selectionlist[current].id}">
                    {section name=current_field loop=$selectionlist[current].fields}
                        {$selectionlist[current].fields[current_field].title}
                        {$selectionlist[current].fields[current_field].field}
                    {/section}
                </div>
            {/if}
            {/section}
    </div>

    {$order_total_modules_credit_selection}

    <div class="clearfix"></div>
    <h5>{$smarty.const.TABLE_HEADING_COMMENTS}</h5>

    <div class="checkout-payment-textarea">
        {$comments_textarea}
    </div>
    {if $smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}
        <h5>{$smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}</h5>
        {$smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}
    {/if}
    <div class="clearfix">
        <a href="{$checkout_shipping_href}" class="btn btn-primary rounded pull-left">{$smarty.const.IMAGE_BUTTON_BACK}</a>
        <input type="submit" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}" class="btn btn-primary rounded pull-right" />
    </div>
</fieldset>
</form>