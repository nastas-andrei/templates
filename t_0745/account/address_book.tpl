<section id="address-book" class="container">
    <div class="row">
        <div class="col-xs-6">
             <button type="button" class="btn btn-primary" style="margin:20px 0" onclick="window.location='{$account_href}';">
                <span>
                  <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
                </span>
            </button>
            {if $is_addressbook_message}
                <p class="text-center">{$addressbook_message}</p>
            {/if}
            <div class="panel panel-info" style="margin-top:20px">
                <div class="panel-heading">
                    <h3 class="panel-title">{$smarty.const.PRIMARY_ADDRESS_TITLE}</h3>
                </div>
                <div class="panel-body">
                    {$customer_address}
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{$smarty.const.ADDRESS_BOOK_TITLE}</h3>
                </div>
                <div class="panel-body">
                    {section name=current loop=$addresseslist}
                    {$addresseslist[current].firstname}
                    {$addresseslist[current].lastname}
                    {if $addresseslist[current].is_customer_default_address}
                        <i>{$smarty.const.PRIMARY_ADDRESS}</i>
                    {/if}
                    <address>
                        {$addresseslist[current].formated_address}
                    </address>
                    <p class="block">
                        <button type="button" class="btn btn-primary" onclick="window.location='{$addresseslist[current].address_book_edit_row_href}';">
                            <span>
                              <span><i class="fa fa-pencil-square-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_EDIT}</span>
                            </span>
                        </button>
                        <!-- <a href="{$addresseslist[current].address_book_edit_row_href}" class="btn btn-small btn-info"><i class="fa fa-pencil-square-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_EDIT}</a>  -->
                        <button type="button" class="btn btn-primary btn-delete" onclick="window.location='{$addresseslist[current].address_book_delete_row_href}';">
                           <i class="fa fa-trash-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_DELETE}
                        </button>
                        <!-- <a href="{$addresseslist[current].address_book_delete_row_href}" class="btn btn-small btn-danger"><i class="fa fa-trash-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_DELETE}</a>	 -->
                    </p>
                {/section}
                </div>
            </div>
            <p class="text-center">
                {if $is_allow_add}
                <button type="button" class="btn btn-primary" onclick="window.location='{$address_book_add_href}';">
                    <span>
                      <span><i class="fa fa-plus"></i> {$smarty.const.IMAGE_BUTTON_ADD_ADDRESS}</span>
                    </span>
                </button>
                <!-- <a href="{$address_book_add_href}" class="btn btn-primary"><i class="icon-plus"></i> {$smarty.const.IMAGE_BUTTON_ADD_ADDRESS}<a/> -->
                {/if}
            </p>
        </div>
    </div>
</section>
