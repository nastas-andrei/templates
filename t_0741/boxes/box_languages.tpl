<li class="dropdown" style="width: 72px;">
    {if $arr_languagebox}
        <div id="drop-languages">
            <div class="btn-group bootstrap-select">
                <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                    <span>{$arr_languagebox.language.name}</span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    {foreach from=$arr_languagebox.items key=key item=language}
                        {if ($languageview neq $language.directory)}
                            <li rel="0" class="selected">
                                <a role="menuitem" tabindex="-1" href="{$language.link}">{$language.name}</a>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
    {/if}
</li>