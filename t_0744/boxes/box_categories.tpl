<div id="nav" class="clearfix">
    <div class="container">
        <ul id="jetmenu" class="jetmenu blue">
            {foreach from=$categories key=key item=category name=catlist}
                <li>
                    <a href="{$category.link}">{$category.name}</a>
                    {if $category.children|@count > 0}
                    <div class="megamenu full-width">
                        <div class="row">
                            {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                                <div class="col1">
                                    <h5 class="title">
                                        <a href="{$categorySub.link}">{$categorySub.name|truncate:15:"...":true}
                                        </a>
                                    </h5>
                                    <ul class="scroll_list">
                                        {if $categorySub.children|@count > 0}
                                            {foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}
                                                <li><a href="{$categorySub2.link}"><i class="fa fa-plus-circle"></i> {$categorySub2.name|truncate:15:"...":true}</a>
                                            {/foreach}
                                        {/if}
                                    </ul>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                    {/if}
                </li>
            {/foreach}
        </ul>
    </div>
</div>

