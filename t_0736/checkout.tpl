{include file="header.tpl"}
{if $DESIGN_VARIANT}
    {assign var=imagepathcss value=$imagepath|replace:'images':''}
    {assign var=imagepathcss value='images_'}
    {assign var=imagepathcss2 value='/buttons'}
    {assign var=imagepathcss value=$imagepathcss$DESIGN_VARIANT$imagepathcss2}

    {assign var=imagepathsubcat value='images_'}
    {assign var=imagepathsubcat2 value='/subcat.gif'}
    {assign var=imagepathsubcat value=$imagepathsubcat$DESIGN_VARIANT$imagepathsubcat2}

{/if}
<div class="main-container">
    <div class="container">
        <div class="checkout-container">
            {$content}
        </div>
{include file="footer.tpl"}