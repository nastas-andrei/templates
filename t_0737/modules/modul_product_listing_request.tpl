  {if $arr_modul_productlisting.products}  

  
         <div class="row" id="scroll_wrapper">  
 
           {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            <div class="col-lg-4 col-sm-4 hero-feature text-center">
              
                <div class="thumbnail" >

                  {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                  {/if}
                  <a href="{$product.link}">  
                    <div style="width: 230px; height: 280px; margin: 3px auto 0 auto; background-position: center; background-size: cover; background-image: url('{$src}')"></div>                  
                        <!-- <img src="{$src}" alt="" >     -->                                       
                  </a>
                </div>
                <div class="caption prod-caption">
                    <a href="{$product.link}">
                        <h4 class="product-name">{$product.products_name}</h4>
                    </a>
                                 
            <div class="btn-group">

                             <a href="#" class="btn btn-default">{$product.newprice|regex_replace:"/[()]/":""}</a>
                              <a href="{$product.buy_now_link}" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> Buy</a>
             </div>                                             
                  <!-- <div class="product-price">
                    {if $product.oldprice  > 0}
                    <span class="oldprice">{$product.oldprice}</span>
                    {/if} 
                    <span>{$product.newprice|regex_replace:"/[()]/":""}</span>
                    <span class="newprice">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                  </div>
                  <div class="product-action">
                        <a href="{$product.link}" class="addcart">
                            <i class="icon-basket"></i>
                            Add to cart
                        </a>
                   </div>  -->
                
                    <!-- <div class="product-rating">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i> 
                    </div> -->
                </div>
            </div>
       
        {/foreach}
    </div>
    {/if}
  
