{$contact_us_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}
		{include file="pieces/page_title_section.tpl"}
    	{$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" cellspacing="0" cellpadding="0" class="pageContentTable">
			<tr>
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
      		<tr>
        		<td>
    				{include file="pieces/page_title_section.tpl"}
        		</td>
      		</tr>
			{$smarty.const.BETWEEN_HL_CONTENT}
 	{/if}
 	
	<tr>
    	<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="contact_us_area">
				{if $contact_message_exists}
      			<tr>
        			<td>{$contact_message}</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
				{/if}
				{if $is_action_success}
      			<tr>
        			<td class="main" align="center">{$smarty.const.TEXT_SUCCESS}</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
              							<tr>
                							<td width="10">{$pixel_trans_10_1}</td>
                							<td align="right"><a href="{$button_continue_url}">{$button_continue_img}</a></td>
                							<td width="10">{$pixel_trans_10_1}</td>                							                							
              							</tr>
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
				{else}
      			<tr>
        			<td width="100%">
        				<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          					<tr>
            					<td width="100%">
	            					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	            						{if $smarty.const.CONTACT_RESPONSE_TIME  != ''}							
	              						<tr>
	                						<td class="main">{$smarty.const.CONTACT_RESPONSE_TIME}</td>
	              						</tr>
	              						<tr>
	                						<td>{$pixel_trans_100_10}</td>
	              						</tr>
										{/if}
	              						<tr>
	                						<td class="main">{$smarty.const.ENTRY_NAME}</td>
	              						</tr>
	              						<tr>
	                						<td class="main">{$name_input}</td>
	              						</tr>
	              						<tr>
	                						<td class="main">{$smarty.const.ENTRY_EMAIL}</td>
	              						</tr>
	              						<tr>
	                						<td class="main">{$email_input}</td>
	              						</tr>
	              						<tr>
	                						<td class="main">{$smarty.const.ENTRY_CONTACT_SUBJECT}</td>
	              						</tr>
	              						<tr>
											<td class="main">{$anliegen_pull_down}</td>
	              						</tr>
	              						<tr id="artikel_title" name="artikel_title" style="display:none">
	              							<td class="main">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</td>
	              						</tr>
	              						<tr id="artikel" style="display:none">
											<td class="main">{$artikel_input}</td>
										</tr>
	              						<tr id="bestellung_title" style="display:none">
	              							<td class="main">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</td>
	              						</tr>
										<tr id="bestellung" style="display:none">
											<td class="main">{$bestellung_input}</td>
										</tr>
										<tr>
                      <td class="main">{$smarty.const.ENTRY_ENQUIRY}</td>
                    </tr>
                    <tr>
                      <td class="main">{$enquiry_textarea}</td>
                    </tr>
										<tr>
                      <td class="main">&nbsp;</td>
                    </tr>              						
                    {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
										<tr>
                      <td class="main">{*$smarty.const.CONTACTFORM_CAPTCHA_INFOTEXT*}</td>
                    </tr>
                    <tr>
                      <td class="main" valign="middle">
                       <div class="antistalker hidden" style="display:none;">
                          Antispam: *<br />
                          <span>{$smarty.const.BOT_CHECK}</span>
                          <select name="pf_antispam" id="pf_antispam">
                            <option value="1" selected="selected">{$smarty.const.YES}</option>
                            <option value="2">{$smarty.const.NO}</option>
                          </select>
                        </div>
                        <script language="javascript" type="text/javascript">
                        <!--
                        var sel = document.getElementById('pf_antispam');var opt = new Option('{$smarty.const.NO}',2,true,true);sel.options[sel.length] = opt;
                        sel.selectedIndex = 1;
                        -->
                        </script>
                        <noscript>
                          <div class="antistalker">
                            Antispam: *<br />
                            <span>{$smarty.const.BOT_CHECK}</span>
                            <select name="pf_antispam" id="pf_antispam">
                              <option value="1" selected="selected">{$smarty.const.YES}</option>
                              <option value="2">{$smarty.const.NO}</option>
                            </select>
                          </div>
                        </noscript>
                        {*<table border="0" width="100%" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="50%" align="left"><img src="captcha.php" border="1" /></td>
                            <td width="50%" align="right">{$security_input}</td>
                          </tr>
												</table>*}
           						</td>
         						</tr>
         						{/if}
           					</table>
	            				</td>
    	      				</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
	        		<td>
    	    			<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
        	  				<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
              							<tr>
                							<td width="10">{$pixel_trans_10_1}</td>
                							<td align="right">{$button_continue}</td>
                							<td width="10">{$pixel_trans_10_1}</td>
              							</tr>
            						</table>            					
            						{$smarty.const.CONTACT_BOTTOM_TEXT}
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
				{/if}
       		</table>       	              
 		</td>
	</tr>
</table>    
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>		
{/if}

{$smarty.const.AFTER_CONTENT}
</form>