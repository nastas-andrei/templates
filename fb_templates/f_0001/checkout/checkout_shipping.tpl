
<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 current"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3"><span>{$smarty.const.CHECKOUT_BAR_PAYMENT}</span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>
 
{$checkout_address_form}
<fieldset>
	
	<a href="{$checkout_shipping_address_href}" class="btn btn-primary">
        <i class="icon-edit"></i>
		{$arr_smartyconstants.IMAGE_BUTTON_CHANGE_ADDRESS}
	</a>

		<address>
			<h5>{$smarty.const.TITLE_SHIPPING_ADDRESS}</h5>
			{$sendto_address_label}
		</address>
    {if $smarty.const.TEXT_CHOOSE_SHIPPING_DESTINATION}
	    {$smarty.const.TEXT_CHOOSE_SHIPPING_DESTINATION}
    {/if}
	{if $tep_count_shipping_modules_vs > 0}
    {if $is_multiple_quotes}
  	{elseif $free_shipping == false}
			{$smarty.const.TEXT_ENTER_SHIPPING_INFORMATION}
		{/if}
    {if $free_shipping == true}												
       <!--{*$smarty.const.FREE_SHIPPING_TITLE} {$quotes_icon*}-->
       {$shipping_description}
       {$shipping_hidden}
    {else}
      {section name=current loop=$quoteslist}
        <!--{*$quoteslist[current].module} {$quoteslist[current].icon*}-->
        {if $quoteslist[current].is_error}    
          {$quoteslist[current].error}
        {else}

        	{section name=current_method loop=$quoteslist[current].methods}					


        		{*{if $quoteslist[current].methods[current_method].is_row_selected} *}
        		{*active*}
        		{*{/if}          	*}
            	{if $quoteslist[current].methods[current_method].is_multiple_methods}
			
				<p><b>{$quoteslist[current].methods[current_method].title}</b></p>
			
				<label class="radio">  
					{$quoteslist[current].module}
	            	{$quoteslist[current].methods[current_method].formated_cost_with_tax}
	            	{$quoteslist[current].methods[current_method].shipping_radio}
            	</label>
				
			
              {else}
                <h6>{$quoteslist[current].module}</h6>
                {$quoteslist[current].methods[current_method].title}
                {$quoteslist[current].methods[current_method].shipping_hidden}{$quoteslist[current].methods[current_method].formated_cost_with_tax}
              {/if}
            
          	{/section}

        {/if}
      {/section}
    {/if}
	{else}
		{$smarty.const.TEXT_NO_SHIPPING}
	{/if}

	<div>
		<label>
				{$smarty.const.TABLE_HEADING_COMMENTS}
		</label>
		<div class="checkout-shipping-textarea">
			{if $tep_count_shipping_modules_vs > 0}						
			{$comments_textarea}						
		{/if}	
		</div>	
	</div>

	{if $tep_count_shipping_modules_vs > 0}
        {if $smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE && $smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}
			{$smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}<br>{$smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}
            {/if}
		<input type="submit" class="btn btn-primary rounded pull-right" value="{$arr_smartyconstants.IMAGE_BUTTON_CONTINUE}"/>
	{/if}

</fieldset>
</form>