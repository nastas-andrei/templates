<div class="page-info">
	<div class="text-center page-info-title">
		{if empty($custom_heading_title)}
			<h1 class="pprdHEAD">{$HEADING_TITLE}</h1>
		{else}
			<h1 class="pprdHEAD">{$custom_heading_title}</h1>
		{/if}
	</div>
	<div class="page-info-description">
		{$INFO_DESCRIPTION}
	</div>
</div>