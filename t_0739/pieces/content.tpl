{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}
    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
    {*<div id="content">*}
    {*<div class="slideshow">*}
    {*<div id="slideshow0" class="nivoSlider">*}
    {*{$user_html}*}
    {*</div>*}
    {*</div>*}
    {*</div>*}
    {*{/if}*}
    {if $slider_images != ""}
        <div id="content">
            <div class="slideshow">
                <div id="slideshow0" class="nivoSlider">
                    {foreach from=$slider_images item=slider}
                        <a href="{$slider.link}">
                            <img src="{$slider.image}?t[]=resize:width=900,height=400" alt="{$slider.title}" />
                        </a>
                    {/foreach}
                </div>
            </div>
        </div>
    {/if}

    {$UPCOMING_PRODUCTS_HTML}
    <div class="box">
        {$FILENAME_FEATURED_HTML}
    </div>
    {if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}
        {$define_main_bottom}
    {/if}
    <div class="box">
        {include file="boxes/box_whats_new.tpl"}
    </div>
{/if}

{if $is_nested}

    <!--       {if !empty($categories_html)}
        {$categories_html}
      {/if}   -->
    <!-- {if isset($categorieslist)}
        <div id="sns_content2" class="wrap">
          <div class="container">
            <div class="row-fluid">
              <div id="sns_main" class="span12 col-main">
                  <!-- global messages --
                <div id="sns_mainmidle" class="span12 clearfix">
                   <!-- primary content --
                  <div class="std"><div class="no-display">&nbsp;</div></div>
                  <div class="sns-pdt-content wide-4 normal-4 tabletlandscape-4 tabletportrait-3 mobilelandscape-2 mobileportrait-1">
                    <div class="pdt-content tab-content-actived is-loaded pdt_best_sales" >
                      <div class="pdt-list products-grid zoomOut">
                        {section name=current loop=$categorieslist}
                        <div class="item item-animate mobileportrait-first show-addtocart" style="-webkit-animation-delay:300ms;-moz-animation-delay:300ms;-o-animation-delay:300ms;animation-delay:300ms;">
                          <div class="item-inner clearfix">
                            <div class="item-img have-additional clearfix">
                              <div class="item-img-info">
                                <a href="{$categorieslist[current].seolink}" title="{$categorieslist[current].categories_name}" class="product-image">
                                  {if $categorieslist[current].categories_image}
                                    <div class="image_view_product_list" style="background-image:url('{$categorieslist[current].categories_image}')"></div>
                                  {else}
                                    <div class="image_view_product_list" style="background-image:url('{$imagepath}/folder.png')"></div>
                                  {/if}
                                </a>
                              </div>
                            </div>
                            <div class="item-info">
                              <div class="info-inner">
                                <div class="item-title">
                                  <a href="{$categorieslist[current].seolink}" onclick="javascript: return true" title="{$product.name}">{$categorieslist[current].categories_name}</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        {/section}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      {else}
        {$FILENAME_FEATURED_HTML}
      {/if}  -->
    {if !empty($categories_html_footer)}
        {$categories_html_footer}

    {/if}
{/if}

{if $is_products == 1}

    {$smarty.const.HEADING_TITLE}

    {if $filterlistisnotempty}
        {$filter_form}
        {$smarty.const.TEXT_SHOW}
    {/if}

    {if !empty($categories_html)}
        {$categories_html}
    {/if}

    {if !empty($manufacturers_html_header)}
        {$manufacturers_html_header}
    {/if}


    {if !empty($manufacturers_html_header) || isset($categorieslist)}

        <!--     {if isset($categorieslist)}
        {if $categorieslist|@count > 0}
          <div id="sns_content2" class="wrap">
            <div class="container">
              <div class="row-fluid">
                <div id="sns_main" class="span12 col-main">
                    <!-- global messages
                  <div id="sns_mainmidle" class="span12 clearfix">
                     <!-- primary content
                    <div class="std"><div class="no-display">&nbsp;</div></div>
                    <div class="sns-pdt-content wide-4 normal-4 tabletlandscape-4 tabletportrait-3 mobilelandscape-2 mobileportrait-1">
                      <div class="pdt-content tab-content-actived is-loaded pdt_best_sales" >
                        <div class="pdt-list products-grid zoomOut">
                          {assign var=row value=0}
                          {section name=current loop=$categorieslist}
                          <div class="item item-animate mobileportrait-first show-addtocart" style="-webkit-animation-delay:300ms;-moz-animation-delay:300ms;-o-animation-delay:300ms;animation-delay:300ms;">
                            <div class="item-inner clearfix">
                              <div class="item-img have-additional clearfix">
                                <div class="item-img-info">
                                  <a href="{$categorieslist[current].seolink}" title="{$categorieslist[current].categories_name}" class="product-image">
                                    {if $categorieslist[current].categories_image}
                                      <div class="image_view_product_list" style="background-image:url('{$categorieslist[current].categories_image}')"></div>
                                    {else}
                                      <div class="image_view_product_list" style="background-image:url('{$imagepath}/folder.png')"></div>
                                    {/if}
                                  </a>
                                </div>
                              </div>
                              <div class="item-info">
                                <div class="info-inner">
                                  <div class="item-title">
                                    <a href="{$categorieslist[current].seolink}" onclick="javascript: return true" title="{$product.name}">{$categorieslist[current].categories_name}</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          {/section}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        {/if}
      {else}
        {if !empty($manufacturers_html_header)}
            {*$tep_manufacturers_html_header*}
        {/if}
      {/if}  -->

    {/if}


    {if !empty($PRODUCT_LISTING_HTML)}
        {$PRODUCT_LISTING_HTML}
    {/if}

    {if !empty($manufacturers_id)}
        {$manufacturers_html_footer}
    {/if}

    {if !empty($categories_html_footer)}
        {$categories_html_footer}
    {/if}

{else}
    {if $is_default}
        &nbsp;
    {else}
        <!-- <p class="block">{$tep_image_category}</p>  -->
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}