{if $arr_currenciesbox}
<div class="btn-group">
    <form id="drop-currencies" name="currencies" action="{$arr_currencies.formaction}" method="get" style="display: inline-block;">
        <select id="basic" name="currency" onChange="this.form.submit();" class="selectpicker" data-width="34px" data-size="5" data-live-search="false">
            {html_options options=$arr_currenciesbox.items selected=$currency}
        </select>
        {foreach from=$arr_currenciesbox.hiddenfields key=key item=fieldname}
            <input type="hidden" name="{$key}" value="{$fieldname}">
        {/foreach}
    </form>
</div>
{/if}