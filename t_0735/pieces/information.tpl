<section class="content-wrapper">
    <div class="container">
        <div class="row">
		{if empty($custom_heading_title)}
			<h1>{$HEADING_TITLE}</h1>
		{else}
			<h1>{$custom_heading_title}</h1>
		{/if}
		{$INFO_DESCRIPTION}
		</div>
	</div>
</section>