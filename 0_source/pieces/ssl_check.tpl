{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}	
	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
     	<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}

	<tr>
    	<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0">
      			<tr>
        			<td class="main">
        				<table border="0" width="40%" cellspacing="0" cellpadding="0" align="right">
          					<tr>
            					<td>{$info_box_heading}</td>
          					</tr>
          					<tr>
	            				<td>{$info_box_content}</td>
          					</tr>
          					
        				</table>
        				{$smarty.const.TEXT_INFORMATION}
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td align="right" class="main"><a href="{$login_href}">{$button_continue_img_button}</a></td>
      			</tr>
        	</table>
        </td>
	</tr>
</table>
    
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}
{$smarty.const.AFTER_CONTENT}