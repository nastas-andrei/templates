<div id="price_query" class="price_query_style">
    {if $sendung}
        {assign var=trenner value="/"}
        {assign var=targetUrl value=$domain$trenner$return_url_final$products_id}
        <table style="width:100%">
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="pageHeading" nowrap>{$PRICE_QUERY_TITLE}</td>
            </tr>
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="main" nowrap>{$PRICE_QUERY_THANKS}<br>
                </td>
            </tr>
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="main" nowrap>{$PRICE_QUERY_THANKS_DELEGATION}<br>
                </td>
            </tr>
        </table>
        <meta http-equiv="refresh" content="2; URL={$targetUrl}">
    {else}
        <table style="width:100%">

            <tr>
                <td rowspan="2"><img width="100" src="{$products_image}"></td>
                <td colspan="2" class="pageHeading" nowrap>{$products_name}</td>
            </tr>
            <tr>
                <td colspan="4" class="main" align="right">{$products_price}</td>
            </tr>

        </table>
        {assign var=trenner value="/"}
        {assign var=ret_url value=$domain$trenner$return_url}
        <div class="col-xs-12 col-md-6">
            <form id="preice_query_form" class="form-horizontal" method="POST" action="{$PRICE_INQUIRY_URL1}">

                {if $pAnfrage eq "1"}
                    <label style="border-bottom: 1px solid #000000">{$PRICE_QUERY_TITLE}</label>
                {else}
                    <label style="border-bottom: 1px solid #000000">{$PRODUCTS_INQUIRY}</label>
                {/if}

                <div class="text-center">
                    <small class="muted">{$FORM_REQUIRED_INFORMATION}</small>
                </div>

                {if $MESSAGE}
                    <div class="alert">{$MESSAGE}</div>
                {/if}


                <div class="control-group">
                    <label class="control-label" for="name">{$PRICE_QUERY_NAME}</label>

                    <div class="controls">
                        <input type="text" name="name" value="{$name}" class="general_inputs" />
                        <input type="hidden" name="products_id" value="{$products_id}" />
                        <input type="hidden" name="myaction" value="{$myaction}" />
                        <input type="hidden" name="pAnfrage" value="{$pAnfrage}" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="control-group">
                    <label class="control-label" for="email">{$PRICE_QUERY_EMAIL}</label>

                    <div class="controls">
                        <input type="text" name="email" value="{$email_address}" class="general_inputs" />
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="control-group">
                    <label class="control-label" for="fon">{$PRICE_QUERY_FON}</label>

                    <div class="controls">
                        <input type="text" name="fon" value="{$fonnumber}" class="general_inputs" />
                    </div>
                </div>
                <div class="clearfix"></div>
                {if $pAnfrage eq "1"}
                    <div class="control-group">
                        <label class="control-label" for="mitbewerberpreis">{$PRICE_QUERY_COMPETITORS_PRICE}</label>

                        <div class="controls">
                            <input type="text" name="mitbewerberpreis" value="{$mitbewerberpreis}" class="general_inputs" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label" for="mitbewerberurl">{$PRICE_QUERY_COMPETITORS_PRICE_URL}</label>

                        <div class="controls">
                            <input type="text" name="mitbewerberurl" value="{$mUrl}" />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                {/if}

                {if $standardtext}
                    <div class="control-group">
                        <label class="control-label" for="enquiry">&nbsp;</label>

                        <div class="controls">
                            <textarea readonly="true" class="textareanochange" name="standardtext" cols="60" rows="3">{$standardtext}</textarea>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                {/if}

                <div class="control-group">
                    <label class="control-label" for="enquiry">{$PRICE_QUERY_COMMENT}</label>

                    <div class="controls">
                        <textarea name="enquiry" cols="40" rows="10">{$PRICE_QUERY_COMMENT_VALUE}</textarea>
                    </div>
                </div>
                <div class="clearfix"></div>

                {*{if $STORE_SECURITY_CAPTCHA_ENABLED == 'true' }*}
                    {*<tr>*}
                        {*<td class="main" colspan="3">*}
                            {*<div class="antistalker hidden" style="display:none;">*}
                                {*Antispam: *<br />*}
                                {*<span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>*}
                                {*<select name="pf_antispam" id="pf_antispam">*}
                                    {*<option value="1" selected="selected">Ja</option>*}
                                    {*<option value="2">Nein</option>*}
                                {*</select>*}
                            {*</div>*}
                            {*<script language="javascript" type="text/javascript">*}

                            {*</script>*}
                            {*<noscript>*}
                                {*<div class="antistalker">*}
                                    {*Antispam: *<br />*}
                                    {*<span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>*}
                                    {*<select name="pf_antispam" id="pf_antispam">*}
                                        {*<option value="1" selected="selected">Ja</option>*}
                                        {*<option value="2">Nein</option>*}
                                    {*</select>*}
                                {*</div>*}
                            {*</noscript>*}
                        {*</td>*}
                    {*</tr>*}
                {*{/if}*}

                {assign var=targetUrl value=$domain$trenner$return_url}

                <div class="form-actions">
                    <a href="{$targetUrl}" class="btn btn-small">{$smarty.const.IMAGE_BUTTON_BACK}</a>
                    <input type="submit" class="btn button_add pull-right" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
                </div>
            </form>
        </div>
    {/if}
</div>
