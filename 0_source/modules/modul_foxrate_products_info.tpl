{if $is_ajax != true}{literal}
        <script type="text/javascript">
        var tabact = 0;
        var frTabs = function(){
            $$('a.frtabbie').each(function(e,i){
              if (!e.getParent().hasClass('active')){
                $('frtab-'+i+'').setStyle('display', 'none');
              }
              e.addEvent('click',function() {
                initfrTabs(e.getParent(), i);
              });
            });
        };
        var initfrTabs = function(el,i){
            $$('a.frtabbie').getParent().each(function(e){
              if(e.hasClass('active')) {
                e.removeClass('active');
                $('frtab-'+tabact+'').setStyle('display','none');
                tabact = i;
              }
            });
            el.addClass('active');
            $('frtab-'+i+'').setStyle('display','block');
        };
    
        var _r = function(){
          var _e = $(this).getParent('ul');
          new Request({
              url: '/foxrate_request.php',
              method: 'get',
              data: ($(this).hasClass('abuse') ? {review_id:$(this).get('id'),abuse:1} : {review_id:$(this).get('id'),useful:$(this).get('rel')}),
              onFailure: function(){
                _e.getElements('a').addEvent('click', _r);
              },
              onRequest: function(){
                _e.getElements('a').removeEvent('click', _r);
                _e.getElements('a').addClass('active');
              },
              onSuccess: function(){
                new Fx.Tween(_e, {duration: 200,property: 'opacity',
                  onComplete: function(){
                    _e.getElements('li + li').dispose();
                    _e.getElements('li > span').set('text','{/literal}{$PRODUCTS_FOXRATE_RATING_THANKYOU}{literal}');
                    _e.tween('opacity',1);
                  }
                }).start(1,0);
              }
          }).send(); 
        };
        var _g = function(param,value){
            if(value == '') {return false;}
            if(param=='page'){
                //e.stop();
                new Fx.Scroll(window).toElement('frRatingAnchor');
            }
          new Request({
              url: '/foxrate_request.php',
              method: 'get',
              data:{product_id:{/literal}{$arr_product_info.products_id}{literal},param:param,value:value},
        //      data: ($(this).hasClass('abuse') ? {review_id:$(this).get('id'),abuse:1} : {review_id:$(this).get('id'),useful:$(this).get('rel')}),
              onFailure: function(){
                  new Fx.Tween($('frReviewArea'), {duration: 200,property: 'opacity'}).start(0,1);   
                //_e.getElements('a').addEvent('click', _r);
              },
              onRequest: function(){
                new Fx.Tween($('frReviewArea'), {duration: 200,property: 'opacity'}).start(1,0);  
                //_e.getElements('a').removeEvent('click', _r);
                //_e.getElements('a').addClass('active');
              },
              onSuccess: function(response){
          
                  document.id('frReviewArea').set('html',response);
                  new Fx.Tween($('frReviewArea'), {duration: 200,property: 'opacity'}).start(0,1);
                      document.getElements('a.foxrate-feedback').addEvent('click', _r);
                }
          }).send(); 
        };   
    
        window.addEvent("domready", function(){
          document.getElements('a.foxrate-feedback').addEvent('click', _r);
          var unsetTab = function(a){
               for(i=0;i<a.length;i++){
                try{
                  $$('a.frtabbie')[a[i]].getParent().removeClass('active');
                }catch(e){}
              }
          }
          if(unescape(self.document.location.hash.substring(1)) == "read"){ 
            new Fx.Scroll(window).toElement('frRatingAnchor');
            initfrTabs($$('a.frtabbie').getParent(),1);
            unsetTab([0,2]);
          }
          $('jumpPinfo').addEvent('click',function(e) {
            e.stop();
            new Fx.Scroll(window).toElement('frRatingAnchor');
            initfrTabs($$('a.frtabbie').getParent(),1);
            unsetTab([0,2]);
          });   
          $('jumpPwrite').addEvent('click',function(e){
            e.stop();
            new Fx.Scroll(window).toElement('frRatingAnchor');
            initfrTabs($$('a.frtabbie').getParent(),2);
            unsetTab([0,1]);
          });
          frTabs();
        });
        </script>
    {/literal}
    <p><a name="frRatingAnchor" id="frRatingAnchor"></a></p>
    <ul class="clear-list border">
        <li class="frtab inset active">
        	<a href="javascript:void(0);" class="frtabbie"><span>Produktbeschreibung</span></a>
        </li>
        <li class="frtab inset">
        	<a href="javascript:void(0);" class="frtabbie">
        		<span>{$nav_reiter1} ({$foxrate->GetVariable('count')})</span>
        	</a>
        </li>
        {* <li class="frtab inset">
        	<a href="javascript:void(0);" class="frtabbie">
        		<span>{$nav_reiter2}</span>
        	</a>
        </li> *}
    </ul>

    <div id="frtab-0" class="frtab-cnt">
        <!-- 0 -->
        {$arr_product_info.products_description}
        <!-- /0 -->
    </div> 

    <div id="frtab-1" class="frtab-cnt">
        <!-- 1 -->
        <div class="frRating-top-all">

            <div class="right light-grey text">powered by:&nbsp;&nbsp;<img src="templates/0_source/images/foxrate_shopmodule/foxrate-logo.png" class="inset align-middle" /></div>
            <div class="frRating-top">
                <div class="frStars inset"><div style="width:{$foxrate->GetVariable('average')*38}px;" class="frStars yellow"></div></div>
                <div class="inset frStars-txt">
                    {$foxrate_rating|regex_replace:'/<a[^>]*>(.*?)<\/a>/':'$1'}<br />
                    <strong>{$foxrate->GetVariable('average')/5*100|round}%</strong> w&uuml;rden das Produkt weiterempfehlen
                </div>
            </div>
            <div class="frRating-left fleft">
                {foreach from=$foxrate->GetVariable('counts') key=key item=arr_item name=arr_count}
                    <div class="rating-all">
                        <div class="inset align-middle">
                            {if $arr_item.value}
                                <a href="javascript:void(0)" onclick="_g('filter[rating]',{$arr_item.key});">{$arr_item.key} Sterne</a>
                            {else}
                                {$arr_item.key} Sterne
                            {/if}
                            &nbsp;&nbsp;
                        </div>
                        <div class="rating-bar inset align-middle left">
                            <div class="rating-bar-fill" style="width:{$arr_item.value*100/$foxrate->GetVariable('count')}%;"></div>
                        </div>
                        <div class="inset align-middle">({$arr_item.value})</div>
                    </div>
                {/foreach}    
            {/if}

        </div>
     <div id="frReviewArea">
     {if $activeparams == true}  
        <div class="fclear"></div> 
    <div class="frRating-top-all">
        <div class="frRating-left fleft">
                
            <div class="rating-all">
                <div class="inset align-middle"><a href="javascript:void(0)" onclick="_g('filter[rating]','0');">Alle anzeigen</a>&nbsp;&nbsp;</div>
            </div>
        
        </div>
     </div>       
      {/if}  

    <div class="frRating-right fright">&nbsp;</div>
    <div class="fclear"></div>
<!--</div>-->

    {if $foxrate->GetVariable('counts') gt 0}
        <div class="frRating-head">Kundenmeinungen
            <span class="frRatingSortSearch" style="float:right;">Sortierung:
                <select name="sort" onchange="_g('sort',this.value);">
                    <option value="date_up"{if $sort=="date_up"} selected="selected"{/if}>Datum&nbsp;&nbsp;↑</option>
                    <option value="date_down"{if $sort=="date_down"} selected="selected"{/if}>Datum&nbsp;&nbsp;↓</option>
                    <option value="rating_up"{if $sort=="rating_up"} selected="selected"{/if}>Rating&nbsp;&nbsp;↑</option>
                    <option value="rating_down"{if $sort=="rating_down"} selected="selected"{/if}>Rating&nbsp;&nbsp;↓</option>
                    <option value="helpful_up"{if $sort=="helpful_up"} selected="selected"{/if}>Hilfreich&nbsp;&nbsp;↑</option>
                    <option value="helpful_down"{if $sort=="helpful_down"} selected="selected"{/if}>Hilfreich&nbsp;&nbsp;↓</option>
                </select>
                &nbsp;
<!--                Suche:<form name="frSearchRate" style="display:inline" onsubmit="return _g('search',document.getElementById('search').value);">-->
                    <input id="search" type="text" name="search" value="{$search}" class="frRatingSearchInput" />
                    <input type="button" name="submit" value="Go" oninput="_g('search',document.getElementById('search').value);" onclick="_g('search',document.getElementById('search').value);" />
<!--                </form>-->
            </span>
            <div class="fclear"></div>   
        </div>

        {assign var=reviews_count value=$foxrate->GetVariable('reviews_count')}
        <table width="96%" border="0" cellspacing="0" cellpadding="0" class="frRating-recisions">
            {foreach from=$foxrate->GetVariable('reviews') key=key item=obj_rew}
                {cycle values='odd,even' assign=cycler}
                <tr class="{$cycler}">
                    <td width="25%" class="align-top">
                        <img src="{$foxrate->getImageRew($key)}" class="frRating-stars-small" />
                        {if $obj_rew->name|count_characters > 0}
                            <strong>{$obj_rew->name}</strong>
                        {else}
                            <strong>anonym</strong>
                        {/if}<br />
                        {$obj_rew->since}<!--// vor {$obj_rew->tage} Tagen //-->
                    </td>
                    <td class="align-abstop">
                        {if $obj_rew->comment_pros|count_characters gt 0}
                            <p><strong>{$PRODUCTS_FOXRATE_RATING_PRO}:</strong><br />{$obj_rew->comment_pros}</p>
                            {/if}
                            {if $obj_rew->comment_cons|count_characters gt 0}
                            <p><strong>{$PRODUCTS_FOXRATE_RATING_CONTRA}:</strong><br />{$obj_rew->comment_cons}</p>
                            {/if}
                            {if $obj_rew->comment_conclusion|count_characters gt 0}
                            <p><strong>{$PRODUCTS_FOXRATE_RATING_CONCLUSION}:</strong><br />{$obj_rew->comment_conclusion}</p>
                            {/if}
                            {if $obj_rew->comment|count_characters gt 0}
                            <p><strong>{$PRODUCTS_FOXRATE_RATING_COMMENT}:</strong><br />{$obj_rew->comment}</p>
                            {/if}          
                    </td>
                </tr>
                <tr class="{$cycler}">
                    <td colspan="2" class="right">
                        <ul class="clear-list sibling inset align-middle">
                            <li>{if $obj_rew->cookieUseful eq 0}<span class="align-middle">{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST}{else}{$PRODUCTS_FOXRATE_RATING_THANKYOU}</span>{/if}</li>
                            {if $obj_rew->cookieUseful eq 0}
                                <li><a href="javascript:void(0);" id="{$obj_rew->id}" title="{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_YES}" rel="{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_YES}" class="foxrate-feedback"><span>{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_YES}</span></a></li>	
                                <li><a href="javascript:void(0);" id="{$obj_rew->id}" title="{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_NO}" rel="{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_NO}" class="foxrate-feedback"><span>{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_NO}</span></a></li>
                                <li class="last"><a href="javascript:void(0);" id="{$obj_rew->id}" title="{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_ABUSE}" class="foxrate-feedback abuse"><span>{$PRODUCTS_FOXRATE_RATING_HELPFUL_QUEST_ABUSE}</span></a></li>
                            {/if}

                        </ul>
                    </td>
                </tr>
            {/foreach}
            <tr><td colspan="2" >{$foxrate->getPagingLinks()}</td></tr>
        </table>
    {/if}
</div>
{if $is_ajax != true}    
    <!-- /1 -->
</div>

<div id="frtab-2" class="frtab-cnt">
    <!-- 2 -->
     <!-- <p>#### schreiben ####</p> *} -->
    <!-- /2 --> 
</div>
{/if}