{include file="header.tpl"}

<div {if $SCRIPT_NAME == "/account_newsletters.php"}class="container"{/if}>
	{if $is_default}

	{else}
	<div class="container">
	 	<div class="breadcrumb">      
	   		{$breadcrumb|replace:'|':''}     
		</div>
	</div>
	{/if}   

    {$content}
	    {if $content_template}
	      {include file="$content_template"}
	    {/if} 
</div>		
{include file="footer.tpl"}