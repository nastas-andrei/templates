{if $arr_shopping_cart}
    {if $SCRIPT_NAME !== '/shopping_cart.php'}
        <div class="block mini-cart sns-ajaxcart have-item">
            <div class="block-title" onclick="location.href='shopping_cart.php'">
                {if $arr_shopping_cart.count_contents > 0}
                    <a href="{$arr_shopping_cart.cart_link}" class="icos-view"></a>
                    <div class="cart-status">
                        <span class="label">{$arr_shopping_cart.title}</span>
                            <span class="subtotal">
                                  <span class="amount">{$arr_shopping_cart.count_contents}</span>
                                  <span>{$arr_shopping_cart.cart_prod} </span>
                                  <span class="price">{$arr_shopping_cart.gesamtsumme}</span>
                            </span>
                    </div>
                {/if}
            </div>
            {if $arr_shopping_cart.count_contents > 0}
                <div class="block-content">
                    <div class="block-inner">
                        <form name="cart_quantity" action="shopping_cart.php?action=update_product" method="post">
                            <ol id="minicart-sidebar" class="mini-products-list">
                                {foreach from=$arr_shopping_cart.products key=key item=product}
                                    <li class="item last odd">
                                        <a href="{$product.link}" title="{$product.name}" class="product-image">
                                            <div class="img_mini_cart">{$product.image}</div>
                                        </a>
                                        <div class="product-details">
                                            <p class="product-name">
                                                <a href="{$product.link}">{$product.name|truncate:50:"...":true}</a>
                                            </p>
                                        </div>
                                        <div class="product-details-bottom">
                                            <input id="cart-product-{$product.id}" type="checkbox" name="cart_delete[]"
                                                   value="{$product.id}" style="display: none;">
                                            <a href="javascript: del_item_shoppingcart('cart-product-{$product.id}');"
                                               title="Remove This Item"
                                               onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');"
                                               class="btn-remove">
                                            </a>
                                            <div class="price-box">
                                                <span class="price">{$product.final_price}</span>
                                            </div>
                                            <p class="quantity-container" style="margin-left:170px">
                                                {*<span class="label-qty">{$smarty.const.TEXT_DISPLAY_NUMBER_OF_PRODUCTS_TOTAL|sprintf:''}</span>*}
                                                <input type="text" name="cart_quantity[]" value="{$product.quantity}"
                                                       size="4" maxlength="10"
                                                       class="validate-zero-or-greater validate-number qty input-text"
                                                       onblur="if (checkValue('cartadd0')) document.forms.cart_quantity.submit()"
                                                       id="cartadd0">
                                                <input type="hidden" name="products_id[]" value="{$product.id}">
                                                <input type="hidden" name="old_qty[]" value="{$product.quantity}">
                                            </p>
                                        </div>
                                    </li>
                                {/foreach}
                            </ol>
                        </form>
                        <div class="bottom-action actions">
                            <div class="shopping_cart_mini">{$arr_shopping_cart.cart_summe} {$arr_shopping_cart.gesamtsumme}</div>
                            <button data-original-title="Go to cart" data-toggle="tooltip" type="button"
                                    title="Go to cart" class="button" onclick="setLocation('{$arr_shopping_cart.cart_link}')">
                                {$arr_shopping_cart.title|strip_tags:false}
                            </button>
                            <button data-original-title="Update Shopping Cart" data-toggle="tooltip" type="button"
                                    name="update_cart_action" value="update_qty" onclick="location.reload();"
                                    title="Update Shopping Cart" class="button btn-update">
                                {$smarty.const.IMAGE_BUTTON_UPDATE_CART}
                            </button>
                        </div>
                    </div>
                </div>
            {/if}
        </div>
    {/if}
{/if}

