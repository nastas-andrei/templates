{if $arr_languagebox}
<label for="header-select-language">{$arr_languagebox.title}</label>
<select id="header-select-language" class="flag" onchange="window.location.href=this.value">
    {foreach from=$arr_languagebox.items key=key item=lang}
        {$language}
        {$lang.directory}
        {if $lang.directory == $language}
            {assign var="selected" value="selected=\"selected\""}
        {else}
            {assign var="selected" value=""}
        {/if}
        <option data-image="{$lang.image}" value="{$lang.link}" {$selected}>
            {$lang.name|truncate:2:"":true}
        </option>
    {/foreach}
</select>
{/if}