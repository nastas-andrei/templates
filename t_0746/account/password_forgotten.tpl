<seaction id="forgot-password">
    <div class="container">
        <!-- primary content -->
        <div class="page-title">
            <h1>{$smarty.const.LOGIN_BOX_PASSWORD_FORGOTTEN}</h1>
        </div>
        {$password_forgotten_form}
        <div class="fieldset">
            <p>{$smarty.const.TEXT_MAIN}</p>
            <ul class="form-list">
                <li>
                    <label for="email_address" class="required"><em>*</em>{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>

                    <div class="input-box">
                        {$email_address_input}
                    </div>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <a href="{$LOGIN_URL}" class="btn btn-default" style="margin: 16px 40px 0">
            <small>&larr;&nbsp;</small> {$smarty.const.IMAGE_BUTTON_BACK}
        </a>
        <button type="submit" title="Submit" class="btn btn-primary">Submit</button>
        </form>
        <div class="margin-bottom"></div>
        <!-- // primary content -->
    </div>
</seaction>