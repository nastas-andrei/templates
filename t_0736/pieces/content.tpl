{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}
    {if $slider_images != ''}
        <div id="sns_slideshow" class="wrap">
            <div class="slideshow-inner clearfix">
                <div class="sns-slideshow-wrap">
                    <div class="responisve-container">
                        <div class="slider sns-fraction-slider container" style="max-height:651px">
                            <div class="fs_loader"></div>
                            {foreach from=$slider_images item=slider}
                                <div class="slide align-left">
                                    <p class="claim large" data-position="234,0" data-in="right" data-out="fade">{$slider.title}</p>

                                    <p class="teaser large" data-position="295,0" data-in="bottom" data-out="left" data-delay="500">
                                        {$slider.description}
                                        <label class="buttons-action">
                                            {if $slider.link}
                                                <a class="button btn-shopnow have-borderinset" href="{$slider.link}">
                                                    <span>{$slider.button_title}</span>
                                                </a>
                                            {/if}
                                            {*<a class="button btn-member have-borderinset" href="{$slider.link}">*}
                                            {*<span>Member</span>*}
                                            {*</a>*}
                                        </label>
                                    </p>
                                    <img src="{$slider.image|replace:'.jpg':'.png'}?t[]=maxSize:width=600" data-position="103,685" data-in="left" data-delay="800" data-out="fade" />
                                </div>
                            {/foreach}
                            {*<div class="slide align-left">*}
                            {*<p class="claim large" data-position="234,0" data-in="right" data-out="fade">Digital Collection </p>*}
                            {*<p class="teaser large" data-position="295,0" data-in="bottom" data-out="fade" data-delay="500">*}
                            {*Etiam auctor, mauris eget lobortis blandit, tellus nisl convallis turpis, non auctor ante nisl eget eros. Donec rhoncus purus nec nunc. Suspendisse eros. Fusce et nisl. Morbi condimentum enim sed ipsum...*}
                            {*<label class="buttons-action">*}
                            {*<a class="button btn-shopnow have-borderinset" href="#">*}
                            {*<span>Shop Now</span>*}
                            {*</a>*}
                            {*<a class="button btn-member have-borderinset" href="#"><span>Member</span></a>*}
                            {*</label>*}
                            {*</p>*}
                            {*<img src="templates/t_0736/images/top-slider/img1-slide2.png" data-position="120,535" data-in="left" data-delay="800" data-out="fade"/>*}
                            {*</div>*}
                            {*<div class="slide align-left">*}
                            {*<p class="claim large" data-position="234,0" data-in="right" data-out="fade">Digital Collection </p>*}
                            {*<p class="teaser large" data-position="295,0" data-in="bottom" data-out="fade" data-delay="500">*}
                            {*Praesent sagittis, justo id malesuada tincidunt, ipsum leo elementum risus, at pulvinar ante urna et sem. Proin posuere metus sed tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos...*}
                            {*<label class="buttons-action">*}
                            {*<a class="button btn-shopnow have-borderinset" href="#"><span>Shop Now</span></a>*}
                            {*<a class="button btn-member have-borderinset" href="#"><span>Member</span></a>*}
                            {*</label>*}
                            {*</p>*}
                            {*<img src="templates/t_0736/images/top-slider/img1-slide3.png" data-position="207,514" data-in="fade" data-delay="0" data-out="fade"/>*}
                            {*<img src="templates/t_0736/images/top-slider/img2-slide3.png" data-position="171,815" data-in="fade" data-delay="500" data-out="fade"/>*}
                            {*</div>*}
                            {*<div class="slide align-left">*}
                            {*<p class="claim large" data-position="234,0" data-in="right" data-out="fade">Digital Collection </p>*}
                            {*<p class="teaser large" data-position="295,0" data-in="bottom" data-out="fade" data-delay="500">*}
                            {*Aliquam dictum lectus. Morbi pulvinar lacus et diam. Maecenas nunc massa, ultrices eget, nonummy nec, condimentum et, risus. Proin convallis dapibus nisi. Maecenas porta, augue quis porttitor consectetuer...*}
                            {*<label class="buttons-action">*}
                            {*<a class="button btn-shopnow have-borderinset" href="#"><span>Shop Now</span></a>*}
                            {*<a class="button btn-member have-borderinset" href="#"><span>Member</span></a>*}
                            {*</label>*}
                            {*</p>*}
                            {*<img src="templates/t_0736/images/top-slider/img1-slide4.png" data-position="110,562" data-in="left" data-delay="800" data-out="fade"/>*}
                            {*<img src="templates/t_0736/images/top-slider/img2-slide4.png" data-position="110,843" data-in="left" data-delay="1600" data-out="fade"/>*}
                            {*<img src="templates/t_0736/images/top-slider/img3-slide4.png" data-position="110,1127" data-in="left" data-delay="2400" data-out="fade"/>*}
                            {*</div>*}
                            {*<div class="slide align-left">*}
                            {*<p class="claim large" data-position="234,0" data-in="right" data-out="fade">Digital Collection </p>*}
                            {*<p class="teaser large" data-position="295,0" data-in="bottom" data-out="fade" data-delay="500">*}
                            {*Donec bibendum, purus id bibendum sagittis, mauris est tincidunt risus, nec fermentum diam velit pellentesque dolor.  Maecenas sit amet ipsum. Pellentesque sapien pede  libero eget arcu vestibulum auctor...*}
                            {*<label class="buttons-action">*}
                            {*<a class="button btn-shopnow have-borderinset" href="#"><span>Shop Now</span></a>*}
                            {*<a class="button btn-member have-borderinset" href="#"><span>Member</span></a>*}
                            {*</label>*}
                            {*</p>*}
                            {*<img src="templates/t_0736/images/top-slider/img1-slide5.png" data-position="80,765" data-in="fade" data-delay="800" data-out="fade"/>*}
                            {*<img src="templates/t_0736/images/top-slider/img2-slide5.png" data-position="427,735" data-in="fade" data-delay="1600" data-out="fade"/>*}
                            {*</div>*}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    {/if}

    {$UPCOMING_PRODUCTS_HTML}

    {if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}
        {$define_main_bottom}
    {/if}
    <div id="sns_content2" class="wrap">
        <div class="container">
            <div class="row-fluid">
                <div id="sns_main" class="span12 col-main">
                    <!-- global messages -->
                    <div id="sns_mainmidle" class="span12 clearfix">
                        <!-- primary content -->
                        <div class="std">
                            <div class="no-display">&nbsp;</div>
                        </div>
                        {literal}
                            <script type="text/javascript">
                                //<![CDATA[
                                $sns_jq(document).ready(function ($) {

                                    $('#featured').addClass('tab-nav-actived');
                                    $(".featured").addClass("actived-content");
                                    $("#featured").click(function () {
                                        $(".loaderS").addClass("loader_listing_select");
                                        $(this).addClass('tab-nav-actived');
                                        $(".featured").addClass("actived-content");
                                        $("#best, #new").removeClass("tab-nav-actived");
                                        $(".best, .new").removeClass("actived-content");
                                    });
                                    $("#best").click(function () {
                                        $(".loaderS").addClass("loader_listing_select");
                                        $(this).addClass('tab-nav-actived');
                                        $(".best").addClass("actived-content");
                                        $("#featured, #new").removeClass("tab-nav-actived");
                                        $(".featured, .new").removeClass("actived-content");
                                    });
                                    $("#new").click(function () {
                                        $(".loaderS").addClass("loader_listing_select");
                                        $(this).addClass('tab-nav-actived');
                                        $(".new").addClass("actived-content");
                                        $("#featured, #best").removeClass("tab-nav-actived");
                                        $(".featured, #best").removeClass("actived-content");
                                    });

                                    (function (element) {
                                        $element = $(element);
                                        function setAnimate(el) {
                                            $_items = $('.item-animate', el);
                                            $_items.each(function (i) {
                                                $(this).attr("style", "-webkit-animation-delay:" + i * 300 + "ms;"
                                                + "-moz-animation-delay:" + i * 300 + "ms;"
                                                + "-o-animation-delay:" + i * 300 + "ms;"
                                                + "animation-delay:" + i * 300 + "ms;");
                                                if (i == $_items.size() - 1) {
                                                    $(".pdt-list", el).addClass("play");
                                                }
                                            });
                                        }

                                        setAnimate($('.tab-content-actived', $element));
                                    })('#sns_producttabs');

                                    (function (element) {
                                        $element = $(element);
                                        function setAnimate(el) {
                                            $_items = $('.item-animate', el);
                                            $_items.each(function (i) {
                                                $(this).attr("style", "-webkit-animation-delay:" + i * 300 + "ms;"
                                                + "-moz-animation-delay:" + i * 300 + "ms;"
                                                + "-o-animation-delay:" + i * 300 + "ms;"
                                                + "animation-delay:" + i * 300 + "ms;");
                                                if (i == $_items.size() - 1) {
                                                    $(".pdt-list", el).addClass("play");
                                                }
                                            });
                                        }

                                        setAnimate($('.tab-content-actived-best-seller', $element));
                                    })('#sns_producttabs');

                                    (function (element) {
                                        $element = $(element);
                                        function setAnimate(el) {
                                            $_items = $('.item-animate', el);
                                            $_items.each(function (i) {
                                                $(this).attr("style", "-webkit-animation-delay:" + i * 300 + "ms;"
                                                + "-moz-animation-delay:" + i * 300 + "ms;"
                                                + "-o-animation-delay:" + i * 300 + "ms;"
                                                + "animation-delay:" + i * 300 + "ms;");
                                                if (i == $_items.size() - 1) {
                                                    $(".pdt-list", el).addClass("play");
                                                }
                                            });
                                        }

                                        setAnimate($('.tab-content-actived-new', $element));
                                    })('#sns_producttabs');

                                });
                                //]]>
                            </script>
                        {/literal}
                        <div id="sns_producttabs" class="sns-producttabs">
                            <div class="sns-pdt-container">
                                <!--Begin Tab Nav -->
                                <div class="sns-pdt-nav">
                                    <ul class="pdt-nav">
                                        {if $arr_modul_previewproducts}
                                            <li class="item-nav tab-loaded"
                                                data-type="order"
                                                data-catid=""
                                                data-orderby="best_sales"
                                                data-href="pdt_best_sales"
                                                id="featured">
                                                <span class="title-navi">{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</span>
                                            </li>
                                        {/if}
                                        {if $arr_bestsellersbox}
                                            <li class="item-nav"
                                                data-type="order"
                                                data-catid=""
                                                data-orderby="created_at"
                                                data-href="pdt_created_at"
                                                id="best">
                                                <span class="title-navi new">{$arr_bestsellersbox.title}</span>
                                            </li>
                                        {/if}
                                        {if $arr_whatsnewbox}
                                            <li class="item-nav"
                                                data-type="order"
                                                data-catid=""
                                                data-orderby="most_reviewed"
                                                data-href="pdt_most_reviewed"
                                                id="new">
                                                <span class="title-navi">{$arr_whatsnewbox.title}</span>
                                            </li>
                                        {/if}
                                    </ul>
                                </div>
                                <!-- End Tab Nav -->
                                <div class="featured no-actived-content">
                                    {$FILENAME_FEATURED_HTML}
                                </div>
                                <div class="best no-actived-content">
                                    {include file="boxes/box_best_sellers.tpl"}
                                </div>
                                <div class="new no-actived-content">
                                    {include file="boxes/box_whats_new.tpl"}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{/if}

{if $is_products == 1}

    {$smarty.const.HEADING_TITLE}

    {if $filterlistisnotempty}
        {$filter_form}
        {$smarty.const.TEXT_SHOW}
    {/if}

    {if !empty($categories_html)}
        {$categories_html}
    {/if}

    {if !empty($manufacturers_html_header)}
        {$manufacturers_html_header}
    {/if}

    {if !empty($PRODUCT_LISTING_HTML)}
        {$PRODUCT_LISTING_HTML}
    {/if}

    {if !empty($manufacturers_id)}
        {$manufacturers_html_footer}
    {/if}

    {if !empty($categories_html_footer)}
        {$categories_html_footer}
    {/if}

{else}
    {if $is_default}
        &nbsp;
    {else}
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}