
<table border="0" cellspacing="0" cellpadding="0" class="pageContentTable" width="100%">
	<tr>
		<td>
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
      			<tr>
        			<td class="cont_abst_sitemap">       
						{*<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								<td class="cont_desc">{$arr_smartyconstants.HEADING_DESCRIPTION_SITEMAP}</td>
							</tr>
						</table>*}
						<table border="0" cellpadding="2" cellspacing="10" width="100%">
							<tr>
								<td class="smallTextwhite">{$content_array.pagenav.count}</td>
								<td class="smallTextwhite" align="right">{$content_array.pagenav.nav}&nbsp;&nbsp;</td>
							</tr>
						</table>
						
						{if $arr_newslistview}
							<div style="padding:10px;">
								<h2><a href="{$BOX_FOOTER_NEWS_NEXT_LINK}">News</a></h2>
								<p style="padding-left:30px;margin:10px 0px 20px 0px;">
								{foreach from=$arr_newslistview.arr_current_news key=key item=news}
									<a href="{$news.seolink}">{$news.news_title}</a><br />
								{/foreach}
								</p>
							</div>
						{/if}
						
						<div style="padding:10px;">
						{foreach from=$content_array.products key=key item=blocks}
							<h2>{$blocks.heading|regex_replace:"/^[&nbsp;]*/":""}</h2>
							<p style="padding-left:30px;margin:10px 0px 20px 0px;">
								{foreach from=$blocks.products key=key item=product}
									<a href="{$product.link}">{$product.name}</a><br />
								{/foreach}
							</p>
						{/foreach}
						</div>
						<table border="0" cellpadding="2" cellspacing="10" width="100%">
							<tr>
								<td class="smallTextwhite">{$content_array.pagenav.count}</td>
								<td class="smallTextwhite" align="right">{$content_array.pagenav.nav}&nbsp;&nbsp;</td>
							</tr>
						</table>
       	 			</td>
      			</tr>
    		</table>		
		</td>
	</tr>
</table>
