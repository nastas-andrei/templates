{$checkout_payment_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr>
	        	<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}
	
		<tr>
			<td class="cont_abst">
				<table border="0" width="100%" cellspacing="0" cellpadding="0" class="checkout_area">
					{if $is_payment_error}	
					<tr>
						<td>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="main"><b>{$payment_error_title}</b></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<div class="checkout_payment_error">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBoxNotice">
									<tr class="infoBoxNoticeContents">
										<td>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>{$pixel_trans_10_1}</td>
													<td class="main" width="100%" valign="top">{$payment_error_description}</td>
													<td>{$pixel_trans_10_1}</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
					</tr>
					{/if}
					{if $is_error}
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<div class="checkout_payment_error">
        						<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBoxNotice">
          							<tr class="infoBoxNoticeContents">
            							<td>
            								<table border="0" width="100%" cellspacing="0" cellpadding="0">
              									<tr>
                									<td>{$pixel_trans_10_1}</td>
                									<td class="main" width="100%" valign="top">{$error_message}</td>
                									<td>{$pixel_trans_10_1}</td>
              									</tr>
            								</table>
            							</td>
          							</tr>
        						</table>
        					</div>
        				</td>
      				</tr>
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
      				</tr>
      				{/if}
					<tr>
						<td align="left">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="main">
										<div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_BILLING_ADDRESS}</b></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="left">
							<div class="checkoutFrame">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
									<tr>
										<td>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
													<td class="main" width="50%" valign="top">
														{$smarty.const.TEXT_SELECTED_BILLING_DESTINATION}
														<br><br>
														<a href="{$checkout_payment_address_href}">{$button_change_address_img_button}</a>
													</td>
													<td align="right" width="50%" valign="top">
														<table border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td class="main checkoutOTLeft" align="center" valign="top">
																	<b>{$smarty.const.TITLE_BILLING_ADDRESS}</b>
																	<br>
																	{$arrow_south_east_img}
																</td>
																<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																<td class="main checkoutOTRight" valign="top">{$billto_address_label}</td>
																<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
					</tr>
					<tr>
						<td align="left">
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="main"><div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_PAYMENT_METHOD}</b></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="left">
							<div class="checkoutFrame">
							
							
							
							
								<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
									<tr>
										<td>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">								
												{if $is_multi_selection}
												<tr>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
													<td class="main" width="50%" valign="top">{$smarty.const.TEXT_SELECT_PAYMENT_METHOD}</td>
													<td class="main" width="50%" valign="top" align="right">
														<b>{$smarty.const.TITLE_PLEASE_SELECT}</b>
														<br>
														{$arrow_east_south_img}
													</td>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
												</tr>
												{else}
												<tr>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
													<td class="main" width="100%" colspan="2">{$smarty.const.TEXT_ENTER_PAYMENT_INFORMATION}</td>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
												</tr>
												{/if}
												{section name=current loop=$selectionlist}
												<tr>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
													<td colspan="2">
														<table border="0" width="100%" cellspacing="0" cellpadding="0">
															{if $selectionlist[current].is_payment_selection}												
															<tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$selectionlist[current].radio_buttons}, '{$selectionlist[current].id}')">
															{else}
															<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$selectionlist[current].radio_buttons}, '{$selectionlist[current].id}')">
															{/if}
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																<td class="main trenner anbieterbilder{$selectionlist[current].index}" colspan="3">
																	<b>{$selectionlist[current].module}</b>
																	<br>
																	{$selectionlist[current].info}
																</td>
																<td class="main trenner" align="right">
																	{if $is_multi_selection}
																		{$selectionlist[current].payment_radio}
																	{else}
																		{$selectionlist[current].payment_hidden}
																	{/if}
																</td>
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
															</tr>
															{if $selectionlist[current].is_error}
															<tr>
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																<td class="main" colspan="4">{$selectionlist[current].error}</td>
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
															</tr>													
															{/if}
																
															{if $selectionlist[current].is_fields}
															<tr>
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																<td colspan="4" id="wirecard_onoff">
																	<table border="0" cellspacing="0" cellpadding="0" class="paymentsTable_{$selectionlist[current].id}">
																		{section name=current_field loop=$selectionlist[current].fields}
																		<tr>
																			<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																			<td class="main">{$selectionlist[current].fields[current_field].title}</td>
																			<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																			<td class="main">{$selectionlist[current].fields[current_field].field}</td>
																			<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
																		</tr>
																		{/section}
																	</table>
																</td>
																<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
															</tr>
															{/if}
														</table>
													</td>
													<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
												</tr>
												{/section}
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
					</tr>
					{$order_total_modules_credit_selection}
					<tr>
						<td align="left">
							<table class="table_heading_comment" border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td class="main"><div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_COMMENTS}</b></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="left">
							<div class="checkoutFrame removeit">
								<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox table_textarea_comment">
									<tr>
										<td>
											<table border="0" width="100%" cellspacing="0" cellpadding="0">
												<tr>
													<td>{$comments_textarea}</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</div>
						</td>
					</tr>
					<tr>
						<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
					</tr>
					<tr>
						<td>
							<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
								<tr>
									<td>
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
												<td class="main"><b>{$smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}</b><br>{$smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}</td>
												<td class="main" align="right">{$button_continue_img_submit}</td>
												<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
					</tr>
					<tr>
						<td>
							<table border="0" width="100%" cellspacing="0" cellpadding="0">
								<tr>
									<td width="25%">
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td width="50%" align="right">{$pixel_trans_1_5}</td>
												<td width="50%">{$pixel_trans_100_1}</td>
											</tr>
										</table>
									</td>
									<td width="25%">
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td width="50%">{$pixel_trans_100_1}</td>
												<td>{$checkout_bullet_img}</td>
												<td width="50%">{$pixel_trans_100_1}</td>
											</tr>
										</table>
									</td>
									<td width="25%">{$pixel_trans_100_1}</td>
									<td width="25%">
										<table border="0" width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td width="50%">{$pixel_trans_100_1}</td>
												<td width="50%">{$pixel_trans_1_5}</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="center" width="25%" class="checkoutBarFrom"><a href="{$checkout_shipping_href}" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></td>
									<td align="center" width="25%" class="checkoutBarCurrent">{$smarty.const.CHECKOUT_BAR_PAYMENT}</td>
									<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</td>
									<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_FINISHED}</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
</form>