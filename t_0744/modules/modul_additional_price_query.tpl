<div id="price_query" class="price_query_style">
    {if $sendung}
        {assign var=trenner value="/"}
        {assign var=targetUrl value=$domain$trenner$return_url_final$products_id}
        <table style="width:100%">
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="pageHeading" nowrap>{$PRICE_QUERY_TITLE}</td>
            </tr>
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="main" nowrap>{$PRICE_QUERY_THANKS}<br>
                </td>
            </tr>
            <tr>
                <td class="main" nowrap>&nbsp;</td>
                <td class="main" nowrap>{$PRICE_QUERY_THANKS_DELEGATION}<br>
                </td>
            </tr>
        </table>
        <meta http-equiv="refresh" content="2; URL={$targetUrl}">
    {else}
        <table style="width:100%">
            <tr>
                <td rowspan="2"><img width="100" src="{$products_image}"></td>
                <td class="main" nowrap>&nbsp;</td>
                <td colspan="2" class="pageHeading" nowrap>{$products_name}</td>
            </tr>
            <tr>
                <td colspan="4" class="main" align="right">{$products_price}</td>
            </tr>
        </table>
        {assign var=trenner value="/"}
        {assign var=ret_url value=$domain$trenner$return_url}
        <form id="preice_query_form" class="form-horizontal" method="POST" action="{$PRICE_INQUIRY_URL1}">
            {if $pAnfrage eq "1"}
                <p class="text-center">{$PRICE_QUERY_TITLE}</p>
            {else}
                <p class="text-center">{$PRODUCTS_INQUIRY}</p>
            {/if}
            <div class="text-center"><small class="muted">{$FORM_REQUIRED_INFORMATION}</small></div>
            {if $MESSAGE}
                <div class="alert">{$MESSAGE}</div>
            {/if}
            <div class="control-group">
                <label class="control-label" for="name">{$PRICE_QUERY_NAME}</label>
                <div class="controls">
                    <input type="text" name="name" value="{$name}" />
                    <input type="hidden" name="products_id" value="{$products_id}" />
                    <input type="hidden" name="myaction" value="{$myaction}" />
                    <input type="hidden" name="pAnfrage" value="{$pAnfrage}" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="email">{$PRICE_QUERY_EMAIL}</label>
                <div class="controls">
                    <input type="text" name="email" value="{$email_address}" />
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fon">{$PRICE_QUERY_FON}</label>
                <div class="controls">
                    <input type="text" name="fon" value="{$fonnumber}" />
                </div>
            </div>
            {if $pAnfrage eq "1"}
                <div class="control-group">
                    <label class="control-label" for="mitbewerberpreis">{$PRICE_QUERY_COMPETITORS_PRICE}</label>
                    <div class="controls">
                        <input type="text" name="mitbewerberpreis" value="{$mitbewerberpreis}" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="mitbewerberurl">{$PRICE_QUERY_COMPETITORS_PRICE_URL}</label>
                    <div class="controls">
                        <input type="text" name="mitbewerberurl" value="{$mUrl}" />
                    </div>
                </div>
            {/if}
            {if $standardtext}
                <div class="control-group">
                    <label class="control-label" for="enquiry">&nbsp;</label>
                    <div class="controls">
                        <textarea readonly="true" class="textareanochange" name="standardtext" cols="60" rows="3">{$standardtext}</textarea>
                    </div>
                </div>
            {/if}
            <div class="control-group">
                <label class="control-label" for="enquiry">{$PRICE_QUERY_COMMENT}</label>
                <div class="controls">
                    <textarea name="enquiry" cols="40" rows="10">{$PRICE_QUERY_COMMENT_VALUE}</textarea>
                </div>
            </div>
            {assign var=targetUrl value=$domain$trenner$return_url}
            <div class="form-actions">
                <a href="{$targetUrl}" class="btn btn-small">{$smarty.const.IMAGE_BUTTON_BACK}</a>
                <input type="submit" class="btn btn-primary btn-small" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
            </div>
        </form>
    {/if}
</div>
