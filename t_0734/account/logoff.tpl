<div class="account-container">
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        {$smarty.const.TEXT_MAIN}
    </div>
    <div class="text-center">
        <a href="{$DEFAULT_URL}" class="btn btn-default"><i class="fa fa-home"></i> {$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
    </div>
</div>