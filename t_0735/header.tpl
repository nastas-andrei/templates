<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}" />
    <meta name="description" content="{$meta_description}" />
    <meta name="robots" content="index, follow" />
    <meta charset="utf-8">
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}" />
    {/if}

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/owl.carousel.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/smoothproducts.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/selectbox.css" />
    <link rel="stylesheet" href="{$templatepath}lib/css/animate.css">
    <link rel="stylesheet" href="{$templatepath}lib/css/costumizefont.css" />
    <link rel="stylesheet" href="{$templatepath}custom.css" />

    <script src="includes/general.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{$templatepath}lib/bootstrap.min.js"></script>
    <script src="{$templatepath}lib/application.js"></script>
    <script src="{$templatepath}lib/main_ready.js"></script>
    <script src="{$templatepath}lib/mootools-core-1.4.js"></script>
    <script src="{$templatepath}lib/owl.carousel.min.js"></script>
    <script src="{$templatepath}lib/main.js"></script>
    <script src="{$templatepath}lib/html5shiv.js"></script>
    <script src="{$templatepath}lib/smoothproducts.js"></script>
    <script src="{$templatepath}lib/selectbox.js"></script>
    <script src="{$templatepath}lib/gmap3.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
</head>
<body>
<div class="outter">
    <!-- BEGIN TOP BLOCK -->
    <section class="top-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sel-top">
                        <div class="dropdown">
                            {include file="boxes/box_languages.tpl"}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- END TOP BLOCK -->
    <!-- BEGIN HEADER -->
    <section class="header">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="sitelogo-block">
                        <div class="logo">
                            {$cataloglogo}
                        </div>
                        <!--/.logo-->
                        <div class="titledesc">
                            {$smarty.const.HEADER_TITLE_TOP}
                        </div>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="topinfo-block">
                        {include file="boxes/box_search.tpl"}
                        {include file="boxes/box_loginbox.tpl"}
                        <a href="{$arr_shopping_cart.cart_link}" class='info cart'>
                            <span class="icon"><i class="icon-basket"></i></span>
                        </a>
                        {$arr_shopping_cart.title}
                        <span> {$arr_shopping_cart.count_contents}</span>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container -->
    </section>
    <!-- END HEADER -->
    <!-- BEGIN NAVIGATION -->
    <section class="navigation">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container-fluid">
                            <div id="categories-box"></div>
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <!-- END NAVIGATION -->
    <section class="content-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-meta">
                        <ul class="product-breadcrumb">
                            <li>{$breadcrumb|replace:'|':' <span style="font-size: 20px; color:#DDD;margin-left:5px">></span>'}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>