{$smarty.const.BEFORE_CONTENT}
{$account_newsletter_form}
{if !$smarty.const.PH_INSIDE}
{include file="pieces/page_title_section.tpl"}
{$smarty.const.BETWEEN_HL_CONTENT}
<table border="0" width="100%" cellspacing="0" cellpadding="0">
    <tr>
        <td class="pageContentTable">
            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                {else}
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            {include file="pieces/page_title_section.tpl"}
                        </td>
                    </tr>
                    {$smarty.const.BETWEEN_HL_CONTENT}
                    {/if}
                    <tr>
                        <td class="cont_abst">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
                                <tr>
                                    <td class="main"><b>{$smarty.const.MY_NEWSLETTERS_TITLE}</b></td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                        <tr>
                                                            <td>{$pixel_trans_10_1}</td>
                                                            <td>
                                                                <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                                    <tr class="moduleRow" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="document.getElementById('newsletter_general').checked = !document.getElementById('newsletter_general').checked">
                                                                        <td class="main">{$newsletter_general_input}</td>
                                                                        <td class="main">
                                                                            <b>{$smarty.const.MY_NEWSLETTERS_GENERAL_NEWSLETTER}</b>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="main">&nbsp;</td>
                                                                        <td>
                                                                            <table border="0" cellspacing="0" cellpadding="2">
                                                                                <tr>
                                                                                    <td width="10">{$pixel_trans_10_1}</td>
                                                                                    <td class="main">{$smarty.const.MY_NEWSLETTERS_GENERAL_NEWSLETTER_DESCRIPTION}</td>
                                                                                    <td width="10">{$pixel_trans_10_1}</td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td>{$pixel_trans_10_1}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>{$pixel_trans_100_10}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
                                            <tr>
                                                <td>
                                                    <table border="0" width="100%" cellspacing="0" cellpadding="2">
                                                        <tr>
                                                            <td width="10">{$pixel_trans_10_1}</td>
                                                            <td>
                                                                <a href="{$account_link}">{$button_back}</a>
                                                            </td>
                                                            <td align="right">{$button_continue}</td>
                                                            <td width="10">{$pixel_trans_10_1}</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                {if !$smarty.const.PH_INSIDE}
                </td>
                </tr>
            </table>
            {/if}
            {$smarty.const.AFTER_CONTENT}
            </form></td>