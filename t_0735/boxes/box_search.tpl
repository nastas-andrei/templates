{if $arr_searchbox}
    {if $js_products_search}
        {$js_products_search}
    {/if}
        {$arr_searchbox.form}
            <input type="text" class="input-search" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}" value="{$arr_searchbox.inputtextfield}"/>
            <button type='submit' class="submit-search"><i class="icon-search"></i></button>
            {$arr_searchbox.hidden_sessionid}
        </form>
{/if}

