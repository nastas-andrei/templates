<div class="account-container">
{$create_account_frm}

	{$head_regions_js}
	{$login_link}

{if $messages_exists}
	{$message_create_account}
{/if}

		{$smarty.const.CATEGORY_PERSONAL}
		{$smarty.const.FORM_REQUIRED_INFORMATION}
	


	{if $smarty.const.ACCOUNT_GENDER == 'true'}
   
          	<div class="control-group">
              <label class="control-label">
                {$smarty.const.ENTRY_GENDER}
              </label>
              <div class="controls">
                <label class="radio inline">
                  {$gender_radio_m}
                  {$smarty.const.MALE}
                </label>          
                <label class="radio inline">
                  {$gender_radio_f}
                  {$smarty.const.FEMALE}                
                </label>  
                <span class="help-inline">
                  {if $entry_gender_text}
                    {$smarty.const.ENTRY_GENDER_TEXT}
                  {/if}
                </span>               
              </div>
            </div>
  {/if}
            

              <div class="control-group">             
                  <label class="control-label">
                    {$smarty.const.ENTRY_FIRST_NAME}  
                  </label>
                  <div class="controls">
                  {$firstname_input}
                    <span class="help-inline">
                    {if $entry_firstname_text}
                      {$smarty.const.ENTRY_FIRST_NAME_TEXT}
                    {/if}
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                      {$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH}
                    {/if}
                    </span>
                  </div>
                    
              </div>
                    
              <div class="control-group">             
                  <label class="control-label">
                    {$smarty.const.ENTRY_LAST_NAME}
                  </label>  
                  <div class="controls">
                  {$lastname_input} 
                    <span class="help-inline">
                    {if $entry_lastname_text}
                      {$smarty.const.ENTRY_LAST_NAME_TEXT}
                    {/i
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                      {$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})
                    {/if} 
                    </span>
                  </div>  
              </div>

	{if $smarty.const.ACCOUNT_DOB == 'true'}


              <div class="control-group">
                <div class="label control-label">{$smarty.const.ENTRY_DATE_OF_BIRTH}</div>
                <div class="controls">{$dob_input}
                  <span class="help-inline">
                    {if $entry_dob_text}
                      {$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}
                    {/if}
                  </span>
                </div>
              </div>  
  {/if}
       

            <div class="control-group">
              <label for="" class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
              <div class="controls">{$email_address_input}
                <span class="help-inline">
                  {if $entry_email_address_text}
                    {$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}
                  {/if}
                </span>
              </div>
            </div>  

        
	<!--PIVACF start-->
	{if $smarty.const.ACCOUNT_PIVA == 'true'}

            <div class="control-group">
              <label for="" class="control-label">{$smarty.const.ENTRY_CF}</label>
              <div class="controls">{$cf_input}
                <span class="help-inline">
                  {if $entry_cf_text}
                    {$smarty.const.ENTRY_CF_TEXT}
                  {/if}
                </span>
              </div>
            </div>

  {/if}
	<!--PIVACF end-->

{if $smarty.const.ACCOUNT_COMPANY == 'true'}
            <div class="control-group">
	            <label for="" class="control-label">
                {$smarty.const.CATEGORY_COMPANY}
                {$smarty.const.ENTRY_COMPANY}
  				  	 </label>
  	          <div class="controls">{$company_input}
                  <span class="help-inline">
                    {if $entry_company_text}
                      {$smarty.const.ENTRY_COMPANY_TEXT}
                    {/if}
                  </span>    
                  <span class="help-inline">
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                      ({$smarty.const.ENTRY_COMPANY_MAX_LENGTH})
                    {/if}  
                  </span>              					
			        </div>
            </div>

	   <!--PIVACF start -->
	   {if $smarty.const.ACCOUNT_CF == 'true'}
          <div class="control-group">
              <label for="" class="control-label">
                {$smarty.const.ENTRY_PIVA}
              </label>
              <div class="controls">{$piva_input}
                <span class="help-inline">
                  {if $entry_piva_text}
                    {$smarty.const.ENTRY_PIVA_TEXT}
                  {/if}
                </span>                       
              </div>
          </div>
	   			
	   {/if}
	   <!-- PIVACF end -->
			
{/if}
          
	   {$smarty.const.CATEGORY_ADDRESS}

          <div class="control-group">
              <label for="" class="control-label">
  	             {$smarty.const.ENTRY_STREET_ADDRESS}
              </label>
              <div class="controls">{$street_address_input}
               <span class="help-inline">   
                  {if $entry_street_address_text}
                    {$smarty.const.ENTRY_STREET_ADDRESS_TEXT}
                  {/if}
                </span>
                <span class="help-inline">
                  {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
                    ({$smarty.const.ENTRY_STREET_ADDRESS_MAX_LENGTH})
                  {/if}
                </span>
              </div>
          </div>

		{if $smarty.const.ACCOUNT_SUBURB == 'true'}

		      <div class="control-group">
              <label for="" class="control-label">
  				      {$smarty.const.ENTRY_SUBURB}
              </label>
  				    <div class="controls">{$suburb_input}
              <span class="help-inline">
                {if $entry_suburb_text}
                  {$smarty.const.ENTRY_SUBURB_TEXT}
                {/if}
			        </span>
             </div>
         </div>

		{/if}
			   <div class="control-group">
              <label for="" class="control-label">
  				      {$smarty.const.ENTRY_POST_CODE}
              </label>
  				    <div class="controls">{$postcode_input}
              <span class="help-inline">
              {if $entry_postcode_text}
                {$smarty.const.ENTRY_POST_CODE_TEXT}
              {/if}
              </span>
              </div>
         </div>

		    <div class="control-group">
              <label for="" class="control-label">
  				      {$smarty.const.ENTRY_CITY}
              </label>
  				    <div class="controls">{$city_input}
              <span class="help-inline">
                {if $entry_city_text}
                  {$smarty.const.ENTRY_CITY_TEXT}
                {/if}
              </span>
              </div>
         </div>


         <div class="control-group">
              <label for="" class="control-label">
                {$smarty.const.ENTRY_COUNTRY}
              </label>
					    <div class="controls">{$country_list}
              <span class="help-inline">
              {if $entry_country_text}
                {$smarty.const.ENTRY_COUNTRY_TEXT}
              {/if}
		          </span>
              </div>
         </div>
			
		{if $smarty.const.ACCOUNT_STATE == 'true'}

			   <div class="control-group">
              <label for="" class="control-label">
  				      {$smarty.const.ENTRY_STATE}
              </label>
              <div class="controls">{$state_field_html}
              <span class="help-inline">
              {if $entry_state_text}
                {$smarty.const.ENTRY_STATE_TEXT}
              {/if}
              </span>
              </div>
         </div>
  			
		{/if}

        {$smarty.const.CATEGORY_CONTACT}

              
          <div class="control-group">
              <label for="" class="control-label">
  			         {$smarty.const.ENTRY_TELEPHONE_NUMBER}
              </label>
  			      <div class="controls">{$telephone_input}
              <span class="help-inline">
                {if $entry_telephone_text}
                  {$smarty.const.ENTRY_TELEPHONE_NUMBER_ERROR}
                {/if}
			        </span>
              </div>
         </div>

        <div class="control-group">
              <label for="" class="control-label">
  			         {$smarty.const.ENTRY_FAX_NUMBER}
              </label>
  			      <div class="controls">{$fax_input}
			        </div>
         </div>
		
        {$smarty.const.CATEGORY_OPTIONS}
  				
{if $is_data_security}
        <div class="control-group">
              <label for="" class="checkbox">
              {$smarty.const.ENTRY_DATENSCHUTZ}
     		       {$datenschutz_checkbox}
              </label>
              <span class="help-inline">
                {if $entry_datenschutz_text}
                  {$smarty.const.ENTRY_DATENSCHUTZ_TEXT}
                {/if}
              </span>
         </div>
     	

		{$data_security_text}
     <a onclick="setDatenschutz('1')" >{$TEXT_DATENSCHUTZ_NOTICE}</a>			             			
     

		<div id="dsid" style="display:none">
			<span class="info_schliessen">
				<a style="cursor:pointer;" onclick="setDatenschutz('2')">{$smarty.const.ENTRY_DATA_SECURITY_CLOSE}</a>
   		</span>
			{$data_security_inf_desc}

			<a style="cursor:pointer;" onclick="setDatenschutz('2')">{$smarty.const.ENTRY_DATA_SECURITY_CLOSE}</a>
    </div>

{/if}
      <div class="control-group">
          <label for="" class="control-label">
  		      {$smarty.const.ENTRY_NEWSLETTER}
            {$newsletter_checkbox}
          </label>
          <span class="help-inline">
            {if $entry_newsletter_text}
              {$smarty.const.ENTRY_NEWSLETTER_TEXT}
            {/if}
          </span>
      </div>
         

{if $smarty.const.MEMBER == 'false'}				

  {$smarty.const.CATEGORY_PASSWORD}

	   <div class="control-group">
        <label for="" class="control-label">
          {$smarty.const.ENTRY_PASSWORD}
        </label>
	      <div class="controls">{$password_input}
        <span class="help-inline">
          {if $entry_password_text}
            {$smarty.const.ENTRY_PASSWORD_TEXT}
          {/if}
        </span>
        </div>
    </div>

    <div class="control-group">
        <label for="" class="control-label">
	         {$smarty.const.ENTRY_PASSWORD_CONFIRMATION}
        </label>
	      <div class="controls">{$password_confirmation_input}
        <span class="help-inline">
          {if $entry_password_confirmation_text}
            {$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}
          {/if}
        </span>
        </div>
    </div>
{/if}

<button class="btn" type="submit">{$button_continue}</button>

</form>
</div>