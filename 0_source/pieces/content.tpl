{if $products_not_found_message}
	<table border="0" width="100%" cellspacing="0" cellpadding="2" class="listing_tab_header">
   		<tr>
  			<td class="productListing-data" >
  				<h2><span class="noProduktFound">{$products_not_found_message}</span></h2>
  			</td>
  		</tr>
 	</table>	
{/if}
    
{$smarty.const.BEFORE_CONTENT}

{if $is_default}
  {if !$smarty.const.PH_INSIDE}  
    
    {if $smarty.const.SHOW_EXTRA_INDEX_HEADLINE == 'true'}  
      {include file="pieces/page_title_section.tpl"}
    {/if}
    
    {$smarty.const.BETWEEN_HL_CONTENT}
    
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="pageContentTable">
          <table border="0" width="100%" cellspacing="0" cellpadding="0">  
  
  {else}    
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
    {$smarty.const.BETWEEN_HL_CONTENT}
          
  {/if}
    
    <tr>
      <td class="cont_abst">        
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td>
              <table border="0" width="100%" cellspacing="0" cellpadding="0">
                {if $smarty.const.TEXT_DEFINE_MAIN != ""}
                <!-- maindist1 begin -->
                <tr>
                  <td>
                    <span class="categorySpacer01">{$pixel_trans_5}</span>
                  </td>
                </tr>
                <!-- maindist1 end -->
                <tr>
                  <td class="main">{$user_html}</td>
                </tr>
                <!-- maindist2 begin -->
                <tr>
                  <td>
                    <span class="categorySpacer02">{$pixel_trans_5}</span>
                  </td>
                </tr>
                <!-- maindist2 end -->
                {/if}
                <tr>
                  <td>
                    <!-- ESCHER Shop Hack -->
                    <div id="mainnopic">
                      {$FILENAME_FEATURED_HTML}
                    </div> 
                    <!-- ESCHER Shop Hack ENDE -->
                  </td>
                </tr>
                {$UPCOMING_PRODUCTS_HTML}
                {$SHOP_NEWS_HTML}
                <!-- Bottom Text auf Startseite -->
                {if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}
                <tr>
                  <td class="main">{$define_main_bottom}</td>
                </tr>
                {/if}
              </table>
            </td>
          </tr>
        </table>    
      </td>
    </tr>
  </table>  
{/if}

{if $is_nested}
  {if !$smarty.const.PH_INSIDE}
    {include file="pieces/listing_title_section.tpl"}
      {$smarty.const.BETWEEN_HL_CONTENT}
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="pageContentTable_index">
          <table border="0" width="100%" cellspacing="0" cellpadding="0">  
  {else}
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          {include file="pieces/listing_title_section.tpl"}        
        </td>
      </tr>
      {$smarty.const.BETWEEN_HL_CONTENT}    
  {/if}
    <tr>
      <td class="cont_abst">
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          {if !empty($categories_html)}
          <!-- ausgabe categories_html beginn -->
          <tr>
            <td>
              <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="category_desc">{$categories_html}</td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- ausgabe categories_html ende -->
          {/if}
          <tr>
            <td>
              <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <!-- ausgabe catbilder featured beginn -->
                <tr>
                  <td>
                    <table border="0" width="100%" cellspacing="0" cellpadding="0" class="categorietext_tbl">                      
                      {if isset($categorieslist)}
                        <tr>
                        {section name=current loop=$categorieslist}                        
                          <td align="center" class="categorietext" width="{$categorieslist[current].width}" valign="top" height="40">
                            <a href="{$categorieslist[current].seolink}">{$categorieslist[current].CATE}</a>
                          </td>
                          {$categorieslist[current].cat_divider}                        
                        {/section}
                        </tr>
                      {else}
                        <tr>
                          <td>&nbsp;</td>
                        </tr>                  
                      {/if}
                    </table>
                  </td>
                </tr>
                <tr>
                  <td>
                    <span class="categorySpacer03">{$pixel_trans_20}</span>
                   </td>
                 </tr>
                <!-- ausgabe catbilder featured ende -->
                <tr>
                  <td>
                    <div id="mainnopicPL">{$FILENAME_FEATURED_HTML}</div>
                   </td>
                 </tr>
              </table>
            </td>
          </tr>
        </table>        
        {if !empty($categories_html_footer)}{$categories_html_footer}{/if}
      </td>
    </tr>
  </table>  
{/if}

{if $is_products}
  {if !$smarty.const.PH_INSIDE}
      <table border="0" width="100%" cellspacing="0" cellpadding="0" class="pageHeadingTable machweg">
        <tr>
          {$smarty.const.BEFORE_HL_LEFT}
          <td class="pageHeading" nowrap {$smarty.const.HL_LEFT_STYLE}>{$smarty.const.HEADING_TITLE}</td>
          {$smarty.const.BETWEEN_HL_LEFT_RIGHT}
          {if $filterlistisnotempty}
            <td align="center" class="PageHeadingCenter">
            {*$filter_form*}
            {$smarty.const.TEXT_SHOW}&nbsp;
          {/if}                          
          <td class="pageHeadingRight" align="right" {$smarty.const.HL_RIGHT_STYLE}>{$tep_image_category}</td>
          {$smarty.const.AFTER_HL_RIGHT}
        </tr>
      </table>
    
    <!-- ausgabe pageHeading ende -->
    {$smarty.const.BETWEEN_HL_CONTENT}
  
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td class="pageContentTable_index">
          <table border="0" width="100%" cellspacing="0" cellpadding="0">        
          
  {else}
    <table border="0" width="100%" cellspacing="0" cellpadding="0">
      <tr>
        <td>
          <table border="0" width="100%" cellspacing="0" cellpadding="0" class="pageHeadingTable machweg">
            <tr>
              {$smarty.const.BEFORE_HL_LEFT}
              <td class="pageHeading" nowrap {$smarty.const.HL_LEFT_STYLE}>{$smarty.const.HEADING_TITLE}</td>
              {$smarty.const.BETWEEN_HL_LEFT_RIGHT}
          
              {if $filterlistisnotempty}
                <td align="center" class="PageHeadingCenter">
                {$filter_form}
                {$smarty.const.TEXT_SHOW}&nbsp;
              {/if}                          
              <td class="pageHeadingRight" align="right" {$smarty.const.HL_RIGHT_STYLE}>{$tep_image_category}</td>
               
               {$smarty.const.AFTER_HL_RIGHT}
            </tr>
          </table>
        </td>
      </tr>
          {$smarty.const.BETWEEN_HL_CONTENT}      
  {/if}
    <tr>
      <td class="cont_abst">    
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          {if !empty($categories_html)}
          <!-- ausgabe categories_html beginn -->
          <tr>
            <td>
              <table border="0" width="100%" cellspacing="0" cellpadding="0">
                <tr>
                  <td class="category_desc">{$categories_html}</td>
                </tr>
              </table>
            </td>
          </tr>
          <!-- ausgabe categories_html ende -->
          {/if}
          
          {if !empty($manufacturers_html_header)}
          <!-- ausgabe categories_html beginn -->
            <tr>
              <td>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr><td class="category_desc">{* $manufacturers_html_header *}</td></tr>
                </table>
              </td>
            </tr>
          <!-- ausgabe categories_html ende -->
          {/if}
          
          {if !empty($manufacturers_html_header) || isset($categorieslist)}
            <!-- ausgabe catbilder listing beginn -->
            <tr>
              <td>
                <table border="0" width="100%" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>            
                      <table border="0" width="100%" cellspacing="0" cellpadding="0" class="categorietext_tbl">
                        {if isset($categorieslist)}
                        {assign var=row value=0}
                          {section name=current loop=$categorieslist}
                          	{if $row eq 0}
                            	<tr>
                           	{/if}                      
	                            <td align="center" class="categorietext" width="{$categorieslist[current].width}" valign="top" height="40">
	                            	<a href="{$categorieslist[current].seolink}">{$categorieslist[current].CATE}</a></td>
			                {assign var=row value=$row+1}
                            {if $row eq $MAX_DISPLAY_CATEGORIES_PER_ROW}
                            	</tr>
                            	{assign var=row value=0}
                            {/if}
                          {/section}
                        {else}
                          {if !empty($manufacturers_html_header)}
                            <!-- ausgabe manufacturers_html beginn -->
                              <tr>
                                <td>
                                  <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                    <tr><td class="category_desc">
                                      {$tep_manufacturers_html_header}
                                    </td></tr>
                                  </table>
                                </td>
                              </tr>
                            <!-- ausgabe manufacturers_html ende -->
                          {/if}                          
                        {/if}
                      </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- ausgabe catbilder listing ende --><!-- dist -->
          {/if}  
          
          <tr>
            <td>
              <span class="categorySpacer00">{$pixel_trans_20}</span>
            </td>
          </tr><!-- dist -->
          <tr>
            <td>{$PRODUCT_LISTING_HTML}</td>
          </tr>
        </table>    
      </td>
    </tr>
    
    {if !empty($manufacturers_id)}    
    <!-- ausgabe manufacturers_html beginn -->
    <tr>    
      <td><span class="categorySpacer00">{$pixel_trans_20}</span></td>
    </tr>
       <tr>
      <td>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="category_desc">{$manufacturers_html_footer}</td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- ausgabe manufacturers_html ende -->
    {/if}

    {if !empty($categories_html_footer)}
    <!-- ausgabe categories_html_footer beginn -->
    <tr>
      <td>
        <table border="0" width="100%" cellspacing="0" cellpadding="0">
          <tr>
            <td class="category_desc">{$categories_html_footer}</td>
          </tr>
        </table>
      </td>
    </tr>
    <!-- ausgabe categories_html_footer ende -->
    {/if}
  </table>
{/if}

{if !$smarty.const.PH_INSIDE}
      </td>
    </tr>
  </table>

{/if}
{$smarty.const.AFTER_CONTENT}