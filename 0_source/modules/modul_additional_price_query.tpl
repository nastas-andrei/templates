<!-- contentbereich -->

{$BEFORE_CONTENT} 
{$BETWEEN_HL_CONTENT}
<div id="price_query" class="price_query_style"> {if $sendung}
  {assign var=trenner value="/"}
  {assign var=targetUrl value=$domain$trenner$return_url_final$products_id}
  <table border="0">
    <tr>
      <td class="main" nowrap>&nbsp;</td>
      <td class="pageHeading" nowrap>{$PRICE_QUERY_TITLE}</td>
    </tr>
    <tr>
      <td class="main" nowrap>&nbsp;</td>
      <td class="main" nowrap>{$PRICE_QUERY_THANKS}<br>
      </td>
    </tr>
    <tr>
      <td class="main" nowrap>&nbsp;</td>
      <td class="main" nowrap>{$PRICE_QUERY_THANKS_DELEGATION}<br>
      </td>
    </tr>
  </table>
  <meta http-equiv="refresh" content="2; URL={$targetUrl}">
  {else}
  <table border="0">
    <tr>
      <td width="100%" class="main" nowrap>&nbsp;</td>
    </tr>
    <tr>
      <td rowspan="2"><img width="100" src="{$products_image}"></td>
      <td class="main" nowrap>&nbsp;</td>
      <td colspan="2" class="pageHeading" nowrap>{$products_name}</td>
    </tr>
    <tr>
      <td colspan="4" class="main" align="right">{$products_price}</td>
    </tr>
    <tr>
      <td colspan="4" class="main" align="right">
        <hr width="100%" height="3" bgcolor="Black">
      </td>
    </tr>
  </table>
  {assign var=trenner value="/"}
  {assign var=ret_url value=$domain$trenner$return_url}
  <form id="preice_query_form" method="POST" action="{$PRICE_INQUIRY_URL1}">
    <table border="0">
      <tr>
        <td class="main" nowrap>&nbsp;</td>
        <td class="pageHeading" nowrap>
        {if $pAnfrage eq "1"}
          {$PRICE_QUERY_TITLE}
          {else}
          {$PRODUCTS_INQUIRY}
        {/if}
        </td>
        <td align="right" class="main2">{$FORM_REQUIRED_INFORMATION}</td>
      </tr>
      {if $MESSAGE}
      <tr>
        <td colspan="3">{$MESSAGE}</td>
      </tr>
      {/if}
      <tr>
        <td class="main" >{$PRICE_QUERY_NAME} <span style="color: rgb(255, 0, 0);">*</span>:</td>
        <td colspan="2" class="main" nowrap>
          <input type="text" name="name" value="{$name}" />
          <input type="hidden" name="products_id" value="{$products_id}" />
          <input type="hidden" name="myaction" value="{$myaction}" />
          <input type="hidden" name="pAnfrage" value="{$pAnfrage}" />
        </td>
      </tr>
      <tr>
        <td class="main" nowrap>{$PRICE_QUERY_EMAIL}<span style="color: rgb(255, 0, 0);">*</span>:</td>
        <td colspan="2"  class="main" nowrap>
          <input type="text" name="email" value="{$email_address}" />
        </td>
      </tr>
      <tr>
        <td class="main" >{$PRICE_QUERY_FON}</td>
        <td colspan="2"  class="main" nowrap>
          <input type="text" name="fon" value="{$fonnumber}" />
        </td>
      </tr>
      {if $pAnfrage eq "1"}
      <tr>
        <td class="main" >{$PRICE_QUERY_COMPETITORS_PRICE}</td>
        <td colspan="2"  class="main" nowrap>
          <input type="text" name="mitbewerberpreis" value="{$mitbewerberpreis}" />
        </td>
      </tr>
      <tr>
        <td class="main" >{$PRICE_QUERY_COMPETITORS_PRICE_URL}<span style="color: rgb(255, 0, 0);">*</span>:</td>
        <td colspan="2"  class="main" nowrap>
          <input type="text" name="mitbewerberurl" value="{$mUrl}" />
        </td>
      </tr>
      {/if}
      {if $standardtext}
      <tr>
        <td class="main" >&nbsp;</td>
        <td colspan="2"  class="main">
          <textarea readonly="true" class="textareanochange" name="standardtext" cols="60" rows="3">{$standardtext}</textarea>
        </td>
      </tr>
      {/if}
      <tr>
        <td class="main" >{$PRICE_QUERY_COMMENT}<span style="color: rgb(255, 0, 0);">*</span>:</td>
        <td colspan="2"  class="main" nowrap>
          <textarea name="enquiry" cols="40" rows="10">{$PRICE_QUERY_COMMENT_VALUE}</textarea>
        </td>
      </tr>
      {* captcha *}
      {if $STORE_SECURITY_CAPTCHA_ENABLED == 'true' }
      <tr>
        <td class="main" colspan="3">
        	<div class="antistalker hidden" style="display:none;">
            Antispam: *<br />
            <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
            <select name="pf_antispam" id="pf_antispam">
              <option value="1" selected="selected">Ja</option>
              <option value="2">Nein</option>
            </select>
          </div>
					<script language="javascript" type="text/javascript">
          <!--
          var sel = document.getElementById('pf_antispam');var opt = new Option('Nein',2,true,true);sel.options[sel.length] = opt;   
          sel.selectedIndex = 1;
          -->
          </script>
          <noscript>
            <div class="antistalker">
              Antispam: *<br />
              <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
              <select name="pf_antispam" id="pf_antispam">
                <option value="1" selected="selected">Ja</option>
                <option value="2">Nein</option>
              </select>
            </div>
          </noscript>
        </td>
      </tr>
      {/if}
      {* end captcha *}
      <tr>
        <td align="left"> {assign var=targetUrl value=$domain$trenner$return_url} <a href="{$targetUrl}">{$PRICE_QUERY_BACK_BUTTON}</a> </td>
        <td width="10"><img src="{$imagepath}/spacer.gif" width="10" height="1"></td>
        <td align="right">{$PRICE_QUERY_BUTTON}</td>
      </tr>
    </table>
  </form>
  {/if}
</div>
<img src="{$imagepath}/spacer.gif" border="0" alt="" hspace="250"  height="1" width="12"><br>
<!-- contentbereich --> 
{$AFTER_CONTENT}