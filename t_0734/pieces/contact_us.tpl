<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="col-lg-12 col-sm-12 hero-feature mail-form">
            <div class="form-horizontal">
                {$contact_us_form}
                {if $contact_message_exists}
                    {$contact_message}
                {/if}
                {if $is_action_success}
                    {$smarty.const.TEXT_SUCCESS}
                    <a href="{$button_continue_url}">{$button_continue_img}</a>
                {else}
                    <div class="control-group">
                        <label class="control-label">{$smarty.const.ENTRY_NAME}</label>

                        <div class="controls">
                            {$name_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label">{$smarty.const.ENTRY_EMAIL}</label>

                        <div class="controls">
                            {$email_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label">{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>

                        <div class="controls">
                            {$anliegen_pull_down}
                        </div>
                    </div>
                    <div class="control-group" id="artikel" style="display: none">
                        <label class="control-label" id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</label>

                        <div class="controls">
                            {$artikel_input}
                        </div>
                    </div>
                    <div class="control-group" id="bestellung" style="display: none">
                        <label class="control-label" id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>

                        <div class="controls">
                            {$bestellung_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label">{$smarty.const.ENTRY_ENQUIRY}</label>

                        <div class="controls">
                            {$enquiry_textarea}
                        </div>
                    </div>
                {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                    <div class="control-group">
                        <div class="controls">
                            <span>Bist du ein Spammer oder ein BOT? </span>
                            <br>
                            <span>(Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                            <select name="pf_antispam" id="pf_antispam">
                                <option value="1" selected="selected">Ja</option>
                                <option value="2">Nein</option>
                            </select>
                        </div>
                    </div>
                    <script language="javascript" type="text/javascript">
                        var sel = document.getElementById('pf_antispam');
                        var opt = new Option('Nein', 2, true, true);
                        sel.options[sel.length] = opt;
                        sel.selectedIndex = 1;
                    </script>
                    <noscript>
                        <div class="antistalker">
                            <span>Bist du ein Spammer oder ein BOT? </span>
                            <br><span>(Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                            <select name="pf_antispam" id="pf_antispam">
                                <option value="1" selected="selected">Ja</option>
                                <option value="2">Nein</option>
                            </select>
                        </div>
                    </noscript>
                {/if}
                    <div class="control-group">
                        <div style="margin-top:20px">
                            <input type="submit" class="btn btn-primary" value="{$smarty.const.CONTACT_BOTTOM_TEXT}">
                        </div>
                    </div>
                {/if}
                </form>
            </div>
        </div>
    </div>
    <!-- map -->
    <div class="clearfix visible-sm"></div>
    <div class="col-lg-6 col-md-6 col-sm-12">
        <!-- OUR LOCATION -->
        <div class="col-lg-12 col-md-12 col-sm-12">
            <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=cihanjuang&amp;sll=37.0625,-95.677068&amp;sspn=56.637293,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=Cihanjuang.,+Cimahi+Utara,+Cimahi,+Jawa+Barat,+Indonesia&amp;t=m&amp;z=14&amp;ll=-6.858623,107.5664&amp;output=embed"></iframe>
            <br />
            <small>
                <a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=cihanjuang&amp;sll=37.0625,-95.677068&amp;sspn=56.637293,79.013672&amp;ie=UTF8&amp;hq=&amp;hnear=Cihanjuang.,+Cimahi+Utara,+Cimahi,+Jawa+Barat,+Indonesia&amp;t=m&amp;z=14&amp;ll=-6.858623,107.5664"
                   style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a></small>
        </div>
        <!-- End OUR LOCATION -->
    </div>
</div>    