{if $boxes_content.$key}
    <div class="widget">
        <div class="title">
            <h3>{$boxes_content.$key.boxes_title} &nbsp;</h3>
        </div>
        <ul>
            {$boxes_content.$key.boxes_content}
        </ul>
    </div>
{/if}