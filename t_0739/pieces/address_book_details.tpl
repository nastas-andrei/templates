<div class="col-xs-7">
    <div class="inputHLDbox">
        <div class="inputHLDhead">{$smarty.const.FORM_REQUIRED_INFORMATION}</div>
        <div class="inputHLDinner" style="margin-top:10px">
            {*******************GENRE****************}
            <div class="inputHLD">
                <div class="logradioHLD">
                    {if $smarty.const.ACCOUNT_GENDER == 'true'}
                        <label class="control-label" style="margin-right:10px">
                            {$ab_gender_m_radio}
                            {$smarty.const.MALE}
                        </label>
                        <label class="control-label">
                            {$ab_gender_f_radio}
                            {$smarty.const.FEMALE}
                        </label>
                        {if $ab_entry_gender_text}
                            <span style="color:red;margin-left:5px">
                            {$smarty.const.ENTRY_GENDER_TEXT}
                        </span>
                        {/if}
                    {/if}
                </div>
            </div>
            <div class="clearer"></div>
            {*******************FIRST NAME****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_FIRST_NAME}
                    {if $ab_entry_firstname_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_FIRST_NAME_TEXT}
                        </small>
                    {/if}
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}
                        <small style="color:red">
                            ({$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH})
                        </small>
                    {/if}
                </div>
                {$ab_firstname_input}
            </div>
            {*******************LAST NAME****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_LAST_NAME}
                    {if $ab_entry_lastname_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_LAST_NAME_TEXT}
                        </small>
                    {/if}
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}
                        <small style="color:red">
                            ({$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})

                        </small>
                    {/if}
                </div>
                {$ab_lastname_input}
            </div>
            <div class="clearer"></div>
            {*******************COMPANY ISSET****************}
            {if $smarty.const.ACCOUNT_COMPANY == 'true'}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_COMPANY}
                        {if $ab_entry_company_text}
                            <small style="color:red">
                                {$smarty.const.ENTRY_COMPANY_TEXT}
                            </small>
                        {/if}
                        {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}
                            <small style="color:red">
                                ({$smarty.const.ENTRY_COMPANY_MAX_LENGTH})
                            </small>
                        {/if}
                    </div>
                    {$ab_company_input}
                </div>
            {/if}
            {*******************STREET ADDRESS****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_STREET_ADDRESS}
                    {if $ab_entry_street_address_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_STREET_ADDRESS_TEXT}
                        </small>
                    {/if}
                    {if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH == true}
                        <small style="color:red">
                            ({$smarty.const.ENTRY_STREET_ADDRESS_MAX_LENGTH})
                        </small>
                    {/if}
                </div>
                {$ab_street_address_input}
            </div>
            {*******************ACCOUNT_SUBURB****************}
            {if $smarty.const.ACCOUNT_SUBURB == 'true'}
                <div class="inputHLD">
                    <div class="inputTXT" style="white-space: nowrap;">{$smarty.const.ENTRY_SUBURB}
                        {if $ab_entry_suburb_text}
                            <small style="color:red">
                                {$smarty.const.ENTRY_SUBURB_TEXT}
                            </small>
                        {/if}
                    </div>
                    {$ab_suburb_input}
                </div>
            {/if}
            {*******************POST CODE****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_POST_CODE}
                    {if $ab_entry_postcode_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_POST_CODE_TEXT}
                        </small>
                    {/if}
                </div>
                {$ab_postcode_input}
            </div>
            {*******************CITY****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_CITY}
                    {if $ab_entry_city_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_CITY_TEXT}
                        </small>
                    {/if}
                </div>
                {$ab_city_input}
            </div>
            {*******************STATE****************}
            {if $smarty.const.ACCOUNT_STATE == 'true'}
                <div class="inputHLD">
                    <div class="inputTXT">{$smarty.const.ENTRY_STATE}
                        {if $ab_entry_state_text}
                            <small style="color:red">
                                {$smarty.const.ENTRY_STATE_TEXT}
                            </small>
                        {/if}
                    </div>
                    {$ab_state_field}
                </div>
            {/if}
            {*******************COUNTRY****************}
            <div class="inputHLD">
                <div class="inputTXT">{$smarty.const.ENTRY_COUNTRY}
                    {if $ab_entry_country_text}
                        <small style="color:red">
                            {$smarty.const.ENTRY_COUNTRY_TEXT}
                        </small>
                    {/if}
                </div>
                {$ab_country_field}
            </div>

            {if $ab_is_pimary_checkbox}
                <div class="form-group">
                <span style="float: left;margin-right: 5px;">
                    {$ab_primary_checkbox}
                </span>
                    <label class="control-label">
                        {$smarty.const.SET_AS_PRIMARY}
                    </label>
                </div>
            {/if}
        </div>
        <div class="clearer"></div>
    </div>
</div>
