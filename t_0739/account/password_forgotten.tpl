<div id="content" class="box-wrap">
    <!-- global messages -->
    <div class="location-bar">
        <h1>Forgot Your Password?</h1>
    </div>
    {$password_forgotten_form}
        <h2 class="h2-wm">
            <span class="heading-lines">{$smarty.const.NAVBAR_TITLE_2}</span>
        </h2>
        <div class="content">
            <table class="form">
                <tbody><tr>
                    <td>{$smarty.const.ENTRY_EMAIL_ADDRESS}</td>
                    <td>{$email_address_input}</td>
                </tr>
                </tbody></table>
        </div>
        <div class="buttons">
            <div class="left"><a href="{$LOGIN_URL}" class="button">&larr;&nbsp; {$smarty.const.IMAGE_BUTTON_BACK}</a></div>
            <div class="right">
                <input type="submit" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}" class="button">
            </div>
        </div>
    </form>
</div>