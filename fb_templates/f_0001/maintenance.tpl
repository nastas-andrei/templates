{include file="header.tpl"}
<div class="container canvas main-container">
<div class="row">
  <div class="span3">    
    <div class="logo">{$cataloglogo}</div>   
  </div>
  <div class="span9">  
	  {$content}
  </div>
  <div class="span12 footer-container">
    {include file="pieces/footer.tpl"}
  </div>
</div>
</div>
{include file="footer.tpl"}