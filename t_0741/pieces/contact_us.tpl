<iframe width="100%" height="350" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyBzBxYxCJDus32apdAMyDlkvNX3qNYgOsE">
</iframe>
<section class="postwrapper clearfix">
    <div class="container">
        <div class="row">
            <div class="fullwidth clearfix">
                <div class="contact_details">
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="miniboxes text-center" data-effect="helix">
                            <div class="miniicon"><i class="fa fa-map-marker fa-2x"></i></div>
                            <p>{$contact_data.shop_streat.value}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="miniboxes text-center" data-effect="helix">
                            <div class="miniicon"><i class="fa fa-mobile-phone fa-2x"></i></div>
                            <p>{$contact_data.shop_mobilephone.value}</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-6 col-xs-12">
                        <div class="miniboxes text-center" data-effect="helix">
                            <div class="miniicon"><i class="fa fa-headphones fa-2x"></i></div>
                            <p><a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                {if $contact_message_exists}
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {$contact_message}
                    </div>
                {/if}
                {$contact_us_form}
                    {if $is_action_success}
                        <div class="text-center">
                        {$smarty.const.TEXT_SUCCESS}
                            <br>
                                <a class="btn btn-primary btn-icon" href="{$button_continue_url}">{$IMAGE_BUTTON_CONTINUE_SHOPPING}</a>
                        </div>
                    {else}
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="text-center">
                            <strong>{$smarty.const.ENTRY_NAME}</strong>
                        </div>
                        {$name_input}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="text-center">
                            <strong>{$smarty.const.ENTRY_EMAIL}</strong>
                        </div>
                        {$email_input}
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="text-center">
                            <strong>{$smarty.const.ENTRY_CONTACT_SUBJECT}</strong>
                        </div>
                        <div class="input-box">
                            <div style="position: relative;z-index: 1;margin-right: -4px">{$anliegen_pull_down}</div>
                        </div>
                        <div class="field" id="artikel" style="display: none">
                            <div class="text-center">
                                <span id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</span>
                            </div>
                            <div class="clearfix">
                                {$artikel_input}
                            </div>
                        </div>
                        <div class="field" id="bestellung" style="display: none">
                            <div class="text-center">
                                <span id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</span>
                            </div>
                            <div class="clearfix">
                                {$bestellung_input}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                        <br>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center">
                            <strong>{$smarty.const.ENTRY_ENQUIRY}</strong>
                        </div>
                        <br>
                        {$enquiry_textarea}
                        {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                            <div class="form-group">
                                    <span>
                                        Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                                    </span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </div>
                            <script language="javascript" type="text/javascript">
                                var sel = document.getElementById('pf_antispam');var opt = new Option('Nein',2,true,true);sel.options[sel.length] = opt;
                                sel.selectedIndex = 1;
                            </script>
                            <noscript>
                                <div class="antistalker">
                                    <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                    <select name="pf_antispam" id="pf_antispam">
                                        <option value="1" selected="selected">Ja</option>
                                        <option value="2">Nein</option>
                                    </select>
                                </div>
                            </noscript>
                        {/if}
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-lg btn-primary">{$IMAGE_ANI_SEND_EMAIL}</button>
                    </div>
                    {/if}
                </form>
            </div>
        </div>
    </div>
</section>
