{if $arr_informationbox}
    <div class="block block-Information span3">
        <div class="block-title">{$arr_informationbox.title}</div>
        <div class="block-content">
            <ul>
                {foreach from=$arr_informationbox.items key=key item=link name=element}
                    <li><a href="{$link.target}">{$link.text}</a></li>
                {/foreach}
            </ul>
        </div>
    </div>
{/if}