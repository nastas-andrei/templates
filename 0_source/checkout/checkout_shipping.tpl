{$checkout_address_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr>
	        	<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}
 		<tr>
        	<td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="checkout_area">
      				<tr>
        				<td align="left">
        					<table border="0" width="100%" cellspacing="0" cellpadding="0">
          						<tr>
            						<td class="main"><div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_SHIPPING_ADDRESS}</b></div></td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td align="left">
        					<div class="checkoutFrame">
        						<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          							<tr>
            							<td>
            								<table border="0" width="100%" cellspacing="0" cellpadding="0">
              									<tr>
                									<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                									<td class="main" width="50%" valign="top">
                										{$smarty.const.TEXT_CHOOSE_SHIPPING_DESTINATION}
                										<br><br>
                										<a href="{$checkout_shipping_address_href}">{$button_change_address_img_button}</a>
                									</td>
                									<td align="right" width="50%" valign="top">
                										<table border="0" cellspacing="0" cellpadding="2">
                  											<tr>
                    											<td class="main checkoutOTLeft" align="center" valign="top">
                    												<b>{$smarty.const.TITLE_SHIPPING_ADDRESS}</b>
                    												<br>
                    												{$arrow_south_east_img}
                    											</td>
                    											<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                    											<td class="main checkoutOTRight" valign="top">{$sendto_address_label}</td>
                    											<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
                										</table>
                									</td>
              									</tr>
            								</table>
            							</td>
          							</tr>
        						</table>
        					</div>
        				</td>
      				</tr>
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
      				</tr>
      				{if $tep_count_shipping_modules_vs > 0}
      				<tr>
        				<td align="left">
        					<table border="0" width="100%" cellspacing="0" cellpadding="0">
          						<tr>
            						<td class="main"><div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_SHIPPING_METHOD}</b></div></td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td align="left">
        					<div class="checkoutFrame">
        						<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          							<tr>
            							<td>
            								<table border="0" width="100%" cellspacing="0" cellpadding="0">
            									{if $is_multiple_quotes}												
								              	<tr>
								                	<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
								                	<td class="main mainspacer" width="50%" valign="top">{$smarty.const.TEXT_CHOOSE_SHIPPING_METHOD}</td>
								                	<td class="main mainspacer" width="50%" valign="top" align="right">
								                		<b>{$smarty.const.TITLE_PLEASE_SELECT}</b>
								                		<br>
								                		{$arrow_east_south_img}
								                	</td>
								                	<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
								              	</tr>
								              	{elseif $free_shipping == false}												
								              	<tr>
								                	<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
								                	<td class="main" width="100%" colspan="2">{$smarty.const.TEXT_ENTER_SHIPPING_INFORMATION}</td>
								                	<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
								              	</tr>
												{/if}
												{if $free_shipping == true}												
              									<tr>
                									<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                									<td colspan="2" width="100%">
                										<table border="0" width="100%" cellspacing="0" cellpadding="2">
                  											<tr>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                    											<td class="main mainspacer" colspan="3"><b>{$smarty.const.FREE_SHIPPING_TITLE}</b>&nbsp;{$quotes_icon}</td>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
                  											<tr id="defaultSelected" class="moduleRowSelected" onMouseOver="rowOverEffect(this)" onMouseOut="rowOutEffect(this)" onClick="selectRowEffect(this, 0, '')">
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                    											<td class="main mainspacer" width="100%">{$shipping_description} {$shipping_hidden}</td>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
                										</table>
                									</td>
                									<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
              									</tr>              									
												{else}
              									
              									{section name=current loop=$quoteslist}
              									<tr>
                									<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                									<td colspan="2">
                										<table border="0" width="100%" cellspacing="0" cellpadding="0">
                  											<tr>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                    											<td class="main mainspacer" colspan="3"><b>{$quoteslist[current].module}</b>&nbsp;{$quoteslist[current].icon}</td>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
															{if $quoteslist[current].is_error}
															<tr>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                    											<td class="main" colspan="3"> if $quoteslist[current].error</td>
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
                  											{else}
															
															{section name=current_method loop=$quoteslist[current].methods}															
															{if $quoteslist[current].methods[current_method].is_row_selected}															
															<tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$quoteslist[current].methods[current_method].radio_buttons}, '')">
															{else}
															<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$quoteslist[current].methods[current_method].radio_buttons}, '')">
															{/if}
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>                    											
                    											<td class="main trenner" width="75%">
                    												{$quoteslist[current].methods[current_method].title}
                    											</td>
																{if $quoteslist[current].methods[current_method].is_multiple_methods}
                    											<td class="main trenner">
                    												{$quoteslist[current].methods[current_method].formated_cost_with_tax}
                    											</td>
                    											<td class="main trenner" align="right">
                    												{$quoteslist[current].methods[current_method].shipping_radio}
                    											</td>
																{else}
                    											<td class="main trenner" align="right" colspan="2">
                    												{$quoteslist[current].methods[current_method].formated_cost_with_tax}
                    												{$quoteslist[current].methods[current_method].shipping_hidden}
                    											</td>
																{/if}
                    											<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
                  											</tr>
															{/section}
															
                  											{/if}
														</table>
													</td>
                									<td class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
              									</tr>
              									{/section}
              									
              									{/if}
            								</table>
            							</td>
          							</tr>
        						</table>
        					</div>
        				</td>
      				</tr>
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>        				
      				</tr>
					{else}
					<!--
					  	// Wenn keine zutreffende Versand-Module gefunden wurden
					  	// Mögliche Fehler:
					  	//  - Modul inaktive
					  	//  - Versandland ist nicht aktiv für das Versand-Modul
					  	//  - Das Region aus dem Verandland ist nicht aktiv beim Versand-Modul
					  	//  - Das Bestellungsgewicht übersteigt oder unterschreitet die angaben bei Versand-Moduls
					-->
      				<tr>
        				<td align="left">
        					<table border="0" width="100%" cellspacing="0" cellpadding="0">
          						<tr>
            						<td class="main">
            							<div class="checkoutFrameHead"><b>{$smarty.const.TEXT_NO_SHIPPING}</b><!--Bestellung ist Übergewichtig--></div>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
					{/if}
      				<tr>
        				<td align="left">
        					<table class="table_heading_comment" border="0" width="100%" cellspacing="0" cellpadding="0">
          						<tr>
            						<td class="main"><div class="checkoutFrameHead"><b>{$smarty.const.TABLE_HEADING_COMMENTS}</b></div></td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td align="left">
        					<div class="checkoutFrame removeit">
        						<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox table_textarea_comment">
          							<tr>
            							<td>
            								{if $tep_count_shipping_modules_vs > 0}
	            								<table border="0" width="100%" cellspacing="0" cellpadding="0">
	              									<tr>	
	                									<td>{$comments_textarea}</td>
	              									</tr>
	            								</table>
            								{/if}	
            							</td>
          							</tr>
        						</table>
        					</div>
        				</td>
      				</tr>
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td align="left">
        					<table border="0" width="100%" cellspacing="0" cellpadding="0" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="0">
              								<tr>
                								<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
	              								{if $tep_count_shipping_modules_vs > 0}
	                								<td class="main">
	                									<b>{$smarty.const.TITLE_CONTINUE_CHECKOUT_PROCEDURE}</b>
	                									<br>
	                									{$smarty.const.TEXT_CONTINUE_CHECKOUT_PROCEDURE}
	                								</td>
                									<td class="main" align="right">{$button_continue_img_submit}</td>
               									{/if}
                								<td width="10" class="checkoutFrame_separator">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td class="checkoutFrame_separator">{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td align="left">
        					<table border="0" width="100%" cellspacing="0" cellpadding="0">
          						<tr>
            						<td width="25%">
            							<table border="0" width="100%" cellspacing="0" cellpadding="0">
              								<tr>
                								<td width="50%" align="right">{$checkout_bullet_img}</td>
                								<td width="50%">{$pixel_silver_100_1}</td>
              								</tr>
            							</table>
            						</td>
            							<td width="25%">{$pixel_silver_100_1}</td>
            							<td width="25%">{$pixel_silver_100_1}</td>
            							<td width="25%">
            								<table border="0" width="100%" cellspacing="0" cellpadding="0">
              									<tr>
                									<td width="50%">{$pixel_silver_100_1}</td>
                									<td width="50%">{$pixel_silver_1_5}</td>
              									</tr>
            								</table>
            							</td>
          							</tr>
          							<tr>
            							<td align="center" width="25%" class="checkoutBarCurrent">{$smarty.const.CHECKOUT_BAR_DELIVERY}</td>
            							<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_PAYMENT}</td>
            							<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</td>
            							<td align="center" width="25%" class="checkoutBarTo">{$smarty.const.CHECKOUT_BAR_FINISHED}</td>
          							</tr>
        						</table>
        					</td>
      					</tr>
        			</table>
        		</td>
      		</tr>
    	</table>

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
</form>
    
