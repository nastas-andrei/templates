<ul>
    {foreach from=$categories key=key item=category name=catlist}
        <li>
            <a href="{$category.link}" id="{$category.id}">{$category.name}</a>
            {if $category.children|@count > '0'}
                <div>
                    <ul>
                        {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                            <li><a href="{$categorySub.link}">{$categorySub.name|truncate:20:"...":true}</a></li>
                        {/foreach}
                    </ul>
                </div>
            {/if}
        </li>
    {/foreach}
</ul>
<select onchange="location = this.options[this.selectedIndex].value;">
    <option>Menu</option>
    {foreach from=$categories key=key item=category name=catlist}
        <option value="{$category.link}" >{$category.name}</option>
        {if $category.children|@count > '0'}
            {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                <option value="{$categorySub.link}" >&nbsp;- {$categorySub.name}</option>
            {/foreach}
        {/if}
    {/foreach}
</select>