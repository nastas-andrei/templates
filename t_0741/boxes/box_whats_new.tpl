{if $arr_whatsnewbox}
    <div class="title text-center">
        <h1>{$arr_whatsnewbox.title}</h1>
        <hr>
    </div>
    <div class="paddingtop clearfix text-center">
        {foreach from=$arr_whatsnewbox.products key=key item=product name=productpreviewitems}
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 grid cs-style-3" data-effect="slide-bottom">
                {if $product.image}
                    {php}
                        $product = $this->get_template_vars('product');
                        $pattern_src = '/src="([^"]*)"/';
                        preg_match($pattern_src, $product['image'], $matches);
                        $src = $matches[1];
                        $this->assign('src', $src);
                    {/php}
                {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                {/if}
                <div class="shopstyle1">
                    <div class="media_element">
                        <figure>
                            {*<span class="onsale">NEW!</span>*}
                            <a href="{$product.link}">
                                <div class="image_view_product" style="background-image:url('{$src}')"></div>
                            </a>
                            <figcaption>
                                <a class="external" rel="prettyPhoto" href="{$src}">
                                    <i class="fa fa-search"></i>
                                </a>
                                <a class="zoom" href="{$product.link}"><i class="fa fa-eye"></i></a>
                                <a class="addtocart" href="{$product.buy_now_link}">
                                    <i class="fa fa-shopping-cart"></i> {$smarty.const.IMAGE_BUTTON_IN_CART}
                                </a>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="product_title title">
                        <h3><a href="{$product.link}" title="{$product.name}">{$product.name|truncate:20:"...":true}</a></h3>
                    </div>
                    <div class="price-detail">{$product.preisneu|regex_replace:"/[()]/":""}</div>
                </div>
            </div>
        {/foreach}
    </div>
{/if}




