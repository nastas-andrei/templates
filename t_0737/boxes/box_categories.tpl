<div class="menu_block_dropdown">
    <nav id="main_menu_outer">
        <ul class="nav navbar-nav">
            <li class="level0">
                <a href="{$damain}" class="image-link">
                    <span><img src="templates/t_0737/images/icon_home.png" alt=""></span>
                </a>
            </li>
            {foreach from=$categories key=key item=category}
                <li class="dropdown level0">
                    <a href="{$category.link}" id="level0">
                        <span>{$category.name}</span>
                        {if $category.children|@count > 0}<i class="fa fa-caret-down float-right-ico-arrow"></i>{/if}
                    </a>
                    {if $category.children|@count > 0}
                        <ul class="dropdown-menu" id="category_level1">
                            {foreach from=$category.children key=key item=categorylevel2}
                                <li class="dropdown-submenu">
                                    <a href="{$categorylevel2.link}" class="submenu" >
                                        {$categorylevel2.name}
                                        {if $categorylevel2.children|@count > 0}<i class="fa fa-caret-right float-right-ico-arrow arrow-level2"></i>{/if}
                                    </a>
                                    {if $categorylevel2.children|@count > '0'}
                                        <ul class="dropdown-menu" id="category_level2">
                                            {foreach from=$categorylevel2.children key=key item=categorySub2}
                                                <li>
                                                    <a class="submenu" href="{$categorySub2.link}">{$categorySub2.name}</a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                            {/foreach}
                        </ul>
                    {/if}
                </li>
            {/foreach}
        </ul>
    </nav>
</div>




