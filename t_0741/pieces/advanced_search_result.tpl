<div class="whitewrapper bordertop clearfix">
    <div class="container">
        {if $arr_modul_productlisting.products|@count lt 1}
        <p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}
            <a href="{$arr_searchbox.advanced_search_link}" class="btn btn-primary btn-icon">{$arr_searchbox.advanced_search_text}</a>
        </p>
        <a href="javascript:history.go(-1)" class="btn btn-primary btn-icon">{$smarty.const.IMAGE_BUTTON_BACK}</a>
        {else}
        {if $is_search_message}
            <p class="text-center">{$search_message}</p>
        {/if}
            {$PRODUCT_LISTING_HTML}
            <div id="sidebar" class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                {include file="boxes/box_best_sellers.tpl"}
            </div>
        {/if}
    </div>
</div>

