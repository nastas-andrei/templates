<section class="blog-wrapper">
    <div class="container">
        <div class="col-lg-5">
            {if $is_search_message}
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    {$search_message}
                </div>
            {/if}
            {$advanced_search_form}
            <div class="fieldset advanced-search">

                <ul class="form-list" id="advanced-search-list">
                    <li class="odd">
                        <label>{$IMAGE_BUTTON_QUICK_FIND}</label>
                        <div class="input-box" style="max-width: 359px;">
                            <input type="text" class="input-text" name="keywords" value="{$arr_searchbox.inputtextfield}">
                        </div>
                    </li>
                    <li>
                        <table>
                            <tr>
                                <td>
                                <label for="price">{$smarty.const.ENTRY_PRICE_FROM}</label>
                                </td>
                                <td></td>
                                <td>
                                <label for="price">{$smarty.const.ENTRY_PRICE_TO}</label>
                                <td>
                            </tr>
                            <tr>
                                <td>
                                    {$pfrom_field}
                                </td>
                                <td><strong>-</strong></td>
                                <td>
                                    {$pto_field}
                                <td>
                            </tr>
                        </table>
                    </li>
                    <li class="odd">
                        <label for="color">{$smarty.const.ENTRY_CATEGORIES}</label>
                        <div class="input-box">
                            {$categories_id_field}
                        </div>
                    </li>
                    <li class="odd">
                        <label for="color">{$smarty.const.ENTRY_MANUFACTURERS}</label>
                        <div class="input-box">
                            {$manufacturers_id_field}
                        </div>
                    </li>
                    <li class="even">
                        <div class="input-box">
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        {$inc_subcat_field}{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="last odd">
                        <div class="input-box" >
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="1" name="search_in_description" checked="checked"
                                               id="1">{$smarty.const.TEXT_SEARCH_IN_DESCRIPTION}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="buttons-set">
                <button type="submit" title="Search" class="btn btn-primary btn-icon">
                    {$smarty.const.IMAGE_BUTTON_SEARCH}
                </button>
            </div>
            </form>
        </div>
    </div>
</section>
