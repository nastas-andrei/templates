<div class="account-container">
<b>{$smarty.const.HEADING_RETURNING_CUSTOMER}</b>
<p><!--{$smarty.const.TEXT_RETURNING_CUSTOMER} --></p>
<form class="form-horizontal"  name="login" action="{$login_form_url}" method="post">

{if $LoginMessageStackSize}
  <div class="alert alert-error">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
      {$smarty.const.TEXT_LOGIN_ERROR}
  </div>
{/if}

  
  <div class="control-group">
    <label class="control-label" for="mailadress">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
    <div class="controls">
      <input type="text" id="mailadress" name="email_address" placeholder="{$FORM_BOX_EMAIL}">
    </div>
  </div>
  <div class="control-group">
    <label class="control-label" for="passwortfeld">{$smarty.const.ENTRY_PASSWORD}</label>
    <div class="controls">
      <input type="password" id="passwortfeld" placeholder="{$smarty.const.ENTRY_PASSWORD_TEXT}" name="password">
    </div>
  </div>
  <div class="control-group">
    <div class="controls"> 
      <label>
        <a href="{$PASSWORD_FORGOTTEN_URL}">{$smarty.const.TEXT_PASSWORD_FORGOTTEN}</a><br>        
      </label>
      <button type="submit" class="btn btn-primary">{$smarty.const.IMAGE_BUTTON_LOGIN}</button>
    </div> 
  </div>
</form>
<hr style="border-top:1px solid #a2a2a2">
{if $mode_retail_active}      
  <b>{$smarty.const.HEADING_NEW_CUSTOMER}</b>
  <p>{$smarty.const.TEXT_NEW_CUSTOMER_INTRODUCTION}</p> 
  <a href="{$CREATE_ACCOUNT_URL}" class="btn btn-small btn-primary">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
{/if}


{if $mode_merchant_active}    

  <div class="clearfix">
    <hr>
  <p><b>{$smarty.const.HEADING_NEW_MERCHANT}</b> </p>
  <p>{$smarty.const.TEXT_NEW_MERCHANT_INTRODUCTION}</p>
  <a href="{$CREATE_ACCOUNT_B2B_URL}" class="btn btn-small btn-primary">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
  </div>
{/if}
</div>