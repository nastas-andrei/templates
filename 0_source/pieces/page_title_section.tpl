<table border="0" width="100%" cellspacing="0" cellpadding="0" class="pageHeadingTable">
	<tr>
    	{$smarty.const.BEFORE_HL_LEFT}       	               	        
        <td class="pageHeading" nowrap {$smarty.const.HL_LEFT_STYLE}>{if empty($custom_heading_title)} {$HEADING_TITLE} {else} {$custom_heading_title} {/if}</td>
        {$smarty.const.BETWEEN_HL_LEFT_RIGHT}
        <td class="pageHeadingRight" align="right" {$smarty.const.HL_RIGHT_STYLE}>&nbsp;</td>         
        {$smarty.const.AFTER_HL_RIGHT}       	 
    </tr>
</table>