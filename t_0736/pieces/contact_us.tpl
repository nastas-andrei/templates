<div id="sns_content" class="wrap">
    <div class="container">
        <div class="row-fluid">
            <div id="sns_main" class="span12 col-main">
                <!-- global messages -->
                <div id="sns_mainmidle" class="span12 clearfix">
                    <!-- primary content -->
                    <div class="contact-page">
                        {if $contact_message_exists}
                            <div class="alert alert-danger">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                {$contact_message}
                            </div>
                        {/if}
                        <div class="page-title">
                            <h1>{$smarty.const.NAVBAR_TITLE}</h1>
                        </div>
                        <div class="span12 contact-content">
                            <div class="map-wrap">
                                <iframe width="1170" height="450" frameborder="0" style="border:0"
                                        src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyASNmy5aLU4Gv3ExDUfmCIbmOpE6MgXzmg"></iframe>
                            </div>
                        </div>
                        <div class="row-fluid clearfix">
                            <div class="span3">
                                <div class="block  block-connectus">
                                    <div class="block-content clearfix">
                                        <p class="c-address">{$contact_data.shop_streat.value}
                                            , {$contact_data.shop_city.value}, {$contact_data.shop_country.value}</p>

                                        <p class="c-email"><label>Email</label>: <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">{$smarty.const.HEAD_REPLY_TAG_ALL}</a>
                                        </p>

                                        <p class="c-phone"><label>Phone</label>: {$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="span9">
                                {$contact_us_form}
                                {if $is_action_success}

                                {$smarty.const.TEXT_SUCCESS}
                                <a href="{$button_continue_url}">{$button_continue_img}</a>
                                {else}
                                <ul class="form-list">
                                    <li class="fields">
                                        <div class="field">
                                            {$smarty.const.ENTRY_NAME}
                                            <div class="input-box">
                                                {$name_input|replace:'class="general_inputs"':'class="input-text required-entry"'}
                                            </div>
                                        </div>
                                        <div class="field">
                                            {$smarty.const.ENTRY_EMAIL}
                                            <div class="input-box">
                                                {$email_input|replace:'class="general_inputs"':'class="input-text required-entry"'}
                                            </div>
                                        </div>
                                    </li>
                                    <li class="fields">
                                        <div class="field">
                                            {$smarty.const.ENTRY_CONTACT_SUBJECT}
                                            <div class="input-box">
                                                <div style="position: relative;z-index: 1;margin-right: -4px">{$anliegen_pull_down}</div>
                                            </div>
                                        </div>
                                        <div class="field" id="artikel" style="display: none">
                                            <span id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</span>

                                            <div class="input-box">
                                                {$artikel_input|replace:'general_inputs':'input-text required-entry'}
                                            </div>
                                        </div>
                                        <div class="field" id="bestellung" style="display: none">
                                            <span id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</span>

                                            <div class="input-box">
                                                {$bestellung_input|replace:'general_inputs':'input-text required-entry'}
                                            </div>
                                        </div>
                                    </li>
                                    <li class="wide">
                                        <div style="margin:10px 0 5px 0">{$smarty.const.ENTRY_ENQUIRY}</div>
                                        <div class="input-box">
                                            {$enquiry_textarea|replace:'class="textarea"':'class="input-text required-entry"'}
                                        </div>
                                    </li>
                                </ul>
                                {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                                    <div class="form-group">
                                      <span>
                                          Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                                      </span>
                                        <select name="pf_antispam" id="pf_antispam">
                                            <option value="1" selected="selected">Ja</option>
                                            <option value="2">Nein</option>
                                        </select>
                                    </div>
                                    <script language="javascript" type="text/javascript">

                                        var sel = document.getElementById('pf_antispam');
                                        var opt = new Option('Nein', 2, true, true);
                                        sel.options[sel.length] = opt;
                                        sel.selectedIndex = 1;

                                    </script>
                                    <noscript>
                                        <div class="antistalker">
                                            <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                            <select name="pf_antispam" id="pf_antispam">
                                                <option value="1" selected="selected">Ja</option>
                                                <option value="2">Nein</option>
                                            </select>
                                        </div>
                                    </noscript>
                                {/if}
                                <div class="buttons-set">
                                    <button type="submit" class="button">{$smarty.const.CONTACT_BOTTOM_TEXT}</button>
                                </div>
                            </div>
                            {/if}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>