/**************START Categories hover******************************/
 var loading = false; 
 var dpp = jQuery;
dpp.noConflict();
  dpp(document).on('mouseover', function() {
    dpp('.menu-item').on('mouseover',function(){
        var current_item = dpp(this)          
          if (!current_item.hasClass('visited')) {
            if (loading == false) {    
              current_item.addClass('visited');
              loading = true
              dpp.ajax({
                type: 'GET',
                url: "read_subcategories.php",      
                data: { category: current_item.attr('id')},        
                dataType: 'json'
              }).success(function(data) {
                    if (data.count > 0) {
                        current_item.parent().append('<ul class="dropdown-menu"></ul>')
                        current_list = current_item.next('.dropdown-menu')

                        dpp.each( data.categories, function( i, item) {
                            if (item.has_subcategories == true) {
                              current_list.append('<li class="dropdown-submenu"><a tabindex="-1" data-toggle="dropdown" class="menu-item submenu" href="'+item.link+'" id="'+item.id+'">'+item.text+'</a></li>')           
                            }else{
                              current_list.append('<li><a class="menu-item visited" href="'+item.link+'" id="'+item.id+'">'+item.text+'</a></li>')  
                            }                            
                          });
                    };
              }).complete(function(){  
                  loading = false 
                  dpp('.menu-item.submenu').on('mouseover', function(){
                    var current_subitem = dpp(this)  
                      if (!current_subitem.hasClass('visited')) {
                        if (loading == false) {    
                          current_subitem.addClass('visited');
                          loading = true
                          dpp.ajax({
                            type: 'GET',
                            url: "read_subcategories.php",      
                            data: { category: current_subitem.attr('id')},        
                            dataType: 'json'
                          }).success(function(data) {                       
                                if (data.count > 0) {
                                    current_subitem.parent().append('<ul class="dropdown-menu"></ul>')
                                    current_list = current_subitem.next('.dropdown-menu')

                                    dpp.each( data.categories, function( i, item) {
                                        if (item.has_subcategories == true) {
                                          current_list.append('<li class="dropdown-submenu"><a tabindex="-1" data-toggle="dropdown" class="menu-item submenu" href="'+item.link+'" id="'+item.id+'">'+item.text+'</a></li>')           
                                        }else{
                                          current_list.append('<li><a class="menu-item visited" href="'+item.link+'" id="'+item.id+'">'+item.text+'</a></li>')  
                                        }                            
                                      });
                                };
                          }).complete(function(){  
                            loading = false                                                
                          });
                        }//loading  false
                    }//not visited 
                  })        
              });
            }//loading  false
        }//not visited     
    }) //hover
  }) //ready
/**************END Categories hover********************/
