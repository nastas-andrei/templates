<section id="advanced-search" class="container">
    {if $is_search_message}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {$search_message}
        </div>
    {/if}
    <div class="row">
        <div class="col-sm-8">
            <div class="contact-form">
                <h2 class="title text-center">{$smarty.const.CONTACT_RESPONSE_TIME}</h2>
                {$advanced_search_form}
                    <h2 class="legend">
                        {if empty($custom_heading_title)}
                            {$HEADING_TITLE}
                        {else}
                            {$custom_heading_title}
                        {/if}
                    </h2>
                    <div class="form-group col-md-6">
                        <input type="text" class="form-control" name="keywords">
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <label>{$smarty.const.ENTRY_PRICE_FROM}</label>
                        {$pfrom_field|replace:'class="general_inputs"':'class="form-control"'}
                    </div>
                    <div class="form-group col-md-6">
                        <label>&nbsp;</label>
                        {$pto_field|replace:'class="general_inputs"':'class="form-control"'}
                    </div>
                    <div class="form-group col-md-12">
                        <label>{$smarty.const.ENTRY_CATEGORIES}</label>
                        {$categories_id_field|replace:'name="categories_id"':'name="categories_id" class="form-control"'}
                    </div>
                    <div class="form-group col-md-12">
                        <label>{$smarty.const.ENTRY_MANUFACTURERS}</label>
                        {$manufacturers_id_field|replace:'name="manufacturers_id"':'name="manufacturers_id" class="form-control"'}
                    </div>
                    <div class="form-group col-md-12">
                        {$inc_subcat_field}
                        <label>{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}</label>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="checkbox" value="1" name="search_in_description" checked="checked" id="1">
                        <label>{$smarty.const.TEXT_SEARCH_IN_DESCRIPTION}</label>
                    </div>
                    <button type="submit" title="Search" class="btn btn-primary margin-bottom">
                        {$smarty.const.IMAGE_BUTTON_SEARCH}
                    </button>
                </form>
            </div>
        </div>
    </div>
</section>