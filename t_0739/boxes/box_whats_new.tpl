<!--Begin New Content -->
{if $arr_whatsnewbox}
    <div class="box-heading"><span>{$arr_whatsnewbox.title}</span></div>
    <div class="box-content">
        <div class="latest">
            <div class="box-product">
                {foreach from=$arr_whatsnewbox.products key=i item=product name=newproducts}
                    {if $product.image}
                        {php}
                            {$product = $this->get_template_vars('product');}
                            {$pattern_src = '/src="([^"]*)"/';}
                            {$pattern_width = '/width="([^"]*)"/';}
                            {$pattern_height = '/height="([^"]*)"/';}
                            {preg_match($pattern_src, $product['image'], $matches);}
                            {$src = $matches[1];}
                            {preg_match($pattern_width, $product['image'], $matches);}
                            {$width = $matches[1];}
                            {preg_match($pattern_height, $product['image'], $matches);}
                            {$height = $matches[1];}
                            {$this->assign('src', $src);}
                            {$this->assign('img_width', $width);}
                            {$this->assign('img_height', $height);}
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <div>
                        <div class="image">
                            <a href="{$product.link}">
                                <div class="image_view_product_list" style="background-image: url('{$src}')"></div>
                            </a>
                        </div>
                        <div class="name"><a href="{$product.link}" title="{$product.name}">{$product.name|truncate:26:"...":true}</a></div>
                        <div class="price">
                            <div class="sale"><i class="icon-barcode" rel="tooltip" data-placement="bottom" data-original-title="SALE"></i></div>
                            <span class="price-new">{if $product.preisalt > 0}{$product.preisalt}{/if}</span>
                            <div class="price">
                                {$product.preisneu|regex_replace:"/[()]/":""}
                            </div>
                        </div>
                        {*<div class="rating"><img src="{$templatepath}images/stars-5.png" alt="Based on 2 reviews." /></div>*}
                        <div class="cart"><input type="button" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" onclick="addToCart('{$product.buy_now_link}');" class="button" /></div>
                    </div>
                {/foreach}
            </div>
        </div>
        <span id="more-latest" class="more"><i class="fa fa-align-justify" rel="tooltip" data-placement="top" data-original-title="SHOW MORE"></i></span>
    </div>
{/if}
<!--END New Content -->
