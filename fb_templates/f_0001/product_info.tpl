{include file="header.tpl"}
{literal}
<script type="text/javascript">
    console.log('test_p_info');
  jQuery(document).ready(function($) {
      jQuery('.slider-xsell, .slider-also-purchased, .slider-review2preview').bxSlider({
        slideWidth: 97,
        minSlides: 3,
        maxSlides: 7,
        moveSlides: 1,
        slideMargin: 10,
        auto:false,
        pager: false
      }); 
  });  
</script>
{/literal}
<div id="wrapper" class="container canvas main-container">

    <div class="navigation-controls clearfix">
      <div class="pull-left">
        <i class="icon-caret-left"></i> <a href="{$home_link}">Home</a>
      </div>
    </div>


<div class="row-fluid"> 
  <div class="span12">        
    <div class="product-info-page-container">
    <div class="product-info-page">
        {if $pAnfrage}  
          {if $pAnfrage == 1}
            {include file="modules/modul_additional_price_query.tpl"}
          {else}
            {include file="modules/modul_additional_price_query.tpl"}
          {/if}
        {else}
          {if $arr_product_info.product_check == true}                  
                 {include file="pieces/review2preview.tpl"}
                <div class="product-info-media-container">
                 

                 
                  <div class="media">
                    {if $arr_product_info.image_source}               
                        {if $arr_product_info.image_linkLARGE}                  
                          <a class="thumbnail pull-left modal-item" data-title="{$arr_product_info.products_name}"  href="{if $arr_product_info.image_linkLARGE|preg_match:"\.[jpg|png|gif|jpeg]"}{$arr_product_info.image_linkLARGE}{else}{$arr_product_info.image_linkLARGE}{/if}">
                         
                            <div class="img-canvas" style="width:130px;height:130px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$arr_product_info.image_link}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$arr_product_info.image_link}', sizingMethod='scale');"></div>                 
                          </a>                   
                        {else}  
                          <a href="{$arr_product_info.image_link}" class="pull-left thumbnail modal-box">                         
                             <div class="img-canvas" style="width:130px;height:130px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$arr_product_info.image_link}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$arr_product_info.image_link}', sizingMethod='scale');"></div>                 
                          </a>
                        {/if}               
                    {else}
                      <a href="#" class="pull-left thumbnail">                                  
                        <img src="{$imagepath}/empty-album.png" style="width:130px;height:130px;"/>
                      </a>
                    {/if}
                    <div class="media-body">
                      <h1 class="meadia-heading product-title">{$arr_product_info.products_name}</h1>
                      <p class="product-category">{$categories_name}</p>
                      <p>
                        <div class="fb-like" data-href="{$arr_product_info.link}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                      </p>
                    </div>
                  </div>
                </div>
          
                <div class="product-info-price-container clearfix">                    
                        <div class="pull-left">                                                              
                            <span class="product-info-price-old">{if $arr_product_info.preisalt > 0}{$arr_product_info.preisalt}{/if}</span>
                            <span class="product-info-price-new" id="pricefield"><span id="products_price">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</span></span>                       
                        </div>
                        <div class="pull-right">  
                            <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post">                      
                                    {if $LOAD_CLIENT}                                 
                                      <button type="submit" class="btn btn-primary shopping-cart-btn">{$smarty.const.IMAGE_BUTTON_IN_CART}</button>
                                      <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">                                                
                                    {else}
                                       <button class="btn btn-primary disabled shopping-cart-btn" disabled>{$smarty.const.IMAGE_BUTTON_IN_CART}</button>
                                       <input type="hidden" name="products_id" value="{$arr_product_info.products_id}"/>    
                                      <!-- <span id="hinweis_variante" class="hinweis_variante">{$PRODUCTS_VARIANTS_TIP}</span> -->
                                    {/if}                             
                          </div>
                </div>                         
          
                <div class="product-info-description-container">              
                      {if $arr_additional_images.products}    
                            {include file="modules/modul_additional_images.tpl"}                        
                      {/if}  
                         
                        <table class="product-info-table-description">
                          {*-------------------------------------------*}
                         
                          {if $opt_count < 1}                                         
                            {if $arr_product_info.arr_products_options}
                            <tr>
                              <td style="vertical-align:middle">{$arr_product_info.TEXT_PRODUCT_OPTIONS}</td>
                              <td>
                                {if $arr_product_info.product_options_javascript}
                                   {$arr_product_info.product_options_javascript}
                                {/if}
                                <table>
                                  {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                  <tr>
                                    <td>{$productoption.name}:</td>
                                    <td >{$productoption.pulldown}</td>
                                  </tr>
                                  {/foreach}
                                </table>
                              </td>
                            </tr>
                            {/if} 
                          {/if} 
                          {*-------------------------------------------*}
                          {if $opt_count > 0}
                              {if $var_options_array}
                                  <tr>
                                        <td>
                                          <input type="hidden" id="master_id" value="{$master_id}">
                                          <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                          <input type="hidden" id="cPath" value="{$cPath}">
                                          <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                          {if $Prodopt}
                                            <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                          {/if}
                                          {if $prod_hidden}
                                            {foreach from=$prod_hidden key=key item=hidden_input}
                                              {$hidden_input}
                                            {/foreach}
                                          {/if}
                                          {if $select_ids}
                                            {foreach from=$select_ids key=key item=select_input}
                                              {$select_input}
                                            {/foreach}
                                          {/if}
                                          
                                          {if $javascript_hidden}
                                            {$javascript_hidden}
                                          {/if}
                                  </td>
                                    </tr>
                                  <tr class="messageStackError">
                                    <td colspan="2"> <span id="opterror" style="display:none">Bitte weitere Optionen auswählen</span> </td>
                                  </tr>

                                  <tr class="messageStackError">
                                    <td colspan="2"> <span id="arterror" style="display:none">Leider kein Artikel in dieser Ausführung vorhanden </span></td>
                                  </tr>                                         
                              {foreach from=$var_options_array key=key item=options}
                                  <tr>
                                    <td>{$options.title}:</td>
                                    <td>{$options.dropdown}</td>
                                  </tr>                                             
                              {/foreach}
                              {/if}                                        
                          {/if}
                          {*-------------------------------------------*}
                         
            
            
                          {*-------------------------------------------*}
                          {if $arr_product_info.base_price}
                          <tr>
                            <td colspan="2">{$arr_product_info.base_price}</td>                            
                          </tr>
                          {/if}
                          {*-------------------------------------------*}
                          {if $arr_product_info.shipping_value gt 0}
                          <tr>
                            <td>{$TEXT_SHIPPING}:</td>
                            <td>({$arr_product_info.shipping})</td>
                          </tr>
                          {/if}
                          {*-------------------------------------------*}                           
                          {if $arr_product_info.products_uvp}
                          <tr>
                            <td>{$arr_product_info.TEXT_USUAL_MARKET_PRICE}:</td>
                            <td>{$arr_product_info.products_uvp}</td>
                          </tr>
                          <tr>
                            <td>{$arr_product_info.TEXT_YOU_SAVE}:</td>
                            <td>{$arr_product_info.diff_price}</td>
                          </tr>
                          {/if}
                          {*-------------------------------------------*}
                          {if $count_bestprice gt 0}                  
                          <tr>
                            <td colspan="2" >{$staffelpreis_hinweis}</td>
                          </tr>
                          <tr>
                            <td colspan="2">                    
                              <table>                     
                                  {foreach from=$bestprice_arr key=key item=bestprice}
                                  {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                    <tr>
                                      <td>{$bestprice.ausgabe_stueck}</td>                            
                                      <td>{$bestprice.ausgabe_preis}</td>
                                    </tr>
                                  {/foreach}
                              </table>                    
                            </td>
                          </tr>
                          {/if}
                          {*-------------------------------------------*}
                            {if $PRICE_QUERY_ON eq 'true'}                          
                                          <tr>
                                            <td>{$PRICE_QUERY}:</td>
                                            <td> <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a></td>
                                          </tr>
                                          {/if}
                          {*-------------------------------------------*}
                                          {if $PRODUCTS_QUERY_ON eq 'true'}                                
                                          <tr>
                                            <td>{$PRODUCTS_INQUIRY_TEXT}:</td>
                                            <td> <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a></td>
                                          </tr>
                                          {/if}       
                                          {*-------------------------------------------*}                                
                          
                          {* $arr_product_info.availability.text => z.b. "Sofort lieferbar"*} 
                          {* $arr_product_info.availability.value => 0-3 => 0 = Keine Angabe, 1 = Sofort lieferbar, 2 = beschr�nkt lieferbar, 3 = nicht lieferbar *}  
                          {if count($arr_product_info.availability)}
                          <tr>
                            <td>{$arr_product_info.availability.TEXT_AVAILABILITY}:</td>
                            <td >{$arr_product_info.availability.text}</td>
                          </tr>
                          
                          {/if}                  
                          {*-------------------------------------------*}
                          {if count($arr_product_info.delivery_time)}
                            <tr>
                              <td>{$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}</td>
                              <td>{$arr_product_info.delivery_time.text} *</td>
                            </tr>                 
                          {/if}
                          {*-------------------------------------------*}

                          {if $arr_product_info.text_date_available}
                          <tr>
                            <td>&nbsp;</td>
                            <td>{$arr_product_info.text_date_available}</td>
                          </tr>
                          {/if}
                          {*-------------------------------------------*} 
                          {if $arr_product_info.products_url}
                          <tr>
                            <td>&nbsp;</td>
                            <td>{$arr_product_info.products_url} </td>
                          </tr>
                          {/if}
                          {*-------------------------------------------*} 
                          {if $SANTANDER_LOANBOX}
                          <tr>
                            <td id="santandertitle_pinfo">{$SANTANDER_LOANBOX_TITLE}:</td>
                          </tr>
                          <tr>
                            <td>&nbsp;</td>
                            <td>
                              {$SANTANDER_LOANBOX}
                            </td>
                          </tr>
                          {/if}
              
              
                          {*-------------------------------------------*} 
                          {if $PRODUCTS_WEIGHT_INFO}
                            {if $arr_product_info.products_weight > 0}
                            <tr>
                              <td>{$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:</td>
                              <td>{$arr_product_info.products_weight}&nbsp;{$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}</td>
                            </tr>             
                            {/if}
                          {/if}

                          
                          {*-------------------------------------------*} 
                  
                        
                          {if count($arr_product_info.products_model)}
                            <tr>
                              <td>Model:</td>
                              <td>{$arr_product_info.products_model}</td>
                            </tr>
                          {/if}
                        
                                         {*-------------------------------------------*}               
                          {if $arr_manufacturer_info.manufacturers_name}
                          <tr>
                            <td>{$manufacturer}:</td>
                            <td class="manufacturer-image">
                            {if $arr_manufacturer_info.manufacturers_image neq ""}
                              {if $arr_manufacturer_info.manufacturers_url neq ""}
                                <a href="{$arr_manufacturer_info.manufacturers_url}">
                                  <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                </a>
                              {else}
                                <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                              {/if}
                            {else}
                              {if $arr_manufacturer_info.manufacturers_url neq ""}
                                <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                              {else}
                                {$arr_manufacturer_info.manufacturers_name}
                              {/if}
                            {/if}
                            </td>
                          </tr>
                          {/if} 
             
                          {if $widget_view && $widget_view eq 1}
                          <tr>               
                            <td colspan="2">                          
                                <div id="widget_views"></div>                             
                            </td>
                          </tr>  
                          {/if}
                        </table>              
                      </form>

                      <div class="product-info-description clearfix">
                

                          {$arr_product_info.products_description}
                          {if $foxrate && $foxrate->GetVariable('counts') != 0}
                                {*include file="../0_source/modules/modul_foxrate_products_info.tpl"*}         
                          {else}
                                            
                          {/if}


                          {if $foxrate_rating}
                                                      
                        
                          {/if}
                              
                      </div>  

                </div>
                 {include file="modules/modul_xsell_products.tpl"}   
                      <br>             
                 {include file="modules/modul_also_purchased_products.tpl"}     
          {else}
              {$TEXT_PRODUCT_NOT_FOUND}
          {/if}  
        {/if}            
    </div>                      
  </div>
</div>
</div>
{include file="footer.tpl"}
