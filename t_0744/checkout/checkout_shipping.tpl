<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 current"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3"><span>{$smarty.const.CHECKOUT_BAR_PAYMENT}</span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>
{$checkout_address_form}
	<div class="col-xs-12">
		<div class="panel panel-primary">
		    <div class="panel-heading">
		        <h3 class="panel-title">{$smarty.const.TITLE_SHIPPING_ADDRESS}
		         	<a href="{$checkout_shipping_address_href}" class="modify_address">
		         		<i class="fa fa-pencil-square-o"></i>
						<!-- {$arr_smartyconstants.IMAGE_BUTTON_CHANGE_ADDRESS} -->
					</a>
				</h3>
		    </div>
		    <div class="panel-body">
		        	{$sendto_address_label}
		    </div>
		</div>
	</div>
	<div class="col-xs-12">
		<div class="panel panel-primary">
		    <div class="panel-heading">
		        <h3 class="panel-title">{$smarty.const.TITLE_SHIPPING_ADDRESS}</h3>
		    </div>
		    <div class="panel-body">
		{if $tep_count_shipping_modules_vs > 0}
		    {if $is_multiple_quotes}
		  	{elseif $free_shipping == false}
				{$smarty.const.TEXT_ENTER_SHIPPING_INFORMATION}
			{/if}
		    {if $free_shipping == true}
		       <!--{$smarty.const.FREE_SHIPPING_TITLE} {$quotes_icon}-->
		       {$shipping_description}
		       {$shipping_hidden}
		    {else}
		      {section name=current loop=$quoteslist}
		        <!--{$quoteslist[current].module} {$quoteslist[current].icon}-->
		        {if $quoteslist[current].is_error}
		          {$quoteslist[current].error}
		        {else}
		        	{section name=current_method loop=$quoteslist[current].methods}
		        		<div class="co_selectionHLD{if $quoteslist[current].methods[current_method].is_row_selected} active{/if}">
		            	{if $quoteslist[current].methods[current_method].is_multiple_methods}
		               		<div class="co_selectionCOLleft">{$quoteslist[current].methods[current_method].shipping_radio}<div class="co_selectionCOLlefttxt"><h6 class="co_selectionHEADLINE">{$quoteslist[current].module}</h6>{$quoteslist[current].methods[current_method].title}</div></div>
		                	<div class="co_selectionCOLright">{$quoteslist[current].methods[current_method].formated_cost_with_tax}</div>
		              	{else}
		                	<div class="co_selectionCOLleft"><h6 class="co_selectionHEADLINE">{$quoteslist[current].module}</h6>{$quoteslist[current].methods[current_method].title}</div>
		                	<div class="co_selectionCOLright">{$quoteslist[current].methods[current_method].shipping_hidden}{$quoteslist[current].methods[current_method].formated_cost_with_tax}</div>
		              	{/if}
		            	</div>
		          	{/section}
		        {/if}
		      {/section}
		    {/if}
		{else}
			{$smarty.const.TEXT_NO_SHIPPING}
		{/if}
		    </div>
		</div>
	</div>
	<div class="col-xs-12">
	    <div class="panel panel-primary">
	        <div class="panel-heading">
	            <h3 class="panel-title">{$smarty.const.TABLE_HEADING_COMMENTS}</h3>
	        </div>
	        <div class="panel-body">
	            {if $tep_count_shipping_modules_vs > 0}						
					<div class="col-xs-6">{$comments_textarea}</div>
				{/if}	
	        </div>
	    </div>
	</div>
    <div class="col-xs-12">
        <button type="button" class="btn btn-default btn-icon pull-left" onclick="window.location='{$arr_shopping_cart.shoppingcart_link}';">
            <span>
              <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</i></span>
            </span>
        </button>
        {if $tep_count_shipping_modules_vs > 0}
            <button type="submit" title="{$arr_smartyconstants.IMAGE_BUTTON_CONTINUE}" class="btn btn-default btn-icon pull-right">
                <span>
                    <span>{$arr_smartyconstants.IMAGE_BUTTON_CONTINUE}</span>
                </span>
            </button>
        {/if}
    </div>
	<div class="clearfix"></div>
</form>