{$account_password_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}

		<tr>
        	<td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
					{if $is_account_password_message}
      				<tr>
        				<td>{$account_password_message}</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
					{/if}
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td class="main"><b>{$smarty.const.MY_PASSWORD_TITLE}</b></td>
                								<td class="inputRequirement" align="right">{$smarty.const.FORM_REQUIRED_INFORMATION}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              								<tr>
                								<td>
                									<table border="0" cellspacing="2" cellpadding="2">
                  										<tr>
										                    <td class="main">{$smarty.const.ENTRY_PASSWORD_CURRENT}</td>
										                    <td class="main">{$password_current_field}&nbsp;{if $entry_password_current_text}<span class="inputRequirement">{$smarty.const.ENTRY_PASSWORD_CURRENT_TEXT}</span>{/if}</td>
                  										</tr>
                  										<tr>
                    										<td colspan="2">{$pixel_trans_100_10}</td>
                  										</tr>
                  										<tr>
                    										<td class="main">{$smarty.const.ENTRY_PASSWORD_NEW}</td>
                    										<td class="main">{$password_new_field}&nbsp;{if $entry_password_new_text}<span class="inputRequirement">{$smarty.const.ENTRY_PASSWORD_NEW_TEXT}</span>{/if}</td>
                  										</tr>
                  										<tr>
                    										<td class="main">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</td>
                    										<td class="main">{$password_confirmation_field}&nbsp;{if $entry_password_confirmation_text}<span class="inputRequirement">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}</span>{/if}</td>
                  										</tr>
                									</table>
                								</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td><a href="{$account_href}">{$button_back_img_button}</a></td>
                								<td align="right">{$button_continue_img_submit}</td>
                								<td width="10">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
        		</table>
        	</td>
      	</tr>
 	</table>

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
</form>
    