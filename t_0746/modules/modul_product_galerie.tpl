{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    <article class="col-xs-12 col-md-9 m_top_20">
        <div class="sort_shop txt-1 clearfix no_bord m_top_20">
            <div class="fleft clearfix">
                {if $manufacturer_select}
                    {$manufacturer_select}
                {/if}
                <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink"/>
                <input type="hidden" value="{$max}" id="max" name="max"/>
                <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template"/>
                <select name="sortierung" id="sortierung" onchange="goHin(this)">
                    {foreach from=$arr_sort_new key=key item=sort}
                        {if $sort.class == "p"}
                            {if $sort_select eq $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {else}
                            {if $sort_select == $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div id="navigation" class="navigation_grid">
                <a class="active btn-tags hasTooltip" href="#" title="" data-original-title="Grid">Grid<i class="fa fa-th"></i></a>
                {if $standard_template == "galerie"}
                    <a class="btn-tags hasTooltip" href="{$templ_link}" title="List" data-original-title="List">List<i class="fa fa-th-list"></i></a>
                {/if}
            </div>
        </div>
        <div class="clearfix sort_shop txt-1">
            <span class="fleft">{$arr_modul_productlisting.count_products_info}</span>

            <div class="v_block-control fright">
                <div class="control-post">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <a href="javascript:void(0)" class="active">{$page.text}</a>
                        {elseif $page.previous}
                            <a class="prev-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        {elseif $page.next}
                            <a class="next-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        {else}
                            <a href="{$page.link}" title="{$page.title}">{$page.text}</a>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="row m_top_20">
            {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
                {if $product.image}
                    {php}
                        $product = $this->get_template_vars('product');
                        $pattern_src = '/src="([^"]*)"/';
                        preg_match($pattern_src, $product['image'], $matches);
                        $src = $matches[1];
                        $this->assign('src', $src);
                    {/php}
                {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                {/if}
                <article class="col-xs-12 col-sm-6 col-md-4 product-block">
                    <div class="block-info maxheight">
                        <div class="box_inner">
                            <div class="inner-block">
                                <a href="{$product.name_link}">
                                    <div style="background: url('{$src}') center no-repeat" class="preview-image-product"></div>
                                </a>

                                <div class="box-text-small">
                                    <h4>
                                        <a href="{$product.name_link}">{$product.products_name|truncate:50:"..."}</a>
                                    </h4>
                                    <span class="price">{$product.newprice}</span>

                                    <div class="btn-small">
                                        <div class="block-btns">
                                            <a href="{$product.buy_now_link}" class="button-cart"> {$smarty.const.IMAGE_BUTTON_IN_CART}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            {/foreach}
        </div>
        <div class="clearfix sort_shop txt-1 m_t_no_space">
            <span class="fleft">{$arr_modul_productlisting.count_products_info}</span>

            <div class="v_block-control fright">
                <div class="control-post">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <a href="javascript:void(0)" class="active">{$page.text}</a>
                        {elseif $page.previous}
                            <a class="prev-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        {elseif $page.next}
                            <a class="next-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        {else}
                            <a href="{$page.link}" title="{$page.title}">{$page.text}</a>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </article>
{/if}