<div class="box box-information">    
	<div class="box-heading">
		<h3>{$arr_informationbox.title}</h3>     			
	</div>
	<ul class="nav nav-tabs nav-stacked">
      {foreach from=$arr_informationbox.items key=key item=link name=element}
        {if $request_uri eq $link.target}
          <li><a href="{$link.target}">{$link.text}</a></li>
        {else}
          <li><a href="{$link.target}">{$link.text}</a></li>
        {/if}          
      {/foreach}
</ul>				
</div>