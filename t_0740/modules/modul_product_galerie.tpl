{assign var=arr1 value=" "|explode:$arr_modul_productlisting.count_products_info}
{assign var=total_items value=$arr1[0]|regex_replace:"/[^0-9]+/":""}
{if $arr_modul_productlisting.products}
    {literal}
        <script type="text/javascript">
            function goHin(das) {
                var slink = document.getElementById('sortlink').value;
                var sort = document.getElementById('sortierung').selectedIndex;
                var wert = das[sort].value;
                var max = document.getElementById('max').value;
                var link = slink + '&sort='+wert+'&max='+max;
                location.href = link;
            }
        </script>
    {/literal}
    <div id="product-grid-container">
        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/nopicture.gif"}
            {/if}
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="shop_item">
                    <div class="entry">
                       <div class="image_view_gallery" style="background-image:url('{$src}')"></div>
                        <div class="magnifier" onclick="location.href='{$product.link}'">
                            <div class="buttons" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" onclick="setLocation('{$product.buy_now_link}')">
                                <a href="{$product.link}" class="st btn btn-default">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                            </div>
                        </div>
                    </div>
                    <div class="shop_desc">
                        <div class="shop_title pull-left">
                            <a href="{$product.link}" title=" Sunlight Collection of 2014">
                                <span>{$product.products_name}</span></a>
                        </div>
                    </div>
                    <div class="price-box">
                        <span class="price pull-right">{$product.newprice|regex_replace:"/[()]/":""}</span>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
{/if}