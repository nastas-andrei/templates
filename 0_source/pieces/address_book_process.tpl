{if !$is_delete}{$addressbook_edit_form}{/if}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}    
	 	<tr>
			<td class="cont_abst">
	        	<table border="0" width="100%" cellspacing="0" cellpadding="0">
					{if $is_addressbook_message}
	      			<tr>
	        			<td>{$addressbook_message}</td>
	      			</tr>
	      			<tr>
	        			<td>{$pixel_trans_100_10}</td>
	      			</tr>
	      			{/if}
	    			{if $is_delete}
	      			<tr>
	        			<td class="main"><b>{$smarty.const.DELETE_ADDRESS_TITLE}</b></td>
	      			</tr>
	      			<tr>
	        			<td>
	        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
	          					<tr>
	            					<td>
	            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
	              							<tr>	              							
	                							<td>{$pixel_trans_10_1}</td>
	                							<td class="main" width="50%" valign="top">{$smarty.const.DELETE_ADDRESS_DESCRIPTION}</td>
	                							<td align="right" width="50%" valign="top">
	                								<table border="0" cellspacing="0" cellpadding="2">
				                  						<tr>
				                    						<td class="main" align="center" valign="top">
				                    							<b>{$smarty.const.SELECTED_ADDRESS}</b>
				                    							<br>
				                    							{$arrow_south_east_img}
				                    						</td>
				                    						<td>{$pixel_trans_10_1}</td>
				                    						<td class="main" valign="top">{$delete_address_label}</td>
				                    						<td>{$pixel_trans_10_1}</td>
				                  						</tr>
		                							</table>
		                						</td>
		              						</tr>
		            					</table>
		            				</td>
		          				</tr>
		        			</table>
		        		</td>
		      		</tr>
		      		<tr>		      		
		        		<td>{$pixel_trans_100_10}</td>
		      		</tr>
		      		<tr>
		        		<td>
		        			<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
		          				<tr>
		            				<td>
		            					<table border="0" width="100%" cellspacing="0" cellpadding="2">
		    	          					<tr>
								                <td width="10">{$pixel_trans_10_1}</td>
								                <td><a href="{$addressbook_href}">{$button_back_img_button}</a></td>
								                <td align="right"><a href="{$addressbook_delete_href}">{$button_delete_img_button}</a></td>
								                <td width="10">{$pixel_trans_10_1}</td>
		              						</tr>
		            					</table>
		            				</td>
		          				</tr>
		        			</table>
		        		</td>
		      		</tr>
					{else}
		      		<tr>
		        		<td>{$ADDRESS_BOOK_DETAILS_HTML}</td>
		      		</tr>
		      		<tr>
		        		<td>{$pixel_trans_100_10}</td>
		      		</tr>
					{if $is_edit}
		      		<tr>
		        		<td>
		        			<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
		          				<tr>
		            				<td>
		            					<table border="0" width="100%" cellspacing="0" cellpadding="2">
		              						<tr>
		                						<td width="10">{$pixel_trans_10_1}</td>
		                						<td><a href="{$addressbook_href}">{$button_back_img_button}</a></td>
		                						<td align="right">{$edit_hidden_fields}{$button_update_img_submit}</td>
		                						<td width="10">{$pixel_trans_10_1}</td>
		              						</tr>
		            					</table>		            					
		            				</td>
		          				</tr>
		        			</table>
		        		</td>
		      		</tr>
					{else}		
		      		<tr>
		        		<td>
		        			<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
		          				<tr>
		            				<td>
		            					<table border="0" width="100%" cellspacing="0" cellpadding="2">
		              						<tr>
								                <td width="10">{$pixel_trans_10_1}</td>
								                <td><a href="{$back_link}">{$button_back_img_button}</a></td>
								                <td align="right">{$action_hidden_field}{$button_continue_img_submit}</td>
								                <td width="10">{$pixel_trans_10_1}</td>
		              						</tr>
		            					</table>
		            				</td>
		          				</tr>
		        			</table>
		        		</td>
		      		</tr>
		      		{/if}
		      		{/if}
		        </table>
			</td>
		</tr>
	</table>

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
	
{if !$is_delete}</form>{/if}