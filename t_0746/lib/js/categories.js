jQuery(document).ready(function () {
	jQuery.post('categories/tree', function (data) {
  		jQuery('#categories-box').empty().append(data);
        var mq = window.matchMedia('(min-width: 768px)');
        if (mq.matches) {
            $('ul.navbar-nav > li').addClass('hovernav');
        } else {
            $('ul.navbar-nav > li').removeClass('hovernav');
        }
        if (matchMedia) {
            mq = window.matchMedia('(min-width: 768px)');
            mq.addListener(WidthChange);
            WidthChange(mq);
        }
        function WidthChange(mq) {
            if (mq.matches) {
                $('ul.navbar-nav > li').addClass('hovernav');
            } else {
                $('ul.navbar-nav > li').removeClass('hovernav');
            }
        }
    });
});
