{$cart_quantity_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
       		<table class="pageContentTable" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr>
	        	<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>

	{/if}
 					{if $any_out_of_stock == 1}
			    	{if $smarty.const.STOCK_ALLOW_CHECKOUT == 'true'}
      				{* <tr>
        				<td class="stockWarning" align="center"><br>{$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}</td>
      				</tr> *}
          <tr><td>
          <div id="stockWarning" style="visibility:hidden; width:600px; position:absolute; z-index:999; border:3px solid #C00; background-color:#ffe4e4; top:150px; padding:30px;">
          		<div id="stockWarningClose" style="font-size:20px; position:absolute; top:-3px; right:5px;"><a style="text-decoration:none; color:#C00; font-weight:bold; outline:none;" href="javascript:;" onmouseover="this.style.color = '#fff'" onmouseout="this.style.color = '#C00'" onClick="document.getElementById('stockWarning').style.visibility='hidden';return false;">x</a></div>
												<div class="stockMessage" style="color:#C00;">{$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}</div>
          </div>
          </td></tr>
          <noscript>
          <tr>
        				<td class="stockWarning" style="border:3px solid #C00; background-color:#ffe4e4; padding:10px; color:#C00;" align="center"><br>{$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}</td>
      				</tr>
          </noscript>
			    	{else}
      				{* <tr>
        				<td class="stockWarning" align="center"><br>{$smarty.const.OUT_OF_STOCK_CANT_CHECKOUT}</td>
      				</tr> *}
          <tr><td>
          <div id="stockWarning" style="visibility:hidden; width:600px; position:absolute; z-index:999; border:3px solid #C00; background-color:#ffe4e4; top:150px; padding:30px;">
          		<div id="stockWarningClose" style="font-size:20px; position:absolute; top:-3px; right:5px;"><a style="text-decoration:none; color:#C00; font-weight:bold; outline:none;" href="javascript:;" onmouseover="this.style.color = '#fff'" onmouseout="this.style.color = '#C00'" onClick="document.getElementById('stockWarning').style.visibility='hidden';return false;">x</a></div>
												<div class="stockMessage" style="color:#C00;">{$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}</div>
          </div>
          </td></tr>
          <noscript>
          <tr>
        				<td class="stockWarning" style="border:3px solid #C00; background-color:#ffe4e4; padding:10px; color:#C00;" align="center"><br>{$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}</td>
      				</tr>
          </noscript>
					   {/if}
        {literal}
        <script type="text/javascript">
        window.onload = 	function () {
									document.getElementById('stockWarning').style.visibility='visible';
								}
								document.getElementById('stockWarning').style.visibility='visible';
								</script>
        <style type="text/css">
								   .markProductOutOfStock { color:#C00; }
								</style>
        {/literal}
					{/if}
		<tr>
	        <td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="shopping_cart_cnt_area">
        			{if $count_contents > 0}        			
					<tr>
        				<td>
        					{$hidden_fields}
             <div class="shoppingCartHeadCnt">{$arr_smartyconstants.HEADING_TITLE}</div>
    						<div class="shoppingCartCONTENTHOLDERinner">{$table_box_content}</div>
        				</td>
      				</tr>
      				<tr>
        				<td align="right" class="shoppingCartSUBTOTAL">
        						{$smarty.const.SUB_TITLE_SUB_TOTAL} 
        		  				<span class="shoppingCart_price">{$formated_products_price}</span>
        				</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2" class="shopping_cart_buttons">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td>{$button_update_cart_img_submit}</td>
                								{*{if $is_navigation_back} *}
                								<td>
                									<a href="{$seo_link_back_href}">{$button_continue_shopping_img_button}</a>
                								</td>
												{*{/if} *}
                								<td align="right"><a href="{$checkout_shipping_href}">{$button_checkout_img_button}</a></td>
                								<td width="10">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        					{if $central_user_id == '16210'}
            				<table border="0" width="100%" cellspacing="0" cellpadding="2">
            					<tr>
            						<td>
            							{include file="modules/modul_xsell_products.tpl"}
						           </td>
            					</tr>
            				</table>
            				{/if}
        				</td>
      				</tr>
					{else}
      				<tr>
        				<td align="center" class="shopping_cart_area">{$smarty.const.TEXT_CART_EMPTY}</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" >
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td align="right"><a href="{$seo_index_link}">{$button_continue_img_button}</a></td>
                								<td width="10">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
					{/if}
        		</table>
        	</td>
      	</tr>
    </table>
    
	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}
	
    </form>