{if $arr_searchbox}
  {if $js_products_search}
      {$js_products_search}
  {/if}
  <div id="search_dresscode">
    {$arr_searchbox.form}             
      <div class="form-search">
        <input type="text" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}" value=""/>
        <input name="" type="submit" value="search">
        <div id="search_autocomplete" class="search-autocomplete"></div>  
      </div>
      {$arr_searchbox.hidden_sessionid}
    </form>
  </div>
{/if}
