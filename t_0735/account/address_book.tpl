<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
            	<a href="{$account_href}" class="btn btn-default">&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>
				{if $is_addressbook_message}
					<p class="text-center">{$addressbook_message}</p>
				{/if}
	            <div class="panel panel-success" style="margin-top:20px">
	                <div class="panel-heading">
	                    <h3 class="panel-title">{$smarty.const.PRIMARY_ADDRESS_TITLE}</h3>
	                </div>
	                <div class="panel-body">
	                    {$customer_address}
	                </div>
	            </div>
	            <div class="panel panel-primary">
	                <div class="panel-heading">
	                    <h3 class="panel-title">{$smarty.const.ADDRESS_BOOK_TITLE}</h3>
	                </div>
	                <div class="panel-body">
	                    {section name=current loop=$addresseslist}
						<div class="span6">
						<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onClick="document.location.href='{$addresseslist[current].address_book_edit_row_href}'">
						{$addresseslist[current].firstname} 
						{$addresseslist[current].lastname}
						{if $addresseslist[current].is_customer_default_address}
							<i>{$smarty.const.PRIMARY_ADDRESS}</i>
						{/if}

						
						<address>
							{$addresseslist[current].formated_address}
						</address>
						<p class="block">
							<a href="{$addresseslist[current].address_book_edit_row_href}" class="btn btn-small btn-info"><i class="fa fa-pencil-square-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_EDIT}</a> 
							<a href="{$addresseslist[current].address_book_delete_row_href}" class="btn btn-small btn-danger"><i class="fa fa-trash-o"></i> {$smarty.const.SMALL_IMAGE_BUTTON_DELETE}</a>	
						</p>
						</div>
					{/section}
	                </div>
	            </div>
	            <p class="text-center">
					{if $is_allow_add}
					<a href="{$address_book_add_href}" class="btn btn-primary"><i class="icon-plus"></i> {$smarty.const.IMAGE_BUTTON_ADD_ADDRESS}<a/>
					{/if}
				</p>
        <!--         <div class="content-box">
           <div class="account-container">
<a href="{$account_href}" class="btn btn-default">&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>
{if $is_addressbook_message}
	<p class="text-center">{$addressbook_message}</p>
{/if}


	<h5>{$smarty.const.PRIMARY_ADDRESS_TITLE}</h5>
	<address>
	{$customer_address}	
	</address>	

<hr>
<h5 class="text-center">{$smarty.const.ADDRESS_BOOK_TITLE}</h5>

<div class="clearfix">
{section name=current loop=$addresseslist}
	<div class="span6">
	<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onClick="document.location.href='{$addresseslist[current].address_book_edit_row_href}'">
	{$addresseslist[current].firstname} 
	{$addresseslist[current].lastname}
	{if $addresseslist[current].is_customer_default_address}
		<i>{$smarty.const.PRIMARY_ADDRESS}</i>
	{/if}

	
	<address>
		{$addresseslist[current].formated_address}
	</address>
	<p class="block">
		<a href="{$addresseslist[current].address_book_edit_row_href}" class="btn btn-small btn-info"><i class="icon-edit"></i> {$smarty.const.SMALL_IMAGE_BUTTON_EDIT}</a> 
		<a href="{$addresseslist[current].address_book_delete_row_href}" class="btn btn-small btn-danger"><i class="icon-trash"></i> {$smarty.const.SMALL_IMAGE_BUTTON_DELETE}</a>	
	</p>
	</div>
{/section}
</div>
<hr>
<p class="text-center">
{if $is_allow_add}
<a href="{$address_book_add_href}" class="btn btn-primary"><i class="icon-plus"></i> {$smarty.const.IMAGE_BUTTON_ADD_ADDRESS}<a/>
{/if}
</p>
</div> -->

            </div>
        </div>
    </div>
</section>
