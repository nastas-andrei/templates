{if $arr_modul_previewproducts.products}
    <!-- BEGIN FEATURED -->
    <section class="featured">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box-title">
                        <h2>{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div id="featured-slider" class="owl-carousel">
                            {foreach from=$arr_modul_previewproducts.products key=key item=product name=productpreviewitems}
                                <div class="item">
                                    <!-- PRODUCT -->
                                    <div class="col-xs-12">
                                        <div class="product-block">
                                            {if $product.image}
                                                {php}
                                                    $product = $this->get_template_vars('product');
                                                    $pattern_src = '/src="([^"]*)"/';
                                                    preg_match($pattern_src, $product['image'], $matches);
                                                    $src = $matches[1];
                                                    $this->assign('src', $src);
                                                {/php}
                                            {else}
                                                {assign var=src value=$imagepath|cat:"/empty-album.png"}
                                            {/if}
                                            <div class="product-image" style="height: 200px;">
                                                <a href="{$product.link}">
                                                    <figure class="product-display">
                                                        <!--                                 <span class="product-label-special label">
                                                                                            <i>Sale</i>
                                                                                            <span class="special">50%</span>
                                                                                        </span> -->
                                                        <div class="product-mainpic width-image-product" style="background-image:url('{$src}')"></div>
                                                        {*<img data-src="{$src}" alt="" class="lazyOwl product-mainpic" src="{$src}" /> *}
                                                        {*<img src="{$src}" alt="" class="product-secondpic"/> *}
                                                        <div class="product-secondpic width-image-product" style="background-image:url('{$src}')"></div>
                                                    </figure>
                                                </a>
                                            </div>
                                            <div class="product-meta">
                                                <div class="product-action">
                                                    <a href="{$product.buy_now_link}" class="addcart">
                                                        <i class="icon-basket"></i>
                                                        {$smarty.const.IMAGE_BUTTON_IN_CART}
                                                    </a>
                                                    <!--                <a href="javascript:;" class="wishlist">
                                                                       <i class="fa fa-heart"></i>
                                                                   </a>
                                                                   <a href="javascript:;" class="compare">
                                                                       <i class="fa fa-retweet"></i>
                                                                   </a> -->
                                                </div>
                                            </div>
                                            <div class="product-info">
                                                <a href="{$product.link}">
                                                    <h5 class="product-name">{$product.name|truncate:150:"...":true}</h5>
                                                </a>

                                                <div class="product-price">
                                                    <span>{$product.preisneu|regex_replace:"/[()]/":""}</span>
                                                </div>
                                            </div>
                                            <div class="product-rating">
                                                <!--                               <i class="fa fa-star"></i>
                                                                              <i class="fa fa-star"></i>
                                                                              <i class="fa fa-star"></i> -->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            {/foreach}
                            <!--END Owl -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/.container -->
    </section>
{/if}
 