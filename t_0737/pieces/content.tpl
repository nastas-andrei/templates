{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}

    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
    {*<!-- Slider -->*}
    {*<div id="slider_top">*}
    {*<ul class="rslides" id="carousel1">*}
    {*{$user_html}*}
    {*</ul>*}
    {*</div>*}
    {*<!-- End Slider -->*}
    {*{/if}*}
    {if $slider_images != ""}
        <!-- Slider -->
        <script type="text/javascript" src="{$templatepath}lib/slider.js"></script>
        <div id="slider_top">
            <ul class="rslides" id="carousel1">
                {section name=image loop=$slider_images step=2}
                    <li>
                        <div class="overlap_widget_wrapper">
                            <div class="left_image">
                                <div class="placeholder">
                                    <img src="{$slider_images[image.index].image}" alt="{$slider.title}">
                                </div>
                            </div>
                            <div class="right_image">
                                <div class="placeholder">
                                    {assign var="next_image" value=$smarty.section.image.index+1}
                                    <img src="{$slider_images[$next_image].image}" alt="{$slider.title}">
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </li>
                {/section}
            </ul>
        </div>
        <!-- End Slider -->
    {/if}
    <!-- Featured -->
    <div class="container" style="margin-top:20px">
        <div class="col1-layout">
            <div class="main">
                <div class="col-main sixteen columns">
                    <div>
                        <div class="clearfix tabs">
                            {if $arr_modul_previewproducts}
                                <div>
                                    <a id="bestsellers_activate">{$smarty.const.TABLE_HEADING_FEATURED_PRODUCTS}</a><span>/</span>
                                </div>
                            {/if}
                            {if $arr_bestsellersbox}
                                <div><a id="specials_activate">{$arr_bestsellersbox.title}</a><span>/</span></div>
                            {/if}
                            {if $arr_whatsnewbox}
                                <div><a id="newproducts_activate">{$arr_whatsnewbox.title}</a></div>
                            {/if}
                        </div>
                    </div>
                    <div>
                        <div class="list_carousel responsive">
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="sixteen columns alpha all_slider_wrapper">
                        <div id="bestsellers_slider">
                            <div id="carousel_bestsellers" class="es-carousel-wrapper">
                                <div class="es-carousel container">
                                    <ul>
                                        {$FILENAME_FEATURED_HTML}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="specials_slider">
                            <div id="carousel_specials" class="es-carousel-wrapper">
                                <div class="es-carousel container">
                                    <ul>
                                        {include file="boxes/box_best_sellers.tpl"}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div id="newproducts_slider">
                            <div id="carousel_newproducts" class="es-carousel-wrapper">
                                <div class="es-carousel container">
                                    <ul>
                                        {include file="boxes/box_whats_new.tpl"}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Featured end-->
    {$UPCOMING_PRODUCTS_HTML}

{/if}

<div class="container">
    {if $is_products == 1}
        {$smarty.const.HEADING_TITLE}
        {if $filterlistisnotempty}
            {$filter_form}
            {$smarty.const.TEXT_SHOW}
        {/if}

        {*{if !empty($categories_html)} *}
        {*{$categories_html}*}
        {*{/if}*}

        {if !empty($manufacturers_html_header)}
            {$manufacturers_html_header}
        {/if}

        {if !empty($PRODUCT_LISTING_HTML)}
            {$PRODUCT_LISTING_HTML}
        {/if}

        {if !empty($manufacturers_id)}
            {$manufacturers_html_footer}
        {/if}

        {if !empty($categories_html_footer)}
            {$categories_html_footer}
        {/if}

    {else}
        {if $is_default}

        {else}
            <p class="block">{$tep_image_category}</p>
            <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
        {/if}
    {/if}
</div> 

