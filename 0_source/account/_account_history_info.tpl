{$smarty.const.BEFORE_CONTENT}

{if !$smarty.const.PH_INSIDE}	
	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
     	<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}

		<tr>
        	<td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td class="main" colspan="2"><b>123{$order_id} <small>({order_info_status})</small></b></td>
          						</tr>
          						<tr>
            						<td class="smallText">{$smarty.const.HEADING_ORDER_DATE} {$order_info_date_purchased}</td>
            						<td class="smallText" align="right">{$smarty.const.HEADING_ORDER_TOTAL} {$order_info_total}</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr> 
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
          							{if $is_order_delivery}
            						<td width="30%" valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td class="main"><b>{$smarty.const.HEADING_DELIVERY_ADDRESS}</b></td>
              								</tr>
              								<tr>
                								<td class="main">{$delivery_format_id}</td>
              								</tr>
											{if is_shipping_method}
              								<tr>
                								<td class="main"><b>{$smarty.const.HEADING_SHIPPING_METHOD}</b></td>
              								</tr>
              								<tr>
                								<td class="main">{$shipping_method_info}</td>
              								</tr>
											{/if}
            							</table>
            						</td>
									{/if}
            						<td width="{if $is_order_delivery}70%{else}100%{/if}" valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="0">
              								<tr>
                								<td>
                									<table border="0" width="100%" cellspacing="0" cellpadding="2">
														{if $is_multi_tax_groups}
                  										<tr>
                    										<td class="main" colspan="2"><b>{$smarty.const.HEADING_PRODUCTS}</b></td>
                    										<td class="smallText" align="right"><b>{$smarty.const.HEADING_TAX}</b></td>
                    										<td class="smallText" align="right"><b>{$smarty.const.HEADING_TOTAL}</b></td>
                  										</tr>
														{else}
                  										<tr>
                    										<td class="main" colspan="3"><b>{$smarty.const.HEADING_PRODUCTS}</b></td>
                  										</tr>                  										
														{/if}
														{section name=current loop=$orderhistorylist}
														<tr>
         													<td class="main" align="right" valign="top" width="30">{$orderhistorylist[current].qty}&nbsp;x</td>
         													<td class="main" valign="top">
         														{$orderhistorylist[current].name}
         														{section name=attr_current loop=$historyattributeslist}
         														<br>
         														<nobr>
         															<small>&nbsp;<i> - {$historyattributeslist[attr_current].option}: {$historyattributeslist[attr_current].value}</i></small>
         														</nobr>
         														{/section}
														    </td>
														    {if $is_multi_tax_groups}
														    	<td class="main" valign="top" align="right">{$orderhistorylist[current].tax}%</td>
														    {/if}
														    <td class="main" align="right" valign="top" nowrap>
														    	{$orderhistorylist[current].final_row_price}
														    </td>
														</tr>
														{/section}
                									</table>
                								</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>      				
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td class="main"><b>{$smarty.const.HEADING_BILLING_INFORMATION}</b></td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td width="30%" valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td class="main"><b>{$smarty.const.HEADING_BILLING_ADDRESS}</b></td>
              								</tr>
              								<tr>
                								<td class="main">{$billing_format_id}</td>
              								</tr>
              								<tr>
                								<td class="main"><b>{$smarty.const.HEADING_PAYMENT_METHOD}</b></td>
              								</tr>
              								<tr>
                								<td class="main">{$payment_method}</td>
              								</tr>
            							</table>
            						</td>
            						<td width="70%" valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
            								{section name=current loop=$ordertotalslist}
											<tr>
												<td class="main" align="right" width="100%">{$ordertotalslist[current].title}</td>
												<td class="main" align="right" nowrap>{$ordertotalslist[current].text}</td>
											</tr>
											{/section}
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td class="main"><b>{$smarty.const.HEADING_ORDER_HISTORY}</b></td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td valign="top">
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
            								{section name=current loop=$orderstatuseslist}
										    <tr>										    
										    	<td class="main" valign="top" width="70">{$orderstatuseslist[current].date_added}</td>
										    	<td class="main" valign="top" width="70">{$orderstatuseslist[current].orders_status_name}</td>
										        <td class="main" valign="top">{$orderstatuseslist[current].comments}</td>
										    </tr>
            								{/section}
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
      				{$DOWNLOADS_HTML}      				
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td><a href="{$account_history_href}">{$button_back_img_button}</a></td>
                								<td width="10">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
        		</table>
        	</td>
      	</tr>
	</table>

{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}