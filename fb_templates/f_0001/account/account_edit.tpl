<div class="account-container">
{$account_edit_form}
<div class="form-horizontal">
<legend class="text-center">
	{$smarty.const.MY_ACCOUNT_TITLE}
</legend>

<a href="{$account_href}"><i class="icon-caret-left"></i> {$smarty.const.IMAGE_BUTTON_BACK}</a>	
<fieldset>

{if $account_message_exists} 

   <div class="alert alert-warning text-center">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
     {$account_message}
  </div>

{/if}



			<p class="text-center">{$smarty.const.FORM_REQUIRED_INFORMATION}</p>
			
			
	{if $smarty.const.ACCOUNT_GENDER == 'true'}
	
			<div class="control-group">
				<label class="control-label">
					{$smarty.const.ENTRY_GENDER}
				</label>
				<div class="controls">
					<label class="radio inline">
						{$gender_radio_m}
						{$smarty.const.MALE}
					</label>					
					<label class="radio inline">
						{$gender_radio_f}
						{$smarty.const.FEMALE}            		
					</label>	
					<span class="help-inline">
						{if $entry_gender_text}
            				{$smarty.const.ENTRY_GENDER_TEXT}
            			{/if}
					</span>            		
				</div>
			</div>
	{/if}
			

          	<div class="control-group">          		
          		<label class="control-label">
          			{$smarty.const.ENTRY_FIRST_NAME}	
          		</label>
          		<div class="controls">
          			{$firstname_input}
          			<span class="help-inline">
          			{if $entry_firstname_text}
            			{$smarty.const.ENTRY_FIRST_NAME_TEXT}
            		{/if}
          			{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
            			{$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH}
            		{/if}
          		</span>
          		</div>
          		
          	</div>
            	
          	<div class="control-group">          		
		  		<label class="control-label">
					{$smarty.const.ENTRY_LAST_NAME}
				</label>	
				<div class="controls">
				{$lastname_input}	
				<span class="help-inline">
					{if $entry_lastname_text}
						{$smarty.const.ENTRY_LAST_NAME_TEXT}
					{/if}

					{if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}
						{$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})
					{/if}	
				</span>
				</div>	
			</div>	            
    	 	

			{if $smarty.const.ACCOUNT_DOB == 'true'}
			<div class="control-group">
				<label class="control-label">{$smarty.const.ENTRY_DATE_OF_BIRTH}</label>
				<div class="controls">{$dob_input}
				<span class="help-inline">
					{if $entry_dob_text}
						{$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}
					{/if}
				</span>
				</div>
			</div>		
			{/if}

			<div class="control-group">
				<label for="" class="control-label">{$smarty.const.ENTRY_EMAIL_ADDRESS}</label>
				<div class="controls">{$email_address_input}
				<span class="help-inline">
					{if $entry_email_address_text}
						{$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}
					{/if}
				</span>
				</div>
				
			</div>	
				
			<div class="control-group">
				<label for="" class="control-label">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</label>
				<div class="controls">{$telephone_input}
				<span class="help-inline">
					{if $entry_telephone_text}
						{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
					{/if}	
				</span>
			</div>
				
			</div>	
				
			<div class="control-group">
				<label for="" class="control-label">{$smarty.const.ENTRY_FAX_NUMBER}</label>
				<div class="controls">{$fax_input}
				<span class="help-inline">
					{if $entry_fax_text}
						{$smarty.const.ENTRY_FAX_NUMBER_TEXT}
					{/if}
				</span>
				</div>				
			</div>
				
			
			{if $is_active_b2b}
				<p class="text-center"><strong>{$smarty.const.MY_ACCOUNT_TAX_ID}</strong></p>
				{if $smarty.const.ACCOUNT_CF == 'true'}
				<div class="control-group">
					<label for="" class="control-label">{$smarty.const.ENTRY_CF}</label>
					<div class="controls">{$entry_cf}</div>
				</div>
				<div class="control-group">
					<label for="" class="control-label">{$smarty.const.ENTRY_PIVA}</label>
					<div class="controls">{$entry_piva}</div>
				</div>	
			{/if}

		{/if}
	<div class="text-center">		
	    <input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">	
	</div>
    
 </fieldset>
 </div>
</form>
</div>