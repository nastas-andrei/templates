{if $arr_modul_productlisting.products}  

<section class="content-wrapper">
    <div class="container">
        <div class="row" id="scroll_wrapper">
           {foreach from=$arr_modul_productlisting.products key=key item=product name=modpp}
            <div class="col-xs-3" style="height:310px;">
              <div class="product-block">
                <div class="product-image">
                {if $product.image}
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern = '/src="([^"]*)"/';}
                   {preg_match($pattern, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {$this->assign('src', $src);}
                  {/php}
                {else}
                  {assign var=src value=$imagepath|cat:"/nopicture.gif"}
                {/if}
                  <a href="{$product.link}">
                    <figure class="product-display">
                      <div class="product-mainpic" style="width:260px;height:200px;background-image:url('{$src}');background-position: center;;background-size: cover;"></div>
                      <div class="product-secondpic" style="width:260px;height:200px;background-image:url('{$src}');background-position: center;;background-size: cover;"></div>
                    </figure>
                  </a>
                </div>
                <div class="product-meta">
                    <div class="product-action">
                        <a href="{$product.buy_now_link}" class="addcart">
                            <i class="icon-basket"></i>
                            {$smarty.const.IMAGE_BUTTON_IN_CART}
                        </a>
                        <!-- <a href="javascript:;" class="wishlist">
                            <i class="fa fa-heart"></i>
                        </a>
                        <a href="javascript:;" class="compare">
                            <i class="fa fa-retweet"></i>
                        </a> -->
                    </div>
                </div>
                <div class="product-info">
                    <a href="{$product.link}">
                        <h5 class="product-name">{$product.products_name}</h5>
                    </a>
                                            
                  <div class="product-price">
                    {if $product.oldprice  > 0}
                    <span class="oldprice">{$product.oldprice}</span>
                    {/if} 
                    <span>{$product.newprice|regex_replace:"/[()]/":""}</span>
                    <span class="newprice">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                  </div>
                </div>
                <div class="product-rating">
<!--                     <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i> -->
                </div>
            </div>
        </div>
        {/foreach}
      </div>
    </div>
    {/if}
  </div>
    <!-- /.container -->
</section>
