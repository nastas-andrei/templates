{assign var=arr1 value=" "|explode:$arr_modul_productlisting.count_products_info}
{assign var=total_items value=$arr1[0]|regex_replace:"/[^0-9]+/":""}
{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    <div id="product-grid-container">
        {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/nopicture.gif"}
            {/if}
            <div class="col-md-12">
                <div class="product-item list-item box">
                    <div class="post-image col-md-4 col-sm-4 col-xs-6">
                        <a title="{$product.name}" href="{$product.link}">
                            <div class="image_view_list" style="background-image:url('{$src}')"></div>
                        </a>
                    </div>
                    <div class="product-shop col-md-8 col-sm-8 col-xs-6">
                        <h4 class="product-name"><a href="{$product.link}">{$product.products_name}</a></h4>

                        <div class="price-box">
                            <span class="price">{$product.newprice|regex_replace:"/[()]/":""}</span>
                        </div>
                        <div class="desc std"></div>
                        <a href="{$product.link}" type="button" class="btn btn-primary btn-icon">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
{/if}
