<div class="panel panel-default">
{foreach from=$categories key=key item=category}
        <div class="panel-heading">
            <h4 class="panel-title">
                {if $category.children|@count > 0}
                <a data-toggle="collapse" data-parent="#categories" href="#{$key}">
                    <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                {else} <a href="{$category.link}">{/if}
                    {$category.name}
                </a>
            </h4>
        </div>
        {if $category.children|@count > 0}
            <div id="{$key}" class="panel-collapse collapse">
                <div class="panel-body">
                    <ul>
                        {foreach from=$category.children key=key item=categoryTwo}
                            <li>
                                <a {if $categoryTwo.children|@count > 0}data-toggle="collapse" href="#{$key}"}{else}href="{$categoryTwo.link}"{/if} class="level2">
                                    {$categoryTwo.name}
                                </a>
                                {if $categoryTwo.children|@count > 0}
                                    <ul id="{$key}" class="panel-collapse collapse">
                                        {foreach from=$categoryTwo.children key=key item=categoryTree}
                                            <li><a href="{$categoryTree.link}" class="level3">{$categoryTree.name}</a></li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        {/if}
    {/foreach}
</div>

{*<div id="sns_mainnav" class="span9">*}
    {*<div id="sns_megamenu_menu537c4f6ab159d" class="container sns-megamenu-wrap horizontal-menu using-effect">*}
        {*<ul class="mainnav mega-nav">*}
            {*{foreach from=$categories key=key item=category name=catlist}*}
                {*<li class="level0 parent">*}
                    {*<a class="" {if $category.children|@count > 0} data-toggle="dropdown"*}
                        {*href="#"{else}href="{$category.link}"{/if} id="{$category.id}">*}
                        {*<span class="menu-title">{$category.name}</span>*}
                    {*</a>*}
                    {*{if $category.children|@count > 0}*}
                        {*<div class="megamenu-col megamenu-8col mega-content-wrap have-spetitle level0"*}
                             {*style="margin-right:0px">*}
                            {*{foreach from=$category.children key=key item=categorySub name=catlist_sub1}*}
                                {*<div class="megamenu-col megamenu-3col have-spetitle level1 ">*}
                                    {*<div class="mega-title">*}
                                        {*<a class="" {if $categorySub.children|@count > 0}data-toggle="dropdown" href="#"*}
                                           {*{else}href="{$categorySub.link}"{/if} >*}
                                            {*<span>{$categorySub.name|truncate:20:"...":true}</span>*}
                                        {*</a>*}
                                    {*</div>*}
                                    {*{if $categorySub.children|@count > 0}*}
                                        {*{foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}*}
                                            {*<div class="megamenu-col megamenu-12col have-spetitle level2 ">*}
                                                {*<div class="megamenu-col megamenu-12col level3 ">*}
                                                    {*<div class="mega-title">*}
                                                        {*<a class="" href="{$categorySub2.link}">*}
                                                            {*<span>{$categorySub2.name}</span>*}
                                                        {*</a>*}
                                                    {*</div>*}
                                                {*</div>*}
                                            {*</div>*}
                                            {*<div class="clearfix"></div>*}
                                        {*{/foreach}*}
                                    {*{/if}*}
                                {*</div>*}
                            {*{/foreach}*}
                        {*</div>*}
                    {*{/if}*}
                {*</li>*}
            {*{/foreach}*}
        {*</ul>*}
    {*</div>*}
    {*<div id="sns_resmenu" class="sns-resmenu menu-sidebar">*}
        {*<button class="btn btn-navbar sns-resmenu-sidebar" type="button">*}
            {*<i class="fa fa-bars fa-lg"></i>*}
        {*</button>*}
        {*<ul class="nav resmenu" style="display:none">*}
            {*{foreach from=$categories key=key item=category name=catlist_min_sub}*}
                {*<li class="level0 nav-1 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)">*}
                    {*<a href="{$category.link}">*}
                        {*<span>{$category.name}</span>*}
                    {*</a>*}
                    {*<ul class="level0">*}
                        {*{foreach from=$category.children key=key item=category1 name=catlist_min_sub1}*}
                            {*<li class="level1 nav-1-1 first parent" onmouseover="toggleMenu(this,1)"*}
                                {*onmouseout="toggleMenu(this,0)">*}
                                {*<a href="{$category1.link}">*}
                                    {*<span>{$category1.name}</span>*}
                                {*</a>*}
                                {*<ul class="level1">*}
                                    {*{foreach from=$category1.children key=key item=category2 name=catlist_min_sub2}*}
                                        {*<li class="level2 nav-1-1-1 first">*}
                                            {*<a href="{$category2.link}">*}
                                                {*<span>{$category2.name}</span>*}
                                            {*</a>*}
                                        {*</li>*}
                                    {*{/foreach}*}
                                {*</ul>*}
                            {*</li>*}
                        {*{/foreach}*}
                    {*</ul>*}
                {*</li>*}
            {*{/foreach}*}
        {*</ul>*}
    {*</div>*}
{*</div>*}
