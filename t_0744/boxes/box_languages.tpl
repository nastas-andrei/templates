{if $arr_languagebox}
    <div class="btn-group">
        <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
            <i class="fa fa-globe"></i>
        </button>
        <ul class="dropdown-menu" role="menu">
            {foreach from=$arr_languagebox.items key=key item=language}
                {if ($languageview neq $language.directory)}
                    <li>
                        <a href="{$language.link}"> {$language.name}</a>
                    </li>
                {/if}
            {/foreach}
        </ul>
    </div>
{/if}