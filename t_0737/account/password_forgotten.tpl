<div class="account-container">
	{$password_forgotten_form}
		<div class="form-horizontal">
			<div class="pull-left shopping"> 
	          	<a class="button_add" href="{$LOGIN_URL}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>
	      	</div> 
		  	<div style="clear:both">{$smarty.const.TEXT_MAIN}</div>
			<div class="control-group">
				<label for="" class="control-label">
					{$smarty.const.ENTRY_EMAIL_ADDRESS}			
				</label>
				<div class="controls">
					{$email_address_input}	
		  			<button type="submit" class="btn button_add" >{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
				</div>
			</div>
		</div>
	</form>    
</div>
{if $password_forgotten_stack_size}
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		 	{$password_forgotten_message}
	</div>
  {/if}  