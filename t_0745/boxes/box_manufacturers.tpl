<div class="panel panel-default">
{if $arr_manufacturers_box}
    <!-- start manufacturersbox //-->
    <table border="0" width="100%"  cellspacing="0" cellpadding="0">
        <tr>
            <td align="left" valign="top" class="box_set9b_in_mi" width="100%">
                <table border="0" width="100%"  cellspacing="0" cellpadding="0" class="manufacturersBoxInhaltTabelle">
                    <tr>
                        <td align="left" valign="top" width="100%">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        {*-----------------------------------------*}
                                        {* Ausgabe als Liste oder als Drop-Down ?  *}
                                        {*-----------------------------------------*}
                                        {if $arr_manufacturers_box.type eq 'drop-down'}
                                            {$arr_manufacturers_box.form}
                                            {$arr_manufacturers_box.form_pulldown}
                                            {$arr_manufacturers_box.form_hidden_sessionid}
                                        {else}
                                            {$arr_manufacturers_box.manufacturers_list}
                                        {/if}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- end manufacturersbox //-->
{/if}
</div>