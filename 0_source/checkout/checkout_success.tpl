{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}		
	{include file="pieces/page_title_section.tpl"}
	{$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td>        
        		{include file="pieces/page_title_section.tpl"}        
        	</td>
		</tr>
		{$smarty.const.BETWEEN_HL_CONTENT}
 {/if}
  
	<tr>
		<td class="cont_abst">
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						{$text_success}	
						<br>                       
						{$paypal_button}
						{$finanzkauf_button}
						<br>				
						{$text_see_orders}						
						<br>
						<br>
	            		<h3>{$smarty.const.TEXT_THANKS_FOR_SHOPPING}</h3>
						{if $is_money_order}
							<h4>{$text_money_order_success_edit}</h4>
						{/if}

						{$trusted_shop_garantie}
						<br>
						{$tracking_output}					
						<!-- </div>  -->
					</td>
       			</tr>			
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="0">
          					<tr>
            					<td width="25%">
            						<table border="0" width="100%" cellspacing="0" cellpadding="0">
              							<tr>
	                						<td width="50%" align="right">{$pixel_trans_1_5}</td>
                							<td width="50%">{$pixel_trans_100_1}</td>
              							</tr>
            						</table>
            					</td>
            					<td width="25%">{$pixel_trans_100_1}</td>
            					<td width="25%">{$pixel_trans_100_1}</td>
            					<td width="25%">
            						<table border="0" width="100%" cellspacing="0" cellpadding="0">
              							<tr>
                							<td width="50%">{$pixel_trans_100_1}</td>
                							<td width="50%">{$checkout_bullet_img}</td>
              							</tr>
            						</table>
            					</td>
          					</tr>
          				<tr>          				
            				<td align="center" width="25%" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_DELIVERY}</td>
            				<td align="center" width="25%" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_PAYMENT}</td>
            				<td align="center" width="25%" class="checkoutBarFrom">{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</td>
            				<td align="center" width="25%" class="checkoutBarCurrent">{$smarty.const.CHECKOUT_BAR_FINISHED}</td>
          				</tr>
					</table>
				</td>
      		</tr>
			{$DOWNLOADS_HTML}
        	</table>
		</td>
	</tr>
</table>

{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}
