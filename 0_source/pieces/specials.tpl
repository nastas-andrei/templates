{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}		
	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
     	<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}
	<tr>
    	<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0">
        		{if $is_top_pager}
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="smallText">{$top_pager_count}</td>
            					<td align="right" class="smallText">{$smarty.const.TEXT_RESULT_PAGE} {$top_pager_links}</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
        		{/if}
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
          						{section name=current loop=$specialslist}          						
          						<td align="center" width="33%" class="smallText">
          							<a href="{$specialslist[current].product_info_href}">{$specialslist[current].products_image}</a>
          							<br>
          							<a href="{$specialslist[current].product_info_href}">{$specialslist[current].products_name}</a>
          							<br>
          							<s>{$specialslist[current].products_price}</s>
          							<br>
          							<span class="productSpecialPrice">{$specialslist[current].specials_new_products_price}</span>
          						
          						{if $specialslist[current].is_need_separator}
			          			</tr>
			          			<tr>
			            			<td>{$pixel_trans_100_10}</td>
			          			</tr>
			          			<tr>          									
          						{/if}
          							
          						</td>
          						{/section}
          					</tr>
        				</table>
        			</td>
      			</tr>
      			{if $is_bottom_pager}
      			<tr>
        			<td>
        				<br>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="smallText">{$bottom_pager_count}</td>
            					<td align="right" class="smallText">{$smarty.const.TEXT_RESULT_PAGE} {$bottom_pager_links}</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			{/if}
        	</table>
        </td>
	</tr>
</table>
    
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}
{$smarty.const.AFTER_CONTENT}