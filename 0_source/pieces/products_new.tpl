{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}
	{include file="pieces/page_title_section.tpl"}
	{$smarty.const.BETWEEN_HL_CONTENT}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">		

{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
    	<tr>
        	<td>        
				{include file="pieces/page_title_section.tpl"}
        	</td>
      	</tr>
     	{$smarty.const.BETWEEN_HL_CONTENT}
        		
{/if}

	<tr>
		<td class="cont_abst">        
			<table border="0" width="100%" cellspacing="0" cellpadding="0">
				{if $show_pager_top}
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
			          		<tr>
			            		<td class="smallText">{$products_new_split_count_top}</td>
			            		<td align="right" class="smallText">
			            			{$smarty.const.TEXT_RESULT_PAGE} {$products_new_split_links_top}
			            		</td>
			          		</tr>
			        	</table>
			        </td>
				</tr>
			    <tr>
					<td>{$pixel_trans_10}</td>
				</tr>
				{/if}
				<tr>
			    	<td>
			    		<table border="0" width="100%" cellspacing="0" cellpadding="2">
						{if $number_of_rows > 0}    
			    			{section name=current loop=$products_list}
					        <tr>
					         	<td width="{$small_image_width}" valign="top" class="main">
					         		<a href="{$products_list[current].product_link}">
					         			{$products_list[current].product_image}		         				         		
					         		</a>
					         	</td>
					            <td valign="top" class="main">
					            	<a href="{$categorieslist[current].product_link}">
					            		<b><u>{$products_list[current].products_name}</u></b>
					            	</a>
					            	<br>
					            	{$smarty.const.TEXT_DATE_ADDED} {$products_list[current].products_date_added}
					            	<br>
					            	{$smarty.const.TEXT_MANUFACTURER} {$products_list[current].manufacturers_name}		            	
					            	<br><br>
					            	{$smarty.const.TEXT_PRICE} {$products_list[current].products_price}
					            </td>
					            <td align="right" valign="middle" class="main">
					            	<a href="{$products_list[current].product_buy_now_link}">{$products_list[current].button_in_cart}</a>
					            </td>
					     	</tr>
					        <tr>
					       		<td colspan="3">{$pixel_trans_10}</td>
					        </tr>    				
			    			{/section}
						{else}
			          		<tr>
			            		<td class="main">{$smarty.const.TEXT_NO_NEW_PRODUCTS}</td>
			          		</tr>
			          		<tr>
			            		<td>{$pixel_trans_10}</td>
			          		</tr>			
						{/if}
			        	</table>
			    	</td>
				</tr>
			 	{if $show_pager_bottom}
			    <tr>
					<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
			          		<tr>
			            		<td class="smallText">{$products_new_split_count_bottom}</td>
			            		<td align="right" class="smallText">            			
			            			{$smarty.const.TEXT_RESULT_PAGE} {$products_new_split_links_bottom}
			            		</td>
			          		</tr>
			        	</table>
			        </td>
			  	</tr>
			   	{/if} 
			</table>
		</td>
	</tr>
</table>
	
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}
