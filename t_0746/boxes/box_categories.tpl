<nav class="navbar navbar-inverse menu-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-nav-bar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse padding-0" id="header-nav-bar">
            <ul class="nav navbar-nav menu-ul">
                {foreach from=$categories key=key item=category name=catlist}
                    <li class="dropdown">
                        <a {if $category.children|@count > 0} href="{$category.link}" data-toggle="dropdown"
                           class="dropdown-toggle level0" {else} href="{$category.link}" {/if}>
                            {$category.name} {if $category.children|@count > 0} <span class="caret"></span> {/if}
                            <span class="border-top"></span>
                        </a>
                        {if $category.children|@count > 0}
                            <ul class="dropdown-menu" role="menu">
                                {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                                    <li class="{if $categorySub.children|@count > 0}dropdown-submenu {/if}">
                                        <a {if $categorySub.children|@count > 0} class="dropdown-toggle" data-toggle="dropdown" href="#"
                                           {else}href="{$categorySub.link}"{/if}>{$categorySub.name}
                                        </a>
                                        {if $categorySub.children|@count > 0}
                                            <ul class="dropdown-menu level-3" role="menu">
                                                {foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}
                                                    <li><a href="{$categorySub2.link}">{$categorySub2.name|truncate:15:"...":true}</a></li>
                                                {/foreach}
                                            </ul>
                                        {/if}
                                    </li>
                                {/foreach}
                            </ul>
                        {/if}
                    </li>
                {/foreach}
            </ul>
        </div>
    </div>
</nav>