	{$smarty.const.BEFORE_CONTENT}
	
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}

	<tr>
		<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
				{if $is_addressbook_message}
      			<tr>
        			<td>{$addressbook_message}</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			{/if}
      			<tr>
        			<td class="main"><b>{$smarty.const.PRIMARY_ADDRESS_TITLE}</b></td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
              							<tr>
                							<td>{$pixel_trans_10_1}</td>
                							<td class="main" width="50%" valign="top">{$smarty.const.PRIMARY_ADDRESS_DESCRIPTION}</td>
                							<td align="right" width="50%" valign="top">
                								<table border="0" cellspacing="0" cellpadding="2">
			                  						<tr>
			                    						<td class="main" align="center" valign="top">
			                    							<b>{$smarty.const.PRIMARY_ADDRESS_TITLE}</b>
			                    							<br>
															{$arrow_south_east_img}
			                    						</td>
			                    						<td>{$pixel_trans_10_1}</td>
			                    						<td class="main" valign="top">{$customer_address}</td>
			                    						<td>{$pixel_trans_10_1}</td>
			                  						</tr>
                								</table>
                							</td>
              							</tr>
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="main"><b>{$smarty.const.ADDRESS_BOOK_TITLE}</b></td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
	            						{section name=current loop=$addresseslist}
	              						<tr>
	                						<td>{$pixel_trans_10_1}</td>
	                						<td>
	                							<table border="0" width="100%" cellspacing="0" cellpadding="2">
                  									<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onClick="document.location.href='{$addresseslist[current].address_book_edit_row_href}'">
                    									<td class="main">
                    										<b>{$addresseslist[current].firstname} {$addresseslist[current].lastname}</b>
															{if $addresseslist[current].is_customer_default_address}&nbsp;<small><i>{$smarty.const.PRIMARY_ADDRESS}</i></small>{/if}
                    									</td>
                    									<td class="main" align="right">
                    										<a href="{$addresseslist[current].address_book_edit_row_href}">{$small_edit_img_button}</a> 
                    										<a href="{$addresseslist[current].address_book_delete_row_href}">{$small_delete_img_button}</a>
                    									</td>
                  									</tr>
                  									<tr>
                    									<td colspan="2">
                    										<table border="0" cellspacing="0" cellpadding="2">
                      											<tr>
											                        <td width="10">{$pixel_trans_10_1}</td>
											                        <td class="main">{$addresseslist[current].formated_address}</td>
											                        <td width="10">{$pixel_trans_10_1}</td>
                      											</tr>
                    										</table>
                    									</td>
                  									</tr>
                								</table>
                							</td>
                							<td>{$pixel_trans_10_1}</td>
              							</tr>            					
            							{/section}
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          					<tr>
            					<td>
            						<table border="0" width="100%" cellspacing="0" cellpadding="2">
              							<tr>
                							<td width="10">{$pixel_trans_10_1}</td>
                							<td class="smallText">
                								<a href="{$account_href}">{$button_back_img_button}</a>
                							</td>
											{if $is_allow_add}
                							<td class="smallText" align="right"><a href="{$address_book_add_href}">{$button_add_address_img_button}</a></td>
											{/if}
                							<td width="10">{$pixel_trans_10_1}</td>
              							</tr>
            						</table>
            					</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="smallText">{$address_book_max_text}</td>
      			</tr>
        	</table>
        </td>
	</tr>
</table>

	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}

	{$smarty.const.AFTER_CONTENT}

