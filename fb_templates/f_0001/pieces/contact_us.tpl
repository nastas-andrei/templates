<div class="form-horizontal">
{$contact_us_form}

        {if $contact_message_exists}
          {$contact_message}          
        {/if}

        {if $is_action_success}

            {$smarty.const.TEXT_SUCCESS}
            <a href="{$button_continue_url}">{$button_continue_img}</a>                
        {else}

        {if $smarty.const.CONTACT_RESPONSE_TIME  != ''}             
            <td class="main">{$smarty.const.CONTACT_RESPONSE_TIME}</td>
        {/if}
            
        <div class="control-group">
          <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_NAME}</label>
          <div class="controls">
              {$name_input}
          </div>
        </div>      

        <div class="control-group">
          <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_EMAIL}</label>
          <div class="controls">
              {$email_input}
          </div>
        </div>              
                                   
        <div class="control-group">
          <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>
          <div class="controls">
              {$anliegen_pull_down}
          </div>
        </div> 
                       
         <div class="control-group">
          <label class="control-label" for="inputPriceFrom"> {$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL} </label>
          <div class="controls">
              {$artikel_input}
          </div>
        </div> 

        <div class="control-group">
          <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>
          <div class="controls">
              {$bestellung_input}
          </div>
        </div> 

         <div class="control-group">
          <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_ENQUIRY}</label>
          <div class="controls">
               {$enquiry_textarea}
          </div>
        </div>          
                                  
      {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}        
         <div class="control-group">         
                  <div class="controls">
                      <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                      <select name="pf_antispam" id="pf_antispam">
                        <option value="1" selected="selected">Ja</option>
                        <option value="2">Nein</option>
                   </select>
              </div>
          </div>      

          <script language="javascript" type="text/javascript">
         
          var sel = document.getElementById('pf_antispam');var opt = new Option('Nein',2,true,true);sel.options[sel.length] = opt;   
          sel.selectedIndex = 1;
         
          </script>
          
          <noscript>
            <div class="antistalker"> 
              <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
              <select name="pf_antispam" id="pf_antispam">
                <option value="1" selected="selected">Ja</option>
                <option value="2">Nein</option>
              </select>
            </div>
          </noscript>         
      {/if}  

      <div class="control-group">         
          <div class="controls">             
             <input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">             
          </div>          
      </div>
      <p class="text-center">{$smarty.const.CONTACT_BOTTOM_TEXT}</p>
      {/if}    


</form>
</div>