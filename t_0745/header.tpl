<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}

    <link rel="stylesheet" href="{$templatepath}lib/css/font-awesome.css">
    <link rel="stylesheet" href="{$templatepath}lib/css/animate.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}custom.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}lib/css/responsive.css" type="text/css"/>
    <link rel="stylesheet" href="{$templatepath}lib/css/cloud-zoom.css" type="text/css"/>
</head>
<body>
    <header id="header"><!--header-->
        <div class="header_top"><!--header_top-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="contactinfo">
                            {if $smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT || $smarty.const.HEAD_REPLY_TAG_ALL}
                                <ul class="nav nav-pills">
                                    {if $smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
                                        <li>
                                            <a href="tel:{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}">
                                                <i class="fa fa-phone"></i> {$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}
                                            </a>
                                        </li>
                                    {/if}
                                    {if $smarty.const.HEAD_REPLY_TAG_ALL}
                                        <li>
                                            <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">
                                                <i class="fa fa-envelope"></i> {$smarty.const.HEAD_REPLY_TAG_ALL}
                                            </a>
                                        </li>
                                    {/if}
                                </ul>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header_top-->
        <div class="header-middle"><!--header-middle-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="logo pull-left">
                            {$cataloglogo}
                        </div>
                        {*<div class="btn-group pull-right">*}
                            {*<div class="btn-group" id="language">*}
                                {*{include file="boxes/box_languages.tpl"}*}
                            {*</div>*}
                        {*</div>*}
                    </div>
                    <div class="form-horizontal">
                        <div class="col-sm-3 has-feedback ">
                            {include file="boxes/box_search.tpl"}
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="shop-menu pull-right" id="account">
                            {include file="boxes/box_loginbox.tpl"}
                        </div>
                    </div>
                </div>
            </div>
        </div><!--/header-middle-->
    </header><!--/header-->
