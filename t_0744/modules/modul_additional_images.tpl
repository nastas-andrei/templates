{if $arr_additional_images}
    {if $arr_additional_images.products|@count > 1}
        <div class="groom">
            <div class="product1 additional_image" style="margin: 0 10px !important; width: 121px !important;">
                {if $arr_product_info.image_linkLARGE}
                    <a href="{$arr_product_info.image_linkLARGE}" class="zoom" rel="prettyPhoto[product-gallery]">
                        <img width="120" height="120" src="{$arr_product_info.image_link}" />
                    </a>
                {else}
                    <a href="{$arr_product_info.image_link}" class="zoom" rel="prettyPhoto[product-gallery]">
                        <img class="img-thumbnail" width="120" height="120" src="{$arr_product_info.image_link}" />
                    </a>
                {/if}
            </div>
            {foreach from=$arr_additional_images.products key=index item=product_additional name="addimages"}
                {if $product_additional.image_src}
                    {php}
                        $product = $this->get_template_vars('product_additional');
                        $pattern_src = '/src="([^"]*)"/';
                        preg_match($pattern_src, $product['image_src'], $matches);
                        $src = $matches[1];
                        $this->assign('src', $src);
                    {/php}
                {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                {/if}
                <div class="product1 additional_image" style="margin: 0 10px !important;width: 121px !important;">
                    <a href="{$product_additional.popup_img}" rel="prettyPhoto[product-gallery]">
                        <img src="{$src}" width="120" height="120" />
                    </a>
                </div>
            {/foreach}
        </div>
    {else}
        <div class="thumbnails img-inner-shadow clearfix">
            {if $arr_product_info.image_linkLARGE}
                <a href="{$arr_product_info.image_linkLARGE}" class="zoom" rel="prettyPhoto[product-gallery]">
                    <img class="img-thumbnail" width="120" height="120" src="{$arr_product_info.image_link}" />
                </a>
            {else}
                <a href="{$arr_product_info.image_link}" class="zoom" rel="prettyPhoto[product-gallery]">
                    <img class="img-thumbnail" width="120" height="120" src="{$arr_product_info.image_link}" />
                </a>
            {/if}
            {foreach from=$arr_additional_images.products key=index item=product_additional name="addimages"}
                {if $product_additional.image_src}
                    {php}
                        $product = $this->get_template_vars('product_additional');
                        $pattern_src = '/src="([^"]*)"/';
                        preg_match($pattern_src, $product['image_src'], $matches);
                        $src = $matches[1];
                        $this->assign('src', $src);
                    {/php}
                {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                {/if}
                <a href="{$product_additional.popup_img}" class="zoom" rel="prettyPhoto[product-gallery]">
                    <img class="img-thumbnail" width="120" height="120" src="{$src}" />
                </a>
            {/foreach}
        </div>
    {/if}
{/if}