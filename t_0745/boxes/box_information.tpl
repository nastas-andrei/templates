{if $arr_informationbox}
    <h2>{$arr_informationbox.title}</h2>
    <ul class="nav nav-pills nav-stacked">
        {foreach from=$arr_informationbox.items key=key item=link name=element}
            <li><a href="{$link.target}">{$link.text}</a></li>
        {/foreach}
    </ul>
{/if}