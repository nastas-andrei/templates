<div class="location-bar">
    <h1>{$arr_product_info.products_name}</h1>
</div>
<div id="content">
    <div id="tabs" class="htabs">
        <a href="#tab-information" class="selected" style="display: inline;">General Info</a>
        <a href="#tab-description" class="" style="display: inline;">Description</a>
        <a href="#tab-review" class="" style="display: inline;">Reviews (1)</a>
        <a href="#tab-related" class="" style="display: inline;">Related Products (7)</a>
    </div>
    <div class="product-info">
        <div id="tab-information" class="tab-content" style="display: block;">

            <div class="review">
                <div><img src="catalog/view/theme/purewhite/image/stars-3.png" alt="1 reviews"><br><a onclick="$('a[href=\'#tab-review\']').trigger('click');">1 reviews</a>&nbsp;&nbsp;&nbsp;&nbsp;<a onclick="$('a[href=\'#tab-review\']').trigger('click');"><i class="icon-pencil"></i>Write a review</a></div>
                <div class="share">
                    <!-- AddThis Button BEGIN -->
                    <!--div class="addthis_default_style"><a class="addthis_button_compact">Share</a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
                    <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script-->
                    <!-- AddThis Button END -->
                </div>
            </div>

            <div class="product-description">

                <div class="description">
                    <span class="dcolumn1">Brand:</span><span class="dcolumn2"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/manufacturer/info&amp;manufacturer_id=8">Apple</a></span><br>
                    <span class="dcolumn1">Product Code:</span><span class="dcolumn2">product 20</span><br>
                    <span class="dcolumn1">Availability:</span><span class="dcolumn2">In Stock</span>
                </div>

            </div>


            <div class="product-images">
                <div class="image">
                    <a href="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--STANDARD-800x800.jpg" title="Sciarpa lord in cachemire e seta" class="colorbox cboxElement" rel="colorbox"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--STANDARD-420x420.jpg" title="Sciarpa lord in cachemire e seta" alt="Sciarpa lord in cachemire e seta" id="image"></a></div>
                <div class="image-additional">
                    <a href="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--DETAIL_2-800x800.jpg" title="Sciarpa lord in cachemire e seta" class="colorbox cboxElement" rel="colorbox"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--DETAIL_2-74x74.jpg" title="Sciarpa lord in cachemire e seta" alt="Sciarpa lord in cachemire e seta"></a>
                    <a href="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--DETAIL_1-800x800.jpg" title="Sciarpa lord in cachemire e seta" class="colorbox cboxElement" rel="colorbox"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-LORD-CASHMERE-AND-SILK-SCARF--DETAIL_1-74x74.jpg" title="Sciarpa lord in cachemire e seta" alt="Sciarpa lord in cachemire e seta"></a>
                    <a href="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-SCIARPA-LORD-IN-CACHEMIRE-E-SETA-BUNDLE_1-800x800.jpg" title="Sciarpa lord in cachemire e seta" class="colorbox cboxElement" rel="colorbox"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070279-SCIARPA-LORD-IN-CACHEMIRE-E-SETA-BUNDLE_1-74x74.jpg" title="Sciarpa lord in cachemire e seta" alt="Sciarpa lord in cachemire e seta"></a>
                </div>
            </div>


            <div class="product-options">
                <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post" id="product_addtocart_form">
                    <div class="price">
                        {if $arr_product_info.preisalt > 0}
                            <span class="price-old">{$arr_product_info.preisalt}</span>
                        {/if}
                        <div class="price-regular">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</div>
                    </div>

                    <div class="cart">
                        {if $LOAD_CLIENT}
                            <input type="submit" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button" onclick="productAddToCartForm.submit(this)">
                            <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                        {else}
                            <input type="button" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button" disabled>
                        {/if}
                    </div>
                    <div style="margin:10px 0">
                        {if $arr_product_info.arr_products_options}
                            <div class="form-group">
                                <label class="control-label">
                                    {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                </label>
                                {if $arr_product_info.product_options_javascript}
                                    {$arr_product_info.product_options_javascript}
                                {/if}
                                <table>
                                    {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                        <tr>
                                            <td>{$productoption.name}:</td>
                                            <td >{$productoption.pulldown}</td>
                                        </tr>
                                    {/foreach}
                                </table>
                            </div>
                        {/if}
                        {*-------------------------------------------*}
                        {if $opt_count > 0}
                            {if $var_options_array}
                                <input type="hidden" id="master_id" value="{$master_id}">
                                <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                <input type="hidden" id="cPath" value="{$cPath}">
                                <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                {if $Prodopt}
                                    <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                {/if}
                                {if $prod_hidden}
                                    {foreach from=$prod_hidden key=key item=hidden_input}
                                        {$hidden_input}
                                    {/foreach}
                                {/if}
                                {if $select_ids}
                                    {foreach from=$select_ids key=key item=select_input}
                                        {$select_input}
                                    {/foreach}
                                {/if}
                                {if $javascript_hidden}
                                    {$javascript_hidden}
                                {/if}
                                {foreach from=$var_options_array key=key item=options}
                                    <label>{$options.title}:</label>
                                    {$options.dropdown}
                                {/foreach}
                            {/if}
                        {/if}

                        {*-------------------------------------------*}
                        {if $arr_product_info.base_price}
                            {$arr_product_info.base_price}
                        {/if}

                        {*-------------------------------------------*}
                        {if $arr_product_info.shipping_value gt 0}
                            <label class="control-label">{$TEXT_SHIPPING}:</label>
                            ({$arr_product_info.shipping})
                        {/if}

                        {*---------------------------------------------*}
                        {if $arr_product_info.products_uvp}
                            {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                            {$arr_product_info.products_uvp}
                            {$arr_product_info.TEXT_YOU_SAVE}:
                            {$arr_product_info.diff_price}
                        {/if}

                        {*-------------------------------------------*}
                        {if $count_bestprice gt 0}
                            {$staffelpreis_hinweis}

                            {foreach from=$bestprice_arr key=key item=bestprice}
                                {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                {$bestprice.ausgabe_stueck}
                                {$bestprice.ausgabe_preis}
                            {/foreach}
                        {/if}

                        {*-------------------------------------------*}
                        <div class="clearfix"></div>
                        {if $PRICE_QUERY_ON eq 'true'}
                            <div style="font-size:15px;margin: 10px 0;">
                                {$PRICE_QUERY}:
                                <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                            </div>
                        {/if}
                        <div class="clearfix"></div>
                        {*-------------------------------------------*}
                        {if $PRODUCTS_QUERY_ON eq 'true'}
                            <div style="font-size:15px;margin: 10px 0;">
                                {$PRODUCTS_INQUIRY_TEXT}: <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                            </div>
                        {/if}
                        <div class="clearfix"></div>
                        {*-------------------------------------------*}

                        {* $arr_product_info.availability.text => z.b. "Sofort lieferbar"*}
                        {* $arr_product_info.availability.value => 0-3 => 0 = Keine Angabe, 1 = Sofort lieferbar, 2 = beschr?nkt lieferbar, 3 = nicht lieferbar *}
                        {if count($arr_product_info.availability)}

                            {$arr_product_info.availability.TEXT_AVAILABILITY}:
                            {$arr_product_info.availability.text}

                        {/if}

                        {*-------------------------------------------*}
                        {if count($arr_product_info.delivery_time)}
                            {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                            {$arr_product_info.delivery_time.text} *
                        {/if}

                        {*-------------------------------------------*}
                        {if $arr_product_info.text_date_available}
                            &nbsp;
                            {$arr_product_info.text_date_available}
                        {/if}
                        {*-------------------------------------------*}
                        {if $arr_product_info.products_url}
                            &nbsp;
                            {$arr_product_info.products_url}
                        {/if}

                        {*-------------------------------------------*}
                        {if $SANTANDER_LOANBOX}
                            <span id="santandertitle_pinfo">
                                    {$SANTANDER_LOANBOX_TITLE}:
                                  </span>
                            &nbsp;
                            {$SANTANDER_LOANBOX}
                        {/if}

                        {*-------------------------------------------*}
                        {if $PRODUCTS_WEIGHT_INFO}
                            {if $arr_product_info.products_weight > 0}
                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                {$arr_product_info.products_weight}
                                &nbsp;
                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                            {/if}
                        {/if}

                        {*-------------------------------------------*}
                        {if count($arr_product_info.products_model)}
                            <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">Model: </span>
                            {$arr_product_info.products_model}
                            <br/>
                        {/if}

                        {*-------------------------------------------*}
                        {if $arr_manufacturer_info.manufacturers_name}
                            <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">{$manufacturer}: </span>
                            {if $arr_manufacturer_info.manufacturers_image neq ""}
                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                    <a href="{$arr_manufacturer_info.manufacturers_url}">
                                        <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                    </a>
                                {else}
                                    <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                {/if}
                            {else}
                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                    <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                {else}
                                    {$arr_manufacturer_info.manufacturers_name}
                                {/if}
                            {/if}
                        {/if}

                        {*---------------------------------------------*}
                        {if $widget_view && $widget_view eq 1}
                            <div id="widget_views"></div>
                        {/if}
                    </div>
                </form>

            </div> <!-- END product-content -->
        </div> <!-- END tab-information -->

        <div id="tab-description" class="tab-content" style="display: none;"><p></p><div class="cpt_product_description ">
                <div>
                    <p><strong>More room to move.</strong></p>

                    <p>With 80GB or 160GB of storage and up to 40 hours of battery life, the new iPod classic lets you enjoy up to 40,000 songs or up to 200 hours of video or any combination wherever you go.</p>

                    <p><strong>Cover Flow.</strong></p>

                    <p>Browse through your music collection by flipping through album art. Select an album to turn it over and see the track list.</p>

                    <p><strong>Enhanced interface.</strong></p>

                    <p>Experience a whole new way to browse and view your music and video.</p>

                    <p><strong>Sleeker design.</strong></p>

                    <p>Beautiful, durable, and sleeker than ever, iPod classic now features an anodized aluminum and polished stainless steel enclosure with rounded edges.</p>
                </div>
            </div>
            <!-- cpt_container_end --><p></p></div>
        <div id="tab-review" class="tab-content" style="display: none;">
            <div id="review"><div class="review-list">
                    <div class="author"><i class="icon-user"></i><span>Liza</span>  on  22/07/2013</div>
                    <div class="rating"><img src="catalog/view/theme/purewhite/image/stars-3.png" alt="1 reviews"></div>
                    <div class="text">asdfl asdfjkasldf asdfsdifiau ewr wto jcggfd gdfg iertg jgkasdfl asdfjkasldf asdfsdifiau ewr wto jcggfd gdfg iertg jgk asdfl asdfjkasldf asdfsdifiau ewr wto jcggfd gdfg iertg jgkasdfl asdfjkasldf asdfsdifiau ewr wto jcggfd gdfg iertg jgk</div>
                </div>
                <div class="pagination"><div class="results">Showing 1 to 1 of 1 (1 Pages)</div></div>
            </div>
            <h2 id="review-title"><span class="heading-lines">Write a review</span></h2>
            <span class="text-styling">Your Name:</span><br><br>
            <input type="text" name="name" value="">
            <br>
            <br>
            <span class="text-styling">Your Review:</span><br><br>
            <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea><br><br>
            <span style="font-size: 11px;"><span style="color: #FF0000;">Note:</span> HTML is not translated!</span><br>
            <br>
            <span class="text-styling">Rating:</span><br><br><span>Bad</span>&nbsp;
            <input type="radio" name="rating" value="1">
            &nbsp;
            <input type="radio" name="rating" value="2">
            &nbsp;
            <input type="radio" name="rating" value="3">
            &nbsp;
            <input type="radio" name="rating" value="4">
            &nbsp;
            <input type="radio" name="rating" value="5">
            &nbsp;<span>Good</span><br>
            <br>
            <br>
            <span class="text-styling">Enter the code in the box below:</span><br><br>
            <input type="text" name="captcha" value="">
            <br>
            <br>
            <img src="index.php?route=product/product/captcha" alt="" id="captcha"><br>
            <br>
            <div class="buttons">
                <div class="right"><a id="button-review" class="button">Continue</a></div>
            </div>
        </div> <!-- END tab-review -->
    </div> <!-- END product-info -->
    <div id="tab-related" style="display: none;">
        <div class="related-product-wrapper">
            <div class="related">
                <div class="box-product">
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=29"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00070278-LORD-CASHMERE-AND-SILK-SCARF-STANDARD-226x250.jpg" title="Lord cashmere and silk scarf" alt="Lord cashmere and silk scarf" style="opacity: 1;"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=29">Lord cashmere and silk scarf</a></div>
                        <div class="price">
                            $330.99																</div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=40"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00073912-CATANIA-SILK-BUTTON-DOWN-SHIRT-STANDARD-226x250.jpg" title="Catania silk button-down shirt" alt="Catania silk button-down shirt" style="opacity: 1;"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=40">Catania silk button-down shirt</a></div>
                        <div class="price">
                            $120.68																</div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=42"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00071135-SEXY-THING-SUEDE-SANDALS-STANDARD-226x250.jpg" title="Sexy thing suede sandals" alt="Sexy thing suede sandals" style="opacity: 1;"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=42">Sexy thing suede sandals</a></div>
                        <div class="price">
                            <div class="sale"><i class="icon-barcode" rel="tooltip" data-placement="bottom" data-original-title="SALE"></i></div>
                            <span class="price-old">$119.50</span> <span class="price-new">$107.75</span>
                        </div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=43"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00071506-GEOMETRIC-SUNGLASSES--STANDARD-226x250.jpg" title="Geometric sunglasses" alt="Geometric sunglasses"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=43">Geometric sunglasses</a></div>
                        <div class="price">
                            $589.50																</div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=46"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00074668-AMAZONA-23-LEATHER-TOTE-STANDARD-226x250.jpg" title="Amazona 23 leather tote" alt="Amazona 23 leather tote"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=46">Amazona 23 leather tote</a></div>
                        <div class="price">
                            <div class="sale"><i class="icon-barcode" rel="tooltip" data-placement="bottom" data-original-title="SALE"></i></div>
                            <span class="price-old">$1,177.00</span> <span class="price-new">$1,254.55</span>
                        </div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=47"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00072221-WRAP-AROUND-SNAKESKIN-BRACELET-WITH-STUDDED-LOGO-CLOSURE-STANDARD-226x250.jpg" title="Wrap around snakeskin bracelet with studded logo closure" alt="Wrap around snakeskin bracelet with studded logo closure"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=47">Wrap around snakeskin bracelet with studded logo closure</a></div>
                        <div class="price">
                            $119.50																</div>
                    </div> <!-- END div -->
                    <div>
                        <div class="image"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=49"><img src="http://wyloo.me/oc/purewhite/image/cache/data/goods/P00073368-NIGHTINGALE-LEATHER-TOTE-STANDARD-226x250.jpg" title="Nightingale leather tote" alt="Nightingale leather tote"></a></div>
                        <div class="name"><a href="http://wyloo.me/oc/purewhite/index.php?route=product/product&amp;product_id=49">Nightingale leather tote</a></div>
                        <div class="price">
                            <div class="sale"><i class="icon-barcode" rel="tooltip" data-placement="bottom" data-original-title="SALE"></i></div>
                            <span class="price-old">$236.99</span> <span class="price-new">$235.83</span>
                        </div>
                    </div> <!-- END div -->
                </div> <!-- END box-product -->
            </div> <!-- END related -->
            <span id="more-related" class="more"><i class="icon-align-justify" rel="tooltip" data-placement="top" data-original-title="SHOW MORE"></i></span>
        </div> <!-- END related-product-wrapper -->
    </div> <!-- END tab-related -->
</div>