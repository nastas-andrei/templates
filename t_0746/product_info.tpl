{include file="header.tpl"}
<section id="product-info" xmlns="http://www.w3.org/1999/html">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-3">
                <div class="left-sidebar">
                    <h2>{$smarty.const.HEADING_TITLE_CAT}</h2>

                    <div class="panel-group category-products" id="categories"><!--category-productsr-->
                        <div id="categories-box"></div>
                    </div>
                    <!--/category-products-->
                    <div class="shipping text-center"><!--shipping-->
                        {foreach from=$boxes_pos.right key=key item=boxname name=current}
                            {if $boxname eq 'htmlbox'}
                                {if $key == 13}
                                    {include file="boxes/box_$boxname.tpl"}
                                {/if}
                            {/if}
                        {/foreach}
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 padding-right">
                {if $pAnfrage}
                    {if $pAnfrage == 1}
                        {include file="modules/modul_additional_price_query.tpl"}
                    {else}
                        {include file="modules/modul_additional_price_query.tpl"}
                    {/if}
                {else}
                    {if $arr_product_info.product_check == true}
                        <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="POST" id="product_addtocart_form">
                            <div class="product-details"><!--product-details-->
                                <div class="col-sm-5">
                                    <div class="view-product">
                                        {if $arr_product_info.image_source}
                                            {if $arr_product_info.image_linkLARGE}
                                                <a id="sns_cloudzoom" class="cloud-zoom"
                                                   rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1"
                                                   title="{$arr_product_info.products_name}" href="{$arr_product_info.image_linkLARGE}">
                                                    <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}"
                                                         title="{$arr_product_info.products_name}"/>
                                                    <!-- <span class='ico-product ico-new'>New</span>     -->
                                                </a>
                                            {else}
                                                <a id="sns_cloudzoom" class="cloud-zoom"
                                                   rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1"
                                                   title="{$arr_product_info.products_name}" href="{$arr_product_info.image_link}">
                                                    <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}"
                                                         title="{$arr_product_info.products_name}"/>
                                                    <!-- <span class='ico-product ico-new'>New</span>     -->
                                                </a>
                                            {/if}
                                        {else}
                                            <a id="sns_cloudzoom" class="cloud-zoom"
                                               rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1"
                                               title="{$arr_product_info.products_name}" href="{$imagepath}/empty-album.png">
                                                <img id="image" src="{$imagepath}/empty-album.png" alt="{$arr_product_info.products_name}"
                                                     title="{$arr_product_info.products_name}"/>
                                                <!-- <span class='ico-product ico-new'>New</span>     -->
                                            </a>
                                        {/if}
                                    </div>
                                    {if $arr_additional_images.products > 1}
                                        <div id="similar-product" class="carousel slide" data-ride="carousel">
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner">
                                                {include file="modules/modul_additional_images.tpl"}
                                            </div>

                                            <!-- Controls -->
                                            <a class="left item-control" href="#similar-product" data-slide="prev">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                            <a class="right item-control" href="#similar-product" data-slide="next">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </div>
                                    {/if}
                                </div>
                                <div class="col-sm-7">
                                    <div class="product-information"><!--/product-information-->
                                        <h2>{$arr_product_info.products_name}</h2>
                                        <span>
                                            {$arr_product_info.preisneu|regex_replace:"/[()]/":""}

                                        </span>

                                        {if $LOAD_CLIENT}
                                            <button type="submit" class="btn btn-fefault cart" title="{$smarty.const.IMAGE_BUTTON_IN_CART}">
                                                <i class="fa fa-shopping-cart"></i>
                                                {$smarty.const.IMAGE_BUTTON_IN_CART}
                                            </button>
                                            <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                                        {else}
                                            <button type="button" class="btn btn-fefault cart" disabled>
                                                <i class="fa fa-shopping-cart"></i>
                                                {$smarty.const.IMAGE_BUTTON_IN_CART}
                                            </button>
                                        {/if}
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $arr_product_info.shipping_value gt 0}
                                                <label class="control-label">{$TEXT_SHIPPING}:</label>
                                                ({$arr_product_info.shipping})
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $PRODUCTS_QUERY_ON eq 'true'}
                                                <label>{$PRODUCTS_INQUIRY_TEXT}:</label>
                                                <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if count($arr_product_info.products_model)}
                                                <label>Model: </label>
                                                {$arr_product_info.products_model}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $arr_manufacturer_info.manufacturers_name}
                                                <label>{$manufacturer}: </label>
                                                {if $arr_manufacturer_info.manufacturers_image neq ""}
                                                    {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                        <a href="{$arr_manufacturer_info.manufacturers_url}">
                                                            <img src="{$arr_manufacturer_info.manufacturers_image}"
                                                                 alt="{$arr_manufacturer_info.manufacturers_name}">
                                                        </a>
                                                    {else}
                                                        <img src="{$arr_manufacturer_info.manufacturers_image}"
                                                             alt="{$arr_manufacturer_info.manufacturers_name}">
                                                    {/if}
                                                {else}
                                                    {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                        <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                                    {else}
                                                        {$arr_manufacturer_info.manufacturers_name}
                                                    {/if}
                                                {/if}
                                            {/if}
                                        </p>
                                        <hr/>
                                        <p>
                                            {if $arr_product_info.arr_products_options}
                                                <div class="form-group">
                                                    <label>
                                                        {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                                    </label>
                                                    {if $arr_product_info.product_options_javascript}
                                                        {$arr_product_info.product_options_javascript}
                                                    {/if}
                                                    <table>
                                                        {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                                            <tr>
                                                                <td>{$productoption.name}:</td>
                                                                <td>{$productoption.pulldown}</td>
                                                            </tr>
                                                        {/foreach}
                                                    </table>
                                                </div>
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $opt_count > 0}
                                                {if $var_options_array}
                                                    <input type="hidden" id="master_id" value="{$master_id}">
                                                    <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                                    <input type="hidden" id="cPath" value="{$cPath}">
                                                    <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                                    {if $Prodopt}
                                                        <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                                    {/if}
                                                    {if $prod_hidden}
                                                        {foreach from=$prod_hidden key=key item=hidden_input}
                                                            {$hidden_input}
                                                        {/foreach}
                                                    {/if}
                                                    {if $select_ids}
                                                        {foreach from=$select_ids key=key item=select_input}
                                                            {$select_input}
                                                        {/foreach}
                                                    {/if}
                                                    {if $javascript_hidden}
                                                        {$javascript_hidden}
                                                    {/if}
                                                    {foreach from=$var_options_array key=key item=options}
                                                        <label>{$options.title}:</label>
                                                        {$options.dropdown}
                                                    {/foreach}
                                                {/if}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $arr_product_info.base_price}
                                                {$arr_product_info.base_price}
                                            {/if}
                                        </p>
                                        <p>
                                            {*---------------------------------------------*}
                                            {if $arr_product_info.products_uvp}
                                                {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                                {$arr_product_info.products_uvp}
                                                {$arr_product_info.TEXT_YOU_SAVE}:
                                                {$arr_product_info.diff_price}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $count_bestprice gt 0}
                                                {$staffelpreis_hinweis}

                                                {foreach from=$bestprice_arr key=key item=bestprice}
                                                    {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                                    {$bestprice.ausgabe_stueck}
                                                    {$bestprice.ausgabe_preis}
                                                {/foreach}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            <div class="clearfix"></div>
                                            {if $PRICE_QUERY_ON eq 'true'}
                                                <div style="font-size:15px;margin: 10px 0;">
                                                    {$PRICE_QUERY}:
                                                    <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                                </div>
                                            {/if}
                                        </p>
                                        <p>
                                            <div class="clearfix"></div>
                                            {*-------------------------------------------*}

                                            {if count($arr_product_info.availability)}

                                                {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                                {$arr_product_info.availability.text}

                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if count($arr_product_info.delivery_time)}
                                                {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                                {$arr_product_info.delivery_time.text} *
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $arr_product_info.text_date_available}
                                                &nbsp;
                                                {$arr_product_info.text_date_available}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $arr_product_info.products_url}
                                                &nbsp;
                                                {$arr_product_info.products_url}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $SANTANDER_LOANBOX}
                                                <label id="santandertitle_pinfo">
                                                    {$SANTANDER_LOANBOX_TITLE}:
                                                </label>
                                                &nbsp;
                                                {$SANTANDER_LOANBOX}
                                            {/if}
                                        </p>
                                        <p>
                                            {*-------------------------------------------*}
                                            {if $PRODUCTS_WEIGHT_INFO}
                                                {if $arr_product_info.products_weight > 0}
                                                    {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                                    {$arr_product_info.products_weight}
                                                    &nbsp;
                                                    {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                                {/if}
                                            {/if}
                                        </p>
                                        <p>
                                            {*---------------------------------------------*}
                                            {if $widget_view && $widget_view eq 1}
                                                <div id="widget_views"></div>
                                            {/if}
                                        </p>
                                        <!-- AddThis Button BEGIN -->
                                        <div class="addthis_toolbox addthis_default_style ">
                                            <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                            <a class="addthis_button_tweet"></a>
                                            <a class="addthis_counter addthis_pill_style"></a>
                                        </div>
                                        {literal}
                                            <script type="text/javascript">var addthis_config = {"data_track_addressbar": false};</script>
                                            <script type="text/javascript"
                                                    src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-507b2455057cfd5f"></script>
                                        {/literal}
                                        <!-- AddThis Button END -->
                                    </div><!--/product-information-->
                                </div>
                            </div><!--/product-details-->
                        </form>
                        <div class="category-tab shop-details-tab"><!--category-tab-->
                            <div class="col-sm-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#details" data-toggle="tab">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane active" id="details" >
                                    <div style="margin: 20px">{$arr_product_info.products_description}</div></table>
                                </div>
                            </div>
                        </div><!--/category-tab-->
                        {if $arr_modul_xsell}
                            <div class="recommended_items"><!--xsell_items-->
                                <h2 class="title text-center">{$arr_modul_xsell.TEXT_XSELL_PRODUCTS}</h2>

                                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        {include file="modules/modul_xsell_products.tpl"}
                                    </div>
                                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                        {/if}
                        {if $arr_modul_also_purchased_products}
                            <div class="recommended_items"><!--also_items-->
                                <h2 class="title text-center">{$arr_modul_also_purchased_products.title}</h2>

                                <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        {include file="modules/modul_also_purchased_products.tpl"}
                                    </div>
                                    <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                        <i class="fa fa-angle-left"></i>
                                    </a>
                                    <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </div>
                        {/if}
                    {else}
                        {$TEXT_PRODUCT_NOT_FOUND}
                    {/if}
                {/if}
            </div>
        </div>
    </div>
</section>
{include file="footer.tpl"}
