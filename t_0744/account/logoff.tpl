<section class="homecolon">
    <div class="container">
        <div id="content" class="single col-lg-12 col-md-12 col-sm-12">
            <div class="alert alert-info">
                <button type="button" class="btn btn-default btn-icon close" data-dismiss="alert" aria-hidden="true">×
                </button>
                {$smarty.const.TEXT_MAIN}
            </div>
            <div class="text-center">
                <a href="{$DEFAULT_URL}" class="btn btn-default btn-icon">{$smarty.const.IMAGE_BUTTON_CONTINUE}</a>
            </div>
        </div>
    </div>
</section>