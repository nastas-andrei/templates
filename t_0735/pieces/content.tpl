{if $products_not_found_message}
    {$products_not_found_message}
{/if}

{if $is_default}

    {*{if $smarty.const.TEXT_DEFINE_MAIN != ""}*}
    {*{$user_html}*}
    {*{/if}*}
    {if $slider_images != ""}
        <!-- BEGIN  TOP SLIDER -->
        <div class="topslider">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    {foreach from=$slider_images item=slider name=name_slider key=key}
                        <li data-target="#carousel-example-generic" data-slide-to="{$key}" {if $smarty.foreach.name_slider.first}class="active"{/if}></li>
                    {/foreach}
                </ol>
                <div class="carousel-inner">
                    {foreach from=$slider_images item=slider name=name_slider}
                        {if $slider.active == 1}
                            <div class="item {if $smarty.foreach.name_slider.first} active{/if} animated fadeInUp">
                                <img alt="{$slider.title}" src="{$slider.image}">

                                <div class="caption">
                                    <div class=" slider-title animated fadeInDown delay1">{$slider.title}</div>
                                    <div class="hidden-xs delay2 slider-desc animated fadeIn">{$slider.description}</div>
                                    <div class="slider-button animated fadeInUp delay3">
                                        {if $slider.button_title}
                                            <a href="{$slider.link}">{$slider.button_title}</a>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                    <img src="templates/t_0735/images/top-slider/slide-left.png" alt="">
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                    <img src="templates/t_0735/images/top-slider/slide-right.png" alt="">
                </a>
            </div>
        </div>
        <!-- END SLIDER -->
    {/if}

    {$FILENAME_FEATURED_HTML}

    {$UPCOMING_PRODUCTS_HTML}

    {*{if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}*}
    {*{$define_main_bottom}*}

    {*{/if}*}

{/if}

{if $is_products == 1}

    {$smarty.const.HEADING_TITLE}

    {if $filterlistisnotempty}
        {$filter_form}
        {$smarty.const.TEXT_SHOW}
    {/if}

    {if !empty($categories_html)}
        {$categories_html}
    {/if}

    {if !empty($manufacturers_html_header)}
        {$manufacturers_html_header}
    {/if}

    {if !empty($PRODUCT_LISTING_HTML)}
        {$PRODUCT_LISTING_HTML}
    {/if}

    {if !empty($manufacturers_id)}
        {$manufacturers_html_footer}
    {/if}

    {if !empty($categories_html_footer)}
        {$categories_html_footer}
    {/if}

{else}
    {if $is_default}
        &nbsp;
    {else}
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}