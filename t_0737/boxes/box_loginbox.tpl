{if $arr_loginbox}

    {if $arr_loginbox.boxtyp == "myaccount"}
        <li class="first"><a href="{$arr_topmenu.4.link}" title="{$arr_topmenu.4.name}">{$arr_topmenu.4.name}</a></li>
        <li><a href="{$arr_loginbox.items.4.target}" title="{$arr_loginbox.items.4.text}">{$arr_loginbox.items.4.text}</a></li>
    {else}
        <li><a href="{$domain}/login.php" title="{$arr_smartyconstants.LOGIN_BOX_REGISTER}">{$arr_smartyconstants.LOGIN_BOX_REGISTER}</a></li>
    {/if}
    <li>
        <a href="{$arr_topmenu.3.link}" title="{$arr_topmenu.3.name}" class="top-link-cart">{$arr_topmenu.3.name}</a>
    </li>
    {if $arr_loginbox.boxtyp == "myaccount"}
        <li>
            <a href="{$arr_topmenu.5.link}" title="{$arr_topmenu.5.name}" class="top-link-checkout">{$arr_topmenu.5.name}</a>
        </li>
    {/if}
    <li class=" last">
        <a href="{$arr_topmenu.2.link}" title="{$arr_topmenu.2.name}">{$arr_topmenu.2.name}</a>
    </li>
{/if}