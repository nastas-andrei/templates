<div class="boxes-html">
<div class="row-fluid">
	<div class="span3">
		{include file="boxes/box_information.tpl"}
	</div>
	{php}
		$i = 0;
	{/php}

	{foreach from=$boxes_pos.left key=key item=boxname name=current}
	{if $boxname eq 'htmlbox'}
		{php}
			$i++;
			$this->assign('counter_boxes', $i);
		{/php}

	
		<div class="span3">			
			{include file="boxes/box_$boxname.tpl"}
		</div>
	
		{if $counter_boxes == 3}
			</div>
			<div class="row-fluid" style="margin-top:10px;">								
		{/if} 

		{if $counter_boxes % 4 == 0 and $counter_boxes > 5}
			</div>
			<div class="row-fluid" style="margin-top:10px;">								
		{/if}          	
	{/if}  	
	{/foreach}      	
	</div>		
</div> 