<div class="container">
	<!-- button back -->               
	<div class="row">
	    <div class="pull-left shopping"> 
	        <a class="btn btn-primary" href="{$account_href}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a> 
	    </div>
	</div>
	<!-- button back end -->  
	{if !$is_delete}
		{$addressbook_edit_form}
	{/if}
		 
	{if $is_addressbook_message}
		{$addressbook_message}
	{/if}

	{if $is_delete}
		{$smarty.const.DELETE_ADDRESS_TITLE}
		{$smarty.const.DELETE_ADDRESS_DESCRIPTION}
	    {$smarty.const.SELECTED_ADDRESS}
	    {$delete_address_label}
	    <a href="{$addressbook_href}" >{$smarty.const.IMAGE_BUTTON_BACK}</a>
		<a href="{$addressbook_delete_href}" class="btn">{$smarty.const.IMAGE_BUTTON_DELETE}</a>	
	{else}
		<div style="margin-left: -30px;">
			{$ADDRESS_BOOK_DETAILS_HTML}
		</div>
		<div class="clearfix"></div>

		{if $is_edit}
			<div class="text-center">			
				{$edit_hidden_fields}		
				<button type="submit" class="btn btn-primary" ><i class="fa fa-check-circle"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_UPDATE}</button>	
			</div>
		{else}		
			<div class="text-center">			
				{$action_hidden_field}
				<button type="submit" class="btn btn-primary" >{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>
			</div>
		{/if}
	{/if}

{if !$is_delete}
	</form>
{/if}
</div>
