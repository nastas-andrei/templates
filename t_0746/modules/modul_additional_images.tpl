{foreach from=$arr_additional_images.products key=key item=product_additional name=additional_images}
    {if $product_additional.image_src}
        {php}
            $product_additional = $this->get_template_vars('product_additional');
            $pattern_src = '/src="([^"]*)"/';
            preg_match($pattern_src, $product_additional['image_src'], $matches);
            $src = $matches[1];
            $this->assign('src', $src);
        {/php}
    {else}
        {assign var=src value=$imagepath|cat:"/empty-album.png"}
    {/if}
    {if $smarty.foreach.additional_images.index >= 2 && $smarty.foreach.additional_images.index % 3 == 0}</div>{/if}
    {if $smarty.foreach.additional_images.index % 3 == 0}<div class="item {if $key === 1} active{/if}">{/if}
    <a class="cloud-zoom-gallery" rel="useZoom: 'sns_cloudzoom', smallImage: '{$product_additional.link}' " title="{$arr_additional_images.title}" rel="" href="{$product_additional.popup_img}">
        <img src="{$product_additional.link}" alt="{$arr_additional_images.title}" title="{$arr_additional_images.title}" width="84" height="85"/>
    </a>
    {if $smarty.foreach.additional_images.last}</div>{/if}
{/foreach}