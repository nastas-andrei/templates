<footer>
    <!-- BEGIN WIDGET BOTTOM -->
    <div id="sns_botsl2" class="wrap">
        <div class="container">
            <div class="row-fluid">
                <!-- START INFORMATION -->
                {include file="boxes/box_information.tpl"}
                <!-- END INFORMATION -->
                {php}
                    $i = 0;
                {/php}

                {foreach from=$boxes_pos.left key=key item=boxname name=current}
                    {if $boxname eq 'htmlbox'}
                        <div class="block block-Information span3">
                            {include file="boxes/box_$boxname.tpl"}
                        </div>
                    {/if}
                {/foreach}
            </div>
        </div>
    </div>
    <!-- Footer Creeate -->
    <div id="sns_footer" class="wrap">
        <div class="container">
            <div class="row-fluid">
                {*<div class="span12">*}
                    {*<ul class="payment">*}
                        {*{foreach from=$boxes_pos.right key=key item=boxname name=current}*}
                            {*{if $boxname eq 'htmlbox'}*}
                                {*{if $key == 844}*}
                                    {*{include file="boxes/box_$boxname.tpl"}*}
                                {*{/if}*}
                            {*{/if}*}
                        {*{/foreach}*}
                    {*</ul>*}
                {*</div>*}
                <div class="span12">
                    <div class="sns-info clearfix">
                        <ul class="links">
                            <li class="first">
                                <a href="{$arr_footer.sitemap_link}" title="{$smarty.const.TEXT_SITEMAP}">{$smarty.const.TEXT_SITEMAP}</a>
                            </li>
                            <li>
                                <a href="{$arr_searchbox.advanced_search_link}"
                                   title="{$smarty.const.BOX_SEARCH_ADVANCED_SEARCH}">{$smarty.const.BOX_SEARCH_ADVANCED_SEARCH}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="sns-copyright">
                        &copy; {$arr_footer.year}
                        {foreach from=$arr_footer.partner key=key item=partner}
                            <a href="http://{$partner.link}" target="_blank" class="muted" data-toggle="tooltip" original-title="{$partner.name}"
                               title="{$partner.name}">{$partner.text} {$partner.name}
                            </a>
                            <a href="http://{$partner.link}" target="_blank" data-toggle="tooltip" original-title="{$partner.name}" title="{$partner.name}">
                                <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" title="{$partner.name}" />
                            </a>
                        {/foreach}
                    </div>
                </div>
                <div id="sns_tools">
                    <a id="sns-totop" href="#"><i class="icon-angle-up"></i></a>
                    {literal}
                        <script type="text/javascript">
                            $sns_jq(function ($) {
                                $("#sns-totop").hide();
                                $(function () {
                                    var wh = $(window).height();
                                    var whtml = $(document).height();
                                    $(window).scroll(function () {
                                        if ($(this).scrollTop() > whtml / 10) {
                                            $('#sns-totop').fadeIn();
                                        } else {
                                            $('#sns-totop').fadeOut();
                                        }
                                    });
                                    $('#sns-totop').click(function () {
                                        $('body,html').animate({
                                            scrollTop: 0
                                        }, 800);
                                        return false;
                                    });
                                });
                            });
                        </script>
                    {/literal}
                </div>
            </div>
        </div>
    </div>
    <!-- END WIDGET BOTTOM -->
</footer>
</div>
</div>
</section>
</body>
</html>