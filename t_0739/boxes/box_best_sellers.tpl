{if $arr_bestsellersbox}
<!--Begin Best Content -->
<div class="sns-pdt-content wide-4 normal-4 tabletlandscape-4 tabletportrait-3 mobilelandscape-2 mobileportrait-1">
  <div class="pdt-content pdt-loadmore tab-content-actived-best-seller is-loaded pdt_best_sales" >
    <div class="pdt-list products-grid zoomOut">
      {foreach from=$arr_bestsellersbox.products key=i item=product name=previewitems}
        {if $product.image} 
          {php}
           {$product = $this->get_template_vars('product');}
           {$pattern_src = '/src="([^"]*)"/';}
           {$pattern_width = '/width="([^"]*)"/';}
           {$pattern_height = '/height="([^"]*)"/';}
           {preg_match($pattern_src, $product['image'], $matches);}
           {$src = $matches[1];}
           {preg_match($pattern_width, $product['image'], $matches);}
           {$width = $matches[1];}
           {preg_match($pattern_height, $product['image'], $matches);}
           {$height = $matches[1];}
           {$this->assign('src', $src);}
           {$this->assign('img_width', $width);}
           {$this->assign('img_height', $height);}
          {/php}
        {else}
          {assign var=src value=$imagepath|cat:"/empty-album.png"}
        {/if}
        <!-- Modal -->
      <div class="modal fade" id="{$product.products_id}" style="display:none">
        <div class="modal-dialog">
          <div class="modal-content">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="fancybox-close">&times;</button>
            <div class="modal-body">
              <div class="product-img-box span5">
                <a title="{$product.name}" href="{$product.link}" >
                  <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                </a>
              </div>
              <div id="product-shop" class="product-shop span7" data-tablet="product-shop span8" data-mobile="product-shop span8">
                <div class="title-modal">{$product.name}</div>
                <div class="price-box" style="text-align:left">
                  <span class="regular-price" id="product-price-274">
                    <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>             .
                  </span>        
                </div>
                <div class="add-to-cart" style="text-align:left">
                  <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button btn-cart have-borderinset" onclick="setLocation('{$product.buy_now_link}')">
                    <span>
                      <span>{$smarty.const.IMAGE_BUTTON_IN_CART}</span>
                    </span>
                  </button>
                </div>
                <div class="clearer"></div>
              </div>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
      <div class="item item-animate mobileportrait-first show-addtocart" style="-webkit-animation-delay:300ms;-moz-animation-delay:300ms;-o-animation-delay:300ms;animation-delay:300ms;">
        <div class="item-inner clearfix">
          <div class="item-img have-additional clearfix">
            <div class="cart-wrap">
              <div class="cart">
                <button title="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="btn-cart" onclick="setLocation('{$product.buy_now_link}')" data-toggle="tooltip" data-original-title="{$smarty.const.IMAGE_BUTTON_IN_CART}">{$smarty.const.IMAGE_BUTTON_IN_CART}</button>
              </div>
            </div>

            <div class="item-img-info">
              <a href="{$product.link}" title="{$product.name}" class="product-image">
                <span class="img-main">
                  <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                </span>
              </a>
              <div class="item-box-hover number-buttom3 has-btn-qv">
                <div class="box-inner">
                  <div class="quickview-wrap">
                    <a href="{$product.link}" title="{$product.name}" style="display:none"></a>
                    <a class="sns-btn-quickview" aria-hidden="true" data-dismiss="modal" data-toggle="modal" href="#{$product.products_id}"><span>Quick View</span></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item-info">
            <div class="info-inner">
              <div class="item-title">
                <a href="{$product.link}" onclick="javascript: return true" title="{$product.name}">{$product.name|truncate:35:"...":true}</a>
              </div>
              <div class="item-content">
                <div class="item-price">
                  <div class="price-box">
                    <span class="regular-price" id="product-price-2758589588401400575855">
                      <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                    </span>                
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/foreach}
    </div>
  </div>
</div>
<!--END Best Content -->
{/if}

