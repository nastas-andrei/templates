<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 done"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3 current"><span><a href="{$checkout_payment_href}">{$smarty.const.CHECKOUT_BAR_PAYMENT}</a></span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>
{$checkout_payment_form}
	{if $is_payment_error}	
		<div class="alert alert-danger alert_checkout_payment">
	        <button type="button" class="btn btn-primary btn-icon close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{$payment_error_title}</strong>{$payment_error_description}
		</div>
	{/if}
 
	{if $is_error}
	<div class="alert alert-danger">
	        <button type="button" class="btn btn-primary btn-icon close" data-dismiss="alert" aria-hidden="true">&times;</button>
		{$error_message}
	</div>	
	{/if}
		<div class="col-xs-6">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{$smarty.const.TABLE_HEADING_BILLING_ADDRESS}
					</h3>
				</div>
				<div class="panel-body">
						{$billto_address_label}
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{$smarty.const.TABLE_HEADING_PAYMENT_METHOD}</h3>
				</div>
				<div class="panel-body">
					{if $is_multi_selection}
						<h4 class="text-info">{$smarty.const.TEXT_SELECT_PAYMENT_METHOD}</h4>		
					{else}
						<h4 class="text-info">{$smarty.const.TEXT_ENTER_PAYMENT_INFORMATION}</h4>
					{/if}
					{section name=current loop=$selectionlist}
						{if $selectionlist[current].is_error}
							{$selectionlist[current].error}
						{/if}
						{if $is_multi_selection}
							{$selectionlist[current].payment_radio}
							<!-- <img src="{$templatepath}images/arrow_green.gif" class="fleft" /> -->
							<span>{$selectionlist[current].module}, </span>{$selectionlist[current].info|regex_replace:'/<img[^>]*>/':''}
							<img src="{$templatepath}images/icon_payments_{$selectionlist[current].id}.png" style="float:right; width:80px"/>
							<div class="clearfix"></div>
							<hr/>
						{else}
						<h6 class="co_selectionHEADLINE">{$selectionlist[current].module}
							{$selectionlist[current].info|regex_replace:'/<img[^>]*>/':''}</h6>
							{$selectionlist[current].payment_hidden}
						{/if}
					{if $selectionlist[current].is_fields}
					<div class="co_selectionHLDsub"{if $selectionlist[current].is_payment_selection} id="subactive{/if}">
						<div class="co_selectionHLDsubLEFT">
							{section name=current_field loop=$selectionlist[current].fields}
								<div class="co_selectionSUBtitle">{$selectionlist[current].fields[current_field].title|regex_replace:'/<img[^>]*>/':''}</div>
								<div class="co_selectionSUBfield">{$selectionlist[current].fields[current_field].field}</div>
							{/section}
						</div>
						<div class="co_selectionHLDsubRIGHT">
							{if $is_payment_error}
							<div class="paymentERRORtitle">{$payment_error_title}</div>
							<div class="paymentERRORdesc">{$payment_error_description}</div>
							{/if}
							{if $is_error}
								{$error_message}
							{/if}
						</div>
					</div>
					<div class="clearer"></div>
					{/if}
					{/section}
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">{$smarty.const.TABLE_HEADING_COMMENTS}</h3>
				</div>
				<div class="panel-body">
					<div class="col-xs-6">{$comments_textarea}</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="col-xs-12">	
	      	<button type="button" class="btn btn-primary btn-icon pull-left" onclick="window.location='{$checkout_shipping_href}';">
	            <span>
	              <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
	            </span>
	    	</button>   
			<button type="submit" title="{$smarty.const.IMAGE_BUTTON_CONTINUE}" class="btn btn-primary btn-icon pull-right">
				<span>
					<span>{$smarty.const.IMAGE_BUTTON_CONTINUE}</span>
				</span>
			</button>	
			<!-- <input type="submit" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}" class="btn btn-info pull-right"/> -->
		</div>
</form>
