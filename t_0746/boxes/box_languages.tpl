{if $arr_languagebox}
    <span class="language-name">{$arr_languagebox.title_allg}:</span> <a id="drop1" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="icon"><img src="{$arr_languagebox.language.image}"></i> {$arr_languagebox.language.name} <span class="glyphicon glyphicon-chevron-down"></span>
    </a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="drop1">
        {foreach from=$arr_languagebox.items key=key item=language}
            {if ($languageview neq $language.directory)}
                <li role="presentation">
                    <a role="menuitem" tabindex="-1" href="{$language.link}"><i class="icon"><img src="{$language.image}" alt=""></i>{$language.name}</a>
                </li>
            {/if}
        {/foreach}
    </ul>
{/if}
