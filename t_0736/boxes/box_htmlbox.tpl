{if $boxes_content.$key}
    {if $boxes_content.$key.boxes_title}
        <div class="block-title">
            {$boxes_content.$key.boxes_title}
        </div>
    {/if}
    <div class="block-content">
        <ul>
            {$boxes_content.$key.boxes_content}
        </ul>
    </div>
{/if}

