{include file="header.tpl"}

<div id="wrapper" class="container canvas main-container">

 {if $SCRIPT_NAME != "/index.php"}
    <div class="navigation-controls clearfix">
      <div class="pull-left">
        <i class="icon-caret-left"></i> <a href="{$home_link}">Home</a>
      </div>
    </div>
  {/if}
  

<div class="row-fluid">
  <div class="span4">     
    <div class="sidebar ">
      
    <div class="boxes">

      {foreach from=$boxes_pos.left key=key item=boxname}            
            {include file="boxes/box_$boxname.tpl"}                
      {/foreach}
    

      {foreach from=$boxes_pos.right key=key item=boxname}              
              {include file="boxes/box_$boxname.tpl"}                          
      {/foreach}        
      {$SHOP_NEWS_HTML}
    </div> 

    </div> 
  </div>
  <div class="span8">

            
  {$content}

  {if $content_template}
    {include file="$content_template"}
  {/if}
  </div>

</div>
 
{include file="footer.tpl"} 