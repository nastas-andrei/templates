{literal}
    <script type="text/javascript">
        <!--
        function setGet(h) {
            var checkb = window.document.getElementById('alternativPD').checked;
            if (checkb) {
                h += '?alternativPD=1';
                window.location.href = h;
            } else {
                window.location.href = h;
            }
        }
        -->
    </script>
{/literal}

<ol class="steps">
    <li class="step1 current"><span><a
                    href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span>
    </li>
    <li class="step2"><span>{$smarty.const.CHECKOUT_BAR_DELIVERY}</span></li>
    <li class="step3"><span>{$smarty.const.CHECKOUT_BAR_PAYMENT}</span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>

{$cart_quantity_form}

{if $any_out_of_stock == 1}
    {if $smarty.const.STOCK_ALLOW_CHECKOUT == 'true'}
        <div id="stockWarning" class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i>
            </button>
            {$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}
        </div>
    {else}
        <div id="stockWarning" class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i>
            </button>
            {$smarty.const.OUT_OF_STOCK_CAN_CHECKOUT}
        </div>
    {/if}

{literal}
    <script type="text/javascript">
        window.onload = function () {
            document.getElementById('stockWarning').style.visibility = 'visible';
        }
        document.getElementById('stockWarning').style.visibility = 'visible';
    </script>
{/literal}

{/if}

{if $count_contents > 0}
    {$hidden_fields}
    {$table_box_content}

    <div class="text-right">
        <p style="margin-right:10px">
            <span style="font-weight:bold">{$smarty.const.SUB_TITLE_SUB_TOTAL}</span>
            <span style="border-bottom:1px solid #000; font-style:italic; font-weight:bold">{$formated_products_price}</span>
        </p>
    </div>
    <div class="clearfix">
        <div class="pull-left">
            <a class="btn btn-default"
               href="{$seo_link_back_href}">&larr;&nbsp;&nbsp;{$arr_smartyconstants.IMAGE_BUTTON_CONTINUE_SHOPPING}</a>
        </div>
        <div class="pull-right">
                <a class="btn btn-success" href="{$checkout_shipping_href}">
                    {$arr_smartyconstants.IMAGE_BUTTON_CHECKOUT}
                    &nbsp;&nbsp;&nbsp;<i class="fa fa-shopping-cart"></i>
                </a>
        </div>
    </div>
{else}
    <div id="stockWarning" class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-remove"></i></button>
        {$smarty.const.TEXT_CART_EMPTY}
    </div>
    <div class="text-center">
        <a class="btn btn-default"
           href="{$seo_index_link}">&larr;&nbsp;&nbsp;{$arr_smartyconstants.IMAGE_BUTTON_BACK}</a>
    </div>
{/if}
</form>
<div class="accountMarginBottom"></div> 
