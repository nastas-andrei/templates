<h4>{$arr_informationbox.title}</h4>
<ul>
    {foreach from=$arr_informationbox.items key=key item=link name=element}
        <li><a href="{$link.target}">{$link.text}</a></li>
    {/foreach}
</ul>
