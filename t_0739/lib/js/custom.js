$(function() {
    // Language and Currency Swither
    $('.switcher').hover(function() {
        $(this).find('.option').stop(true, true).slideDown(0);
    },function() {
        $(this).find('.option').stop(true, true).slideUp(0);
    });
});

$(function(){
    // Scroll to Top
    $("#back-top").hide();
    $(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#back-top').fadeIn();
            } else {
                $('#back-top').fadeOut();
            }
        });

        $('#back-top a').click(function () {
            $('body,html').animate({
                scrollTop: 0
            }, 300);
            return false;
        });
    });
});

$(function(){
    //Image  Fade
    $('.box img,.box-product img,.product-grid img').hover(function() {
        this.check = this.check || 1;
        $(this).stop().fadeTo('slow',this.check++%2==0 ? 1 : 0.7);
    });
});

$(function(){
    // More Featured
    $('#more-featured').click(function() {
        $('.featured .box-product > div:hidden').slice(0,4).slideDown('slow').css({'display' : 'inline-block'});
        if ($('.featured .box-product > div').length == $('.featured .box-product > div:visible').length) {
            $('#more-featured').hide();
        }
    });
});

$(function(){
    // More Latest
    $('#more-latest').click(function() {
        $('.latest .box-product > div:hidden').slice(0,4).slideDown('slow').css({'display' : 'inline-block'});
        if ($('.latest .box-product > div').length == $('.latest .box-product > div:visible').length) {
            $('#more-latest').hide();
        }
    });
});

$(function(){
    // More Bestseller
    $('#more-bestseller').click(function() {
        $('.bestseller .box-product > div:hidden').slice(0,4).slideDown('slow').css({'display' : 'inline-block'});
        if ($('.bestseller .box-product > div').length == $('.bestseller .box-product > div:visible').length) {
            $('#more-bestseller').hide();
        }
    });
});

$(function(){
    // More Special
    $('#more-special').click(function() {
        $('.special .box-product > div:hidden').slice(0,4).slideDown('slow').css({'display' : 'inline-block'});
        if ($('.special .box-product > div').length == $('.special .box-product > div:visible').length) {
            $('#more-special').hide();
        }
    });
});

$(function(){
    // More Related
    $('#more-related').click(function() {
        $('.related .box-product > div:hidden').slice(0,4).slideDown('slow').css({'display' : 'inline-block'});
        if ($('.related .box-product > div').length == $('.related .box-product > div:visible').length) {
            $('#more-related').hide();
        }
    });
});