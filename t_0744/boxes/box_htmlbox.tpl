{if $boxes_content.$key}
    <div class="widget">
        <h3 class="title">{$boxes_content.$key.boxes_title} &nbsp;</h3>
        <ul>
            {$boxes_content.$key.boxes_content}
        </ul>
    </div>
{/if}