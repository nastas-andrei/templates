<!DOCTYPE HTML>
<html>
<head>	
	<title>{$meta_title}</title>
	<meta name="keywords" content="{$meta_keywords}" />
	<meta name="description" content="{$meta_description}" />
	<meta name="robots" content="index, follow" />
	<meta charset="utf-8">
	{$headcontent}
	{if $FAVICON_LOGO && $FAVICON_LOGO != ''}
		<link rel="shortcut icon" href="{$FAVICON_LOGO}" />
	{/if}
	<link rel="stylesheet" href="{$templatepath}stylesheet.css"></link>
	
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css"></link>
	<link rel="stylesheet" href="{$templatepath}css/modalimages.css"></link>	
	<link rel="stylesheet" href="{$templatepath}custom.css"></link>
	{$clink}
	<script src="includes/general.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<script src="{$templatepath}lib/bootstrap.js"></script>
	<script src="{$templatepath}lib/application.js"></script>
	
	<script src="{$templatepath}lib/scroll.js"></script>
	<script src="{$templatepath}lib/modalimages.js"></script>
	   	
	{literal}
	<script>
		$(document).ready(function($) {
			$('#wrapper').slimscroll({height:'800px',width:'793px',allowPageScroll: true});	
			$('.modal-item').modalDengkul();

			$('#history-back').on('click', function(e) {
			  e.preventDefault();		    
	            window.history.go(-1);	    
	        });

	        $('#history-forward').on('click', function(e) {
			  e.preventDefault();		          
	            window.history.go(+1);	          
	        });
		});	
	</script>
	{/literal}
	

	{if $SCRIPT_NAME == "/product_info.php"}
      <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/bxslider.css"/>
	  <link href="{$templatepath}lib/videojs/video.css" rel="stylesheet" type="text/css">
	  <script src="{$templatepath}lib/videojs/video.js"></script>
	  {literal}
	  <script>
	    videojs.options.flash.swf = "{/literal}{$templatepath}{literal}lib/videojs/video-js.swf";
	  </script>
	  {/literal}
	  <script type="text/javascript" src="{$templatepath}lib/bxslider.js"></script>
	{/if}
    
    {$xajax_script}

</head>
<body> 
<div id="fb-root"></div>
{literal}
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/" + {/literal}"{$language_iso}"{literal} + "/all.js#xfbml=1&appId=341633249301316";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>	
{/literal}
