<section class="homecolon">
    <div class="container">
        <div id="content" class="single col-lg-12 col-md-12 col-sm-12">
            <iframe width="100%" height="350" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyBzBxYxCJDus32apdAMyDlkvNX3qNYgOsE">
            </iframe>
            {if $contact_message_exists}
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {$contact_message}
                </div>
            {/if}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="widget">
                {$contact_us_form}
                    {if $is_action_success}
                        <div class="text-center">
                            {$smarty.const.TEXT_SUCCESS}
                            <br>
                            <a class="btn btn-default" href="{$button_continue_url}">{$IMAGE_BUTTON_CONTINUE_SHOPPING}</a>
                        </div>
                    {else}
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="pull-left">
                            <strong>{$smarty.const.ENTRY_NAME}</strong>
                        </div>
                        {$name_input}
                        <div class="pull-left">
                            <strong>{$smarty.const.ENTRY_EMAIL}</strong>
                        </div>
                        {$email_input}
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="pull-left">
                            <strong>{$smarty.const.ENTRY_CONTACT_SUBJECT}</strong>
                        </div><br>
                        <div class="input-box">
                            <div style="position: relative;z-index: 1;margin-right: -4px">{$anliegen_pull_down}</div>
                        </div>
                        <div class="field" id="artikel" style="display: none">
                            <div class="pull-left">
                                <strong id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</strong>
                            </div>
                            <div class="clearfix">
                                {$artikel_input}
                            </div>
                        </div>
                        <div class="field" id="bestellung" style="display: none">
                            <div class="pull-left">
                                <strong id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</strong>
                            </div>
                            <div class="clearfix">
                                {$bestellung_input}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="text-center">
                            <strong>{$smarty.const.ENTRY_ENQUIRY}</strong>
                        </div>
                        <br>
                        {$enquiry_textarea}
                        {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                            <div class="form-group">
                                    <span>
                                        Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)
                                    </span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </div>
                            <script language="javascript" type="text/javascript">
                                var sel = document.getElementById('pf_antispam');var opt = new Option('Nein',2,true,true);sel.options[sel.length] = opt;
                                sel.selectedIndex = 1;
                            </script>
                            <noscript>
                                <div class="antistalker">
                                    <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                    <select name="pf_antispam" id="pf_antispam">
                                        <option value="1" selected="selected">Ja</option>
                                        <option value="2">Nein</option>
                                    </select>
                                </div>
                            </noscript>
                        {/if}
                    </div>
                    <br>
                    <div class="text-center">
                        <button type="submit" class="btn btn-lg btn-default">{$IMAGE_ANI_SEND_EMAIL}</button>
                    </div>
                    {/if}
                </form>
            </div>
            </div>
        </div>
    </div>
</section>
