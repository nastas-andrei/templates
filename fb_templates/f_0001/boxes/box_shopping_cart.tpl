{if $arr_shopping_cart}
	{if $arr_shopping_cart.count_contents > 0}
		<div class="box box-shopping-cart">
		<div class="box-heading">
			<h3>{$arr_shopping_cart.title} <i class="icon-shopping-cart muted pull-right box-shopping-cart-icon"></i></h3>     			
		</div>
		<div class="box-shopping-cart-body">
			<p><a class="" href="{$arr_shopping_cart.cart_link}">{$arr_shopping_cart.cart_prod} - {$arr_shopping_cart.count_contents}</a></p>
			<p><a class="" href="{$arr_shopping_cart.cart_link}">{$arr_shopping_cart.cart_summe} - ({$arr_shopping_cart.gesamtsumme})</a></p>
		</div>
	</div>
	{/if}
{/if}