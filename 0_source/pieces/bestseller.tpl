{$smarty.const.BEFORE_CONTENT}
{if !$smarty.const.PH_INSIDE}	
	{include file="pieces/page_title_section.tpl"}
    {$smarty.const.BETWEEN_HL_CONTENT}
	<table border="1" width="100%" cellspacing="0" cellpadding="0">
		<tr>
			<td class="pageContentTable">
				<table border="0" width="100%" cellspacing="0" cellpadding="0">
{else}
	<table border="0" width="100%" cellspacing="0" cellpadding="0">
     	<tr>
        	<td>
				{include file="pieces/page_title_section.tpl"}
			</td>
      	</tr>
      {$smarty.const.BETWEEN_HL_CONTENT}
{/if}
	<tr>
    	<td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0">
        		{if $is_need_pager_top}
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="smallText">{$pager_display_number_top}</td>
            					<td align="right" class="smallText">{$smarty.const.TEXT_RESULT_PAGE} {$pager_links_top}</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
				{/if}
				<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
          					{section name=current loop=$specialslist}
      							<td align="center" width="33%" class="smallText">
									<table border="0" cellspacing="0" cellpadding="0" width="100%">
										<tr>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
										</tr>
										<tr>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
											<td width="100%">
												<table border="0" cellspacing="0" cellpadding="0" width="100%">
													<tr>
														<td align="center" valign="middle" class="prod_listing_pic">
															<a href="{$specialslist[current].BILD_HREF}">{$specialslist[current].BILD_IMG}</a>
														</td>
														<td><img src="images/templates/spacer.gif" width="10" height="1"></td>
														<td width="100%">
															<table border="0" cellspacing="0" cellpadding="0" width="100%">
																<tr>
																	<td class="prod_listing_data" width="100%">																		
																		<span class="prod_listing_name">
																			<a href="{$specialslist[current].PROD_NAME_HREF}">{$specialslist[current].products_name}</a>																			
																		</span>
																		<br />
																		<span class="prod_listing_nr">{$specialslist[current].PROD_BESTNR}</span>
																		<br>																		
																		<img src="images/templates/spacer.gif" width="1" height="10"><br>
																		<span class="prod_listing_descr">{$specialslist[current].PROD_DESCR}</span>
																	</td>
																	<td rowspan="2"><img src="images/templates/spacer.gif" width="10" height="1"></td>
																	<td class="prod_listing_price">{$specialslist[current].PREISSCHILD}</td>
																</tr>
																<tr>
																	<td align="left" class="prod_listing_zumangebot">
																		<a href="{$specialslist[current].BTN_INFO_HREF}">...mehr Informationen</a>
																	</td>
																	<td align="left" class="prod_listing_buynow">{$specialslist[current].cart_quantity_form}</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>
											</td>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
										</tr>
										<tr>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
											<td><img src="images/templates/spacer.gif" width="2" height="2"></td>
										</tr>
										<tr>
											<td colspan="3"><img src="images/templates/spacer.gif" width="1" height="10"></td>
										</tr>
									</table>
      							</td>
      						{if $specialslist[current].is_need_separator}      
          					</tr>
          						<tr>            						
            						<td>{$pixel_trans_100_10}</td>
          						</tr>
          					<tr>      
      						{/if}
          					{/section}
          					</tr>
        				</table>
        			</td>
      			</tr>
				{if $is_need_legend}
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="0" cellpadding="2">
  							<tr class="productListing-odd">
    							<td colspan="{$colspan}" class="smallText">&nbsp;*alle Preise gelten zzgl. der g�ltigen gesetzl. MwSt.&nbsp;</td>
  							</tr>
						</table>
					</td>
				</tr>
				{/if}
				{if $is_need_pager_bottom}      
				<tr>
        			<td>
        				<br>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="smallText">{$pager_display_number_bottom}</td>
            					<td align="right" class="smallText">{$smarty.const.TEXT_RESULT_PAGE} {$pager_links_bottom}</td>
          					</tr>
        				</table>
        			</td>
      			</tr>
      			{/if}
        </table>
        </td>
      </tr>
    </table>

{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}

{$smarty.const.AFTER_CONTENT}