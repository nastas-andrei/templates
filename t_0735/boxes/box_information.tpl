{*<div class="box-html-box">*}
{*<div class="box-heading">*}
{*<h5>{$arr_informationbox.title}</h5>*}
{*</div>*}
{*<div class="box-content clearfix">*}
{*<ul class="unstyled">*}
{*{foreach from=$arr_informationbox.items key=key item=link name=element}*}
{*{if $request_uri eq $link.target}*}
{*<li><a href="{$link.target}">{$link.text}</a></li>*}
{*{else}*}
{*<li><a href="{$link.target}">{$link.text}</a></li>*}
{*{/if}*}
{*{/foreach}*}
{*</ul>*}
{*</div>*}
{*</div>*}

{if $arr_informationbox}
    <div class="col-md-3 col-sm-6">
        <div class="widget-box">
            <h3 class="wdgt-title">{$arr_informationbox.title}</h3>
            <ul class="wdgt-ul">
                {foreach from=$arr_informationbox.items key=key item=link name=element}
                    <li><a href="{$link.target}">{$link.text}</a></li>
                {/foreach}
            </ul>
        </div>
    </div>
{/if}

{*<div class="col-md-3 col-sm-6">*}
{*<div class="widget-box">*}
{*<h3 class="wdgt-title">Über uns</h3>*}
{*<ul class="wdgt-ul">*}
{*{foreach from=$arr_informationbox.items key=key item=link name=element}*}
{*{if $link.sort == 2}*}
{*<li><a href="{$link.target}">{$link.text}</a></li>*}
{*{/if}          *}
{*{/foreach}*}
{*</ul>*}
{*</div>*}
{*</div>*}
{*{/if*}