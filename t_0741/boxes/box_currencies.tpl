{if $arr_currenciesbox}
<li class="dropdown">
    <form id="drop-currencies" name="currencies" action="{$arr_currencies.formaction}" method="get" style="display: inline-block;">
        <select id="basic2" name="currency" onChange="this.form.submit();" data-width="100px">
            {html_options options=$arr_currenciesbox.items selected=$currency}
        </select>
        {foreach from=$arr_currenciesbox.hiddenfields key=key item=fieldname}
             <input  type="hidden" name="{$key}" value="{$fieldname}">
        {/foreach}
    </form>
</li>
{/if}


