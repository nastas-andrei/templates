<section class="content-wrapper">
    <div class="container">
        <div class="col-md-6">
            <div class="content-box" style="float: left">
                <h3>Maps</h3>
                <iframe width="540px" height="450" frameborder="0" style="border:0"
                        src="https://www.google.com/maps/embed/v1/place?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyASNmy5aLU4Gv3ExDUfmCIbmOpE6MgXzmg"></iframe>
            </div>
        </div>
        <div class="col-md-6">
            <div class="content-box">
                <h3>Contact</h3>
                {$contact_us_form}
                {if $contact_message_exists}
                    {$contact_message}
                {/if}

                {if $is_action_success}

                    {$smarty.const.TEXT_SUCCESS}
                    <a href="{$button_continue_url}">{$button_continue_img}</a>
                {else}

                    {if $smarty.const.CONTACT_RESPONSE_TIME  != ''}
                        <span>{$smarty.const.CONTACT_RESPONSE_TIME}</span>
                    {/if}
                    <div class="styleInput">
                        <div class="form-group">
                            <label class="control-label">{$smarty.const.ENTRY_NAME}</label>
                            {$name_input|replace:'general_inputs':'form-control'}
                        </div>
                        <div class="form-group">
                            <label class="control-label">{$smarty.const.ENTRY_EMAIL}</label>
                            {$email_input|replace:'general_inputs':'form-control'}

                        </div>
                        <div class="form-group">
                            <label class="control-label">{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>
                            {$anliegen_pull_down|replace:'general_inputs':'form-control'}
                        </div>
                        <div class="form-group" id="artikel" style="display: none">
                            <label class="control-label"
                                   id="artikel_title">{$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL}</label>
                            {$artikel_input|replace:'general_inputs':'form-control'}
                        </div>
                        <div class="form-group" id="bestellung" style="display: none">
                            <label class="control-label"
                                   id="bestellung_title">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>
                            {$bestellung_input|replace:'general_inputs':'form-control'}
                        </div>
                        <div class="form-group">
                            <label class="control-label">{$smarty.const.ENTRY_ENQUIRY}</label>
                            {$enquiry_textarea|replace:'general_inputs':'form-control'}
                        </div>

                        {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                            <div class="form-group">
                                <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </div>
                            <script language="javascript" type="text/javascript">

                                var sel = document.getElementById('pf_antispam');
                                var opt = new Option('Nein', 2, true, true);
                                sel.options[sel.length] = opt;
                                sel.selectedIndex = 1;

                            </script>
                            <noscript>
                                <div class="antistalker">
                                    <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                    <select name="pf_antispam" id="pf_antispam">
                                        <option value="1" selected="selected">Ja</option>
                                        <option value="2">Nein</option>
                                    </select>
                                </div>
                            </noscript>
                        {/if}
                    </div>
                    <button type="submit" class="btn btn-default">{$smarty.const.CONTACT_BOTTOM_TEXT}</button>
                {/if}
                </form>
            </div>
        </div>
    </div>
</section>