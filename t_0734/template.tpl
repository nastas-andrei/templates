{include file="header.tpl"}
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12">
        {*ajax categories*}
        <div id="categories-box"></div>
    </div>
    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        {if $breadcrumb_array|@count > 1}
            <div class="breadcrumb">
                {foreach from=$breadcrumb_array key=key item=crumb}
                    {assign var=last value=$breadcrumb_array|@end}
                    {if $last.title == $crumb.title}
                        <a>{$crumb.title}</a>
                    {else}
                        <a href="{$crumb.link}">{$crumb.title}</a>
                    {/if}
                {/foreach}
                {*{$breadcrumb|replace:'|':''}*}
                {*{debug}*}
            </div>
        {/if}
        {$content}
        {if $content_template}
            {include file="$content_template"}
        {/if}
    </div>
</div>
</div>
{include file="footer.tpl"}