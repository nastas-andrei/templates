{if $arr_searchbox}
{$arr_searchbox.form}
    <div class="input-group">
        <input id="search" type="text" name="keywords" class="input-text form-control search" value="{$arr_searchbox.inputtextfield}" placeholder="{$arr_searchbox.title}">
		<span class="input-group-btn">
		    <button type="submit" target="payment" title="suchen" alt="suchen" class="button btn btn-primary">
                <i class="fa fa-search"></i>
            </button>
		</span>
    </div>
</form>
{/if}
