{if $arr_currenciesbox}
<!-- start currencies box //-->
<span>{$arr_currenciesbox.title}</span>

  <form name="currencies" action="{$arr_currencies.formaction}" method="get" style="display: inline-block;">
   <select name="currency" onChange="this.form.submit();"class="currenciesBoxSel" >
    {html_options options=$arr_currenciesbox.items selected=$currency}
   </select>
    {foreach from=$arr_currenciesbox.hiddenfields key=key item=fieldname}
     <input type="hidden" name="{$key}" value="{$fieldname}">
    {/foreach}
  </form>
           
<!-- end currencies box //-->
{/if}

