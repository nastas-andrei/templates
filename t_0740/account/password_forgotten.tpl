<section class="blog-wrapper">
    <div class="container">
        <div class="row-fluid">
            <!-- global messages -->
            <div class="buttons-set">
                <p class="back-link"><a class="btn btn-primary btn-icon" href="{$LOGIN_URL}">
                        <small>&larr;&nbsp;</small> {$smarty.const.IMAGE_BUTTON_BACK}</a></p>
            </div>
            <div id="sns_mainmidle" class="span12 clearfix">
                <!-- primary content -->
                <div class="page-title">
                    <h1>{$smarty.const.LOGIN_BOX_PASSWORD_FORGOTTEN}</h1>
                </div>
                {$password_forgotten_form}
                <div class="fieldset">
                    <h2 class="legend">{$smarty.const.NAVBAR_TITLE_2}</h2>

                    <p>{$smarty.const.TEXT_MAIN}</p>
                    <ul class="form-list">
                        <li>
                            <label for="email_address" class="required"><em>*</em>{$smarty.const.ENTRY_EMAIL_ADDRESS}
                            </label>

                            <div class="col-md-4">
                                {$email_address_input}
                            </div>
                        </li>
                    </ul>
                </div>
                <button type="submit" title="Submit" class="btn btn-primary btn-icon" style="margin-left: 15px">
                    <span><span>Submit</span></span></button>
                </form>
            </div>
        </div>
    </div>
</section>