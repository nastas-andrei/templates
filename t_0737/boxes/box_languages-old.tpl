{if $arr_languagebox}
<li class="nav-dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		{$smarty.const.BOX_HEADING_LANGUAGES}&nbsp;<span class="caret"></span>
	</a>
	<ul class="dropdown-menu languages">
		{foreach from=$arr_languagebox.items key=key item=language}	
			{if ($languageview neq $language.directory)}
				<li><a href="{$language.link}">{$language.name}</a></li>
			{/if}
			{/foreach}	
	</ul>
</li>
{/if}                    	                    