<section id="account-history-info" class="container">
    <div class="row">
        <div class="col-xs-12">
            <button type="button" class="btn btn-primary" style="margin:20px 0" onclick="window.location='javascript:history.back()';">
                <span>
                  <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
                </span>
            </button>
            <div class="clearfix">
                <p class="pull-left">{$smarty.const.HEADING_ORDER_NUMBER|replace:" %s":": $order_id"}&nbsp;&nbsp;&nbsp;{$smarty.const.HEADING_ORDER_DATE}{$order_info.date_purchased|date_format:"%d.%m.%Y"}</p>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{$smarty.const.HEADING_DELIVERY_ADDRESS}</h3>
                </div>
                <div class="panel-body">
                    {if $order_customer_delivery.name}
                        {$order_customer_delivery.name}<br>
                    {/if}
                    {if $order_customer_delivery.company}
                        {$order_customer_delivery.company}<br>
                    {/if}
                    {if $order_customer_delivery.street_address}
                        {$order_customer_delivery.street_address}<br>
                    {/if}

                    {if $order_customer_delivery.suburb}
                        {$order_customer_delivery.suburb}<br>
                    {/if}

                    {if $order_customer_delivery.postcode}
                        {$order_customer_delivery.postcode}
                        {$order_customer_delivery.city}<br>
                    {/if}
                    {if $order_customer_delivery.state}
                        {$order_customer_delivery.state}<br>
                    {/if}
                    {if $order_customer_delivery.country}
                        {$order_customer_delivery.country}<br>
                    {/if}
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{$smarty.const.HEADING_BILLING_ADDRESS}</h3>
                </div>
                <div class="panel-body">
                    {*$billing_format_id*}

                    {if $order_customer_billing.name}
                        {$order_customer_billing.name} <br>
                    {/if}

                    {if $order_customer_billing.company}
                        {$order_customer_billing.company}<br>
                    {/if}

                    {if $order_customer_billing.street_address}
                        {$order_customer_billing.street_address}<br>
                    {/if}

                    {if $order_customer_billing.suburb}
                        {$order_customer_billing.suburb}<br>
                    {/if}

                    {if $order_customer_billing.postcode}
                        {$order_customer_billing.postcode}{$order_customer_billing.city}<br>
                    {/if}

                    {if $order_customer_billing.state}
                        {$order_customer_billing.state}<br>
                    {/if}

                    {if $order_customer_billing.country}
                        {$order_customer_billing.country}<br>
                    {/if}
                </div>
            </div>
            <div class="span3">
                {if is_shipping_method}
                    <strong>{$smarty.const.HEADING_SHIPPING_METHOD}</strong>
                    <p>{$order_info.shipping_method}</p>
                {/if}
            </div>
            <div class="clearfix"></div>
            <div class="span3">
                <strong>{$smarty.const.HEADING_PAYMENT_METHOD}</strong>
                {$order_info.payment_method}
            </div>
            <div class="clearfix"></div>
            <h4>{$smarty.const.HEADING_BILLING_INFORMATION}</h4>
            {if $is_multi_tax_groups}
                <h5>{$smarty.const.HEADING_PRODUCTS}</h5>
                <h5>{$smarty.const.HEADING_TAX}</h5>
                <h5>{$smarty.const.HEADING_TOTAL}</h5>
            {else}
                <h5>{$smarty.const.HEADING_PRODUCTS}</h5>
            {/if}

            <table class="table table-bordered">
                {section name=current loop=$order_products}
                    <tr>
                    <td>{$order_products[current].qty}</td>
                    <td>{$order_products[current].name}</td>


                    {if $order_products[current].attributes}
                    <td>
                        {assign var=attlist value=$order_products[current].attributes}
                        {foreach from=$attlist key=key item=attr name=attrlister}
                            {$attr.option}: {$attr.value}
                        {/foreach}
                    </td>
                    {/if}

                    {if $historyattributeslist}
                    <td>
                        {section name=attr_current loop=$historyattributeslist}
                            {$historyattributeslist[attr_current].option}: {$historyattributeslist[attr_current].value}
                        {/section}
                    </td>
                    {/if}


                        {if $is_multi_tax_groups}
                        <td>
                            {$order_products[current].tax}
                        </td>
                        {/if}

                    <td>
                    {math equation="x * (y / 100 + 1)" x=$order_products[current].final_price y=$order_products[current].tax format="%.2f"} {$order_info.currency|replace:"EUR":"&euro;"}
                    </td>
                    </tr>
                {/section}
            </table>
            <h4>{$smarty.const.HEADING_ORDER_HISTORY}</h4>
            <p>
                {foreach from=$orderhistorylist key=key item=history name=historylister}
                    {$history.date_added}&nbsp;{$history.orders_status_name}{$history.comments}
                {/foreach}
                {$DOWNLOADS_HTML}
            </p>
            <div>
                <table class="table table-bordered">
                    {section name=current loop=$order_totals}
                    <tr>
                        <td>{$order_totals[current].title}</td>
                        <td>{$order_totals[current].text}</td>
                    </tr>
                    {/section}
                </table>
            </div>
        </div>
    </div>
</section>