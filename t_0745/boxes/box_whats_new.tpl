<div class="carousel-inner">
    {foreach from=$arr_whatsnewbox.products key=key item=product name=newproducts}
    {if $product.image}
        {php}
            $product = $this->get_template_vars('product');
            $pattern_src = '/src="([^"]*)"/';
            preg_match($pattern_src, $product['image'], $matches);
            $src = $matches[1];
            $this->assign('src', $src);
        {/php}
    {else}
        {assign var=src value=$imagepath|cat:"/empty-album.png"}
    {/if}
    {if $smarty.foreach.newproducts.index >= 2 && $smarty.foreach.newproducts.index % 3 == 0}</div>{/if}

{if $smarty.foreach.newproducts.index % 3 == 0}
<div class="item {if $key === 1} active{/if}">{/if}
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <a href="{$product.link}" title="{$product.name}">
                        <div class="width-image-product" style="background-image: url('{$src}');"></div>
                    </a>

                    <h2>{$product.preisneu|regex_replace:"/[()]/":""}</h2>

                    <p>
                        <a href="{$product.link}" title="{$product.name}">
                            {$product.name|truncate:35:"...":true}
                        </a>
                    </p>
                    <a href="{$product.buy_now_link}" class="btn btn-default add-to-cart">
                        <i class="fa fa-shopping-cart"></i>{$smarty.const.IMAGE_BUTTON_IN_CART}
                    </a>
                </div>
            </div>
        </div>
    </div>
    {if $smarty.foreach.newproducts.last}</div>{/if}
{/foreach}
</div>
