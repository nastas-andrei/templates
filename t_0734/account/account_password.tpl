<section class="container row">
    <div class="col-md-8">
        {if $is_account_password_message}
          <p class="text-center">{$account_password_message}</p>
        {/if}               
        <!-- button back -->               
        <div class="row">
            <div class="pull-left shopping"> 
                <a class="btn btn-primary" href="{$account_href}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</a>
                                                
            </div>
        </div>
        <!-- button back end --> 
        <div class="row">
            {$account_password_form}
                <div class="inputHLDbox">
                    <div class="inputHLDhead">{$smarty.const.MY_PASSWORD_TITLE}</div>
                    <div class="inputHLDinnerChangePass">
                        {*******************PASSWORD CURRENT****************}
                        <div class="inputHLD">
                            <div class="inputTXT" style="white-space: nowrap;">{$smarty.const.ENTRY_PASSWORD_CURRENT}
                                <small style="color:red">
                                {if $entry_password_current_text}
                                    {$smarty.const.ENTRY_PASSWORD_CURRENT_TEXT}
                                {/if}
                                </small>
                            </div>
                            {$password_current_field}
                        </div>
                        {*******************PASSWORD NEW****************}
                        <div class="inputHLD">
                            <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD_NEW}
                                <small style="color:red">
                                    {if $entry_password_new_text}
                                        {$smarty.const.ENTRY_PASSWORD_NEW_TEXT}
                                    {/if}
                                </small>
                            </div>
                                {$password_new_field}
                        </div>
                        {*******************PASSWORD CONFIRMATION****************}
                            <div class="inputHLD">
                                <div class="inputTXT">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}
                                <span style="color:red">
                                    {if $entry_password_confirmation_text}
                                        {$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}
                                    {/if}
                                </span>
                                </div>
                                {$password_confirmation_field}
                            </div>  
                    </div>
                    <div class="clearer"></div>
                        <div class="inputHLD" style="float:right">
                            <button type="reset" class="btn btn-primary"><i class="fa fa-trash-o"></i></button>
                            <button type="submit" class="btn btn-primary">{$smarty.const.IMAGE_BUTTON_CONTINUE}&nbsp;<i class="fa fa-arrow-circle-right"></i></button>   
                        </div>
                    <div class="clearer"></div>
                </div>
            </form>
        </div>
    </div>
</section>