<!DOCTYPE HTML>
<html>
<head>
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/bootstrap-select.min.css">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link href="{$templatepath}lib/css/fhmm.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/bbpress.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/simister.css" rel="stylesheet" media="screen">
    <link href="{$templatepath}lib/css/prettyPhoto.css" rel="stylesheet" media="screen">
    <link href="http://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300,200,500,100,600,700,800,900" rel="stylesheet">
    <link href="http://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic">

    <link href="{$templatepath}custom.css" rel="stylesheet">

    <link href="http://fonts.googleapis.com/css?family=Pacifico">
    <link href="http://fonts.googleapis.com/css?family=Nothing+You+Could+Do">
    <link rel="stylesheet" type="text/css" href="{$templatepath}lib/css/settings.css" media="screen" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{$templatepath}lib/js/html5shiv.js"></script>
    <script src="{$templatepath}lib/js/respond.min.js"></script>
    <![endif]-->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{$templatepath}lib/js/categories.js"></script>
    <script type="text/javascript" src="{$templatepath}lib/js/bootstrap-select.js"></script>
    <script src="{$templatepath}lib/js/bootstrap.js"></script>
    <script src="includes/general.js"></script>
    <script src="{$templatepath}lib/js/modernizr.custom.js"></script>
</head>
<body>
<div class="animationload"><div id="intro"></div></div>
<div class="wrapper">
    <section class="topbar topbar-blue clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 pull-right">
                    {*<div class="social clearfix">*}
                        {*<ul>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook" title="" href="#"><i class="fa fa-facebook"></i></a></li>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Google Plus" title="" href="#"><i class="fa fa-google-plus"></i></a></li>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter" title="" href="#"><i class="fa fa-twitter"></i></a></li>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Youtube" title="" href="#"><i class="fa fa-youtube"></i></a></li>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin" title="" href="#"><i class="fa fa-linkedin"></i></a></li>*}
                            {*<li><a data-placement="bottom" data-toggle="tooltip" data-original-title="Skype" title="" href="#"><i class="fa fa-skype"></i></a></li>*}
                        {*</ul>*}
                    {*</div>*}
                    <ul class="nav nav-pills">
                        {include file="boxes/box_languages.tpl"}
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="callus clearfix">
                        <ul>
                            <li>
                                <p>
                                    <a href="mailto:{$smarty.const.HEAD_REPLY_TAG_ALL}">
                                        <i class="fa fa-envelope"></i>
                                        {$smarty.const.HEAD_REPLY_TAG_ALL}
                                    </a>
                                </p>
                            </li>
                            <li><p>
                                    <a href="{$domain}/login.php">
                                        <i class="fa fa-user"></i>
                                        {$arr_loginbox.title}
                                    </a>
                                    {if $arr_loginbox.items}
                                        <a href="{$arr_loginbox.items.4.target}" title="{$arr_loginbox.items.4.text}">| {$arr_loginbox.items.4.text}</a>
                                    {/if}
                                </p>
                            </li>
                            <li><p>
                                    <a href="{$arr_shopping_cart.cart_link}">
                                        <i class="fa fa-shopping-cart"></i>
                                        {$arr_shopping_cart.count_contents} - {$arr_shopping_cart.gesamtsumme}
                                    </a>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <header class="header version-center clearfix">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-9 col-sm-9">
                    <div class="logo-wrapper clearfix">
                        <div class="logo">
                            {$cataloglogo}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-3">
                    <div class="header_search clearfix">
                        {include file="boxes/box_search.tpl"}
                    </div>
                </div>
            </div>
        </div>
        <div id="categories-box"></div>
    </header>
    {if $breadcrumb_array|@count> 1}
        <section class="post-wrapper-top dm-shadow clearfix">
            <div class="container">
                <div class="col-lg-12">
                    {assign var=last value=$breadcrumb_array|@end}
                    <h2>{$last.title}</h2>
                    <ul class="breadcrumb pull-right">
                        {foreach from=$breadcrumb_array key=key item=crumb}
                            {assign var=last value=$breadcrumb_array|@end}
                            {if $last.title == $crumb.title}
                                <li>{$crumb.title}</li>
                            {else}
                                <li><a href="{$crumb.link}">{$crumb.title}</a></li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            </div>
        </section>
    {/if}
