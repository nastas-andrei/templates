{if $arr_modul_productlisting.products}
{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    <div class="pager">
        <p class="amount">
            {$arr_modul_productlisting.count_products_info|strip_tags:false}
        </p>
        {if $arr_modul_productlisting.pagination|@count > 1}
            <div class="pages">
                <strong>Page:</strong>
                <ol>
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <li class="current">
                                {$page.text}
                                {elseif $page.previous}
                            <li>
                                <a class="previous i-previous" href="{$page.link}" title="{$page.title}">
                                    <img src="{$templatepath}images/pager_arrow_left.gif" alt="Previous" class="v-middle">
                                </a>
                                {elseif $page.next}
                            <li>
                                <a class="next i-next" href="{$page.link}" title="{$page.title}">
                                    <img src="{$templatepath}images/pager_arrow_right.gif" alt="Next" class="v-middle">
                                </a>
                                {else}
                            <li>
                            <a href="{$page.link}" title="{$page.title}">
                                {$page.text}
                            </a>
                        {/if}
                        </li>
                    {/foreach}
                </ol>
            </div>
        {/if}
    </div>
    {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
        <div class="featured col-lg-3 col-md-3 col-sm-12 col-xs-12">
            {if $product.image}
                {php}
                    $product = $this->get_template_vars('product');
                    $pattern_src = '/src="([^"]*)"/';
                    preg_match($pattern_src, $product['image'], $matches);
                    $src = $matches[1];
                    $this->assign('src', $src);
                {/php}
            {else}
                {assign var=src value=$imagepath|cat:"/empty-album.png"}
            {/if}
            <a href="{$product.link}" class="link-p">
                <div class="image_view_product_list" style="background-image: url('{$src}')"></div>
            </a>

            <div class="wrapper-hover" style="width: 280px">
                <div class="product-price">
                    <div class="price-box">
                      <span class="regular-price" style="color: rgb(34, 34, 34);">
                        <span class="price" style="color: rgb(34, 34, 34);"> {$product.newprice|regex_replace:"/[()]/":""}</span>
                      </span>
                    </div>
                </div>
                <div class="product-name">
                    <div class="clearfix">
                        <a class="icon_cart_title" href="{$product.link}" onclick="location.href='{$product.link}'" title="{$product.name}">
                            {$product.products_name|truncate:40:"...":true}
                        </a>

                        <div class="icon_cart_rollover">
                            <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" onclick="location.href='{$product.buy_now_link}'"></button>
                            <span class='ajax_loader182' style='display:none'>
                              <img src='/templates/t_0737/images/ajax-loader.gif' />
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    {/foreach}
    <div class="clearfix"></div>
    <div class="pager">
        <p class="amount">
            {$arr_modul_productlisting.count_products_info|strip_tags:false}
        </p>
        {if $arr_modul_productlisting.pagination|@count > 1}
            <div class="pages">
                <strong>Page:</strong>
                <ol>
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <li class="current">
                                {$page.text}
                                {elseif $page.previous}
                            <li>
                                <a class="previous i-previous" href="{$page.link}" title="{$page.title}">
                                    <img src="{$templatepath}images/pager_arrow_left.gif" alt="Previous" class="v-middle">
                                </a>
                                {elseif $page.next}
                            <li>
                                <a class="next i-next" href="{$page.link}" title="{$page.title}">
                                    <img src="{$templatepath}images/pager_arrow_right.gif" alt="Next" class="v-middle">
                                </a>
                                {else}
                            <li>
                            <a href="{$page.link}" title="{$page.title}">
                                {$page.text}
                            </a>
                        {/if}
                        </li>
                    {/foreach}
                </ol>
            </div>
        {/if}
    </div>
    <!-- ****************************************  -->
{/if}


