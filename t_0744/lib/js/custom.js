var revapi;
jQuery(document).ready(function () {
    revapi = jQuery('.tp-banner').revolution(
        {
            delay: 9000,
            startwidth: 1180,
            startheight: 500,
            hideThumbs: 10,
            hideCaptionAtLimit: 650,
            hideAllCaptionAtLimit: 400
        });

    $('.popular, .best-sellers, .groom').bxSlider({
        slideWidth: 275,
        minSlides: 1,
        maxSlides: 4
    });
    //$('.groom').bxSlider({
    //    slideWidth: 121,
    //    minSlides: 2,
    //    maxSlides: 3
    //});
    jQuery("a[rel^='prettyPhoto']").prettyPhoto({animationSpeed: 'slow', theme: 'dark_rounded', slideshow: false, overlay_gallery: false, social_tools: false, deeplinking: false});
});