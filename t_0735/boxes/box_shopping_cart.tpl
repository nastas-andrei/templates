{if $arr_shopping_cart}	
	<div class="box-shopping-cart">
		<div class="box-shopping-cart-body text-right">
			<p class="box-title">{$arr_shopping_cart.title}: ({$arr_shopping_cart.count_contents})</p>     						
			<p class="shopping-cart-ammount"><a href="{$arr_shopping_cart.cart_link}">{$arr_shopping_cart.cart_summe}: ({$arr_shopping_cart.gesamtsumme})</a></p>
		</div>
	</div>	
{/if} 