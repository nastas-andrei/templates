<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{$meta_title}</title>
    <meta name="keywords" content="{$meta_keywords}"/>
    <meta name="description" content="{$meta_description}"/>
    <meta name="robots" content="index, follow"/>
    {$headcontent}
    {if $FAVICON_LOGO && $FAVICON_LOGO != ''}
        <link rel="shortcut icon" href="{$FAVICON_LOGO}"/>
    {/if}
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"/>
    <link href="{$templatepath}custom.css" rel="stylesheet">
    <link href="{$templatepath}lib/css/smoothproducts.css" rel="stylesheet">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="{$templatepath}lib/mootools-core-1.4.js"></script>
</head>
<body>
<header>
    <div class="container">
        <div class="row">
            <div class="col-xs-3" style="min-width:200px"><!-- Logo -->
                <div class="well logo">
                    {$cataloglogo}
                </div>
            </div>
            <!-- End Logo -->
            <div class="col-xs-12 col-md-6"><!-- Search -->
                <div class="well">
                    {include file="boxes/box_search.tpl"}
                </div>
            </div>
            <!-- Search end-->
            <!-- Shopping Cart-->
            <div class="col-xs-12 col-md-3">
                <div class="well">
                    {include file="boxes/box_shopping_cart.tpl"}
                </div>
            </div>
            <!-- Shopping Cart end -->
        </div>
        <!-- row end -->
    </div>
    <!-- container -->
</header>
<!-- Navigation -->
<nav class="navbar navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header"><!-- collapse menu -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand visible-xs" href="index.php">Mimity Online Shop</a><!-- text logo on mobile view -->
        </div>
        <!-- collapse menu end-->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{$domain}" class="menu-item">{$smarty.const.HEADER_TITLE_HOME}</a></li>
                <li><a href="{$arr_shopping_cart.cart_link}" class="menu-item">{$arr_shopping_cart.title|strip_tags:false}</a></li>
                {include file="boxes/box_loginbox.tpl"}
            </ul>
            <ul class="nav navbar-nav pull-right">
                {include file="boxes/box_languages.tpl"}
            </ul>
        </div>
    </div>
</nav>
<!-- End Navigation -->
<div class="container main-container">

