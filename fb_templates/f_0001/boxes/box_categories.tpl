{if $categories_tree}
<div class="box box-categories">    
	<div class="box-heading">
		<h3><a href="{$home_link}" style="text-decoration: none"><i class="icon-home"></i></a>&nbsp;&nbsp;{$arr_categorybox.title}</h3>
	</div>
	<ul class="nav nav-tabs nav-stacked">
	{php}
	function categories_rec($categories) {		
		foreach ($categories as $item) {			
			echo '<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href=' .$item[link]. '>' . $item['name'] . '</a>';

			if ($item['subcategories']) {
				echo '<ul class="dropdown-menu" role="menu">';		
				categories_rec($item['subcategories']);
				echo '</ul>';		
			}
			echo '</li>';
		}		
	}
	categories_rec($this->get_template_vars('categories_tree'));
	{/php}
</ul>				
</div>
{/if}