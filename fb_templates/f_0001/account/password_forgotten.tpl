<div class="account-container">
{$password_forgotten_form}
<div class="form-horizontal">
<a href="{$LOGIN_URL}"><i class="icon-caret-left"></i> {$smarty.const.IMAGE_BUTTON_BACK}</a>
<fieldset>
  {if $password_forgotten_stack_size}

	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	  	{$password_forgotten_message}
	</div>


  {/if}        
  {$smarty.const.TEXT_MAIN}
	<div class="control-group">
		<label for="" class="control-label">
			{$smarty.const.ENTRY_EMAIL_ADDRESS}			
		</label>
		<div class="controls">
			{$email_address_input}			
  			<input type="submit" class="btn btn-primary" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">	
		</div>
	</div>
</fieldset>

</div>
</form>    
</div>