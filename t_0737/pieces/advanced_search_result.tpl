
    <div class="container">
        <div class="row-fluid">
            {if $arr_modul_productlisting.products|@count lt 1}
                <p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}</p>
                <button type="button" class="button pull-left" onclick="window.location='javascript:history.go(-1)';">
		        <span>
		          <span>&larr;&nbsp;&nbsp;{$smarty.const.IMAGE_BUTTON_BACK}</span>
		        </span>
                </button>
            {/if}

            {if $is_search_message}
                <p class="text-center">{$search_message}</p>
            {/if}

            {$PRODUCT_LISTING_HTML}
        </div>
    </div>