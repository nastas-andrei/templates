<ol class="steps">
    <li class="step1 done"><span><a href="{$arr_shopping_cart.shoppingcart_link}">{$smarty.const.BOX_HEADING_SHOPPING_CART}</a></span></li>
    <li class="step2 current"><span><a href="{$checkout_shipping_href}">{$smarty.const.CHECKOUT_BAR_DELIVERY}</a></span></li>
    <li class="step3"><span>{$smarty.const.CHECKOUT_BAR_PAYMENT}</span></li>
    <li class="step4"><span>{$smarty.const.CHECKOUT_BAR_CONFIRMATION}</span></li>
</ol>


{$checkout_address_form}

	{if $checkout_address_exists}        		      			
		{$checkout_address_message}
	{/if}      
		
		{if !$process}  
            <div class="panel panel-info" style="margin-top:20px">
                <div class="panel-heading">
                    <h3 class="panel-title">{$smarty.const.TABLE_HEADING_SHIPPING_ADDRESS}</h3>
                </div>
                <div class="panel-body">
                    {$smarty.const.TEXT_SELECTED_SHIPPING_DESTINATION}				
					{$smarty.const.TITLE_SHIPPING_ADDRESS}
					<address>
						{$sendto_address_label}
					</address>

					{if $addresses_count > 1}
						{$smarty.const.TABLE_HEADING_ADDRESS_BOOK_ENTRIES}
						{$smarty.const.TEXT_SELECT_OTHER_SHIPPING_DESTINATION}
						{$smarty.const.TITLE_PLEASE_SELECT}	

						{section name=current loop=$addresseslist}              							
					<address>
						<hr>
						{if $addresseslist[current].is_sendto}
							<tr id="defaultSelected" class="moduleRowSelected" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})">
						{else}
							<tr class="moduleRow" onmouseover="rowOverEffect(this)" onmouseout="rowOutEffect(this)" onclick="selectRowEffect(this, {$addresseslist[current].radio_buttons})">
						{/if}
			        	{$addresseslist[current].address_radio}

				        {$addresseslist[current].firstname} 
				        {$addresseslist[current].lastname}			        
				        {$addresseslist[current].address_format}
			    	</address>    								
						{/section}								
						
					{/if}
                </div>
            </div>
		{/if}


	{if $addresses_count < $smarty.const.MAX_ADDRESS_BOOK_ENTRIES}
        <div class="panel panel-info" style="margin-top:20px">
            <div class="panel-heading">
                <h3 class="panel-title">{$smarty.const.TABLE_HEADING_NEW_SHIPPING_ADDRESS}</h3>
            </div>
            <div class="panel-body">
                <p class="text-center">{$smarty.const.TEXT_CREATE_NEW_SHIPPING_ADDRESS}</p>

				<div class="col-xs-6 offset2">
					{$CHECKOUT_NEW_ADDRESS_HTML|replace:'Anrede:':''}	
				</div>
            </div>
        </div>
	{/if}
		<div class="text-center">
		{$action_hidden}
			<input type="submit" class="btn btn-success" value="{$smarty.const.IMAGE_BUTTON_CONTINUE}">
		  	{if $process}
				<a href="{$checkout_shipping_address_href}" class="btn-custom btn-custom-small">{$smarty.const.IMAGE_BUTTON_BACK}</a>	      	
			{/if}	     
		</div>	
</form>