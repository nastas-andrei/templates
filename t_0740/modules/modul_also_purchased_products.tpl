{if $arr_modul_also_purchased_products}
    <div class="carousel_wrapper">
        <div class="title">
            <h2>{$arr_modul_also_purchased_products.title}</h2>
        </div>
        <div class="margin-top">
            <div id="owl_shop_carousel_also" class="owl-carousel">
                {foreach from=$arr_modul_also_purchased_products.products key=key item=product}
                    <div class="shop_carousel">
                        <div class="shop_item">
                            <div class="entry">
                                {if $product.image}
                                    {php}
                                        $product = $this->get_template_vars('product');
                                        $pattern_src = '/src="([^"]*)"/';
                                        preg_match($pattern_src, $product['image'], $matches);
                                        $src = $matches[1];
                                        $this->assign('src', $src);
                                    {/php}
                                {else}
                                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                                {/if}
                                <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                                <div class="magnifier" onclick="location.href='{$product.link}'">
                                    <div class="buttons">
                                        <a class="st btn btn-default" rel="bookmark" href="{$product.link}">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a>
                                    </div>
                                </div>
                            </div>
                            <div class="shop_desc">
                                <div class="shop_title pull-left">
                                    <a href="{$product.link}"><span>{$product.name}</span></a>
                                </div><span class="price pull-right">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
{/if}