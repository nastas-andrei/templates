<div class="container row">
    {if empty($custom_heading_title)}
        <h3>{$HEADING_TITLE}</h3>
    {else}
        <h3>{$custom_heading_title}</h3>
    {/if}
    {if $is_search_message}
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {$search_message}
        </div>
    {/if}
    {$advanced_search_form}
    <div class="form-group">
        <input type="text" class="advanced_searchFIELD" name="keywords"/>
        <button type="submit" alt="suchen" class="btn btn-primary advanced-search-button">
            {$smarty.const.IMAGE_BUTTON_SEARCH}
        </button>
    </div>
    <div class="clearfix"></div>
    <div class="form-group">
        <div class="fleft">
            <input type="checkbox" value="1" name="search_in_description" checked="checked" id="1"/>
            Auch in den Beschreibungen suchen
        </div>
        <div class="fleft block1">{$inc_subcat_field}{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}</div>
    </div>
    <div class="fleft block2">{$smarty.const.ENTRY_CATEGORIES}</div>
    <div class="fleft select-advanced-search">{$categories_id_field}</div>
    <div class="clearer"></div>
    <div class="fleft block2">{$smarty.const.ENTRY_MANUFACTURERS}</div>
    <div class="fleft select-advanced-search">{$manufacturers_id_field}</div>
    <div class="clearer"></div>
    <div class="width-input-advanced-search">
        <div class="fleft block2">{$smarty.const.ENTRY_PRICE_FROM}</div>
        <div class="fleft">{$pfrom_field}</div>
    </div>
    <div class="width-input-advanced-search">
        <div class="fleft block2">{$smarty.const.ENTRY_PRICE_TO}</div>
        <div class="fleft">{$pto_field}</div>
    </div>
    </form>
</div>