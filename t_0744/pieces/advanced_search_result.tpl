<section class="homecolon">
    <div class="container">
        <div id="content" class="single col-lg-12 col-md-12 col-sm-12">
            {if $arr_modul_productlisting.products|@count lt 1}
            <p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}
                <a href="{$arr_searchbox.advanced_search_link}" class="btn btn-default btn-icon">{$arr_searchbox.advanced_search_text}</a>
            </p>
            <a href="javascript:history.go(-1)" class="btn btn-default btn-icon">{$smarty.const.IMAGE_BUTTON_BACK}</a>
            {else}
            {if $is_search_message}
                <p class="text-center">{$search_message}</p>
            {/if}
                <div class="shop-top">
                    <div class="clearfix"></div>
                    {$PRODUCT_LISTING_HTML}
                </div>
            {/if}
        </div>
    </div>
<section>

