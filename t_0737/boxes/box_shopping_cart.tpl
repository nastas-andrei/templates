{if $arr_shopping_cart}
    <div class="shopping_cart">
        <div class="shopping_cart_b">
            <a href="shopping_cart.php" id="open_shopping_cart">
                {$arr_shopping_cart.title|strip_tags:false}:&nbsp;{$arr_shopping_cart.count_contents}
                &nbsp;
                <span class="price">- {$arr_shopping_cart.gesamtsumme}</span>
            </a>
        </div>
        {if $SCRIPT_NAME !== '/shopping_cart.php'}
        <div id="shopping_cart_mini">
            {if $arr_shopping_cart.products}
                <div class="inner-wrapper">
                    {$smarty.const.BOX_SHOPPING_CART_PROD}
                    <div class="height_shopping_cart">
                        <form name="cart_quantity" action="shopping_cart.php?action=update_product" method="post">
                            <ol id="cart-sidebar" class="mini-products-list">
                                {foreach from=$arr_shopping_cart.products key=key item=product}
                                    <li class="item">
                                        <input id="cart-product-{$product.id}" type="checkbox" name="cart_delete[]"
                                               value="{$product.id}"
                                               style="display: none;">
                                        <a href="javascript: del_item_shoppingcart('cart-product-{$product.id}');"
                                           title="Remove This Item"
                                           onclick="return confirm('Are you sure you would like to remove this item from the shopping cart?');"
                                           class="button-delete">
                                            <img src="templates/t_0737/images/shopping_cart_mini_delete_button.png"
                                                 alt="Delete">
                                        </a>
                                        <input type="hidden" name="cart_quantity[]" value="{$product.quantity}" size="4"
                                               maxlength="10" id="cartadd0">
                                        <input type="hidden" name="products_id[]" value="{$product.id}">
                                        <input type="hidden" name="old_qty[]" value="{$product.quantity}">
                                        <a href="{$product.link}" title="{$product.name}" class="product-image">
                                            <span class="scale-with-grid"> {$product.image}</span>
                                        </a>

                                        <div class="product-details">
                                            <a class="product-name" href="{$product.link}">
                                                {$product.name|truncate:30:"...":true}
                                            </a>
                                            <br/>
                                            {$product.quantity} x
                                            <br/>
                                            <span class="price">{$product.final_price}</span>
                                        </div>
                                    </li>
                                {/foreach}
                            </ol>
                        </form>
                    </div>
                    <div class="total">
                        {$arr_shopping_cart.cart_summe}: <span class="price">{$arr_shopping_cart.gesamtsumme}</span>
                    </div>
                    <div class="wrapper">
                        <a href="shopping_cart.php" title="View Cart"
                           class="button">{$arr_shopping_cart.title|strip_tags:false}</a>
                        <a href="login.php" title="{$arr_topmenu.5.name}" class="button">{$arr_topmenu.5.name}</a>
                    </div>
                </div>
            {else}
                <p class="empty">
                    {$smarty.const.BOX_SHOPPING_CART_EMPTY}
                </p>
            {/if}
        </div>
        {/if}
    </div>
{/if} 
