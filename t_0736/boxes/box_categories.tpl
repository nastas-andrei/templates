<div id="sns_mainnav" class="span9">
    <div id="sns_megamenu_menu537c4f6ab159d" class="container sns-megamenu-wrap horizontal-menu using-effect">
        <ul class="mainnav mega-nav">
            {foreach from=$categories key=key item=category name=catlist}
                <li class="level0 parent">
                    <a href="{$category.link}" id="{$category.id}">
                        <span class="menu-title">{$category.name}</span>
                    </a>
                    {if $category.children|@count > 0}
                        <div class="megamenu-col megamenu-12col mega-content-wrap have-spetitle level0"
                             style="margin-right:0">
                            {foreach from=$category.children key=key item=categorySub name=catlist_sub1}
                                <div class="megamenu-col have-spetitle level1 ">
                                    <div class="mega-title">
                                        <a href="{$categorySub.link}" >
                                            <span>{$categorySub.name}</span>
                                        </a>
                                    </div>
                                    {if $categorySub.children|@count > 0}
                                        {foreach from=$categorySub.children key=key item=categorySub2 name=catlist_sub2}
                                            <div class="megamenu-col megamenu-12col have-spetitle level2 ">
                                                <div class="megamenu-col megamenu-12col level3 ">
                                                    <div class="mega-title">
                                                        <a href="{$categorySub2.link}">
                                                            <span>{$categorySub2.name}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        {/foreach}
                                    {/if}
                                </div>
                            {/foreach}
                        </div>
                    {/if}
                </li>
            {/foreach}
        </ul>
    </div>
    <div id="sns_resmenu" class="sns-resmenu menu-sidebar">
        <button class="btn btn-navbar sns-resmenu-sidebar" type="button">
            <i class="fa fa-bars fa-lg"></i>
        </button>
        <ul class="nav resmenu" style="display:none">
            {foreach from=$categories key=key item=category name=catlist_min_sub}
                <li class="level0 nav-1 parent" onmouseover="toggleMenu(this,1)" onmouseout="toggleMenu(this,0)">
                    <a href="{$category.link}">
                        <span>{$category.name}</span>
                    </a>
                    <ul class="level0">
                        {foreach from=$category.children key=key item=category1 name=catlist_min_sub1}
                            <li class="level1 nav-1-1 first parent" onmouseover="toggleMenu(this,1)"
                                onmouseout="toggleMenu(this,0)">
                                <a href="{$category1.link}">
                                    <span>{$category1.name}</span>
                                </a>
                                <ul class="level1">
                                    {foreach from=$category1.children key=key item=category2 name=catlist_min_sub2}
                                        <li class="level2 nav-1-1-1 first">
                                            <a href="{$category2.link}">
                                                <span>{$category2.name}</span>
                                            </a>
                                        </li>
                                    {/foreach}
                                </ul>
                            </li>
                        {/foreach}
                    </ul>
                </li>
            {/foreach}
        </ul>
        {literal}
            <script type="text/javascript">
                $sns_jq(document).ready(function ($) {
                    $('body#bd').addClass('resmenu-sb');
                    $('body#bd #sns_wrapper').append('<nav id="sns_off_screennav" class="wrap"><ul></ul></nav>');
                    $('body#bd #sns_wrapper').append('<div id="sns_off_screennav_overlay"></div>');

                    $('#sns_off_screennav ul').html($('#sns_mainnav ul.resmenu').html());
                    $('#sns_mainnav ul.resmenu').html('');

                    $('.btn.sns-resmenu-sidebar').click(function () {
                        if ($('body#bd').hasClass('on-sidebar')) {
                            $('body#bd').removeClass('on-sidebar');
                        } else {
                            $('body#bd').addClass('on-sidebar');
                        }
                    });
                    eventType = (IS_HANDHELD) ? 'touchstart' : 'click';
                    $('#sns_off_screennav_overlay').bind(eventType, function () {
                        $('body#bd').removeClass('on-sidebar');
                    });
                });
            </script>
        {/literal}
    </div>
</div>
