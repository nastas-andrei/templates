{if $arr_modul_productlisting.products}
    <div id="product_list" class="m_top_10 list_grid v_list col-xs-12 col-md-9">
        <div class="sort_shop txt-1 clearfix no_bord m_top_20">
            <div class="fleft clearfix">
                {if $manufacturer_select}
                    {$manufacturer_select}
                {/if}
                <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink"/>
                <input type="hidden" value="{$max}" id="max" name="max"/>
                <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template"/>
                <select name="sortierung" id="sortierung" onchange="goHin(this)">
                    {foreach from=$arr_sort_new key=key item=sort}
                        {if $sort.class == "p"}
                            {if $sort_select eq $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {else}
                            {if $sort_select == $sort.id}
                                <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                            {else}
                                <option value="{$sort.id}">{$sort.text}</option>
                            {/if}
                        {/if}
                    {/foreach}
                </select>
            </div>
            <div id="navigation" class="navigation_grid">
                {if $standard_template == "liste"}
                    <a class="btn-tags hasTooltip" href="{$templ_link}" title="Grid" data-original-title="Grid">Grid<i class="fa fa-th"></i></a>
                {/if}
                <a class="active btn-tags hasTooltip" href="#" title="List" data-original-title="List">List<i class="fa fa-th-list"></i></a>
            </div>
        </div>
        <div class="clearfix sort_shop txt-1">
            <span class="fleft">{$arr_modul_productlisting.count_products_info}</span>

            <div class="v_block-control fright">
                <div class="control-post">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <a href="javascript:void(0)" class="active">{$page.text}</a>
                        {elseif $page.previous}
                            <a class="prev-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        {elseif $page.next}
                            <a class="next-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        {else}
                            <a href="{$page.link}" title="{$page.title}">{$page.text}</a>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
        <div class="poduct_wrap">
            <ul class="row v_index">
                {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
                    {if $product.image}
                        {php}
                            $product = $this->get_template_vars('product');
                            $pattern_src = '/src="([^"]*)"/';
                            preg_match($pattern_src, $product['image'], $matches);
                            $src = $matches[1];
                            $this->assign('src', $src);
                        {/php}
                    {else}
                        {assign var=src value=$imagepath|cat:"/empty-album.png"}
                    {/if}
                    <li class="col-xs-12 m_bottom_20">
                        <div class="block-product">
                            <figure class="box-product">
                                <a href="{$product.name_link}" class="img-product">
                                    <div class="preview-image-product" style="background: url('{$src}')"></div>
                                    {*<img src="http://velikorodnov.com/html/reviver/img/img-product-5.jpg" alt="">*}
                                </a>
                            </figure>
                            <div class="v_space2 v_list_block">
                                <a href="{$product.name_link}" class="m_bottom_15 txt_trans">{$product.products_name}</a>

                                <div class="clearfix"></div>
                                <span class="v_price">{$product.newprice}</span>

                                <div class="clearfix"></div>
                                <hr class="m_bottom_15 m_t_no_space">
                                <table class="description_table m_bottom_15 txt-2">
                                    <tbody>
                                    <tr>
                                        <td>
                                            Aliquam erat volutpat. Duis ac turpis. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna.
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <hr class="m_bottom_15 m_t_no_space">
                                <div class="d_inline_b">
                                    <button type="button" class="btn-cart btn-small btn-blue" onclick="window.location='{$product.buy_now_link}';">
                                        <strong>{$smarty.const.IMAGE_BUTTON_IN_CART}</strong>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
        <div class="clearfix sort_shop txt-1 m_t_no_space">
            <span class="fleft">{$arr_modul_productlisting.count_products_info}</span>

            <div class="v_block-control fright">
                <div class="control-post">
                    {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                        {if $page.active}
                            <a href="javascript:void(0)" class="active">{$page.text}</a>
                        {elseif $page.previous}
                            <a class="prev-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        {elseif $page.next}
                            <a class="next-post control-btn btn-3 v_button_var" href="#">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        {else}
                            <a href="{$page.link}" title="{$page.title}">{$page.text}</a>
                        {/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
{/if}