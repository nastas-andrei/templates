var img = new Asset.images(['templates/0_source/lib/tooltip/progress.gif']),ti,_rl,_f = function(){window.clearTimeout(ti);_rl.send();};
window.addEvent('domready', function(){
    document.getElements('a.fox-rating').addEvents({
        mouseenter: function(e){
            e.stopPropagation();
            _t = ToolTip.instance(this, {autohide:true, hideDelay:200});
            _rl = new Request({
                url: '/foxrate_request.php',
                async: false,
                link: 'ignore',
                method: 'get',
                evalScripts: false,
                data: {product_id: $(this).get('rel')},
                onFailure: function(){
                    _t.set(new Element('p[style="font-weight:bold"]').adopt(document.newTextNode('Connection failed!')));
                    _t.show();
                },
                onRequest: function(){
                    _t.set(new Element('p').adopt(new Element('img[alt="Wird geladen..."][src="'+img.get('src')+'"]')));
                    _t.show();
                },
                onSuccess: function(t){
                    _t.set(t);
                    _t.toolTip.getElement('div.frStars-txt > a').set('href',$(this).get('href'));
                }.bind(this)
            });
            ti = _f.delay(700);
        },
        mouseleave: function(){
            window.clearTimeout(ti);
        }
    });
});
// Callback after Widget is ready @Page:Products_info
var FoxrateProductCallback = function(){
  $$('.frRating-bottom').removeClass('hide');
}