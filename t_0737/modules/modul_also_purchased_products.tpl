{if $arr_modul_also_purchased_products}
    <div class="product-collateral">
        <div class="box-collateral box-up-sell">
            <h2>{$arr_modul_also_purchased_products.title}</h2>

            <div class="products-grid" id="upsell-product-table">
                <div style="clear: both;">
                    {foreach from=$arr_modul_also_purchased_products.products key=key item=product}
                        {if $product.image}
                            {php}
                                $product = $this->get_template_vars('product');
                                $pattern_src = '/src="([^"]*)"/';
                                preg_match($pattern_src, $product['image'], $matches);
                                $src = $matches[1];
                                $this->assign('src', $src);
                            {/php}
                        {else}
                            {assign var=src value=$imagepath|cat:"/empty-album.png"}
                        {/if}
                        <div class="four columns product item item_3cols_1 item_2cols_1">
                            <div class="product-image-wrapper">
                                <a href="{$product.link}" title="{$product.name}">
                                    <div class="image_view_product_list" style="background-image:url('{$src}')"></div>
                                    <div class="product-image-wrapper-hover"></div>
                                </a>
                            </div>
                            <div class="wrapper-hover">
                                <div class="product-price">
                                    <div class="price-box">
                                        <span class="regular-price" id="product-price-45-upsell">
                                          <span class="price">{$product.preisneu|regex_replace:"/[()]/":""}</span>
                                        </span>
                                    </div>
                                </div>
                                <div class="product-name">
                                    <div class="clearfix">
                                        <a class="icon_cart_title" href="{$product.link}" title=" {$product.name}">
                                            {$product.name|truncate:40:"...":true}
                                        </a>

                                        <div class="icon_cart_rollover">
                                            <button type="button" title="{$smarty.const.IMAGE_BUTTON_IN_CART}" onclick="location.href='{$product.buy_now_link}'"></button>
                                                  <span class='ajax_loader45' style='display:none'>
                                                    <img src='templates/t_0737/images/ajax-loader.gif' />
                                                  </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
{/if}
