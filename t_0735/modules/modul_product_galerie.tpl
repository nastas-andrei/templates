{if $arr_modul_productlisting.products}

{literal}
    <script type="text/javascript">
        function goHin(das) {
            var slink = document.getElementById('sortlink').value;
            var sort = document.getElementById('sortierung').selectedIndex;
            var wert = das[sort].value;
            var max = document.getElementById('max').value;
            var link = slink + '&sort=' + wert + '&max=' + max;
            location.href = link;
        }
    </script>
{/literal}
    <section class="content-wrapper row" style="margin-bottom:20px">
        <div class="container">
            <div class="row">
                <input type="hidden" value="{$sort_link}" id="sortlink" name="sortlink" />
                <input type="hidden" value="{$max}" id="max" name="max" />
                <input type="hidden" value="{$standard_template}" id="standard_template" name="standard_template" />

                <div class="col-xs-12">
                    <div class="product-order" style="float:right;margin:-60px 15px 20px 20px">
                        <span>{$arr_modul_productlisting.count_products_info}</span>
                        <div class="select-wrapper" style="margin-left:10px">
                            <select name="sortierung" id="sortierung" onchange="goHin(this)" class="country_id">
                                {foreach from=$arr_sort_new key=key item=sort}
                                    {if $sort.class == "p"}
                                        {if $sort_select eq $sort.id}
                                            <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                        {else}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {/if}
                                    {else}
                                        {if $sort_select == $sort.id}
                                            <option value="{$sort.id}" selected="selected">{$sort.text}</option>
                                        {else}
                                            <option value="{$sort.id}">{$sort.text}</option>
                                        {/if}
                                    {/if}
                                {/foreach}
                            </select>
                        </div>

                        <div class="select-wrapper">
                            {if $manufacturer_select}
                                {$manufacturer_select}
                            {/if}
                        </div>
                    </div>
                </div>
                {foreach from=$arr_modul_productlisting.products key=key item=product name=productlistingitems}
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="height:310px;">
                        <div class="product-block">
                            <div class="product-image">
                                {if $product.image}
                                    {php}
                                        $product = $this->get_template_vars('product');
                                        $pattern = '/src="([^"]*)"/';
                                        preg_match($pattern, $product['image'], $matches);
                                        $src = $matches[1];
                                        $this->assign('src', $src);
                                    {/php}
                                {else}
                                    {assign var=src value="http://placehold.it/300x300&text=No%20image"}
                                {/if}
                                <a href="{$product.link}">
                                    <figure class="product-display">
                                        <div class="product-mainpic width-image-product" style="background-image:url('{$src}')"></div>
                                        <div class="product-secondpic width-image-product" style="background-image:url('{$src}')"></div>
                                    </figure>
                                </a>
                            </div>
                            <div class="product-meta">
                                <div class="product-action">
                                    <a href="{$product.buy_now_link}" class="addcart">
                                        <i class="icon-basket"></i>
                                        {$smarty.const.IMAGE_BUTTON_IN_CART}
                                    </a>
                                </div>
                            </div>
                            <div class="product-info">
                                <a href="{$product.link}">
                                    <h5 class="product-name">{$product.products_name}</h5>
                                </a>

                                <div class="product-price">
                                    {if $product.oldprice  > 0}
                                        <span class="oldprice">{$product.oldprice}</span>
                                    {/if}
                                    <span>{$product.newprice|regex_replace:"/[()]/":""}</span>
                                    <span class="newprice">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
            {if $arr_modul_productlisting.pagination|@count > 1}
            <div class="row">
                <div class="col-md-12">
                    <ul class="pagination">
                        {foreach from=$arr_modul_productlisting.pagination key=key item=page}
                            {if $page.previous}
                                <li class="pag-prev">
                                    <a href="{$page.link}" title="{$page.title}">← prev</a>
                                    {elseif $page.active}
                                <li class="active">
                                    <a href="javascript:void(0)">{$page.text}</a>
                                    {elseif $page.next}
                                <li class="pag-next">
                                    <a href="{$page.link}" title="{$page.title}">next →</a>
                                    {else}
                                <li>
                                <a href="{$page.link}" title="{$page.title}">
                                    {$page.text}
                                </a>
                            {/if}
                            </li>
                        {/foreach}
                    </ul>
                </div>
            </div>
            {/if}
        </div>
    </section>
{/if}