<div id="sns_content2" class="wrap">
    <div class="container">
        <div class="row-fluid">
            <div id="sns_main" class="col-xs-12 col-main" data-stablet="span8 col-main">
                <div id="sns_mainmidle" class="span12 clearfix">
                    {if $is_search_message}
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {$search_message}
                        </div>
                    {/if}
                    {$advanced_search_form}
                    <div class="fieldset advanced-search">
                        <h2 class="legend">
                            {if empty($custom_heading_title)}
                                {$HEADING_TITLE}
                            {else}
                                {$custom_heading_title}
                            {/if}
                        </h2>
                        <ul class="form-list" id="advanced-search-list">
                            <li class="odd">
                                <div class="input-box">
                                    <input type="text" class="input-text" name="keywords">
                                </div>
                            </li>
                            <li class="even">
                                <label for="price">{$smarty.const.ENTRY_PRICE_FROM}</label>

                                <div class="input-range">
                                    {$pfrom_field}
                                    <span class="separator" style="float: left;margin: 5px;">-</span>
                                    {$pto_field}
                                </div>
                            </li>
                            <li class="odd">
                                <label for="color">{$smarty.const.ENTRY_CATEGORIES}</label>

                                <div class="input-box">
                                    {$categories_id_field}
                                </div>
                            </li>
                            <li class="odd">
                                <label for="color">{$smarty.const.ENTRY_MANUFACTURERS}</label>

                                <div class="input-box">
                                    {$manufacturers_id_field}
                                </div>
                            </li>
                            <li class="even">
                                <div class="input-box">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                {$inc_subcat_field}{$smarty.const.ENTRY_INCLUDE_SUBCATEGORIES}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="last odd">
                                <div class="input-box">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="1" name="search_in_description"
                                                       checked="checked" id="1">{$smarty.const.TEXT_SEARCH_IN_DESCRIPTION}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="buttons-set">
                        <button type="submit" title="Search" class="button">
                            <span><span>{$smarty.const.IMAGE_BUTTON_SEARCH}</span></span>
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>