{if $products_not_found_message}
    {$products_not_found_message}
{/if}
{if $is_default}
    {if $smarty.const.TEXT_DEFINE_MAIN != ""}
        <section class="flexslider flexslider-main">
            {$user_html}
        </section>
    {/if}
    <div id="content">

        <div class="content-div">
            <div class="container">
                {if $arr_modul_previewproducts}
                    <section class="box-block row extra">
                        {$FILENAME_FEATURED_HTML}
                    </section>
                {/if}
                {if $arr_whatsnewbox}
                    <hr/>
                    <section class="box-block row extra">
                        {include file="boxes/box_whats_new.tpl"}
                    </section>
                {/if}
                {$UPCOMING_PRODUCTS_HTML}
            </div>
        </div>
    </div>
    {if $smarty.const.TEXT_DEFINE_MAIN_BOTTOM != ''}
        {$define_main_bottom}
    {/if}
{/if}
{if $is_products == 1}
    <div class="container">
        <div class="row">
            {*<div class="col-xs-12 col-sm-4 col-md-3">*}
                {*<div class="left-sidebar">*}
                    {*<h2>{$smarty.const.HEADING_TITLE_CAT}</h2>*}

                    {*<div class="shipping text-center"><!--shipping-->*}
                        {*{foreach from=$boxes_pos.right key=key item=boxname name=current}*}
                            {*{if $boxname eq 'htmlbox'}*}
                                {*{if $key == 13}*}
                                    {*{include file="boxes/box_$boxname.tpl"}*}
                                {*{/if}*}
                            {*{/if}*}
                        {*{/foreach}*}
                    {*</div>*}
                {*</div>*}
            {*</div>*}
            <div class="padding-right">
                {$smarty.const.HEADING_TITLE}

                {if $filterlistisnotempty}
                    {$filter_form}
                    {$smarty.const.TEXT_SHOW}
                {/if}

                {if !empty($categories_html)}
                    {$categories_html}
                {/if}

                {if !empty($manufacturers_html_header)}
                    {$manufacturers_html_header}
                {/if}

                {if !empty($PRODUCT_LISTING_HTML)}
                    {$PRODUCT_LISTING_HTML}
                {/if}

                {if !empty($manufacturers_id)}
                    {$manufacturers_html_footer}
                {/if}

                {if !empty($categories_html_footer)}
                    {$categories_html_footer}s
                {/if}
                <article class="col-xs-3 right-col">
                    <div class="col-left">
                        <div class="block-widget v_space category">
                            <h4 class="heading-h4 m_bottom_15">{$smarty.const.BOX_HEADING_BESTSELLERS}</h4>
                            {include file="boxes/box_best_sellers.tpl"}
                        </div>

                    </div>
                </article>
            </div>
        </div>
    </div>
{else}
    {if $is_default}
        &nbsp;
    {else}
        <p class="block">{$tep_image_category}</p>
        <p class="no-products text-center">{$smarty.const.TEXT_NO_PRODUCTS}</p>
    {/if}
{/if}