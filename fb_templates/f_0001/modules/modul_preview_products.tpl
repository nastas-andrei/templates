{if $arr_modul_previewproducts.products}  
 <div id="items-listing">

      <div class="products-listing">
      {foreach from=$arr_modul_previewproducts.products key=key item=product name=productpreviewitems}

        <div class="media">  
       
                  {if $product.image} 
                  {php}
                   {$product = $this->get_template_vars('product');}
                   {$pattern_src = '/src="([^"]*)"/';}
                   {$pattern_width = '/width="([^"]*)"/';}
                   {$pattern_height = '/height="([^"]*)"/';}
                   {preg_match($pattern_src, $product['image'], $matches);}
                   {$src = $matches[1];}
                   {preg_match($pattern_width, $product['image'], $matches);}
                   {$width = $matches[1];}
                   {preg_match($pattern_height, $product['image'], $matches);}
                   {$height = $matches[1];}
                   {$this->assign('src', $src);}
                   {$this->assign('img_width', $width);}
                   {$this->assign('img_height', $height);}
                  {/php}
                  {else}
                    {assign var=src value=$imagepath|cat:"/empty-album.png"}
                  {/if}

                  
                    <a href="{$product.link}" class="pull-left">                      
                      <div class="img-canvas" style="width:168px;height:168px;display:block;background-position:{$smarty.const.FACEBOOK_THUMBNAIL_POSITION};background-image:url('{$src}');background-size:cover;filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='{$src}', sizingMethod='scale');"></div>
                    </a>
                  
        
                <div class="media-body">       
                  
                    <a href="{$product.link}" class="media-heading">
                      {$product.name|truncate:150:"...":true}
                    </a>
                     <p class="product-category">&bull; {$product.manufacturers_name}</p>
                    <p class="product-price">
                      {if $product.preisalt  > 0}
                        <span class="product-old-price">{$product.preisalt}</span>
                       {/if}   
                        <span class="product-new-price">{$product.preisneu|regex_replace:"/[()]/":""}</span>  
                        <span class="product-new-price" style="display:block">{$product.base_price|regex_replace:'/<br.?>/':''}</span>
                    </p>
                    <p>
                      <div class="fb-like" data-href="{$product.link}" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>
                    </p>                                                                   
                </div>
        </div>
      {/foreach}
      </div> 
</div>
{/if}