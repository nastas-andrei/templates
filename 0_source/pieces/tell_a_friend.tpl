{$email_friend_form}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}	
		{include file="pieces/page_title_section.tpl"}
	    {$smarty.const.BETWEEN_HL_CONTENT}
		<table border="1" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td class="pageContentTable">
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
	     	<tr>
	        	<td>
					{include file="pieces/page_title_section.tpl"}
				</td>
	      	</tr>
	      {$smarty.const.BETWEEN_HL_CONTENT}
	{/if}
		<tr>
        	<td class="cont_abst">
        		<table border="0" width="100%" cellspacing="0" cellpadding="0">
					{if $is_friend_message}
      				<tr>
        				<td>{$friend_message}</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
					{/if}
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="0" cellpadding="2">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td class="main"><b>{$smarty.const.FORM_TITLE_CUSTOMER_DETAILS}</b></td>
                								<td class="inputRequirement" align="right"></td>
              								</tr>
            							</table>
            						</td>
          						</tr>
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              								<tr>
                								<td>
                									<table border="0" cellspacing="0" cellpadding="2">
                  										<tr>
                    										<td class="main">{$smarty.const.FORM_FIELD_CUSTOMER_NAME}</td>
                    										<td class="main">{$from_name_input}</td>
                  										</tr>
                  										<tr>
                    										<td class="main">{$smarty.const.FORM_FIELD_CUSTOMER_EMAIL}</td>
                    										<td class="main">{$from_email_address_input}</td>
                  										</tr>
                									</table>
                								</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
          						<tr>
            						<td>{$pixel_trans_100_10}</td>
          						</tr>
          						<tr>
            						<td class="main"><b>{$smarty.const.FORM_TITLE_FRIEND_DETAILS}</b></td>
          						</tr>
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              								<tr>
                								<td>
                									<table border="0" cellspacing="0" cellpadding="2">
                  										<tr>
                    										<td class="main">{$smarty.const.FORM_FIELD_FRIEND_NAME}</td>
                    										<td class="main">{$to_name_input}&nbsp;<span class="inputRequirement">{$smarty.const.ENTRY_FIRST_NAME_TEXT}</span></td>
                  										</tr>
                  										<tr>
                    										<td class="main">{$smarty.const.FORM_FIELD_FRIEND_EMAIL}</td>
                    										<td class="main">{$to_email_address_input}&nbsp;<span class="inputRequirement">{$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}</span></td>
                  										</tr>
                									</table>
                								</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
          						<tr>
            						<td class="inputRequirement">{$smarty.const.FORM_REQUIRED_INFORMATION}</td>
          						</tr>
          						<tr>
            						<td>{$pixel_trans_100_10}</td>
          						</tr>                
          						<tr>
            						<td class="main"><b>{$smarty.const.FORM_TITLE_FRIEND_MESSAGE}</b></td>
          						</tr>
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
              								<tr>
                								<td>{$message_input}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
	        				</table>
	        			</td>
      				</tr>
      				<tr>
        				<td>{$pixel_trans_100_10}</td>
      				</tr>
      				<tr>
        				<td>
        					<table border="0" width="100%" cellspacing="1" cellpadding="2" class="infoBox">
          						<tr>
            						<td>
            							<table border="0" width="100%" cellspacing="0" cellpadding="2">
              								<tr>
                								<td width="10">{$pixel_trans_10_1}</td>
                								<td><a href="{$product_info_href}">{$button_back_img_button}</a></td>
                								<td align="right">{$button_continue_img_submit}</td>
                								<td width="10">{$pixel_trans_10_1}</td>
              								</tr>
            							</table>
            						</td>
          						</tr>
        					</table>
        				</td>
      				</tr>
        		</table>
        	</td>
      	</tr>
    </table>
    
	{if !$smarty.const.PH_INSIDE}
				</td>
			</tr>
		</table>
	{/if}
	
	{$smarty.const.AFTER_CONTENT}
</form>    