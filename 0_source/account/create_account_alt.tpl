{$create_account_frm}
	{$smarty.const.BEFORE_CONTENT}
	{if !$smarty.const.PH_INSIDE}
		{include file="pieces/page_title_section.tpl"}
   		{$smarty.const.BETWEEN_HL_CONTENT}
		<table class="pageContentTable" border="0" width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table border="0" width="100%" cellspacing="0" cellpadding="0">
	{else}
		<table border="0" width="100%" cellspacing="0" cellpadding="0">
      		<tr>
        		<td>
        			{include file="pieces/page_title_section.tpl"}
        		</td>
      		</tr>
			{$smarty.const.BETWEEN_HL_CONTENT}
{/if}

      <tr>
        <td class="cont_abst">
        	<table border="0" width="100%" cellspacing="0" cellpadding="0" class="account_area">
		    	<tr>
		        	<td class="smallText"><br>{$login_link}</td>
		      	</tr>
      			<tr>
        		<td>{$pixel_trans_100_10}</td>
      			</tr>
				{if $messages_exists}
      			<tr>
        			<td>{$message_create_account}</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
				{/if}	
      			<tr>
        			<td>
        				<table border="0" width="100%" cellspacing="0" cellpadding="2">
          					<tr>
            					<td class="main"><b>{$smarty.const.CATEGORY_PERSONAL}</b></td>
           						<td class="cr_acc_heading" align="left"><span class="inputRequirement">{$smarty.const.FORM_REQUIRED_INFORMATION}</span></td>
          					</tr>
        				</table>
        			</td>
      			</tr>      			
      			<tr>
        			<td align="left">
         				<table border="0" width="100%" cellspacing="1" cellpadding="2" >														
							{if $smarty.const.ACCOUNT_COMMERCIAL_CODE == 'true'}
							<!-- SOF: WerbeCode -->
					 		<tr>
             					<td class="cr_acc_heading">&nbsp;</td>
           					</tr>
					 		<tr>
             					<td class="main" width="30%" valign="top">{$smarty.const.COMMERCIALCODE_OPTIONS}</td>
             					<td class="main">{$commercialcode_input}<br>{$smarty.const.ENTRY_COMMERCIALCODE}</td>
           					</tr>
           					<tr>
             					<td class="cr_acc_heading">&nbsp;</td>
           					</tr>
           					<!-- EOF: WerbeCode -->
					 		{/if}
							{if $smarty.const.ACCOUNT_GENDER == 'true'}
			              	<tr>
			                	<td class="main" width="30%">{$smarty.const.ENTRY_GENDER}</td>
			                	<td class="main">
			                		{$gender_radio_m}&nbsp;&nbsp;{$smarty.const.MALE}&nbsp;&nbsp;{$gender_radio_f}&nbsp;&nbsp;{$smarty.const.FEMALE}&nbsp;{if $entry_gender_text}<span class="inputRequirement">{$smarty.const.ENTRY_GENDER_TEXT}</span>{/if}
			                	</td>
			              	</tr>
			              	{/if}
			              	<tr>
			                	<td class="main" width="30%">{$smarty.const.ENTRY_FIRST_NAME}</td>
			                	<td class="main">
			                		{$firstname_input}&nbsp;{if $entry_firstname_text}<span class="inputRequirement">{$smarty.const.ENTRY_FIRST_NAME_TEXT}</span>{/if}{* if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}<small>&nbsp;({$smarty.const.ENTRY_FIRST_NAME_MAX_LENGTH})</small>{/if *}
			                	</td>
			              	</tr>
			              	<tr>
				                <td class="main" width="30%">{$smarty.const.ENTRY_LAST_NAME}</td>
			    	            <td class="main">
			    	            	{$lastname_input}&nbsp;{if $entry_lastname_text}<span class="inputRequirement">{$smarty.const.ENTRY_LAST_NAME_TEXT}</span>{/if}{* if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}<small>&nbsp;({$smarty.const.ENTRY_LAST_NAME_MAX_LENGTH})</small>{/if *}
			    	            </td>
			        	 	</tr>
							{if $smarty.const.ACCOUNT_DOB == 'true'}
			              	<tr>
			                	<td class="main" width="30%" align="left" valign="top">{$smarty.const.ENTRY_DATE_OF_BIRTH}</td>
			                	<td class="main" align="left" valign="top">{$dob_input}&nbsp;{if $entry_dob_text}<span class="inputRequirement">{$smarty.const.ENTRY_DATE_OF_BIRTH_TEXT}</span>{/if}</td>
			              	</tr>
			              	{/if}
							<tr>
			                	<td class="main" width="30%">{$smarty.const.ENTRY_EMAIL_ADDRESS}</td>
			                	<td class="main">{$email_address_input}&nbsp;{if $entry_email_address_text}<span class="inputRequirement">{$smarty.const.ENTRY_EMAIL_ADDRESS_TEXT}</span>{/if}</td>
			              	</tr>
            			</table>
         			</td>
     			</tr>				
				{if $smarty.const.ACCOUNT_COMPANY == 'true'}
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="cr_acc_heading"><b>{$smarty.const.CATEGORY_COMPANY}</b></td>
      			</tr>
      			<tr>
        			<td align="left">
         				<table border="0" width="100%" cellspacing="1" cellpadding="2" >
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_COMPANY}</td>
                				<td class="main">
                					{$company_input}&nbsp;{if $entry_company_text}<span class="inputRequirement">{$smarty.const.ENTRY_COMPANY_TEXT}</span>{/if}{* if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}<small>&nbsp;({$smarty.const.ENTRY_COMPANY_MAX_LENGTH})</small>{/if *}
                				</td>
              				</tr>
         				</table>
       				</td>
     			</tr>
				{/if}
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="cr_acc_heading"><b>{$smarty.const.CATEGORY_ADDRESS}</b></td>
      			</tr>
      			<tr>
        			<td align="left">
         				<table border="0" width="100%" cellspacing="1" cellpadding="2" >
              				{if $smarty.const.ACCOUNT_SUBURB == 'true'}
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_SUBURB}</td>
                				<td class="main">{$suburb_input}&nbsp;{if $entry_suburb_text}<span class="inputRequirement">{$smarty.const.ENTRY_SUBURB_TEXT}</span>{/if}</td>
              				</tr>
              				{/if}         				
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_STREET_ADDRESS}</td>
                				<td class="main">
                					{$street_address_input}&nbsp;{if $entry_street_address_text}<span class="inputRequirement">{$smarty.const.ENTRY_STREET_ADDRESS_TEXT}</span>{/if}{* if $smarty.const.ENTRY_SHOW_NAME_MAX_LENGTH}<small>&nbsp;({$smarty.const.ENTRY_STREET_ADDRESS_MAX_LENGTH})</small>{/if *} 
                				</td>
              				</tr>
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_POST_CODE}</td>
                				<td class="main">{$postcode_input}&nbsp;{if $entry_postcode_text}<span class="inputRequirement">{$smarty.const.ENTRY_POST_CODE_TEXT}</span>{/if}</td>
              				</tr>
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_CITY}</td>
                				<td class="main">{$city_input}&nbsp;{if $entry_city_text}<span class="inputRequirement">{$smarty.const.ENTRY_CITY_TEXT}</span>{/if}</td>
              				</tr>
            				<tr>
             					<td class="main" width="30%">{$smarty.const.ENTRY_COUNTRY}</td>
             					<td class="main">{$country_list}&nbsp;{if $entry_country_text}<span class="inputRequirement">{$smarty.const.ENTRY_COUNTRY_TEXT}</span>{/if}</td>
            				</tr>
              				{if $smarty.const.ACCOUNT_STATE == 'true'}
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_STATE}</td>
                				<td class="main">
                					<div id="DivRegions">                				
                						{$state_field_html}&nbsp;{if $entry_state_text}<span class="inputRequirement">{$smarty.const.ENTRY_STATE_TEXT}</span>{/if}
                					</div>
                				</td>
              				</tr>
              				{/if}
          				</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="cr_acc_heading"><b>{$smarty.const.CATEGORY_CONTACT}</b></td>
      			</tr>
      			<tr>
        			<td align="left">
        				<table border="0" width="100%" cellspacing="1" cellpadding="2">
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_TELEPHONE_NUMBER}</td>
                				<td class="main">{$telephone_input}&nbsp;{if $entry_telephone_text}<span class="inputRequirement">{$smarty.const.ENTRY_TELEPHONE_NUMBER_TEXT}</span>{/if}</td>
              				</tr>
              				{if $ACCOUNT_TEFAXNUMBER}
	              				<tr>
	                				<td class="main" width="30%">{$smarty.const.ENTRY_FAX_NUMBER}</td>
	                				<td class="main">{$fax_input}&nbsp;{if $entry_fax_text}<span class="inputRequirement">{$smarty.const.ENTRY_FAX_NUMBER_TEXT}</span>{/if}</td>
	              				</tr>
	              			{/if}
            			</table>
        			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td class="cr_acc_heading" id="head_options"><b>{$smarty.const.CATEGORY_OPTIONS}</b></td>
      			</tr>
      			<tr>
        			<td align="left">          				
						{if $is_data_security}
						<table border="0" width="100%" cellspacing="1" cellpadding="2">
			           		<tr>
			             		<td class="main" width="30%">{$smarty.const.ENTRY_DATENSCHUTZ}</td>
			             		<td class="main">
			             			{$datenschutz_checkbox}&nbsp;{if $entry_datenschutz_text}<span class="inputRequirement">{$smarty.const.ENTRY_DATENSCHUTZ_TEXT}</span>{/if}			             			
			             		</td>
			           		</tr>
			            	<tr>
			             		<td class="main" width="30%">&nbsp;</td>
			             		<td class="main">
									{$data_security_text}
			             			<a style="cursor:pointer;" onclick="setDatenschutz('1')" >{$TEXT_DATENSCHUTZ_NOTICE}</a>			             			
			             		</td>
			           		</tr>
						</table>
						<table border="0" width="100%" cellspacing="1" cellpadding="2" >
			           		<tr>
			           			<td class="main" colspan="2">
									<div id="dsid" style="display:none">
										<span class="info_schliessen">
											<a style="cursor:pointer;" onclick="setDatenschutz('2')">{$smarty.const.ENTRY_DATA_SECURITY_CLOSE}</a>
			           					</span>
										{$data_security_inf_desc}
										<span class="info_schliessen">
											<a style="cursor:pointer;" onclick="setDatenschutz('2')">{$smarty.const.ENTRY_DATA_SECURITY_CLOSE}</a>
			           					</span>
			           				</div>
			           			</td>
			           		</tr>
			          	</table>			
						{/if}
            			<table border="0" width="100%" cellspacing="1" cellpadding="2" id="get_newsletter">
              				<tr>
                				<td class="main" width="30%" id="news_title">{$smarty.const.ENTRY_NEWSLETTER}</td>
                				<td class="main" id="news_checkbox">{$newsletter_checkbox}&nbsp;{if $entry_newsletter_text}<span class="inputRequirement"></span>{$smarty.const.ENTRY_NEWSLETTER_TEXT}{/if}</td>
              				</tr>
              				{if $smarty.const.USE_POINTS_SYSTEM == 'true'}
					 		<tr>
             					<td class="main" width="30%">&nbsp;</td>
             					<td class="main bonuspunkte" id="news_notice">{$smarty.const.TEXT_NEWSLETTER_NOTICE}</td>
           					</tr>
           					{/if}
          				</table>
          				{if $is_dubli_modul}
						<!-- Dubli Modul eingebunden ( HACK ) -->
						<table border="0" width="100%" cellspacing="1" cellpadding="2" >
							<tr>
								<td class="main" width="30%">{$smarty.const.ENTRY_DUBLI}:</td>
								<td class="main">{$dubli_checkbox}</td>
							</tr>
							<tr>
								<td class="main" width="30%">&nbsp;</td>								
								<td class="main">{$entry_newsletter_points}</td>
							</tr>
						</table>
						<!-- Dubli Modul eingebunden ( HACK ) ENDE -->
						{/if}
	    			</td>
      			</tr>
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			
      			<tr>
        			<td class="cr_acc_heading"><b>{$smarty.const.CATEGORY_PASSWORD}</b></td>
      			</tr>
      			<tr>
        			<td align="left">
        				<table border="0" width="100%" cellspacing="1" cellpadding="2">
              				<tr>
                				<td class="main" width="30%">{$smarty.const.ENTRY_PASSWORD}</td>
                				<td class="main">{$password_input}&nbsp;{if $entry_password_text}<span class="inputRequirement">{$smarty.const.ENTRY_PASSWORD_TEXT}</span>{/if}</td>
              				</tr>
              				<tr>
                				<td class="main">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION}</td>
                				<td class="main">{$password_confirmation_input}&nbsp;{if $entry_password_confirmation_text}<span class="inputRequirement">{$smarty.const.ENTRY_PASSWORD_CONFIRMATION_TEXT}</span>{/if}</td>
              				</tr>
            			</table>
            		</td>
      			</tr>      			
      			<tr>
        			<td>{$pixel_trans_100_10}</td>
      			</tr>
      			<tr>
        			<td>
                		<table border="0" width="100%" cellspacing="1" cellpadding="2">
                  			<tr>
                    			<td align="right">{$button_continue}</td>
                  			</tr>
                		</table>
              		</td>
            	</tr>
            	<tr>
              		<td>&nbsp;</td>
            	</tr>
          	</table>
        </td>
   	</tr>
</table>
{if !$smarty.const.PH_INSIDE}
			</td>
		</tr>
	</table>
{/if}
{$smarty.const.AFTER_CONTENT}
</form>