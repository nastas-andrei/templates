{literal} 
    <script type="text/javascript">
    var category = '{/literal}{$cPath}{literal}';

    var items_per_page = 20
    var nr_of_items = {/literal}{$nr_of_items}{literal}
    var total_items = {/literal}{$total_items}{literal}
    
    var current_p = Math.ceil(parseInt(nr_of_items)/parseInt(items_per_page))

    var remained_pages = Math.ceil(parseInt(total_items)/items_per_page-current_p)+1
   
    window.addEvent('domready',function(){     
        var infScroll = new InfScroll('scroll_wrapper', {
          url: 'product_listing_request.php',
          method: 'get',
          maxPage: remained_pages,
          loadClassName: 'scrollLoad',
          gifPath:'templates/t_0734/images/loader_white.gif',
          data: {
            page: current_p+1,
            max:items_per_page,
            cPath:category
          }
        });      
      }); 
    </script>
    <script type="text/javascript">
      function goHin(das) {
      var slink = document.getElementById('sortlink').value;
      var sort = document.getElementById('sortierung').selectedIndex;
      var wert = das[sort].value;
      var max = document.getElementById('max').value;
      var link = slink + '&sort='+wert+'&max='+max;
      location.href = link;
      }
    </script>
 {/literal}

{if $arr_modul_productlisting.products|@count lt 1}
<p class="text-center">{$smarty.const.PRODUCTS_NOT_FOUND_MESSAGE}</p>
<a href="javascript:history.go(-1)" class="btn">{$smarty.const.IMAGE_BUTTON_BACK}</a>
{/if}

{if $is_search_message}
 <p class="text-center">{$search_message}</p>
 {/if}

{$PRODUCT_LISTING_HTML}

