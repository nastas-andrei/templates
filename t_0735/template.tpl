{include file="header.tpl"}
<div class="container">
	<div class="row">
        {$content}

        {if $content_template}
          {include file="$content_template"}
        {/if}   
    </div> 
</div>
{include file="footer.tpl"}
