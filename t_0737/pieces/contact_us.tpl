<div class="container row">
    <!-- Cart -->
    <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="col-lg-12 col-sm-12 hero-feature mail-form">
            <!-- card -->
            <div class="form-horizontal">
                {$contact_us_form}

                {if $contact_message_exists}
                    {$contact_message}
                {/if}

                {if $is_action_success}

                    {$smarty.const.TEXT_SUCCESS}
                    <a href="{$button_continue_url}">{$button_continue_img}</a>
                {else}

                    {if $smarty.const.CONTACT_RESPONSE_TIME  != ''}
                        <label style="border-bottom: 1px solid #000000">{$smarty.const.CONTACT_RESPONSE_TIME}</label>
                    {/if}
                    <div class="control-group">
                        <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_NAME}</label>
                        <div class="controls">
                            {$name_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_EMAIL}</label>
                        <div class="controls">
                            {$email_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_CONTACT_SUBJECT}</label>
                        <div class="controls">
                            {$anliegen_pull_down}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group" id="artikel" style="display: none">
                        <label class="control-label" id="artikel_title" for="inputPriceFrom"> {$smarty.const.ENTRY_CONTACT_ZU_ARTIKEL} </label>
                        <div class="controls">
                            {$artikel_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group" id="bestellung" style="display: none">
                        <label class="control-label" id="bestellung_title" for="inputPriceFrom">{$smarty.const.ENTRY_CONTACT_ZU_BESTELLUNG}</label>
                        <div class="controls">
                            {$bestellung_input}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="control-group">
                        <label class="control-label" for="inputPriceFrom">{$smarty.const.ENTRY_ENQUIRY}</label>
                        <div class="controls">
                            {$enquiry_textarea}
                        </div>
                    </div>
                    {if $smarty.const.STORE_SECURITY_CAPTCHA_ENABLED == 'true'}
                        <div class="control-group">
                            <div class="controls">
                                <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>

                                <div class="clearfix" style="margin-bottom:20px"></div>
                            </div>
                        </div>
                        <script language="javascript" type="text/javascript">

                            var sel = document.getElementById('pf_antispam');
                            var opt = new Option('Nein', 2, true, true);
                            sel.options[sel.length] = opt;
                            sel.selectedIndex = 1;

                        </script>
                        <noscript>
                            <div class="antistalker">
                                <span>Bist du ein Spammer oder ein BOT? (Wenn nicht Antworte mit &quot;Nein&quot;)</span>
                                <select name="pf_antispam" id="pf_antispam">
                                    <option value="1" selected="selected">Ja</option>
                                    <option value="2">Nein</option>
                                </select>
                            </div>
                        </noscript>
                    {/if}
                    <br/>
                    <div class="control-group">
                        <div class="controls">
                            <input type="submit" class="btn button_add" value="{$smarty.const.CONTACT_BOTTOM_TEXT}">
                        </div>
                    </div>
                {/if}
                </form>
            </div>
            <!-- End Cart -->
        </div>
    </div>

    <!-- map -->
    <div class="clearfix visible-sm"></div>
    <div class="col-lg-6 col-md-6 col-sm-12">

        <!-- OUR LOCATION -->
        <div class="col-lg-12 col-md-12 col-sm-12">
            <!-- <div class="no-padding">
                    <span class="title">OUR LOCATION</span>
                  </div> -->
            <iframe width="100%" height="350" frameborder="0" style="border:0"
                    src="https://www.google.com/maps/embed/v1/search?q={$contact_data.shop_streat.value}, {$contact_data.shop_city.value} {$contact_data.shop_country.value}&key=AIzaSyBzBxYxCJDus32apdAMyDlkvNX3qNYgOsE"></iframe>
        </div>
        <!-- End OUR LOCATION -->

    </div>
</div>