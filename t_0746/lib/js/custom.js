$(window).scroll(function() {
    if($(this).scrollTop() > 450) {
        $('#gotoTop').fadeIn();
    } else {
        $('#gotoTop').fadeOut();
    }
});

$('#gotoTop').click(function() {
    $('body,html').animate({scrollTop:0},400);
    return false;
});
$(window).load(function(){
    $('.selectpicker').selectpicker();

    $('.flexslider-main').flexslider({
        slideshowSpeed: 7000,
        animation: "slide",
        start: function(slider){
            $('body').removeClass('loading');
        }
    });
});
