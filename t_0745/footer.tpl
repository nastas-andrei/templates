<footer id="footer">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="single-widget">
                        {include file="boxes/box_information.tpl"}
                    </div>
                </div>
                {php}
                    $i = 0;
                {/php}
                {foreach from=$boxes_pos.left key=key item=boxname name=current}
                    <div class="col-sm-3">
                        <div class="single-widget">
                            <ul class="nav nav-pills nav-stacked">
                                {if $boxname eq 'htmlbox'}
                                    {include file="boxes/box_$boxname.tpl"}
                                {/if}
                            </ul>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left">
                    &copy; {$arr_footer.year}
                    {foreach from=$arr_footer.partner key=key item=partner}
                        <a href="http://{$partner.link}" target="_blank" class="muted" data-toggle="tooltip" original-title="{$partner.name}" title="{$partner.name}">
                            {$partner.text} {$partner.name}
                        </a>
                        <a href="http://{$partner.link}" target="_blank" data-toggle="tooltip" original-title="{$partner.name}" title="{$partner.name}">
                            <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" alt="" title="{$partner.name}" />
                        </a>
                    {/foreach}
                </p>
            </div>
        </div>
    </div>
</footer>
<script type="application/javascript" src="{$templatepath}lib/js/jQuery.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/bootstrap.min.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/categories.js"></script>
<script type="application/javascript" src="includes/general.js"></script>
<script type="application/javascript" src="{$templatepath}/lib/js/jQuery.1.8.3.min.js"></script>
<script type="application/javascript" src="{$templatepath}/lib/js/cloud-zoom.1.0.2.min.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/jquery.scrollUp.min.js"></script>
<script type="application/javascript" src="{$templatepath}lib/js/main.js"></script>
</body>
</html>