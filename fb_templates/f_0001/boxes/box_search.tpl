{if $arr_searchbox}

    {if $js_products_search}
        {$js_products_search}
    {/if}

    <div class="box-search">
        {$arr_searchbox.form}
        <div class="controls controls-row">
            <div class="input-append">
                <input type="hidden" name="fb" value="1">
                <input type="text" class="" name="keywords" id="keywords" placeholder="{$arr_searchbox.title}" value="{$arr_searchbox.inputtextfield}"/>
                <button class="btn" type="submit" ><i class="icon-search"></i></button>
            </div>
        </div>
        {$arr_searchbox.hidden_sessionid}
        </form>
    </div>
{/if}
