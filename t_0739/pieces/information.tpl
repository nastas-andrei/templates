<div class="location-bar">
    {if empty($custom_heading_title)}
        <h1>{$HEADING_TITLE}</h1>
    {else}
        <h1>{$custom_heading_title}</h1>
    {/if}
</div>
<div id="content" class="box-wrap">
    {$INFO_DESCRIPTION}
</div>