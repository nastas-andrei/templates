{if $boxes_content.$key}
    <div class="box-html-box col-lg-12 col-md-12  col-sm-12 col-xs-12">
        <div class="box-heading">
            <h4>{$boxes_content.$key.boxes_title} &nbsp;</h4>

        </div>
        <div class="column">
            <ul>
                {$boxes_content.$key.boxes_content}
            </ul>
        </div>
    </div>
{/if}