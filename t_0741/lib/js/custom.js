$(document).ready(function() {
    $("#also_products, #xsell_products").owlCarousel({
        itemsCustom : [
            [0, 1],
            [320, 1],
            [500, 2],
            [900, 2],
            [1200, 3]
        ],
        lazyLoad : true,
        navigation : false,
        autoPlay : true,
        stopOnHover : true

    });
    $("#additional_images").owlCarousel({
        items : 3,
        lazyLoad : true,
        navigation : false
    });
    $('.tp-banner').revolution( {
        delay:9000,
        startwidth:1170,
        startheight:620,
        hideThumbs:10,
        fullWidth:"on",
        forceFullWidth:"on"
    });

    var mySelect = $('#first-disabled2');

    $('#special').on('click', function () {
        mySelect.find('option:selected').attr('disabled', 'disabled');
        mySelect.selectpicker('refresh');
    });

    var $basic2 = $('#basic2').selectpicker({
        liveSearch: false,
        maxOptions: 3
    });

});