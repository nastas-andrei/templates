            <div id="footer">
                <div class="column1">
                    {include file="boxes/box_information.tpl"}
                </div>
                {php}
                    $i = 0;
                {/php}

                {foreach from=$boxes_pos.left key=key item=boxname name=current}
                    {if $boxname eq 'htmlbox'}
                        {php}
                            $i++;
                            $this->assign('counter_boxes', $i);
                        {/php}
                        {include file="boxes/box_$boxname.tpl"}
                    {/if}
                {/foreach}
            </div>
            <div id="powered">
                <div class="clm2">
                    {foreach from=$arr_footer.partner key=key item=partner}
                        <a href="http://{$partner.link}" target="_blank" class="muted" data-toggle="tooltip" original-title="{$partner.name}" title="{$partner.name}">{$partner.text} {$partner.name}</a>
                        <a href="http://{$partner.link}" target="_blank" data-toggle="tooltip" original-title="{$partner.name}" title="{$partner.name}">
                            <img src="{$imagepath}/{$partner.logo|replace:'.gif':'.png'}" alt="" title="{$partner.name}"/>
                        </a>
                    {/foreach}
                    &copy; {$arr_footer.year}
                </div>
            </div>
        </div>
    </body>
</html>