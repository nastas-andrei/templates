{include file="header.tpl"}
    {if $arr_product_info.product_check == true}

        <div class="location-bar">
            <h1>{$arr_product_info.products_name}</h1>
        </div>
        <div id="content">
            <!-- Nav tabs -->
            <div id="navtabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="#tab-information" role="tab" data-toggle="tab">{$smarty.const.TEXT_AVAILABILITY_GENERAL }</a></li>
                    <li><a href="#tab-description" role="tab" data-toggle="tab">{$smarty.const.VIEW_PROD_DESC_CONFIRM}</a></li>
                </ul>
            </div>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="tab-information">
                    <div class="product-info">
                        <div id="tab-information" class="tab-content" style="display: block;">
                            {if $arr_additional_images.products}
                            <div class="product-description">
                                <div class="description">
                                    {if $arr_product_info.image_linkLARGE}
                                        <a class="cloud-zoom-gallery" rel="useZoom: 'sns_cloudzoom', smallImage: '{$arr_product_info.image_link}' " title="{$arr_product_info.products_name}"  rel=""  href="{$arr_product_info.image_linkLARGE}" >
                                    {else}
                                        <a class="cloud-zoom-gallery" rel="useZoom: 'sns_cloudzoom', smallImage: '{$arr_product_info.image_link}' " title="{$arr_product_info.products_name}"  rel=""  href="{$arr_product_info.image_link}" >
                                    {/if}
                                            <img src="{$arr_product_info.image_link}" alt="{$arr_additional_images.title}" title="{$arr_additional_images.title}" width="120px"/>
                                        </a>
                                    {include file="modules/modul_additional_images.tpl"}
                                </div>
                            </div>
                            {/if}
                            <div class="product-images">
                                <div class="image" style="text-align: center">
                                    {if $arr_product_info.image_source}
                                        {if $arr_product_info.image_linkLARGE}
                                            <a id="sns_cloudzoom" class="cloud-zoom" rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1" title="{$arr_product_info.products_name}" href="{$arr_product_info.image_linkLARGE}" >
                                                <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" width="400px"/>
                                                <!-- <span class='ico-product ico-new'>New</span>     -->
                                            </a>
                                        {else}
                                            <a id="sns_cloudzoom" class="cloud-zoom" rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1" title="{$arr_product_info.products_name}" href="{$arr_product_info.image_link}" >
                                                <img id="image" src="{$arr_product_info.image_link}" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" width="400px"/>
                                                <!-- <span class='ico-product ico-new'>New</span>     -->
                                            </a>
                                        {/if}
                                    {else}
                                        <a id="sns_cloudzoom" class="cloud-zoom" rel="zoomWidth:280, zoomHeight:280, adjustX: 20, adjustY: 1" title="{$arr_product_info.products_name}" href="{$imagepath}/empty-album.png" >
                                            <img id="image" src="{$imagepath}/empty-album.png" alt="{$arr_product_info.products_name}" title="{$arr_product_info.products_name}" />
                                            <!-- <span class='ico-product ico-new'>New</span>     -->
                                        </a>
                                    {/if}
                                </div>
                            </div>

                            <div class="product-options">
                                <form name="cart_quantity" action="{$arr_product_info.shopping_card_link}" method="post" id="product_addtocart_form">
                                    <div class="price">
                                        {if $arr_product_info.preisalt > 0}
                                            <span class="price-old">{$arr_product_info.preisalt}</span>
                                        {/if}
                                        <div class="price-regular">{$arr_product_info.preisneu|regex_replace:"/[()]/":""}</div>
                                    </div>

                                    <div class="cart">
                                        {if $LOAD_CLIENT}
                                            <input type="submit" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button" onclick="productAddToCartForm.submit(this)">
                                            <input type="hidden" name="products_id" value="{$arr_product_info.products_id}">
                                        {else}
                                            <input type="button" value="{$smarty.const.IMAGE_BUTTON_IN_CART}" class="button" disabled>
                                        {/if}
                                    </div>
                                    <div style="margin:10px 0">
                                        {if $arr_product_info.arr_products_options}
                                            <div class="form-group">
                                                <label class="control-label">
                                                    {$arr_product_info.TEXT_PRODUCT_OPTIONS}
                                                </label>
                                                {if $arr_product_info.product_options_javascript}
                                                    {$arr_product_info.product_options_javascript}
                                                {/if}
                                                <table>
                                                    {foreach from=$arr_product_info.arr_products_options key=key item=productoption}
                                                        <tr>
                                                            <td>{$productoption.name}:</td>
                                                            <td >{$productoption.pulldown}</td>
                                                        </tr>
                                                    {/foreach}
                                                </table>
                                            </div>
                                        {/if}
                                        {*-------------------------------------------*}
                                        {if $opt_count > 0}
                                            {if $var_options_array}
                                                <input type="hidden" id="master_id" value="{$master_id}">
                                                <input type="hidden" id="anzahl_optionen" value="{$opt_count}">
                                                <input type="hidden" id="cPath" value="{$cPath}">
                                                <input type="hidden" id="FRIENDLY_URLS" value="{$FRIENDLY_URLS}">
                                                {if $Prodopt}
                                                    <input type="hidden" id="Prodopt" value="{$Prodopt}">
                                                {/if}
                                                {if $prod_hidden}
                                                    {foreach from=$prod_hidden key=key item=hidden_input}
                                                        {$hidden_input}
                                                    {/foreach}
                                                {/if}
                                                {if $select_ids}
                                                    {foreach from=$select_ids key=key item=select_input}
                                                        {$select_input}
                                                    {/foreach}
                                                {/if}
                                                {if $javascript_hidden}
                                                    {$javascript_hidden}
                                                {/if}
                                                {foreach from=$var_options_array key=key item=options}
                                                    <label>{$options.title}:</label>
                                                    {$options.dropdown}
                                                {/foreach}
                                            {/if}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $arr_product_info.base_price}
                                            {$arr_product_info.base_price}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $arr_product_info.shipping_value gt 0}
                                            <label class="control-label">{$TEXT_SHIPPING}:</label>
                                            ({$arr_product_info.shipping})
                                        {/if}

                                        {*---------------------------------------------*}
                                        {if $arr_product_info.products_uvp}
                                            {$arr_product_info.TEXT_USUAL_MARKET_PRICE}:
                                            {$arr_product_info.products_uvp}
                                            {$arr_product_info.TEXT_YOU_SAVE}:
                                            {$arr_product_info.diff_price}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $count_bestprice gt 0}
                                            {$staffelpreis_hinweis}

                                            {foreach from=$bestprice_arr key=key item=bestprice}
                                                {cycle values="dataTableRowB,dataTableRow" assign="tr_class"}
                                                {$bestprice.ausgabe_stueck}
                                                {$bestprice.ausgabe_preis}
                                            {/foreach}
                                        {/if}

                                        {*-------------------------------------------*}

                                        {if $PRICE_QUERY_ON eq 'true'}
                                            <div class="clearfix"></div>
                                            <div style="font-size:15px;margin: 10px 0;">
                                                {$PRICE_QUERY}: <a href="{$PRICE_INQUIRY_URL}"> {$PRICE_INQUIRY}</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $PRODUCTS_QUERY_ON eq 'true'}
                                            <div style="font-size:15px;margin: 10px 0;">
                                                {$PRODUCTS_INQUIRY_TEXT}: <a href="{$PRODUCTS_INQUIRY_URL}"> {$PRODUCTS_INQUIRY}</a>
                                            </div>
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if count($arr_product_info.availability)}
                                            {$arr_product_info.availability.TEXT_AVAILABILITY}:
                                            {$arr_product_info.availability.text}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if count($arr_product_info.delivery_time)}
                                            {$arr_product_info.delivery_time.TEXT_DELIVERY_TIME}
                                            {$arr_product_info.delivery_time.text} *
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $arr_product_info.text_date_available}
                                            &nbsp;
                                            {$arr_product_info.text_date_available}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $arr_product_info.products_url}
                                            &nbsp;
                                            {$arr_product_info.products_url}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $SANTANDER_LOANBOX}
                                            <span id="santandertitle_pinfo">
                                                {$SANTANDER_LOANBOX_TITLE}:
                                            </span>
                                            &nbsp;
                                            {$SANTANDER_LOANBOX}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $PRODUCTS_WEIGHT_INFO}
                                            {if $arr_product_info.products_weight > 0}
                                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC}:
                                                {$arr_product_info.products_weight}
                                                &nbsp;
                                                {$smarty.const.PRODUCTS_INFO_WEIGHT_DESC_XTRA}
                                            {/if}
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if count($arr_product_info.products_model)}
                                            <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">Model: </span>
                                                {$arr_product_info.products_model}
                                            <br/>
                                        {/if}

                                        {*-------------------------------------------*}
                                        {if $arr_manufacturer_info.manufacturers_name}
                                            <span style="float:left;font-size:14px;font-weight:bold;margin-right:5px">{$manufacturer}: </span>
                                            {if $arr_manufacturer_info.manufacturers_image neq ""}
                                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                    <a href="{$arr_manufacturer_info.manufacturers_url}">
                                                        <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                                    </a>
                                                {else}
                                                    <img src="{$arr_manufacturer_info.manufacturers_image}" alt="{$arr_manufacturer_info.manufacturers_name}">
                                                {/if}
                                            {else}
                                                {if $arr_manufacturer_info.manufacturers_url neq ""}
                                                    <a href="{$arr_manufacturer_info.manufacturers_url}">{$arr_manufacturer_info.manufacturers_name}</a>
                                                {else}
                                                    {$arr_manufacturer_info.manufacturers_name}
                                                {/if}
                                            {/if}
                                        {/if}

                                        {*---------------------------------------------*}
                                        {if $widget_view && $widget_view eq 1}
                                            <div id="widget_views"></div>
                                        {/if}
                                    </div>
                                </form>
                            </div>
                            {if $foxrate && $foxrate->GetVariable('counts') != 0}
                            {*include file="../0_source/modules/modul_foxrate_products_info.tpl"*}

                            {else}

                            {/if}

                            {if $foxrate_rating}

                            {/if}
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="tab-description" style="padding: 20px">
                    {$arr_product_info.products_description }
                </div>
            </div>
        </div>
    {else}
        {$TEXT_PRODUCT_NOT_FOUND}
    {/if}
{include file="footer.tpl"}
